VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGlobal"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mcnnMain As ADODB.Connection
' Purpose  :
' Inputs   :
' Details  :
'  Source  : Salt Solutions, Inc
'  Created : 6/7/2002 9:39 AM By Dave Kester
'  Modified:
Public Function InputDLLFromProdTable(pintProdID As Integer, pintCalcType As Integer, _
            pstrMsgBox As String, ByVal pcnnMain As ADODB.Connection) As Boolean
'pintProdID As Integer, pintCalcType As Integer, _
               pblnPureEndow As Boolean, pblnSummary _
               As Boolean, pblnDetail As Boolean
Static intLastProdID As Integer

'Variables that are passed as parameters in the value program
Dim pblnPureEndow As Boolean

Dim intAge As Integer
Dim intBeginAge As Integer
Dim intEndAge As Integer
Dim intResMethod As eResMethod
Dim intTiming As eTiming
Dim intCVTiming As eTiming
Dim intTerminateAge As Integer
Dim enuCVMethod As eCVMethod
Dim sngResInterest As Single
Dim sngCVInterest As Single
'Dim sngReserve As Single
Dim intDur As Integer
Dim intStart As Integer
Dim intTotalCount As Integer
Dim asngMortality(120) As Single
Dim i As Integer
Dim j As Integer
'Dim enuTerminalType As enuTerminalIndicator
Dim intReserveTable As Integer
Dim intReserveTable2 As Integer
Dim intCVTable As Integer
Dim rsValue As New ADODB.Recordset
Dim strSQL As String
'Dim aintValueID(15) As eValueDefinition
Dim aintValueVariation(15) As eTableType
Dim aintFormatType(15) As eRateFormat
Dim aintConstantValue(15) As Double
Dim aintCeaseDefn(15) As eTableType
Dim aintCeaseUnits(15) As eTableUnitDefinition
Dim aintCeaseValue(15) As Integer
Dim vntValue As Variant
Dim intGender As eGenderConstants
Dim intTobacco As eTobacco
Dim intUW As eUnderwriting
Dim strFormatType As String
Dim enuProdType As eProductType
Dim rsPlan As New ADODB.Recordset
Dim rsProduct As New ADODB.Recordset
Dim blnETI As Boolean
Dim intLives As Integer
Dim rsSC As ADODB.Recordset
Dim blnSC As Boolean
   
'Hard code of parameters used in the value program
   pblnPureEndow = True
   
'Begin assigning variables
   
   Set mcnnMain = pcnnMain
   If intLastProdID = pintProdID Then
      ' Since this product is the last one that was done, we don't
      '  need to do it again.
      InputDLLFromProdTable = True
      Exit Function
   End If
   intLastProdID = pintProdID
   'rsProduct.MoveFirst
   rsProduct.Open "tbl06Products", mcnnMain, adOpenStatic, adLockReadOnly
   rsProduct.MoveFirst
   rsProduct.MovePrevious
   rsProduct.Find "ProdID = " & pintProdID
   If rsProduct.EOF Then
      ' The record was not found
      pstrMsgBox = "The product was not found"
      Exit Function
   End If
   blnETI = rsProduct("ETIPlan")
   If pintCalcType = 1 And blnETI Then
      MsgBox "Cannot calculate the cash values on an ETI plan code.  Process halted."
      Exit Function
   End If
   strSQL = "SELECT tbl06ValueDefn.* " & _
            "From tbl06ValueDefn " & _
            "WHERE ((tbl06ValueDefn.ProdID)=" & pintProdID & ") " & _
            "Order By tbl06ValueDefn.ValueID;"
   
   rsPlan.Open strSQL, mcnnMain, adOpenStatic, adLockReadOnly
   
   intBeginAge = rsProduct("BeginIssueAge")
   intEndAge = rsProduct("EndIssueAge")
   intResMethod = rsProduct("ReserveMethod")
   If pintCalcType = 1 Then
      intTiming = rsProduct("CVTiming")
      intCVTiming = rsProduct("CVTiming")
      sngResInterest = rsProduct("CVInterest")
      sngCVInterest = rsProduct("CVInterest")
      intReserveTable = rsProduct("CVMort")
      intReserveTable2 = rsProduct("CVMort") 'rsProduct("ReserveMort2") 'added 2/18/03
      intCVTable = rsProduct("CVMort")
   Else
      intTiming = rsProduct("Timing")
      intCVTiming = rsProduct("Timing")
      sngResInterest = rsProduct("GtdInterest")
      sngCVInterest = rsProduct("GtdInterest")
      intReserveTable = rsProduct("ReserveMort")
      intReserveTable2 = rsProduct("ReserveMort") 'rsProduct("ReserveMort2") 'added 2/18/03
      intCVTable = rsProduct("ReserveMort")
   End If
   intTerminateAge = 100 'rsProduct("")
   intGender = rsProduct("GenderType")
   intTobacco = rsProduct("TobaccoType")
   intUW = rsProduct("UnderwritingType")
   enuProdType = rsProduct("ProdType")
   enuCVMethod = rsProduct("CVMethod")
   blnETI = rsProduct("ETIPlan")
   intLives = 1
'   If intTiming = TimingCurtate Then
'      gobjRecursive.blnImmediateBenefit = False
'      gobjRecursive.blnContinuousPayment = False
'   Else
'      gobjRecursive.blnImmediateBenefit = True
'      If intTiming = TimingSemiCont Then
'         gobjRecursive.blnContinuousPayment = False
'      Else
'         gobjRecursive.blnContinuousPayment = True
'      End If
'   End If
'   gobjRecursive.intPremMode = 1
   'gobjRecursive.dblIntRate = sngResInterest
'***********  New  ****************
   oSaltCommon.Insured.Lives = intLives
   For j = 0 To 3
      If j = 0 Then 'CV
         If pintCalcType = 0 Then
            oSaltCommon.CV.CalcCV = False
            'oSaltCommon.Timing = intTiming
            'oSaltCommon.CVTiming = intTiming
         Else
            oSaltCommon.CV.CalcCV = True
            oSaltCommon.Reserves.ReserveMethodType = ResMethodImport
            oSaltCommon.IntRates.InterestRateConstant(j) = rsProduct("CVInterest")
            oSaltCommon.CV.CVMethod = enuCVMethod
            'Cash value Mortality
            Call ReadAgeDur(eTableType.TableAttainedAge, "tbl07Insurance", mcnnMain, _
            "tbl" & intCVTable, "AA", 0, asngMortality(), 0, 115)
   
            For i = 0 To UBound(asngMortality)
               oSaltCommon.Mortality.MortalityRate(j, i) = asngMortality(i) / 1000
            Next i
         End If
      Else 'Basic
         If pintCalcType = 1 Then 'Calculate the reserves using the CV input
            'Use the interest rate, mortality, and timing from the CV.  Only item used for reserves is the method
            oSaltCommon.IntRates.InterestRateConstant(j) = rsProduct("CVInterest")
            
            'Valuation Mortality
            Call ReadAgeDur(eTableType.TableAttainedAge, "tbl07Insurance", mcnnMain, _
            "tbl" & intCVTable, "AA", 0, asngMortality(), 0, 115)
         Else
            'Use the interest rate, mortality, and timing from the CV.  Only item used for reserves is the method
            oSaltCommon.IntRates.InterestRateConstant(j) = rsProduct("GtdInterest")
            
            'Valuation Mortality
            Call ReadAgeDur(eTableType.TableAttainedAge, "tbl07Insurance", mcnnMain, _
            "tbl" & intReserveTable, "AA", 0, asngMortality(), 0, 115)
         End If
         For i = 0 To UBound(asngMortality)
            oSaltCommon.Mortality.MortalityRate(j, i) = asngMortality(i) / 1000

            If i <= 30 Then
               oSaltCommon.Mortality.SelectFactor(j, i) = 1
            End If
         Next i
         If intLives = 2 Then 'joint policy - added 2/18/03
            If pintCalcType = 1 Then 'Calculate the reserves using the CV input
               'And enuCVMethod = CVMethodReserve
            Else
              ' oSaltCommon.IntRates.InterestRateConstant(j) = grsProduct("GtdInterest")
               
               'Valuation Mortality
               Call ReadAgeDur(eTableType.TableAttainedAge, "tbl07Insurance", mcnnMain, _
               "tbl" & intReserveTable2, "AA", 0, asngMortality(), 0, 115)
               
               'oSaltCommon.Timing = intTiming
               'oSaltCommon.CVTiming = intTiming
            End If
   
            For i = 0 To UBound(asngMortality)
               oSaltCommon.Mortality.MortalityRate2(j, i) = asngMortality(i) / 1000
   
'               If i <= 30 Then
'                  oSaltCommon.Mortality.SelectFactor(j, i) = 1
'               End If
            Next i
         End If
         oSaltCommon.Mortality.SelectFactorConstant(j) = 1 ' Me.txtFields(9)
         oSaltCommon.Mortality.SelectFactorDur(j) = 1 'Me.txtFields(10)
         'If Me.chkCommFn(3).Value = 0 Then
            oSaltCommon.Mortality.Use10YrAfter1Segment(j) = False
         'Else
         '   oSaltCommon.Mortality.Use10YrAfter1Segment(j) = True
         'End If
      End If
   Next j
   If pintCalcType = 0 Then
      oSaltCommon.Timing = intTiming
      oSaltCommon.CVTiming = intTiming
   Else
      oSaltCommon.Timing = intCVTiming
      oSaltCommon.CVTiming = intCVTiming
      Set rsSC = New ADODB.Recordset
      rsSC.Open "Select * from tbl04SC Where ProdID = " & pintProdID, mcnnMain, adOpenStatic, dbReadOnly
      If Not rsSC.EOF Then
         rsSC.MoveFirst
         blnSC = True
      Else
         blnSC = False
      End If
   End If
   oSaltCommon.Policy.ProductType = enuProdType
   oSaltCommon.Reserves.ReserveMethod = stUnitaryMethod
   If intTiming = TimingContinuous Then
      oSaltCommon.Reserves.ExpenseAllowCurtate = False
   Else
      oSaltCommon.Reserves.ExpenseAllowCurtate = True
   End If
   'If Me.chkCommFn(1).Value = 0 Then
   '   oSaltCommon.Reserves.NLPConstantPercent = False
   'Else
      'This is used in calculating the (adjusted) net level premium in the expense allowance calculation.
      'See cell E4 in the Basic Unitary sheet of the VE spreadsheet.  It only affects the expense allowance.
      oSaltCommon.Reserves.NLPConstantPercent = rsProduct("NLPremConstantPct")
   'End If
   'Mid year or terminals
   'oSaltCommon.Reserves.ComparisonRule = Me.cmbCommFns(14).ListIndex
   'Mean or Midterminals
   'oSaltCommon.Reserves.MidYear = Me.cmbCommFns(15).ListIndex
   'CRVM, FPT, Graded CRVM
   If intResMethod = ResMethodNJ Then
      pstrMsgBox = "The system does not support the New Jersey method for plans with non level premiums."
      Exit Function
   End If
   If intResMethod <> ResMethodNetLevel Then
      oSaltCommon.Reserves.ModRes = intResMethod 'Me.cmbCommFns(16).ListIndex
   Else 'Net Level
   End If
'   If Me.cmbCommFns(16).ListIndex = 2 Then 'Graded CRVM
'      oSaltCommon.Reserves.CRVMYears = Me.txtFields(12)
'      oSaltCommon.Reserves.GradedYears = Me.txtFields(13)
'   End If
   
   'Premiums
   'oSaltCommon.Premium.PremiumCeaseAge = Me.txtFields(7)
   
   'Common
   oSaltCommon.Insured.ETI = blnETI
   
   
   For Each vntValue In Array(ValueBasePrem1, ValueDeathBenefit, ValueEndowment)
      If blnETI And vntValue = ValueBasePrem1 Then
         aintCeaseDefn(vntValue) = 0
         aintCeaseUnits(vntValue) = 0
         aintCeaseValue(vntValue) = 0
         aintFormatType(vntValue) = 0
         aintValueVariation(vntValue) = 0
         aintConstantValue(vntValue) = 0
      Else
         rsPlan.MoveFirst
         rsPlan.MovePrevious
         rsPlan.Find "[ValueID]=" & vntValue
         If Not rsPlan.EOF Then
            aintCeaseDefn(vntValue) = rsPlan("CeaseDefn")
            aintCeaseUnits(vntValue) = rsPlan("CeaseUnits")
            aintCeaseValue(vntValue) = rsPlan("CeaseValue")
            aintFormatType(vntValue) = rsPlan("FormatType")
            aintValueVariation(vntValue) = rsPlan("ValueVariation")
            If aintValueVariation(vntValue) = TableConstant Then
               aintConstantValue(vntValue) = rsPlan("ConstantValue")
            Else
               aintConstantValue(vntValue) = 0
            End If
         Else
            If ValueEndowment Then
               aintCeaseDefn(vntValue) = 0
               aintCeaseUnits(vntValue) = 0
               aintCeaseValue(vntValue) = 0
               aintFormatType(vntValue) = 0
               aintValueVariation(vntValue) = 0
               aintConstantValue(vntValue) = 0
            Else
               pstrMsgBox = "Could not find " & vntValue & " definition"
            End If
         End If
      End If
   Next
   
   'If Not pblnSummary Then
      oSaltCommon.PrintSummary = False
   'Else
   '   oSaltCommon.PrintSummary = True
   'End If
   'If Not pblnDetail Then
      oSaltCommon.PrintDetail = False
   'Else
   '   oSaltCommon.PrintDetail = True
   'End If
   
   For intAge = intBeginAge To intEndAge
      'Benefits
      'oSaltCommon.Benefits.DBConstant = Me.txtFields(4)
      'oSaltCommon.Benefits.DBType = Me.cmbCommFns(1).ListIndex
      '
   
      'Insured
      oSaltCommon.Insured.IssueAge = intAge
      'oSaltCommon.Insured.Gender = Me.cmbCommFns(8).ListIndex
      
      '***********  Death Benefit  **************
      'Benefit Cease
      If aintCeaseDefn(ValueDeathBenefit) = TableConstant Then 'Dave added this elseif statement on 2/12/02
         If aintCeaseUnits(ValueDeathBenefit) = UnitAge Then
            If intAge = intBeginAge Then 'only need to do it once
               oSaltCommon.Benefits.BenefitCeaseAge = MAX(aintCeaseValue(ValueDeathBenefit), 0)
            End If
         Else
            oSaltCommon.Benefits.BenefitCeaseAge = MAX(aintCeaseValue(ValueDeathBenefit) + intAge, 0)
         End If
      Else
         'intTotalCount = aintCeaseValue(ValueDeathBenefit)
         'If aintCeaseUnits(ValueDeathBenefit) = UnitAge Then
         '   intTotalCount = MAX(intTotalCount - pintAge, 0)
         'End If
      End If
      intTerminateAge = oSaltCommon.Benefits.BenefitCeaseAge
      
      'Death Benefit
      If aintValueVariation(ValueDeathBenefit) = TableConstant Then 'Dave added this elseif statement on 2/12/02
         If intAge = intBeginAge Then 'only need to do it once
            oSaltCommon.Benefits.DBConstant = MAX(aintConstantValue(ValueDeathBenefit), 0)
         End If
         For intDur = intTerminateAge - intAge To 0 Step -1
            oSaltCommon.Benefits.DeathBenefit(intAge + intDur) = MAX(aintConstantValue(ValueDeathBenefit), 0)
         Next intDur
      Else 'Need to add code for non constant DB
      End If
      
      
      '***********  Endowment Benefit  **************
      'Endowment Cease
'      If aintCeaseDefn(ValueEndowment) = TableConstant Then 'Dave added this elseif statement on 2/12/02
'         If aintCeaseUnits(ValueEndowment) = UnitAge Then
'            If intAge = intBeginAge Then 'only need to do it once
'               oSaltCommon.Benefits.BenefitCeaseAge = MAX(aintCeaseValue(ValueEndowment), 0)
'            End If
'         Else
'            oSaltCommon.Benefits.BenefitCeaseAge = MAX(aintCeaseValue(ValueEndowment) + intAge, 0)
'         End If
'      Else
'      End If
      
      'Endowment Benefit
      If aintValueVariation(ValueEndowment) = TableConstant Then 'Dave added this elseif statement on 2/12/02
         If intAge = intBeginAge Then 'only need to do it once
            oSaltCommon.Benefits.EndowAmount = MAX(aintConstantValue(ValueEndowment), 0)
         End If
      Else 'Need to add code for non constant Endowment
      End If
      
      
      '***********  Gross Premium  **************
      'Premium Cease
      If aintCeaseDefn(ValueBasePrem1) = TableConstant Then 'Dave added this elseif statement on 2/12/02
         If aintCeaseUnits(ValueBasePrem1) = UnitAge Then
            If intAge = intBeginAge Then 'only need to do it once
               oSaltCommon.Premium.PremiumCeaseAge = MAX(aintCeaseValue(ValueBasePrem1), 0)
            End If
         Else
            oSaltCommon.Premium.PremiumCeaseAge = MAX(aintCeaseValue(ValueBasePrem1) + intAge, 0)
         End If
      Else
         'intTotalCount = aintCeaseValue(ValueBasePrem1)
         'If aintCeaseUnits(ValueBasePrem1) = UnitAge Then
         '   intTotalCount = MAX(intTotalCount - pintAge, 0)
         'End If
      End If
      
      'Gross Premium
      If aintValueVariation(ValueBasePrem1) = TableConstant Then 'Dave added this elseif statement on 2/12/02
         'If intAge = intBeginAge Then 'only need to do it once
            For intDur = 0 To oSaltCommon.Premium.PremiumCeaseAge - intAge - 1
               oSaltCommon.Premium.GrossPremiums(intDur) = MAX(aintConstantValue(ValueBasePrem1), 0)
            Next intDur
            For intDur = oSaltCommon.Premium.PremiumCeaseAge - intAge To intTerminateAge - intAge
               oSaltCommon.Premium.GrossPremiums(intDur) = 0 'GetEnteredValue(.Fields(strFormatType), 0)   ' .Fields("G" & intGender & "T" & intTobacco)
            Next intDur
'            For intDur = intTerminateAge - intAge - 1 To 0 Step -1
'               oSaltCommon.Premium.GrossPremiums(intDur) = MAX(aintConstantValue(ValueBasePrem1), 0)
'               If intDur < 10 Then
'                  oSaltCommon.Premium.GrossPremiums(intDur) = MAX(aintConstantValue(ValueBasePrem1), 0) * 2
'               End If
'            Next intDur
         'End If
         oSaltCommon.Premium.GrossPremiumType = TableConstant
      Else
         intStart = 1
         intTotalCount = aintCeaseValue(ValueBasePrem1)
'         gintPremiumCease = intTotalCount ' + pintAge
'         If rsPlan("CeaseUnits") = UnitAge Then
'            intTotalCount = MAX(intTotalCount - pintAge, 0)
'            gintPremiumCease = intTotalCount
'         End If

         If intAge = intBeginAge Then
            strSQL = "SELECT * " & _
                     "FROM tbl03InitPrem " & vbNewLine & _
                     "WHERE ((tbl03InitPrem.ProdID)=" & pintProdID & ") " & _
                     "ORDER BY tbl03InitPrem.Age"
            rsValue.Open strSQL, mcnnMain, adOpenStatic, adLockReadOnly
            'rsValue.Open strSQL, mcnnMain, adOpenDynamic, _
               adLockReadOnly
         End If
         strFormatType = SetFormat(aintFormatType(ValueBasePrem1), intGender, intTobacco, intUW)
         With rsValue
            .MoveFirst
            .MovePrevious
            .Find "[Age]=" & intAge
            If Not .EOF Then
               For intDur = 0 To oSaltCommon.Premium.PremiumCeaseAge - intAge - 1
                  oSaltCommon.Premium.GrossPremiums(intDur) = GetEnteredValue(.Fields(strFormatType), 0)   ' .Fields("G" & intGender & "T" & intTobacco)
               Next intDur
               For intDur = oSaltCommon.Premium.PremiumCeaseAge - intAge To intTerminateAge - intAge
                  oSaltCommon.Premium.GrossPremiums(intDur) = 0 'GetEnteredValue(.Fields(strFormatType), 0)   ' .Fields("G" & intGender & "T" & intTobacco)
               Next intDur
            End If
            intStart = intDur
         End With
         oSaltCommon.Premium.GrossPremiumType = TableAttainedAge
      End If
      ' Ultimate premium
'      If aintValueVariation(ValueUltimatePrem) = TableConstant Then  ' Ben added this code on 7/12/2002
'         If intAge = intBeginAge Then 'only need to do it once
'            For intDur = intTerminateAge - intAge - 1 To 0 Step -1
'               oSaltCommon.Premium.GrossPremiums(intDur) = MAX(aintConstantValue(ValueBasePrem1), 0)
'            Next intDur
'         End If
'         oSaltCommon.Premium.GrossPremiumType = TableConstant
'      Else
'         intStart = 1
'         intTotalCount = aintCeaseValue(ValueBasePrem1)
''         gintPremiumCease = intTotalCount ' + pintAge
''         If rsPlan("CeaseUnits") = UnitAge Then
''            intTotalCount = MAX(intTotalCount - pintAge, 0)
''            gintPremiumCease = intTotalCount
''         End If
'
'         If intAge = intBeginAge Then
'            strSQL = "SELECT * " & _
'                     "FROM tbl03InitPrem " & vbNewLine & _
'                     "WHERE ((tbl03InitPrem.ProdID)=" & pintProdID & ") " & _
'                     "ORDER BY tbl03InitPrem.Age"
'            Set rsValue = gdbMain.OpenRecordset(strSQL, dbOpenDynaset, dbReadOnly)
'            'rsValue.Open strSQL, mcnnMain, adOpenDynamic, _
'               adLockReadOnly
'         End If
'         strFormatType = SetFormat(aintFormatType(ValueBasePrem1), intGender, intTobacco, intUW)
'         With rsValue
'            .FindFirst "[Age]=" & intAge
'            If Not .EOF Then
'               For intDur = 0 To oSaltCommon.Premium.PremiumCeaseAge - intAge - 1
'                  oSaltCommon.Premium.GrossPremiums(intDur) = GetEnteredValue(.Fields(strFormatType), "")   ' .Fields("G" & intGender & "T" & intTobacco)
'               Next intDur
'            End If
'            intStart = intDur
'         End With
'         oSaltCommon.Premium.GrossPremiumType = TableConstant
'      End If   '***   End of Input
      If pintCalcType = 1 Then
         oSaltCommon.CV.CalcSC = blnSC
         If blnSC Then
            For intDur = 1 To intTerminateAge - intAge
               rsSC.MoveFirst
               rsSC.MovePrevious
               rsSC.Find "Dur = " & intDur
               If rsSC.EOF Then
                  oSaltCommon.CV.SC(intDur) = 0
               Else
                  If Not IsNull(rsSC("Age " & intAge)) Then
                     oSaltCommon.CV.SC(intDur) = rsSC("Age " & intAge)
                  Else
                     oSaltCommon.CV.SC(intDur) = 0
                  End If
               End If
            Next intDur
         End If
      End If
   
   '***   Calculations
   
      oSaltCommon.CalcFunctions 'oSaltCommon
      
   Next intAge
   InputDLLFromProdTable = True

End Function
Private Function SetFormat(ByVal pintType As Integer, ByVal pintGender As eGenderConstants, _
   pintTobacco As eTobacco, ByVal pintUW As eUnderwriting)
   If pintType = 8 Then
      SetFormat = "Z"
   ElseIf pintType = 7 Then
      SetFormat = "T" & pintTobacco
   ElseIf pintType = 6 Then
      SetFormat = "U" & pintUW
   ElseIf pintType = 5 Then
      SetFormat = "G" & pintGender
   ElseIf pintType = 4 Then
      SetFormat = "U" & pintUW & "T" & pintTobacco
   ElseIf pintType = 3 Then
      SetFormat = "G" & pintGender & "T" & pintTobacco
   ElseIf pintType = 2 Then
      SetFormat = "G" & pintGender & "U" & pintUW
   ElseIf pintType = 1 Then
      SetFormat = "G" & pintGender & "U" & pintUW & "T" & pintTobacco
   End If
End Function
Private Function GetEnteredValue(ByVal pfldField As ADODB.Field, _
                                Optional ByVal pvntIfNull As Variant = "", _
                                Optional ByVal pblnIsFormatted As Boolean = False) As Variant
   If IsNull(pfldField.Value) Then
      If pfldField.Type = adDate Then
         GetEnteredValue = Format(Now(), "MM/DD/YYYY")
      Else
         GetEnteredValue = pvntIfNull
      End If
   Else
      If pblnIsFormatted = True Then
         GetEnteredValue = Format(pfldField.Value, "#0.00##")
      Else
         GetEnteredValue = pfldField.Value
      End If
   End If
End Function
' Purpose  : Populates an array with values from a table
' Inputs   :
'  pstrTableName - String specifying the name of the table or query to use
'  pdbInput - DAO Database object specifying the database that pstrTableName is in
'  pstrDataFieldName - String specifying the name of the data field (ie - "Age 0")
'  pstrIndexFieldName - String specifying the name of the index field (ie - duration,
'     age, ID)
'  pintInitialArrayIndex - Integer specifying the index at which to begin populating
'     the array
'  pasngOutput() - Variant array specifying the array to be populated
'  plngFirstValue - Long specifying the first index value to retrieve
'  plngLastValue - Long specifying the last index value to retrieve (-999 or omit to get
'     all of the indexes)
'  pintProdID - Integer specifying the product ID to get (-999 or omit to not check the
'     product ID)
'  pintSubCode - Integer specifying the sub code to get (-999 or omit to not check the
'     sub code)
' Returns  :
' Details  :
'  Source  : Salt Solutions
'  Created : 10/10/2001 1:37 PM   By Ben Kester
'  Modified:
Private Function ReadAgeDur(ByVal pintValueVariation As eTableType, _
      ByVal pstrTableName As String, ByVal pcnnInput As ADODB.Connection, _
      ByVal pstrDataFieldName As String, ByVal pstrIndexFieldName As String, _
      ByVal pintInitialArrayIndex As Integer, ByRef pasngOutput() As Single, _
      ByVal plngFirstValue As Long, Optional ByVal plngLastValue As Long _
      = -999, Optional ByVal pintProdID As Integer = -999, Optional ByVal _
      pintSubCode As Integer = -999) As Boolean
 
Dim intIndex As Integer, rsInput As New ADODB.Recordset, strSQL As String
Dim strWhere As String, i As Integer
   
   On Error GoTo Err_ReadAgeDur
   gerrHandler.Push "ReadAgeDur", "modIllus"
   ' If an error occurrs, False will be returned
   ReadAgeDur = False
   strWhere = ""
   If plngLastValue <> -999 Then
      strWhere = strWhere & " AND ((" & pstrTableName & "." & pstrIndexFieldName & _
         ")<=" & MAX(plngLastValue, plngFirstValue) & ")"
   End If
   If pintProdID <> -999 Then
      strWhere = strWhere & " AND ((" & pstrTableName & ".ProdID)=" & pintProdID & ")"
   End If
   If pintSubCode <> -999 Then
      strWhere = strWhere & " AND ((" & pstrTableName & ".SubCode)=" & pintSubCode & ")"
   End If
   strSQL = "SELECT [" & pstrDataFieldName & "] FROM " & pstrTableName & vbNewLine & _
      "WHERE (((" & pstrTableName & "." & pstrIndexFieldName & ")>=" & _
         plngFirstValue & ")" & strWhere & ")" & vbNewLine & _
      "ORDER BY " & pstrIndexFieldName & ";"
   ' Open up the recordset
   rsInput.Open strSQL, mcnnMain, adOpenForwardOnly, adLockReadOnly
   'Set rsInput = New ADODB.Recordset
   'rsInput.Open strSQL, pdbInput, adOpenForwardOnly, adLockReadOnly
   With rsInput
      If Not .EOF Then
         If .BOF Then
            .MoveNext
         End If
         intIndex = pintInitialArrayIndex
         If pintValueVariation = TableIssueAge Then
            plngLastValue = MIN(plngLastValue, 100)
            For i = plngFirstValue To plngLastValue
               pasngOutput(intIndex) = GetEnteredValue(.Fields(pstrDataFieldName), "")
               intIndex = intIndex + 1
            Next i
         Else
            Do Until .EOF
               If IsNull(.Fields(pstrDataFieldName)) Then
                  pasngOutput(intIndex) = 0
               Else
                  pasngOutput(intIndex) = GetEnteredValue(.Fields(pstrDataFieldName), "")
               End If
               intIndex = intIndex + 1
               .MoveNext
            Loop
         End If
      Else
         MsgBox ("Could not find rate for sql:" & vbNewLine & strSQL)
      End If
   End With
   ReadAgeDur = True
   
Exit_ReadAgeDur:
   gerrHandler.Pop
   Exit Function
   
Err_ReadAgeDur:
   gerrHandler.HandleError
   Resume Exit_ReadAgeDur
   Resume
End Function
Public Property Get oSaltCommon() As SaltTRCommon
   If gSaltTRCommon Is Nothing Then
      Set gSaltTRCommon = New SaltTRCommon
   End If
   Set oSaltCommon = gSaltTRCommon
End Property
Public Function MAX(pvarVal1 As Variant, pvarVal2 As Variant) As Variant
 
    If pvarVal1 > pvarVal2 Then
       MAX = pvarVal1
    Else
       MAX = pvarVal2
    End If

End Function
Public Function MIN(ByVal pvarVal1 As Variant, ByVal pvarVal2 As Variant) As Variant

    If pvarVal1 < pvarVal2 Then
       MIN = pvarVal1
    Else
       MIN = pvarVal2
    End If

End Function
