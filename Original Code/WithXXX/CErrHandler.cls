VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CErrHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

' Class       : CErrHandler
' Description : Global Error Handler
' Source      : Total Visual SourceBook 2000
'
' stEvents
' ------------------------------------------------------
' Raise when an error is trapped. User may set Cancel
' argument to True to bypass further processing of the error
Public Event BeforeHandlerCalled(Cancel As Boolean)

' Raise an error after the event has been handled, so the user
' can take additional action if desired. For example a form might
' be shown to display the error codes and the error log generated.
Public Event AfterHandlerCalled()

' Local variables to hold property values
' ------------------------------------------------------
Private m_intMaxProcStackItems As Integer
Private m_fIncludeExpandedInfo As Boolean
Private m_fDisplayMsgOnError As Boolean
Private m_lngErrorNumber As Long
Private m_lngErrorLine As Long
Private m_strErrorDescription As String
Private m_strProcName As String
Private m_strAppTitle As String
Private m_varDestination As Variant
Private m_strCurrentOperation As String
Private m_fTraceExecution As Boolean

' Private class-specific variables
' ------------------------------------------------------

' Array to hold a stack of procedure names. The
' size of the stack is determined by the setting of the
' MaxProcStackItems property. Default is 20.
Private mastrErrorStack() As String

' Pointer to the current position in the stack
Private mintCurStackPointer As Integer

' Add two for our own handling
Private mintUseItems As Integer

Private mstrLogFile As String
Private mintLogFile As Integer
Private Const mcstrTraceFile = "trace.log"
' Set initial values to defaults which may be overridden
' with property settings
' Source: Total Visual SourceBook 2000
Private Sub Class_Initialize()

   m_intMaxProcStackItems = 100
   mintUseItems = m_intMaxProcStackItems + 2
   
   m_fIncludeExpandedInfo = True
   m_fDisplayMsgOnError = False
   m_varDestination = "C:\ERRORS.TXT"
   TraceExecution = True
   
   ReDim mastrErrorStack(0 To mintUseItems)
   mintCurStackPointer = LBound(mastrErrorStack)
   
End Sub
' Close trace log if opened, and check to be sure
' that all Push calls are balanced by Pop calls
' Source: Total Visual SourceBook 2000
Private Sub Class_Terminate()
   
   If m_fTraceExecution Then
      Close mintLogFile
   End If
   
   'Debug.Assert mintCurStackPointer = 0
   
End Sub
Friend Property Get AppTitle() As String
   ' Returns: The current value of AppTitle
   ' Source: Total Visual SourceBook 2000

   AppTitle = m_strAppTitle

End Property
Friend Property Let AppTitle(ByVal strValue As String)
   ' strValue: A title to be used with the built-in error handler message
   '           box if you wish to override the standard title, which is the
   '           value of the app.title property
   ' Source: Total Visual SourceBook 2000

m_strAppTitle = strValue

End Property
Friend Property Get CurrentOperation() As String
   ' Returns: The current value of CurrentOperation
   ' Source: Total Visual SourceBook 2000

   CurrentOperation = m_strCurrentOperation
   
End Property
Friend Property Let CurrentOperation(ByVal strValue As String)
   ' strValue: A string containing the name of the current operation.
   '           This is a user-defined checkpoint to assist in locating
   '           the source of an error
   ' Source: Total Visual SourceBook 2000
   
   m_strCurrentOperation = strValue
   
End Property
Public Property Get Destination() As Variant
   ' Returns: The current value of the Destination property.
   ' Source: Total Visual SourceBook 2000
   '          If you assigned an error log path name string
   '          to this property, a string is returned. If you
   '          assigned a Recordset object to this property,
   '          an object pointer to that recordset is returned.
   '
   If IsObject(m_varDestination) Then
      Set Destination = m_varDestination
   Else
      Destination = m_varDestination
   End If

End Property
Public Property Let Destination(ByVal varValue As Variant)
   ' strValue: Sets the destination for the err output. This can be
   '           either a string containing the path to a text file,
   '           or a recordset object variable
   ' Source: Total Visual SourceBook 2000
   '
   Select Case VarType(varValue)
      Case vbString
         m_varDestination = varValue
      Case Else
         Err.Raise vbObjectError + 1000, , "Invalid object type: " & VarType(varValue)
   End Select
   
End Property
Public Property Get DisplayMsgOnError() As Boolean
   ' Returns: the current value of DisplayMsgOnError
   ' Source: Total Visual SourceBook 2000
   
   DisplayMsgOnError = m_fDisplayMsgOnError

End Property
Public Property Let DisplayMsgOnError(ByVal fValue As Boolean)
   ' fValue: Assign True (default) to display a messagebox containing basic
   '         information about the error when an error is handled.
   '         Assign False to not display a message box
   ' Source: Total Visual SourceBook 2000

   m_fDisplayMsgOnError = fValue
   
End Property
Friend Property Get ErrorDescription() As String
   ' Returns: The current value of ErrorDescription. This value is
   '          set to the description of the error
   '          which caused the error handler to be triggered.
   '          Refer to this property in the AppSpecificErrorHandler
   '          procedure, or in the code triggered in response to the
   '          AfterHandlerCalled event.
   ' Source: Total Visual SourceBook 2000
   
   ErrorDescription = m_strErrorDescription
   
End Property
Friend Property Get ErrorLine() As Long
   ' Returns: the current value of ErrorLine. This Value is set to the
   '          line number of the procedure which caused the error when
   '          the error handler was triggered.
   '          Refer to this property in the AppSpecificErrorHandler
   '          procedure, or in the code triggered in response to the
   '          AfterHandlerCalled event.
   ' Source: Total Visual SourceBook 2000
   '
   ErrorLine = m_lngErrorLine

End Property
Friend Property Get ErrorNumber() As Long
   ' Returns: The current value of ErrorNumber property. This value is set
   '          to the error code which caused
   '          the error when the error handler is triggered.
   '          Refer to this property in the AppSpecificErrorHandler
   '          procedure, or in the code triggered in response to the
   '          AfterHandlerCalled event.
   ' Source: Total Visual SourceBook 2000
   '
   ErrorNumber = m_lngErrorNumber

End Property
Public Property Get IncludeExpandedInfo() As Boolean
   ' Returns: The current value of IncludeExpandedInfo
   ' Source: Total Visual SourceBook 2000
      
   IncludeExpandedInfo = m_fIncludeExpandedInfo

End Property
Public Property Let IncludeExpandedInfo(ByVal fValue As Boolean)
   ' fValue: Set to True (default) to include additional information about
   '         the user's machine environment in the error log. Set to
   '         False too not includ Expanded Information in the error log
   ' Source: Total Visual SourceBook 2000
   '
   m_fIncludeExpandedInfo = fValue

End Property
Public Property Get MaxProcStackItems() As Integer
   ' Returns: The current value of the MaxProcStackItems property
   ' Source: Total Visual SourceBook 2000
   '
   MaxProcStackItems = m_intMaxProcStackItems

End Property
Public Property Let MaxProcStackItems(ByVal intValue As Integer)
   ' intValue: Set the size of Procedure Name stack to a value other than
   '           the default of 20. Unless you have very deeply nested procedure
   '           calls there is no need to increase this value.
   ' Source: Total Visual SourceBook 2000
   
   m_intMaxProcStackItems = intValue
   mintUseItems = m_intMaxProcStackItems + 2
   ReDim mastrErrorStack(0 To mintUseItems)
   
End Property
Friend Property Get ProcName() As String
   ' Returns: The current value of the ProcName property. This value is set
   '          to the name of the procedure containing the error which caused
   '          the error handler to be triggered. Refer to this property in
   '          the AppSpecificErrorHandler procedure, or in the code
   '          triggered in response to the AfterHandlerCalled event.
   ' Source: Total Visual SourceBook 2000
   '
   ProcName = m_strProcName

End Property
Public Property Get TraceExecution() As Boolean
   ' Returns: the current value of TraceExecution
   ' Source: Total Visual SourceBook 2000

   TraceExecution = m_fTraceExecution

End Property
Public Property Let TraceExecution(ByVal fValue As Boolean)
   ' fValue: Set to True to begin tracing exection of the program.
   '         Set to False to stop. Setting the value to True also
   '         deletes and recreates the log file
   ' Source: Total Visual SourceBook 2000
   '
   On Error GoTo Err_TraceExecution
   
   m_fTraceExecution = fValue
   
   If m_fTraceExecution Then
      ' Start Logging
      mstrLogFile = App.Path & "\" & mcstrTraceFile
   
      On Error Resume Next
      If Dir(mstrLogFile) = "" Or Len(mstrLogFile) = 0 Then 'Dave added 2/20/02
      Else
         Close
         Kill mstrLogFile
      End If
      
      On Error GoTo Err_TraceExecution
      mintLogFile = FreeFile
      Open mstrLogFile For Binary As mintLogFile
   
   End If

Exit_TraceExecution:
   Exit Property

Err_TraceExecution:
   MsgBox "Error: " & Err.Number & ". " & Err.Description, , "TraceExecution"
   Resume Exit_TraceExecution
   
End Property
Public Function ClearLog() As Boolean
   ' Comments : Clear the error log (or table depending on the
   '             setting of the Destination property)
   ' Inputs   : none
   ' Returns  : True if successful, False otherwise
   '  Source  : Total Visual SourceBook 2000
   '
   On Error GoTo Err_ClearLog

   ' Log may be either to a text file or a recordset. Check whether
   ' the Destination property points to an object (recordset) or a string
   ' Destination is a path to a log file
   If VarType(m_varDestination) = vbString Then
      ' File might not exist, so use in-line error handling to avoid
      ' a run-time error
      On Error Resume Next
      Kill m_varDestination
      On Error GoTo Err_ClearLog
   End If
      
   ClearLog = True

Exit_ClearLog:
   Exit Function

Err_ClearLog:
   ClearLog = False
   Resume Exit_ClearLog

End Function
' Comments : The master error handler. This method is called
'             if a run-time error branches to a section of code
'             which calls this method. This method should only
'             be called AFTER an error has occurred. See the reference
'             material for a further explanation, and an example of
'             when and how to call this method.
' Inputs   : none
' Returns  : nothing
'  Source  : Total Visual SourceBook 2000
Public Sub HandleError(Optional ByVal pblnDisplayMsg As Boolean = True)
   Dim strTemp As String
   Dim fCancel As Boolean
   
   ' Get and save the current error information
   m_lngErrorNumber = Err.Number
   m_lngErrorLine = Erl
   m_strErrorDescription = Err.Description
   m_strProcName = mastrErrorStack(mintCurStackPointer)
   
   ' If a specific title value was not assigned, use the App.title property
   If m_strAppTitle = "" Then
      m_strAppTitle = App.Title
   End If
   
   ' Raise an event indicating that the error handler has been triggered.
   ' If the value of the variable 'fCancel' is altered in the raised event,
   ' then bypass further processing. See the reference for information
   ' on this event
   fCancel = False
   RaiseEvent BeforeHandlerCalled(fCancel)
   
   ' No calling code altered the value of fCancel, so continue on
   If Not fCancel Then
   
      ' Since we don't know what state we are in, disable error trapping
      On Error Resume Next
      
      ' If the Destination contains a Recordset object, call the
      ' routine to log the information to a row in that recordset. If it is
      ' a string containing the name of a disk file, call the routine to
      ' write the information to that disk file
      ' object assumed to be a string containing the name of the log file
      Call LogErrorToFile

      ' turn off in-line error handling
      On Error GoTo 0

      ' display error information in a message box if the
      ' DisplayMsgOnError flag is set to True
      If m_fDisplayMsgOnError Then
         
         Beep
         
         ' construct message string
         strTemp = "The following error has occurred in procedure: " & vbCrLf & _
            mastrErrorStack(mintCurStackPointer) & " on line " & _
            IIf(IsNull(m_lngErrorLine), "<unknown>", m_lngErrorLine) & vbCrLf & _
            "Error (" & m_lngErrorNumber & ") " & m_strErrorDescription
            
         If m_strCurrentOperation <> "" Then
            strTemp = strTemp & vbCrLf & "Operation ID: " & m_strCurrentOperation
         End If
         
         MsgBox strTemp, vbCritical, m_strAppTitle
      End If
   
      ' Generate event indicating that the error handler has completed its
      ' operation. This would be a good place for the calling application to
      ' display a custom form or a message box to display the error
      ' information and query the user as to how to proceed.
      RaiseEvent AfterHandlerCalled
      
   End If
   
   ' See comments in procedure
   If pblnDisplayMsg Then
      Call AppSpecificErrHandler
   End If
      
End Sub
' Comments : Pops the current procedure name off the error
'             handling stack.
'             Call this method when your code successfully
'             exits a procedure with no errors
' Inputs   : None
' Returns  : Nothing
'  Source  : Total Visual SourceBook 2000
Public Sub Pop()
   
   ' erase last item on the stack
   If mintCurStackPointer <= m_intMaxProcStackItems Then
      mastrErrorStack(mintCurStackPointer) = ""
   End If

   ' decrement the current stack pointer to point to the previous item
   mintCurStackPointer = mintCurStackPointer - 1
   
   If mintCurStackPointer < 0 Then
      mintCurStackPointer = 0
   End If
   m_strCurrentOperation = ""

End Sub
' Purpose : Pushes the supplied procedure name onto the error
'             handling stack.
'             Call this method at the beginning of a procedure
' Inputs   :
'  pstrProc - name of the currently executing procedure
'  pstrMod - name of the currently executing module
' Returns  : Nothing
'  Source  : Total Visual SourceBook 2000
Public Sub Push(ByVal pstrProc As String, ByVal pstrMod As String)
   
Dim intCount As Integer
   
   On Error GoTo Err_Push
   ' increase the current stack pointer to point to the next free slot
   mintCurStackPointer = mintCurStackPointer + 1
   
   pstrProc = pstrProc & " (" & pstrMod & ")"
   If mintCurStackPointer <= m_intMaxProcStackItems Then
      mastrErrorStack(mintCurStackPointer) = pstrProc
   Else
      mastrErrorStack(mintCurStackPointer + 2) = pstrProc
   End If
   
   ' if Trace logging is enabled, store to log file
   If m_fTraceExecution Then
      Put mintLogFile, , pstrProc & vbCrLf
   End If
   
Exit_Push:
   Exit Sub
   
Err_Push:
   If Err.Number = 9 Then  ' Subscript out of range
      For intCount = LBound(mastrErrorStack) To UBound(mastrErrorStack)
         Debug.Print mastrErrorStack(intCount)
      Next intCount
   End If
   If Err.Number = 9 Then  ' Subscript out of range
      Err.Raise 11111, "CErrHandler.Push", "Stack Exceeded Limit"
   End If
   Err.Raise Err.Number, Err.Source, Err.Description, Err.HelpFile, _
      Err.HelpContext
   
End Sub
' If you are not handling error notification by responding
' to theBeforeHandlerCalled or AfterHandlerCalled events,
' and you want to customize the error handler beyond simply
' displaying a message box with the error information, you
' can put application-specific error handling logic in the
' AppSpecificErrHandler procedure. This is the ONLY procedure
' which would need to be modifed in this class.
Private Sub AppSpecificErrHandler()
      
   ' perform custom action here
   If m_lngErrorLine = 0 Then
      MsgBox "An error has occurred.  Error details:" & vbNewLine & _
            vbTab & "Error Number: " & m_lngErrorNumber & vbNewLine & _
            vbTab & "Error Description: " & m_strErrorDescription & vbNewLine & _
            vbTab & "Procedure Name: " & m_strProcName & vbNewLine & _
            vbTab & "Operation ID: " & m_strCurrentOperation, , m_strAppTitle
   Else
      MsgBox "An error has occurred.  Error details:" & vbNewLine & _
            vbTab & "Error Number:      " & m_lngErrorNumber & vbNewLine & _
            vbTab & "Error Description: " & m_strErrorDescription & vbNewLine & _
            vbTab & "Procedure Name:    " & m_strProcName & vbNewLine & _
            vbTab & "Operation ID:      " & m_strCurrentOperation & vbNewLine & _
            vbTab & "Error Line:        " & m_lngErrorLine, , m_strAppTitle
   End If
   
End Sub
' This sub gets called when there is an error that is very unlikely to occurr, and
'  time cannot be wasted logging the event.  It can also occurr on an event that
'  occurrs frequently, such as MouseMove, Paint, Resize....
Public Sub UnexpectedError(ByVal pstrProcName As String, ByVal pstrModName As String)

Dim lngErrNum As Integer, strErrDescription As String
Dim lngErrLine As Integer, strCurrentOperation As String

   lngErrNum = Err.Number
   lngErrLine = Erl
   strCurrentOperation = m_strCurrentOperation
   strErrDescription = Err.Description
   ' Push the error
   Call Push(pstrProcName, pstrModName)
   ' Handle the error
   Call HandleError(False)
   ' Update the error properties
   m_lngErrorLine = lngErrLine
   m_lngErrorNumber = lngErrNum
   m_strCurrentOperation = strCurrentOperation
   m_strErrorDescription = strErrDescription
   Call AppSpecificErrHandler
   ' Pop the error
   Call Pop
End Sub
' Comments : Logs the most recent error to a disk file. The name
'             of the file is specified by setting the Destination
'             property of the error handler object to a string
'             containing the file name path
' Inputs   :
' Returns  : nothing
'  Source  : Total Visual SourceBook 2000
'
Private Sub LogErrorToFile()
   
Dim intFile As Integer, intCounter As Integer
Dim varTemp As Variant
   
   ' delete the destination file if it already exists
   On Error Resume Next
   Kill m_varDestination
   
   ' Turn off in-line error handling
   On Error GoTo 0

   intFile = FreeFile
   Open m_varDestination For Append As intFile
   
   Print #intFile, m_strAppTitle & " Application Error Information"
   Print #intFile, "===================================="
   Print #intFile, "Current Time   : " & Now
   Print #intFile, "Error String   : "; Left$(m_strErrorDescription, 255)
   Print #intFile, "Error Number   : " & m_lngErrorNumber
   Print #intFile, "Error Line     : " & m_lngErrorLine
   Print #intFile, "Error Procedure: " & _
      mastrErrorStack(mintCurStackPointer)
   Print #intFile,
   Print #intFile, "Procedure Stack"
   Print #intFile, "---------------"
   
   For intCounter = LBound(mastrErrorStack) To UBound(mastrErrorStack)
      If mastrErrorStack(intCounter) <> "" Then
         varTemp = varTemp & _
            Format(intCounter, "00") & Space$(intCounter * 2) & _
            mastrErrorStack(intCounter) & vbCrLf
      End If
   Next intCounter
   
   Print #intFile, varTemp

   If m_strCurrentOperation <> "" Then
      Print #intFile, "Operation ID: " & m_strCurrentOperation
   End If
   
   If m_fIncludeExpandedInfo Then
      Print #intFile,
      Print #intFile, "Additional Information"
      Print #intFile, "----------------------"

      varTemp = "Current Directory: " & CurDir & vbCrLf
      varTemp = varTemp & "App Title: " & App.Title & vbCrLf
      varTemp = varTemp & "App CompanyName: " & App.CompanyName & vbCrLf
      varTemp = varTemp & "App Major: " & App.Major & vbCrLf
      varTemp = varTemp & "App Minor: " & App.Minor & vbCrLf
      varTemp = varTemp & "App Revision: " & App.Revision & vbCrLf
      varTemp = varTemp & "App Path: " & App.Path & vbCrLf
      varTemp = varTemp & "App ProductName: " & App.ProductName
      
      ' if desired, retrieve various other items of system information,
      ' such as the Windows directory, free disk space etc., using the
      ' functions provided by VB SourceBook

      Print #intFile, varTemp

   End If

   ' Close the objects
   Close #intFile
   
End Sub
' Comments : Logs the most recent error to a table. The table is
'             specified by setting the Destination property
'             of the error handler object to a recordset object
'             which you create in your application
' Inputs   :
' Returns  : nothing
'  Source  : Total Visual SourceBook 2000
'
Private Sub LogErrorToTable()
   
Dim intCounter As Integer
Dim varTemp As Variant

   On Error GoTo Err_LogErrorToTable

   If m_varDestination Is Nothing Then
      Beep
      MsgBox "Error handler cannot write to the [" & m_varDestination & "] errors " & _
         "table because the Destination recordset variable has not been " & _
         "initialized.", , m_strAppTitle
   Else
   
      With m_varDestination
         .AddNew
         .Fields("ErrorDate") = Now
         .Fields("ErrorString") = Left$(m_strErrorDescription, 255)
         .Fields("ErrorNumber") = m_lngErrorNumber
         .Fields("ErrorLine") = m_lngErrorLine
         .Fields("ErrorProc") = mastrErrorStack(mintCurStackPointer)

         For intCounter = LBound(mastrErrorStack) To UBound(mastrErrorStack)
            If mastrErrorStack(intCounter) <> "" Then
               varTemp = varTemp & Format(intCounter, "00") & Space$(intCounter * 2) _
                  & mastrErrorStack(intCounter) & vbCrLf
            End If
         Next intCounter

         If m_strCurrentOperation <> "" Then
            varTemp = varTemp & vbCrLf & "Current Operation: " & m_strCurrentOperation
         End If

         .Fields("CallStack") = varTemp

         If m_fIncludeExpandedInfo Then
            varTemp = varTemp & "Current Directory: " & CurDir & vbCrLf
            varTemp = varTemp & "App Title: " & App.Title & vbCrLf
            varTemp = varTemp & "App CompanyName: " & App.CompanyName & vbCrLf
            varTemp = varTemp & "App Major: " & App.Major & vbCrLf
            varTemp = varTemp & "App Minor: " & App.Minor & vbCrLf
            varTemp = varTemp & "App Revision: " & App.Revision & vbCrLf
            varTemp = varTemp & "App Path: " & App.Path & vbCrLf
            varTemp = varTemp & "App ProductName: " & App.ProductName

            ' if desired, retrieve various other items of system information,
            ' such as the Windows directory, free disk space etc., using the
            ' functions provided by VB SourceBook

            .Fields("AdditionalInfo") = varTemp

         End If
         .Update
      End With
      
   End If
   
Exit_LogErrorToTable:
   Exit Sub

Err_LogErrorToTable:
   Err.Raise vbObjectError + 1000, "CErr", "Error opening recordset: "
   Resume Exit_LogErrorToTable
   
End Sub
