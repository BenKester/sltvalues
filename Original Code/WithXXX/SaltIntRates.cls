VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltIntRates"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'I am doing this so that the enum values will keep their correct case.
#If False Then
   Dim stConstant, stVaryByDuration
#End If
Public Enum InterestRateType
   stConstant = 0
   stVaryByDuration = 1
End Enum

Private m_aintInterestRateType(3) As InterestRateType 'local copy
Private m_adblInterestRateConstant(3) As Double 'local copy
Private m_adblInterestRateVary(3, 120) As Double 'local copy
Private m_objTRCommon As SaltTRCommon
Public Property Get TRCommon() As SaltTRCommon
   Set TRCommon = m_objTRCommon
End Property
Public Property Set TRCommon(poTRCommon As SaltTRCommon)
   Set m_objTRCommon = poTRCommon
End Property

Public Property Get InterestRateType(ByVal iIndicator As MortalityType) As Integer
   InterestRateType = m_aintInterestRateType(iIndicator)
End Property
Public Property Let InterestRateType(ByVal iIndicator As MortalityType, ByVal vData As Integer)
   m_aintInterestRateType(iIndicator) = vData
End Property

Public Property Get InterestRateConstant(ByVal iIndicator As MortalityType) As Double
   InterestRateConstant = m_adblInterestRateConstant(iIndicator)
End Property
Public Property Let InterestRateConstant(ByVal iIndicator As MortalityType, ByVal vData As Double)
   m_adblInterestRateConstant(iIndicator) = vData
End Property

Public Property Get InterestRateVary(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer) As Double
   InterestRateVary = m_adblInterestRateVary(iIndicator, iIndex1)
End Property
Public Property Let InterestRateVary(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblInterestRateVary(iIndicator, iIndex1) = vData
End Property
