VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltReserves"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit


'I am doing this so that the enum values will keep the correct case.
#If False Then
   Dim stNetLevel, stCRVM, stILStandard, stNJStandard, stImported, stFPT, stGradedCRVM, stOneHalfCx
   Dim stLowPremium, stHighPremium
   Dim stUnitaryMethod, stSegmentation, stNY147, stXXX1999
   Dim stMidYear, stTerminals, stMean, stMidTerminals
#End If
Public Enum ModResType
   stNetLevel = 0
   stCRVM = 1
   stILStandard = 2
   stNJStandard = 3
   stImported = 4
   stFPT = 5 'Not supported currently
   stGradedCRVM = 6 'Not supported currently
   stOneHalfCx = 7
End Enum
Public Enum PremiumType
   stLowPremium = 0
   stHighPremium = 1
End Enum
Public Enum ReserveMethodInput
   stUnitaryMethod = 0
   stSegmentation = 1
   stNY147 = 2
   stXXX1999 = 3
End Enum
Public Enum ComparisonRuleReserve
   stMidYear = 0
   stTerminals = 1
End Enum
Public Enum MidYearDefn
   stMean = 0
   stMidTerminals = 1
End Enum

Private m_adblDefNLReserves(120) As Double
Private m_adblDefModReserves(120) As Double
Private m_adblNetAnnualPremiums(120) As Double
Private m_adblNetLevelReserves(-1 To 120) As Double
Private m_adblMidYearReserves(120) As Double
Private m_adblMidTermReserves(120) As Double
Private m_adblModifiedReserves(120) As Double
Private m_adblModifiedNetPremium(120) As Double
Private m_adblTotalModReserves(6, 120) As Double
Private m_adblTotalNLReserves(120) As Double

Private m_blnExpenseAllowCurtate As Boolean
Private m_blnNLPConstantPercent As Boolean

Private m_dblBetaFPT As Double
Private m_dblBetaCRVM As Double
Private m_dblELRA As Double
Private m_dblExpenseAllow As Double
Private m_dblRc As Double
Private m_dblRen19PayPrem As Double
Private m_dblRn As Double
Private m_dblRf As Double

Private m_enuComparisonRule As ComparisonRuleReserve
Private m_enuMidYearDefn As MidYearDefn
Private m_enuReserveMethod As ReserveMethodInput
Private m_enuPremiumType As PremiumType

Private m_intCrvmYrs As Integer
Private m_intGradedYrs As Integer
Private m_intPremiumCeaseAge As Integer

Private m_inrIntRate As SaltIntRates
Private m_trcTRCommon As SaltTRCommon

Public Property Get NLPConstantPercent() As Boolean
    NLPConstantPercent = m_blnNLPConstantPercent
End Property
Public Property Let NLPConstantPercent(ByVal vData As Boolean)
    m_blnNLPConstantPercent = vData
End Property

Public Property Get ComparisonRule() As ComparisonRuleReserve
    ComparisonRule = m_enuComparisonRule
End Property
Public Property Let ComparisonRule(ByVal vData As ComparisonRuleReserve)
    m_enuComparisonRule = vData
End Property

Public Property Get ModRes() As ModResType
    ModRes = m_enuModResDefn
End Property
Public Property Let ModRes(ByVal vData As ModResType)
    m_enuModResDefn = vData
End Property

Public Property Get MidYear() As MidYearDefn
    MidYear = m_enuMidYearDefn
End Property
Public Property Let MidYear(ByVal vData As MidYearDefn)
    m_enuMidYearDefn = vData
End Property

Public Property Get IntRates() As SaltIntRates
   Set IntRates = m_inrIntRate
End Property
Public Property Set IntRates(poIntRates As SaltIntRates)
   Set m_inrIntRate = poIntRates
End Property

Public Property Get TRCommon() As SaltTRCommon
   Set TRCommon = m_trcTRCommon
End Property
Public Property Set TRCommon(poTRCommon As SaltTRCommon)
   Set m_trcTRCommon = poTRCommon
End Property

Public Property Get MidYearReserves(ByVal iIndex1 As Integer) As Double
    MidYearReserves = m_adblMidYearReserves(iIndex1)
End Property
Public Property Let MidYearReserves(ByVal iIndex1 As Integer, ByVal vData As Double)
    m_adblMidYearReserves(iIndex1) = vData
End Property

Public Property Get MidTermReserves(ByVal iIndex1 As Integer) As Double
    MidTermReserves = m_adblMidTermReserves(iIndex1)
End Property
Public Property Let MidTermReserves(ByVal iIndex1 As Integer, ByVal vData As Double)
    m_adblMidTermReserves(iIndex1) = vData
End Property

Public Property Get PremiumType() As PremiumType
    PremiumType = m_enuPremiumType
End Property
Public Property Let PremiumType(ByVal vData As PremiumType)
    m_enuPremiumType = vData
End Property

Public Property Get ExpenseAllow() As Double
    ExpenseAllow = m_dblExpenseAllow
End Property
Public Property Let ExpenseAllow(ByVal vData As Double)
    m_dblExpenseAllow = vData
End Property

Public Property Get CRVMYears() As Integer
    CRVMYears = m_intCrvmYrs
End Property
Public Property Let CRVMYears(ByVal vData As Integer)
    m_intCrvmYrs = vData
End Property

Public Property Get GradedYears() As Integer
    GradedYears = m_intGradedYrs
End Property
Public Property Let GradedYears(ByVal vData As Integer)
    m_intGradedYrs = vData
End Property

Public Property Get BetaCRVM() As Double
    BetaCRVM = m_dblBetaCRVM
End Property
Public Property Let BetaCRVM(ByVal vData As Double)
    m_dblBetaCRVM = vData
End Property

Public Property Get BetaFPT() As Double
    BetaFPT = m_dblBetaFPT
End Property
Public Property Let BetaFPT(ByVal vData As Double)
    m_dblBetaFPT = vData
End Property

Public Property Get DefModReserves(ByVal iIndex1 As Integer) As Double
    DefModReserves = m_adblDefModReserves(iIndex1)
End Property
Public Property Let DefModReserves(ByVal iIndex1 As Integer, ByVal vData As Double)
    m_adblDefModReserves(iIndex1) = vData
End Property

Public Property Get DefNLReserves(ByVal iIndex1 As Integer) As Double
    DefNLReserves = m_adblDefNLReserves(iIndex1)
End Property
Public Property Let DefNLReserves(ByVal iIndex1 As Integer, ByVal vData As Double)
    m_adblDefNLReserves(iIndex1) = vData
End Property

Public Property Get ELRA() As Double
    ELRA = m_dblELRA
End Property
Public Property Let ELRA(ByVal vData As Double)
    m_dblELRA = vData
End Property

Public Property Get ExpenseAllowCurtate() As Boolean
    ExpenseAllowCurtate = m_blnExpenseAllowCurtate
End Property
Public Property Let ExpenseAllowCurtate(ByVal vData As Boolean)
    m_blnExpenseAllowCurtate = vData
End Property

Public Property Get ModifiedNetPremium(ByVal iIndex1 As Integer) As Double
    ModifiedNetPremium = m_adblModifiedNetPremium(iIndex1)
End Property
Public Property Let ModifiedNetPremium(ByVal iIndex1 As Integer, ByVal vData As Double)
    m_adblModifiedNetPremium(iIndex1) = vData
End Property

Public Property Get ModifiedReserves(ByVal iIndex1 As Integer) As Double
    ModifiedReserves = m_adblModifiedReserves(iIndex1)
End Property
Public Property Let ModifiedReserves(ByVal iIndex1 As Integer, ByVal vData As Double)
    m_adblModifiedReserves(iIndex1) = vData
End Property

Public Property Get NetAnnualPremiums(ByVal iIndex1 As Integer) As Double
    NetAnnualPremiums = m_adblNetAnnualPremiums(iIndex1)
End Property
Public Property Let NetAnnualPremiums(ByVal iIndex1 As Integer, ByVal vData As Double)
    m_adblNetAnnualPremiums(iIndex1) = vData
End Property

Public Property Get NetLevelReserves(ByVal iIndex1 As Integer) As Double
    NetLevelReserves = m_adblNetLevelReserves(iIndex1)
End Property
Public Property Let NetLevelReserves(ByVal iIndex1 As Integer, ByVal vData As Double)
    m_adblNetLevelReserves(iIndex1) = vData
End Property

Public Property Get rc() As Double
    rc = m_dblRc
End Property
Public Property Let rc(ByVal vData As Double)
    m_dblRc = vData
End Property

Public Property Get Ren19PayPrem() As Double
    Ren19PayPrem = m_dblRen19PayPrem
End Property
Public Property Let Ren19PayPrem(ByVal vData As Double)
    m_dblRen19PayPrem = vData
End Property

Public Property Get ReserveMethod() As ReserveMethodInput
    ReserveMethod = m_enuReserveMethod
End Property
Public Property Get ReserveMethodType() As eResMethod 'ReserveMethodInput
    ReserveMethodType = m_enuReserveMethod
End Property

Public Property Let ReserveMethodType(ByVal vData As eResMethod) 'ReserveMethodInput
    m_enuReserveMethod = vData
End Property


Public Property Let ReserveMethod(ByVal vData As ReserveMethodInput)
    m_enuReserveMethod = vData
End Property

Public Property Get rf() As Double
    rf = m_dblRf
End Property
Public Property Let rf(ByVal vData As Double)
    m_dblRf = vData
End Property

Public Property Get TotalModReserves(ByVal iIndicator As enuTerminalIndicator, ByVal iIndex1 As Integer) As Double
    TotalModReserves = m_adblTotalModReserves(iIndicator, iIndex1)
End Property
Public Property Let TotalModReserves(ByVal iIndicator As enuTerminalIndicator, ByVal iIndex1 As Integer, ByVal vData As Double)
    m_adblTotalModReserves(iIndicator, iIndex1) = vData
End Property

Public Property Get TotalNLReserves(ByVal iIndex1 As Integer) As Double
    TotalNLReserves = m_adblTotalNLReserves(iIndex1)
End Property
Public Property Let TotalNLReserves(ByVal iIndex1 As Integer, ByVal vData As Double)
    m_adblTotalNLReserves(iIndex1) = vData
End Property

Public Function CalcAlpha(ByVal pdblPreviousReserves As Double, ByVal pdblDxNum As Double, ByVal pdblDb As Double, _
                           ByVal pdblCx As Double, ByVal pdblDx As Double) As Double
   CalcAlpha = (pdblPreviousReserves * pdblDxNum + pdblDb * pdblCx) / pdblDx
End Function
Public Sub CalcPremiums(ByVal pintIssueAge As Integer, ByVal pintEndAge As Integer, ByVal pintBeginAge As Integer, _
                        ByVal pblnUseExpAllow As Boolean, ByVal pblnUseEndow As Boolean, ByVal pbenBenefits As _
                        SaltBenefits, ByVal pprePremium As SaltPremium, ByVal pcomCommFunctions As _
                        SaltCommutationFunctions, ByVal penuTiming As TimingType, ByVal penuMortalityType As _
                        MortalityType)
Dim intI As Integer, dblPVFNP As Double, dblAnnuityFactor As Double, dblGradedPrem As Double, dblPVFCRVM As Double
Dim dblEndowAmount As Double

   If pprePremium.PVFGP(pintBeginAge) <> 0 Then 'Net Level
      m_dblRn = (pbenBenefits.PVFB(pintBeginAge)) / pprePremium.PVFGP(pintBeginAge)
   Else
      m_dblRn = 0
   End If
   If pblnUseEndow Then
      dblEndowAmount = pbenBenefits.EndowAmount
   Else
      dblEndowAmount = 0
   End If
   If m_blnNLPConstantPercent Then
      For intI = pintEndAge - 1 To pintBeginAge Step -1
         'Use the first formula if you want the NLP constant for all years.
         'Use the second formula if you want the NLP to be a constant % of the gross premium
         'mvarNetAnnualPremiums(i) = mvarPVFB(pintBeginAge) / mvarax(pintBeginAge) 'Constant Premium
         If intI >= pprePremium.PremiumCeaseAge Then
            m_adblNetAnnualPremiums(intI) = 0
         Else
            m_adblNetAnnualPremiums(intI) = m_dblRn * pprePremium.GrossPremiums(intI - pintBeginAge) 'Constant % of gross premium
         End If
      Next intI
   Else
      For intI = pintEndAge - 1 To pintBeginAge Step -1
         'Use the first formula if you want the NLP constant for all years.
         'Use the second formula if you want the NLP to be a constant % of the gross premium
         'mvarNetAnnualPremiums(i) = mvarPVFB(pintBeginAge) / mvarax(pintBeginAge) 'Constant Premium
         If pprePremium.ax(pintBeginAge) = 0 Or intI >= pprePremium.PremiumCeaseAge Then
            m_adblNetAnnualPremiums(intI) = 0
         Else
            m_adblNetAnnualPremiums(intI) = pbenBenefits.PVFB(pintBeginAge) / pprePremium.ax(pintBeginAge)
         End If
      Next intI
   End If
   If pblnUseExpAllow And pintBeginAge + 1 < pintEndAge Then
      If m_enuModResDefn = stOneHalfCx Then
         For intI = pintEndAge - 1 To pintBeginAge Step -1
            If penuTiming = stcurtate Then
               m_adblModifiedNetPremium(intI) = 0 'pbenBenefits.DeathBenefit(intI) * pcomCommFunctions.Cx(penuMortalityType, intI) / pcomCommFunctions.Dx(penuMortalityType, intI)
            ElseIf penuTiming = stImmediateClaims Then
               m_adblModifiedNetPremium(intI) = 0 'pbenBenefits.DeathBenefit(intI) * pcomCommFunctions.CBarx(penuMortalityType, intI) / pcomCommFunctions.Dx(penuMortalityType, intI)
            Else
               m_adblModifiedNetPremium(intI) = 0 'pbenBenefits.DeathBenefit(intI) * pcomCommFunctions.CBarx(penuMortalityType, intI) / pcomCommFunctions.DBarx(penuMortalityType, intI)
            End If
            m_dblExpenseAllow = 0
         Next intI
      ElseIf m_enuModResDefn = stNetLevel Then
         For intI = pintEndAge - 1 To pintBeginAge Step -1
            m_adblModifiedNetPremium(intI) = m_adblNetAnnualPremiums(intI)
         Next intI
         m_dblExpenseAllow = 0
      Else
         If penuTiming = stcurtate Or m_blnExpenseAllowCurtate Then
            m_dblRen19PayPrem = pcomCommFunctions.Mx(penuMortalityType, pintBeginAge + 1) / (pcomCommFunctions.Nx(penuMortalityType, pintBeginAge + 1) - pcomCommFunctions.Nx(penuMortalityType, pintBeginAge + 20))
         ElseIf penuTiming = stImmediateClaims Then
            m_dblRen19PayPrem = pcomCommFunctions.MBarx(penuMortalityType, pintBeginAge + 1) / (pcomCommFunctions.Nx(penuMortalityType, pintBeginAge + 1) - pcomCommFunctions.Nx(penuMortalityType, pintBeginAge + 20))
         Else
            m_dblRen19PayPrem = pcomCommFunctions.MBarx(penuMortalityType, pintBeginAge + 1) / (pcomCommFunctions.NBarx(penuMortalityType, pintBeginAge + 1) - pcomCommFunctions.NBarx(penuMortalityType, pintBeginAge + 20))
         End If
         If pprePremium.ax(pintBeginAge + 1) <> 0 Then
            m_dblBetaFPT = pbenBenefits.PVFB(pintBeginAge + 1) / pprePremium.ax(pintBeginAge + 1)
         Else
            m_dblBetaFPT = 0
         End If
         
         If pprePremium.GrossPremiumType = stGPConstant And pbenBenefits.DBType = stDBConstant Then
            m_dblELRA = pbenBenefits.DeathBenefit(pintBeginAge)
         Else
            m_dblELRA = (pbenBenefits.PVFB(pintBeginAge + 1) - _
                        dblEndowAmount * pcomCommFunctions.Dx(penuMortalityType, pintEndAge) / _
                        pcomCommFunctions.Dx(penuMortalityType, pintBeginAge + 1)) / _
                        ((pcomCommFunctions.Mx(penuMortalityType, pintBeginAge + 1) - _
                        pcomCommFunctions.Mx(penuMortalityType, pintEndAge)) / _
                        pcomCommFunctions.Dx(penuMortalityType, pintBeginAge + 1))
         End If
         
         If pprePremium.PVFGP(pintBeginAge + 1) <> 0 Then
            m_dblRf = (pbenBenefits.PVFB(pintBeginAge + 1)) / pprePremium.PVFGP(pintBeginAge + 1)
         Else
            m_dblRf = 0
         End If
         
         'If pintBeginAge <> 55 And m_dblRf * pprePremium.GrossPremiums(0) <= m_dblELRA * m_dblRen19PayPrem Or m_enuModResDefn = stFPT Then
         If m_dblRf * pprePremium.GrossPremiums(0) <= m_dblELRA * m_dblRen19PayPrem Or m_enuModResDefn = stFPT Then
            m_enuPremiumType = stLowPremium
            For intI = pintEndAge To pintBeginAge + 1 Step -1
               m_adblModifiedNetPremium(intI) = m_dblRf * pprePremium.GrossPremiums(intI - pintBeginAge)
            Next intI
         Else
            m_enuPremiumType = stHighPremium
            If penuTiming = stcurtate Or m_blnExpenseAllowCurtate Then
               If m_enuModResDefn = stILStandard And pprePremium.PremiumCeaseAge - pintIssueAge > 20 Then
                  m_dblExpenseAllow = (m_dblELRA * m_dblRen19PayPrem - pbenBenefits.DeathBenefit(pintBeginAge) * pcomCommFunctions.Cx(penuMortalityType, pintBeginAge) / pcomCommFunctions.Dx(penuMortalityType, pintBeginAge)) / (pprePremium.PVFGP(pintBeginAge) - pprePremium.PVFGP(pintBeginAge + 20) * pcomCommFunctions.Dx(penuMortalityType, pintBeginAge + 20) / pcomCommFunctions.Dx(penuMortalityType, pintBeginAge))
               Else
                  m_dblExpenseAllow = (m_dblELRA * m_dblRen19PayPrem - pbenBenefits.DeathBenefit(pintBeginAge) * pcomCommFunctions.Cx(penuMortalityType, pintBeginAge) / pcomCommFunctions.Dx(penuMortalityType, pintBeginAge)) / pprePremium.PVFGP(pintBeginAge)
               End If
            Else
               'If penuTiming = stImmediateClaims Then
               If m_enuModResDefn = stILStandard And pprePremium.PremiumCeaseAge - pintIssueAge > 20 Then
                  m_dblExpenseAllow = (m_dblELRA * m_dblRen19PayPrem - pbenBenefits.DeathBenefit(pintBeginAge) * pcomCommFunctions.CBarx(penuMortalityType, pintBeginAge) / pcomCommFunctions.Dx(penuMortalityType, pintBeginAge)) / (pprePremium.PVFGP(pintBeginAge) - pprePremium.PVFGP(pintBeginAge + 20) * pcomCommFunctions.Dx(penuMortalityType, pintBeginAge + 20) / pcomCommFunctions.Dx(penuMortalityType, pintBeginAge))
               Else
                  m_dblExpenseAllow = (m_dblELRA * m_dblRen19PayPrem - pbenBenefits.DeathBenefit(pintBeginAge) * pcomCommFunctions.CBarx(penuMortalityType, pintBeginAge) / pcomCommFunctions.Dx(penuMortalityType, pintBeginAge)) / pprePremium.PVFGP(pintBeginAge)
               End If
               'Else
               '   m_dblExpenseAllow = m_dblELRA * m_dblRen19PayPrem - pbenBenefits.DeathBenefit(pintBeginAge) * pcomCommFunctions.CBarx(penuMortalityType, pintBeginAge) / pcomCommFunctions.DBarx(penuMortalityType, pintBeginAge)
               'End If
            End If
            m_dblRc = (pbenBenefits.PVFB(pintBeginAge)) / pprePremium.PVFGP(pintBeginAge) + m_dblExpenseAllow
            For intI = pintEndAge - 1 To pintBeginAge + 1 Step -1
               m_adblModifiedNetPremium(intI) = m_dblRc * pprePremium.GrossPremiums(intI - pintBeginAge)
            Next intI
         End If
         
         'Get Alpha for low premium.  High premium gets Reserve(1) added to it.
         If m_enuPremiumType = stLowPremium Then
            If penuTiming = stcurtate Then
               m_adblModifiedNetPremium(pintBeginAge) = pbenBenefits.DeathBenefit(pintBeginAge) * pcomCommFunctions.Cx(penuMortalityType, pintBeginAge) / pcomCommFunctions.Dx(penuMortalityType, pintBeginAge)
            ElseIf penuTiming = stImmediateClaims Then
               m_adblModifiedNetPremium(pintBeginAge) = pbenBenefits.DeathBenefit(pintBeginAge) * pcomCommFunctions.CBarx(penuMortalityType, pintBeginAge) / pcomCommFunctions.Dx(penuMortalityType, pintBeginAge)
            Else
               m_adblModifiedNetPremium(pintBeginAge) = pbenBenefits.DeathBenefit(pintBeginAge) * pcomCommFunctions.CBarx(penuMortalityType, pintBeginAge) / pcomCommFunctions.DBarx(penuMortalityType, pintBeginAge)
            End If
            m_dblExpenseAllow = m_adblModifiedNetPremium(pintBeginAge + 1) - m_adblModifiedNetPremium(pintBeginAge)
         End If
         If m_adblModifiedNetPremium(pintBeginAge) > m_adblModifiedNetPremium(pintBeginAge + 1) Then 'Alpha > Beta.  Must use NLP
            'must recalculate modified reserves
            For intI = pintEndAge - 1 To pintBeginAge Step -1
               m_adblModifiedNetPremium(intI) = m_adblNetAnnualPremiums(intI)
            Next intI
            m_dblExpenseAllow = 0
         End If
         'Grading
         If m_enuModResDefn = stGradedCRVM Or m_enuModResDefn = stNJStandard Then
            If m_enuModResDefn = stNJStandard Then
               m_intCrvmYrs = 1
               m_intGradedYrs = 19 ' MAX(pprePremium.PremiumCeaseAge - pintIssueAge - m_intCrvmYrs - 1, 0)
            End If
            If penuTiming = stContinuous Then
               dblPVFCRVM = (pcomCommFunctions.NBarx(penuMortalityType, pintIssueAge + 1) - pcomCommFunctions.NBarx(penuMortalityType, pintIssueAge + m_intCrvmYrs)) / pcomCommFunctions.Dx(penuMortalityType, pintIssueAge + 1) * m_adblModifiedNetPremium(pintIssueAge + 1)
               dblPVFNP = (pcomCommFunctions.NBarx(penuMortalityType, pintIssueAge + m_intCrvmYrs + m_intGradedYrs) - pcomCommFunctions.NBarx(penuMortalityType, pprePremium.PremiumCeaseAge)) / pcomCommFunctions.Dx(penuMortalityType, pintIssueAge + 1) * m_adblNetAnnualPremiums(gGlobal.MIN(pintIssueAge + m_intCrvmYrs + m_intGradedYrs, pprePremium.PremiumCeaseAge - 1))
               dblAnnuityFactor = (pcomCommFunctions.NBarx(penuMortalityType, pintIssueAge + m_intCrvmYrs) - pcomCommFunctions.NBarx(penuMortalityType, pintIssueAge + m_intCrvmYrs + m_intGradedYrs)) / pcomCommFunctions.Dx(penuMortalityType, pintIssueAge + 1)
            Else
               dblPVFCRVM = (pcomCommFunctions.Nx(penuMortalityType, pintIssueAge + 1) - pcomCommFunctions.Nx(penuMortalityType, pintIssueAge + m_intCrvmYrs)) / pcomCommFunctions.Dx(penuMortalityType, pintIssueAge + 1) * m_adblModifiedNetPremium(pintIssueAge + 1)
               dblPVFNP = (pcomCommFunctions.Nx(penuMortalityType, pintIssueAge + m_intCrvmYrs + m_intGradedYrs) - pcomCommFunctions.Nx(penuMortalityType, pprePremium.PremiumCeaseAge)) / pcomCommFunctions.Dx(penuMortalityType, pintIssueAge + 1) * m_adblNetAnnualPremiums(gGlobal.MIN(pintIssueAge + m_intCrvmYrs + m_intGradedYrs, pprePremium.PremiumCeaseAge - 1))
               dblAnnuityFactor = (pcomCommFunctions.Nx(penuMortalityType, pintIssueAge + m_intCrvmYrs) - pcomCommFunctions.Nx(penuMortalityType, pintIssueAge + m_intCrvmYrs + m_intGradedYrs)) / pcomCommFunctions.Dx(penuMortalityType, pintIssueAge + 1)
            End If
            If dblAnnuityFactor <> 0 Then
               dblGradedPrem = (pbenBenefits.PVFB(pintIssueAge + 1) - dblPVFNP - dblPVFCRVM) / dblAnnuityFactor
            End If
            For intI = pintEndAge - 1 To pintBeginAge Step -1
               If intI >= pintIssueAge + m_intCrvmYrs And intI < pintIssueAge + m_intCrvmYrs + m_intGradedYrs And intI < pprePremium.PremiumCeaseAge Then
               'If intI <= pintIssueAge + m_intCrvmYrs And intI < pprePremium.PremiumCeaseAge Then
                  m_adblModifiedNetPremium(intI) = dblGradedPrem
               ElseIf intI >= pintIssueAge + m_intCrvmYrs + m_intGradedYrs And intI < pprePremium.PremiumCeaseAge Then
               'ElseIf intI >= pintIssueAge + m_intCrvmYrs + 1 And intI < pprePremium.PremiumCeaseAge Then
                  m_adblModifiedNetPremium(intI) = m_adblNetAnnualPremiums(intI)
               Else
                  m_adblModifiedNetPremium(intI) = 0
               End If
            Next intI
         End If
      End If
   Else
      For intI = pintEndAge - 1 To pintBeginAge Step -1
         m_adblModifiedNetPremium(intI) = m_adblNetAnnualPremiums(intI)
      Next intI
   End If

End Sub
Public Sub CalcReserve(pblnReCalc As Boolean, pintEndAge As Integer, pintBeginAge As Integer, pblnUseExpAllow As Boolean, pblnUseEndow As Boolean, poMortality As SaltMortality, poIntRates As SaltIntRates, pbenBenefits As SaltBenefits, pprePremium As SaltPremium, pcomCommFunctions As SaltCommutationFunctions, penuTiming As TimingType, penuMortalityType As MortalityType, penuTerminalType As enuTerminalIndicator)

Dim oSaltCommFns As New SaltCommutationFunctions
Dim intI As Integer
Dim sngMyIntRate As Single
Dim intJ As Integer
Dim dblDBTiming As Double, dblPremiumTiming As Double
   
   For intI = pintEndAge To pintBeginAge Step -1
      If poIntRates.InterestRateType(penuMortalityType) = 0 Then 'Constant
         sngMyIntRate = poIntRates.InterestRateConstant(penuMortalityType)
      Else
         sngMyIntRate = poIntRates.InterestRateVary(penuMortalityType, intI - pintBeginAge)
      End If
      If penuTiming = stcurtate Then
         dblDBTiming = 1
         dblPremiumTiming = 1
      Else
         If Delta(sngMyIntRate) <> 0 Then
            dblDBTiming = sngMyIntRate / Delta(sngMyIntRate)
         Else
            dblDBTiming = 1
         End If
         If penuTiming = stImmediateClaims Or pcomCommFunctions.Dx(penuMortalityType, intI) = 0 Then
            dblPremiumTiming = 1
         Else
            dblPremiumTiming = pcomCommFunctions.DBarx(penuMortalityType, intI) / pcomCommFunctions.Dx(penuMortalityType, intI)
         End If
      End If
      If m_enuModResDefn = stOneHalfCx Then
         If intI = pintEndAge Then
            m_adblModifiedReserves(intI) = 0
         Else
            If penuTiming = stcurtate Then
               m_adblModifiedReserves(intI) = 0.5 * pbenBenefits.DeathBenefit(intI) * pcomCommFunctions.Cx(penuMortalityType, intI) / pcomCommFunctions.Dx(penuMortalityType, intI)
            ElseIf penuTiming = stImmediateClaims Then
               m_adblModifiedReserves(intI) = 0.5 * pbenBenefits.DeathBenefit(intI) * pcomCommFunctions.CBarx(penuMortalityType, intI) / pcomCommFunctions.Dx(penuMortalityType, intI)
            Else
               m_adblModifiedReserves(intI) = 0.5 * pbenBenefits.DeathBenefit(intI) * pcomCommFunctions.CBarx(penuMortalityType, intI) / pcomCommFunctions.DBarx(penuMortalityType, intI)
            End If
         End If
      Else
         If intI >= pintBeginAge And intI < pintEndAge Then
            m_adblNetLevelReserves(intI) = pbenBenefits.DeathBenefit(intI) * dblDBTiming * poMortality.FinalMortality(penuMortalityType, intI) / (1 + sngMyIntRate) - m_adblNetAnnualPremiums(intI) * dblPremiumTiming + (1 - poMortality.FinalMortality(penuMortalityType, intI)) * m_adblNetLevelReserves(intI + 1) / (1 + sngMyIntRate)
            If m_enuPremiumType = stHighPremium And intI = pintBeginAge And pblnUseExpAllow Then
               If penuTiming = stcurtate Then
                  m_adblModifiedNetPremium(pintBeginAge) = CalcAlpha(m_adblModifiedReserves(intI + 1), pcomCommFunctions.Dx(penuMortalityType, pintBeginAge + 1), pbenBenefits.DeathBenefit(pintBeginAge), pcomCommFunctions.Cx(penuMortalityType, pintBeginAge), pcomCommFunctions.Dx(penuMortalityType, pintBeginAge))
               ElseIf penuTiming = stImmediateClaims Then
                  m_adblModifiedNetPremium(pintBeginAge) = CalcAlpha(m_adblModifiedReserves(intI + 1), pcomCommFunctions.Dx(penuMortalityType, pintBeginAge + 1), pbenBenefits.DeathBenefit(pintBeginAge), pcomCommFunctions.CBarx(penuMortalityType, pintBeginAge), pcomCommFunctions.Dx(penuMortalityType, pintBeginAge))
               Else
                  m_adblModifiedNetPremium(pintBeginAge) = CalcAlpha(m_adblModifiedReserves(intI + 1), pcomCommFunctions.Dx(penuMortalityType, pintBeginAge + 1), pbenBenefits.DeathBenefit(pintBeginAge), pcomCommFunctions.CBarx(penuMortalityType, pintBeginAge), pcomCommFunctions.DBarx(penuMortalityType, pintBeginAge))
               End If
               'ReCalc variable set up because sometimes the NL premium(issueage) will be slightly larger than NL premium(issueage + 1) due to decimal place errors in VB.  The variable recalc will force the recalc to occur only once.
               If m_adblModifiedNetPremium(pintBeginAge) > m_adblModifiedNetPremium(pintBeginAge + 1) And Not pblnReCalc Then  'Alpha > Beta.  Must use NLP
                  'must recalculate modified reserves
                  For intJ = m_intPremiumCeaseAge To pintBeginAge Step -1
                     m_adblModifiedNetPremium(intJ) = m_adblNetAnnualPremiums(intJ)
                  Next intJ
                  Call CalcReserve(True, pintEndAge, pintBeginAge, pblnUseExpAllow, pblnUseEndow, poMortality, poIntRates, pbenBenefits, pprePremium, pcomCommFunctions, penuTiming, penuMortalityType, penuTerminalType)
               End If
               'This recursive formula is identical to the formula below it.
               'm_adblModifiedReserves(intI) = (m_adblModifiedReserves(intI + 1)) * (1 - poMortality.FinalMortality(penuMortalityType, intI)) / (1 + sngMyIntRate) _
                  + pbenBenefits.DeathBenefit(intI) * dblDBTiming * poMortality.FinalMortality(penuMortalityType, intI) / (1 + sngMyIntRate) _
                  - dblPremiumTiming * m_adblModifiedNetPremium(pintBeginAge)
               
               m_adblModifiedReserves(intI) = pbenBenefits.DeathBenefit(intI) * dblDBTiming * poMortality.FinalMortality(penuMortalityType, intI) / (1 + sngMyIntRate) - m_adblModifiedNetPremium(intI) * dblPremiumTiming + (1 - poMortality.FinalMortality(penuMortalityType, intI)) * m_adblModifiedReserves(intI + 1) / (1 + sngMyIntRate)
            Else
               m_adblModifiedReserves(intI) = pbenBenefits.DeathBenefit(intI) * dblDBTiming * poMortality.FinalMortality(penuMortalityType, intI) / (1 + sngMyIntRate) - m_adblModifiedNetPremium(intI) * dblPremiumTiming + (1 - poMortality.FinalMortality(penuMortalityType, intI)) * m_adblModifiedReserves(intI + 1) / (1 + sngMyIntRate)
            End If
            
            If penuTerminalType = stMinimumUnitary Or penuTerminalType = stMinimumSegmented Then
               If m_adblNetAnnualPremiums(intI) > pprePremium.GrossPremiums(intI - pintBeginAge) Then
                  m_adblDefNLReserves(intI) = (m_adblNetAnnualPremiums(intI) - pprePremium.GrossPremiums(intI - pintBeginAge)) * poMortality.FinalMortality(penuMortalityType, intI) / (1 + sngMyIntRate) + (1 - poMortality.FinalMortality(penuMortalityType, intI)) * m_adblDefNLReserves(intI + 1) / (1 + sngMyIntRate)
               Else
                  m_adblDefNLReserves(intI) = (1 - poMortality.FinalMortality(penuMortalityType, intI)) * m_adblDefNLReserves(intI + 1) / (1 + sngMyIntRate)
               End If
               If m_adblModifiedNetPremium(intI) > pprePremium.GrossPremiums(intI - pintBeginAge) Then
                  m_adblDefModReserves(intI) = (m_adblModifiedNetPremium(intI) - pprePremium.GrossPremiums(intI - pintBeginAge)) * poMortality.FinalMortality(penuMortalityType, intI) / (1 + sngMyIntRate) + (1 - poMortality.FinalMortality(penuMortalityType, intI)) * m_adblDefModReserves(intI + 1) / (1 + sngMyIntRate)
               Else
                  m_adblDefModReserves(intI) = (1 - poMortality.FinalMortality(penuMortalityType, intI)) * m_adblDefModReserves(intI + 1) / (1 + sngMyIntRate)
               End If
            Else
               m_adblDefModReserves(intI) = 0
            End If
            m_adblTotalNLReserves(intI) = m_adblNetLevelReserves(intI) + m_adblDefNLReserves(intI)
            m_adblTotalModReserves(penuTerminalType, intI) = m_adblModifiedReserves(intI) + m_adblDefModReserves(intI)
         ElseIf intI = pintEndAge Then
            m_adblNetLevelReserves(intI) = pbenBenefits.PVFB(intI)
            m_adblModifiedReserves(intI) = pbenBenefits.PVFB(intI)
            m_adblDefNLReserves(intI) = 0
            m_adblTotalNLReserves(intI) = 0
            m_adblTotalModReserves(penuTerminalType, intI) = 0
         End If
      End If
   Next intI
   
End Sub
Public Sub CalcETIReserve(pblnReCalc As Boolean, pintEndAge As Integer, pintBeginAge As Integer, pblnUseExpAllow As Boolean, pblnUseEndow As Boolean, poMortality As SaltMortality, poIntRates As SaltIntRates, pbenBenefits As SaltBenefits, pprePremium As SaltPremium, pcomCommFunctions As SaltCommutationFunctions, penuTiming As TimingType, penuMortalityType As MortalityType, penuTerminalType As enuTerminalIndicator)

Dim oSaltCommFns As New SaltCommutationFunctions
Dim intI As Integer
Dim sngMyIntRate As Single
Dim intJ As Integer
Dim dblDBTiming As Double, dblPremiumTiming As Double
   
   For intI = pintEndAge To pintBeginAge Step -1
      If poIntRates.InterestRateType(penuMortalityType) = 0 Then 'Constant
         sngMyIntRate = poIntRates.InterestRateConstant(penuMortalityType)
      Else
         sngMyIntRate = poIntRates.InterestRateVary(penuMortalityType, intI - pintBeginAge)
      End If
      If penuTiming = stcurtate Then
         dblDBTiming = 1
         'dblPremiumTiming = 1
      Else
         If Delta(sngMyIntRate) <> 0 Then
            dblDBTiming = sngMyIntRate / Delta(sngMyIntRate)
         Else
            dblDBTiming = 1
         End If
'         If penuTiming = stImmediateClaims Or pcomCommFunctions.Dx(penuMortalityType, intI) = 0 Then
'            dblPremiumTiming = 1
'         Else
'            dblPremiumTiming = pcomCommFunctions.DBarx(penuMortalityType, intI) / pcomCommFunctions.Dx(penuMortalityType, intI)
'         End If
      End If
'      If intI >= pintBeginAge And intI < pintEndAge Then
         m_adblNetLevelReserves(intI) = pbenBenefits.DeathBenefit(intI) * dblDBTiming * (pcomCommFunctions.Mx(penuMortalityType, pintBeginAge) - pcomCommFunctions.Mx(penuMortalityType, intI)) / pcomCommFunctions.Dx(penuMortalityType, pintBeginAge)
         m_adblModifiedReserves(intI) = m_adblNetLevelReserves(intI)
         m_adblDefNLReserves(intI) = 0
         m_adblDefModReserves(intI) = 0
         m_adblTotalNLReserves(intI) = m_adblNetLevelReserves(intI) + m_adblDefNLReserves(intI)
         m_adblTotalModReserves(penuTerminalType, intI) = m_adblModifiedReserves(intI) + m_adblDefModReserves(intI)
'      ElseIf intI = pintEndAge Then
'         m_adblNetLevelReserves(intI) = pbenBenefits.PVFB(intI)
'         m_adblModifiedReserves(intI) = pbenBenefits.PVFB(intI)
'         m_adblDefNLReserves(intI) = 0
'         m_adblTotalNLReserves(intI) = 0
'         m_adblTotalModReserves(penuTerminalType, intI) = 0
'      End If
   Next intI
   m_adblNetLevelReserves(-1) = pbenBenefits.DeathBenefit(pintBeginAge) * pcomCommFunctions.Dx(penuMortalityType, pintEndAge) / pcomCommFunctions.Dx(penuMortalityType, pintBeginAge)
   
End Sub

Public Sub CalcMidYearReserve(pintEndAge As Integer, pintBeginAge As Integer, penuTerminalType As enuTerminalIndicator)

Dim intI As Integer
Dim sngMyIntRate As Single
Dim intJ As Integer
   
   For intI = pintBeginAge To pintEndAge Step 1
      If intI > 0 Then
         m_adblMidYearReserves(intI) = 0.5 * (m_adblTotalNLReserves(intI) + m_adblTotalNLReserves(intI - 1))
      Else
         m_adblMidYearReserves(intI) = 0.5 * (m_adblTotalNLReserves(intI))
      End If
      If m_enuMidYearDefn = stMean Then
         m_adblMidYearReserves(intI) = m_adblMidYearReserves(intI) + 0.5 * m_adblModifiedNetPremium(intI)
      End If
   Next intI
   
End Sub
Public Sub CalcMaxTerminals(pintEndAge As Integer, pintBeginAge As Integer, penuTerminalType As enuTerminalIndicator)
    
Dim intI As Integer
Dim sngMyIntRate As Single
Dim intJ As Integer
    
   For intI = pintBeginAge To pintEndAge Step 1
      If m_enuReserveMethod = stUnitaryMethod Then
         m_adblTotalModReserves(penuTerminalType, intI) = m_adblTotalModReserves(penuTerminalType - 2, intI)
      ElseIf m_enuReserveMethod = stSegmentation Then
         m_adblTotalModReserves(penuTerminalType, intI) = m_adblTotalModReserves(penuTerminalType - 1, intI)
      Else
         If m_enuComparisonRule = stTerminals Then
            m_adblTotalModReserves(penuTerminalType, intI) = gGlobal.MAX(m_adblTotalModReserves(penuTerminalType - 1, intI), m_adblTotalModReserves(penuTerminalType - 2, intI))
         End If
      End If
   Next intI
   
End Sub
Private Sub Class_Initialize()
   m_blnExpenseAllowCurtate = True
End Sub
