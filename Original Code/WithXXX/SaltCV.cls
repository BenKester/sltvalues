VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltCV"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private m_adblAdjustedPremium(120) As Double
Private m_adblCashValue(120) As Double
Private m_asngSC(120) As Single
'Private m_adblSC(120) As Double
Private m_adblPureEndowment(120) As Double
Private m_dblEA_CV As Double
Private m_dblAAI As Double
Private m_blnCalcCV As Boolean
Private m_blnCalcSC As Boolean
Private m_trcTRCommon As SaltTRCommon
Private m_enuCVMethod As eCVMethod
Private m_adblNSP(120) As Double
Private m_blnCVMessage(0) As Boolean
'Private m_enuModResDefn As ModResType

Public Property Get NSP(ByVal pintAttAge As Integer) As Double
   NSP = m_adblNSP(pintAttAge)
End Property
Public Property Get TRCommon() As SaltTRCommon
   Set TRCommon = m_trcTRCommon
End Property
Public Property Set TRCommon(ByVal poTRCommon As SaltTRCommon)
   Set m_trcTRCommon = poTRCommon
End Property

Public Property Get CalcCV() As Boolean
   CalcCV = m_blnCalcCV
End Property
Public Property Get CalcSC() As Boolean
   CalcSC = m_blnCalcSC
End Property

Public Sub CalcCashValue(ByVal pintEndAge As Integer, ByVal pintBeginAge As Integer, _
                        ByVal pfUseExpAllow As Boolean, ByVal pfUseEndow As Boolean, _
                        ByVal poIntRates As SaltIntRates, ByVal pobjBenefits As _
                        SaltBenefits, ByVal pobjPremium As SaltPremium, ByVal _
                        poCommFunctions As SaltCommutationFunctions, ByVal _
                        pobjReserves As SaltReserves, ByVal pobjMortality As _
                        SaltMortality, ByVal penuTiming As TimingType, ByVal _
                        penuMortalityType As MortalityType, penuProdType As eProductType)
   ' Comments  :
   ' Parameters: pintEndAge
   '             pintBeginAge
   '             pfUseExpAllow -
   ' Modified  :
   '
   ' --------------------------------------------------
   'CV
   'TVCodeTools ErrorEnablerStart
   On Error GoTo PROC_ERR
   'TVCodeTools ErrorEnablerEnd
   
Dim intI As Integer
Dim sngMyIntRate As Single
Dim m_dblRc As Double
Dim dblDBTiming As Double
Dim dblPremiumTiming As Double
Dim dblAdjustPremTest(1) As Double
Dim dblPVFB As Double
Dim dblAnnuity As Double
Dim dblG As Double 'From Allan's documentation
Dim dblF As Double 'From Allan's documentation
Dim dblNFF1 As Double 'From Allan's documentation
Dim dblNFF2 As Double 'From Allan's documentation
Dim dblNFF3 As Double 'From Allan's documentation
Dim blnEndowIndicator As Boolean
Dim dblNFF2Num1 As Double 'From Allan's documentation
Dim dblNFF2Num2 As Double 'From Allan's documentation
Dim dblNFF2Den As Double 'From Allan's documentation
   
   
'   dblG = 1000 * (poCommFunctions.Mx(stNonforfeitureMort, pintBeginAge) - _
'     poCommFunctions.Mx(stNonforfeitureMort, pintEndAge) + _
'     poCommFunctions.Dx(stNonforfeitureMort, pintEndAge) * -1 * blnEndowIndicator) / _
'     (poCommFunctions.Nx(stNonforfeitureMort, pintBeginAge) - _
'     poCommFunctions.Nx(stNonforfeitureMort, pobjPremium.PremiumCeaseAge))
'   dblF = 1000 * (poCommFunctions.Mx(stNonforfeitureMort, pintBeginAge)) / _
'     (poCommFunctions.Nx(stNonforfeitureMort, pintBeginAge) - _
'     poCommFunctions.Nx(stNonforfeitureMort, pintBeginAge + 20))
'   If dblG <= dblF Then
'      dblNFF1 = 1000 * (poCommFunctions.Mx(stNonforfeitureMort, pintBeginAge) - _
'     poCommFunctions.Mx(stNonforfeitureMort, pintBeginAge + 1)) / _
'     (poCommFunctions.Dx(stNonforfeitureMort, pintBeginAge))
'   Else
'      dblNFF1 = 0
'   End If
'   If dblG <= dblF Then
'      dblNFF2 = 1000 * (poCommFunctions.Mx(stNonforfeitureMort, pintBeginAge + 1) - _
'     poCommFunctions.Mx(stNonforfeitureMort, pintEndAge) + _
'     poCommFunctions.Dx(stNonforfeitureMort, pintEndAge) * -1 * blnEndowIndicator) / _
'     (poCommFunctions.Nx(stNonforfeitureMort, pintBeginAge + 1) - _
'     poCommFunctions.Nx(stNonforfeitureMort, pobjPremium.PremiumCeaseAge))
'   Else
'      dblNFF2Num1 = 1000 * (poCommFunctions.Mx(stNonforfeitureMort, pintBeginAge + 1)) / _
'        (poCommFunctions.Nx(stNonforfeitureMort, pintBeginAge + 1) - _
'        poCommFunctions.Nx(stNonforfeitureMort, pintBeginAge + 20))
'      dblNFF2Num2 = 1000 * (poCommFunctions.Mx(stNonforfeitureMort, pintBeginAge) - _
'        poCommFunctions.Mx(stNonforfeitureMort, pintBeginAge + 1)) / _
'        (poCommFunctions.Dx(stNonforfeitureMort, pintBeginAge))
'      If pobjPremium.PremiumCeaseAge - pintBeginAge >= 20 Then
'         dblNFF2Den = ((poCommFunctions.Nx(stNonforfeitureMort, pintBeginAge) - _
'           poCommFunctions.Nx(stNonforfeitureMort, pintBeginAge + 20)) / _
'           poCommFunctions.Dx(stNonforfeitureMort, pintBeginAge))
'      Else
'         dblNFF2Den = ((poCommFunctions.Nx(stNonforfeitureMort, pintBeginAge) - _
'           poCommFunctions.Nx(stNonforfeitureMort, pobjPremium.PremiumCeaseAge)) / _
'           poCommFunctions.Dx(stNonforfeitureMort, pintBeginAge))
'      End If
'      dblNFF2 = dblG + (dblNFF2Num1 - dblNFF2Num2) / dblNFF2Den
'   End If
'   If dblG <= dblF Then
'      dblNFF3 = 1000 * (poCommFunctions.Mx(stNonforfeitureMort, pintBeginAge + 1) - _
'     poCommFunctions.Mx(stNonforfeitureMort, pintEndAge) + _
'     poCommFunctions.Dx(stNonforfeitureMort, pintEndAge) * -1 * blnEndowIndicator) / _
'     (poCommFunctions.Nx(stNonforfeitureMort, pintBeginAge + 1) - _
'     poCommFunctions.Nx(stNonforfeitureMort, pobjPremium.PremiumCeaseAge))
'   Else
'      dblNFF3 = dblG
'   End If
   If m_enuCVMethod = CVMethodReserve Then
      If poIntRates.InterestRateType(penuMortalityType) = 0 Then 'Constant
         sngMyIntRate = poIntRates.InterestRateConstant(penuMortalityType)
      Else
         sngMyIntRate = poIntRates.InterestRateVary(penuMortalityType, intI - pintBeginAge)
      End If
      If penuTiming = stcurtate Then
         dblDBTiming = 1
         dblPremiumTiming = 1
      Else
         dblDBTiming = sngMyIntRate / Delta(sngMyIntRate)
         If penuTiming = stImmediateClaims Or poCommFunctions.Dx(penuMortalityType, intI) = 0 Then
            dblPremiumTiming = 1
         Else
            dblPremiumTiming = poCommFunctions.DBarx(penuMortalityType, intI) / poCommFunctions.Dx(penuMortalityType, intI)
         End If
      End If
      For intI = pintEndAge To pintBeginAge Step -1
         'm_adblAdjustedPremium(intI) = m_adblModifiedNetPremium(intI)
         'm_adblCashValue(intI) = m_adblModifiedReserves(intI)
         'm_adblNSP(intI) = pobjBenefits.DeathBenefit(intI) * dblDBTiming * pobjMortality.FinalMortality(penuMortalityType, intI) / (1 + sngMyIntRate) + (1 - pobjMortality.FinalMortality(penuMortalityType, intI)) * m_adblNSP(intI + 1) / (1 + sngMyIntRate)
         If m_enuModResDefn = stNetLevel Then
            m_adblAdjustedPremium(intI) = pobjReserves.NetAnnualPremiums(pintBeginAge)
         ElseIf m_enuModResDefn = stCRVM Or m_enuModResDefn = stILStandard Then
            m_adblAdjustedPremium(intI) = pobjReserves.ModifiedNetPremium(intI)
         Else
            If Not m_blnCVMessage(0) Then
               MsgBox "Modified (adjusted) premium is not available"
               m_blnCVMessage(0) = True
            End If
         End If
         If intI >= pintBeginAge And intI < pintEndAge Then
            m_adblCashValue(intI) = pobjBenefits.DeathBenefit(intI) * dblDBTiming * pobjMortality.FinalMortality(penuMortalityType, intI) / (1 + sngMyIntRate) + (1 - pobjMortality.FinalMortality(penuMortalityType, intI)) * m_adblCashValue(intI + 1) / (1 + sngMyIntRate)
            If intI < pobjPremium.PremiumCeaseAge Then
               m_adblCashValue(intI) = m_adblCashValue(intI) - m_adblAdjustedPremium(intI) * dblPremiumTiming
            End If
         ElseIf intI = pintEndAge Then
            m_adblCashValue(intI) = pobjBenefits.PVFB(intI)
         End If
      Next intI
   
   
'         For intI = pintEndAge To pintBeginAge Step -1
'            If poIntRates.InterestRateType(penuMortalityType) = 0 Then 'Constant
'               sngMyIntRate = poIntRates.InterestRateConstant(penuMortalityType)
'            Else
'               sngMyIntRate = poIntRates.InterestRateVary(penuMortalityType, intI - pintBeginAge)
'            End If
'            If penuTiming = stcurtate Then
'               dblDBTiming = 1
'               dblPremiumTiming = 1
'            Else
'               If Delta(sngMyIntRate) <> 0 Then
'                  dblDBTiming = sngMyIntRate / Delta(sngMyIntRate)
'               Else
'                  dblDBTiming = 1
'               End If
'               If penuTiming = stImmediateClaims Or poCommFunctions.Dx(penuMortalityType, intI) = 0 Then
'                  dblPremiumTiming = 1
'               Else
'                  dblPremiumTiming = poCommFunctions.DBarx(penuMortalityType, intI) / poCommFunctions.Dx(penuMortalityType, intI)
'               End If
'            End If
'         Next intI
'
   
   ElseIf m_enuCVMethod = CVMethod1980 Then
      m_dblAAI = 0
      If pobjBenefits.DBType = stDBConstant Then
         m_dblAAI = pobjBenefits.DBConstant
      Else
         For intI = 1 To 10
            m_dblAAI = m_dblAAI + pobjBenefits.DeathBenefit(pintBeginAge + intI - 1)
         Next intI
         m_dblAAI = m_dblAAI / 10
      End If
      'pobjReserves.NetAnnualPremiums(pintBeginAge) = pobjBenefits.PVFB(pintBeginAge) / (poCommFunctions.Nx(pintBeginAge) - poCommFunctions.Nx(pintEndAge)) / poCommFunctions.Dx(pintBeginAge)
      '    If pobjBenefits.PVFB(pintBeginAge) / ((poCommFunctions.Nx(pintBeginAge) - poCommFunctions.Nx(pintEndAge)) / poCommFunctions.Dx(pintBeginAge)) < 0.04 * m_dblAAI Then   'Formula 15.2.7, p438
      '        m_dblEA_CV = 1.25 * pobjBenefits.PVFB(pintBeginAge) / ((poCommFunctions.Nx(pintBeginAge) - poCommFunctions.Nx(pintEndAge)) / poCommFunctions.Dx(pintBeginAge)) + 0.01 * m_dblAAI
      '    Else
      '        m_dblEA_CV = 0.06 * m_dblAAI
      '    End If
      If pobjReserves.NetAnnualPremiums(pintBeginAge) < 0.04 * m_dblAAI Then  'Formula 15.2.7, p438
         m_dblEA_CV = 1.25 * pobjReserves.NetAnnualPremiums(pintBeginAge) + 0.01 * m_dblAAI
      Else
         m_dblEA_CV = 0.06 * m_dblAAI
      End If
      If pfUseExpAllow Then
         m_dblRc = (pobjBenefits.PVFB(pintBeginAge) + m_dblEA_CV) / pobjPremium.PVFGP(pintBeginAge)
      Else
         m_dblRc = (pobjBenefits.PVFB(pintBeginAge)) / pobjPremium.PVFGP(pintBeginAge)
      End If
      For intI = pintEndAge To pintBeginAge Step -1
         If poIntRates.InterestRateType(penuMortalityType) = 0 Then 'Constant
            sngMyIntRate = poIntRates.InterestRateConstant(penuMortalityType)
         Else
            sngMyIntRate = poIntRates.InterestRateVary(penuMortalityType, intI - pintBeginAge)
         End If
         If penuTiming = stcurtate Then
            dblDBTiming = 1
            dblPremiumTiming = 1
         Else
            dblDBTiming = sngMyIntRate / Delta(sngMyIntRate)
            If penuTiming = stImmediateClaims Or poCommFunctions.Dx(penuMortalityType, intI) = 0 Then
               dblPremiumTiming = 1
            Else
               dblPremiumTiming = poCommFunctions.DBarx(penuMortalityType, intI) / poCommFunctions.Dx(penuMortalityType, intI)
            End If
         End If
         If intI >= pintBeginAge And intI < pintEndAge Then
            m_adblAdjustedPremium(intI) = m_dblRc * pobjPremium.GrossPremiums(intI - pintBeginAge)
            m_adblCashValue(intI) = pobjBenefits.DeathBenefit(intI) * dblDBTiming * pobjMortality.FinalMortality(penuMortalityType, intI) / (1 + sngMyIntRate) - m_adblAdjustedPremium(intI) * dblPremiumTiming + (1 - pobjMortality.FinalMortality(penuMortalityType, intI)) * m_adblCashValue(intI + 1) / (1 + sngMyIntRate)
            m_adblNSP(intI) = pobjBenefits.DeathBenefit(intI) * dblDBTiming * pobjMortality.FinalMortality(penuMortalityType, intI) / (1 + sngMyIntRate) + (1 - pobjMortality.FinalMortality(penuMortalityType, intI)) * m_adblNSP(intI + 1) / (1 + sngMyIntRate)
         ElseIf intI = pintEndAge Then
            m_adblCashValue(intI) = pobjBenefits.PVFB(intI)
            m_adblNSP(intI) = pobjBenefits.PVFB(intI)
         End If
      Next intI
   ElseIf m_enuCVMethod = CVMethod1941 Then 'Assume that it is a constant premium and death benefit
      For intI = pintEndAge To pintBeginAge Step -1
         If poIntRates.InterestRateType(penuMortalityType) = 0 Then 'Constant
            sngMyIntRate = poIntRates.InterestRateConstant(penuMortalityType)
         Else
            sngMyIntRate = poIntRates.InterestRateVary(penuMortalityType, intI - pintBeginAge)
         End If
         If penuTiming = stcurtate Then
            dblDBTiming = 1
            dblPremiumTiming = 1
         Else
            dblDBTiming = sngMyIntRate / Delta(sngMyIntRate)
            If penuTiming = stImmediateClaims Or poCommFunctions.Dx(penuMortalityType, intI) = 0 Then
               dblPremiumTiming = 1
            Else
               dblPremiumTiming = poCommFunctions.DBarx(penuMortalityType, intI) / poCommFunctions.Dx(penuMortalityType, intI)
            End If
         End If
         'If intI >= pobjPremium.PremiumCeaseAge Then
         '   dblPremiumTiming = 0
         'End If
         If intI = pobjPremium.PremiumCeaseAge Then
            'Adjusted premium calculations found on page 436 of Bowers 1, table 15.2
            dblPVFB = poCommFunctions.Mx(stNonforfeitureMort, pintBeginAge) / poCommFunctions.Dx(stNonforfeitureMort, pintBeginAge)
            dblAnnuity = (poCommFunctions.Nx(stNonforfeitureMort, pintBeginAge)) / poCommFunctions.Dx(stNonforfeitureMort, pintBeginAge)
            dblAdjustPremTest(0) = (dblPVFB + 0.02) / (dblAnnuity - 0.65) 'Line 1 or 2 if plan is WL
            If dblAdjustPremTest(0) > 0.04 Then 'Recalculate
               dblAdjustPremTest(0) = (dblPVFB + 0.046) / dblAnnuity 'Line 2 if plan is WL
            End If
            If penuProdType = ProductTraditionalWL Then '"Whole Life": Line 1 or 2
               m_adblAdjustedPremium(intI) = dblAdjustPremTest(0)
            Else 'Other
               dblPVFB = pobjBenefits.PVFB(pintBeginAge) / pobjBenefits.DBConstant ' pobjBenefits.CalcPVFB(pintBeginAge, pintEndAge, pfUseEndow, poIntRates, pobjMortality, penuTiming, penuMortalityType) / pobjBenefits.DBConstant 'poCommFunctions.Mx / poCommFunctions.Dx
               dblAnnuity = (poCommFunctions.Nx(stNonforfeitureMort, pintBeginAge) - poCommFunctions.Nx(stNonforfeitureMort, pobjPremium.PremiumCeaseAge)) / poCommFunctions.Dx(stNonforfeitureMort, pintBeginAge)
               If dblAdjustPremTest(0) < 0.04 Then
                  dblAdjustPremTest(1) = (dblPVFB + 0.02) / (dblAnnuity - 0.65) 'Line 3 or 4 or 6
                  If dblAdjustPremTest(1) > dblAdjustPremTest(0) Then
                     dblAdjustPremTest(1) = (dblPVFB + 0.02 + 0.25 * dblAdjustPremTest(0)) / (dblAnnuity - 0.4) 'Line 4 or 6
                  End If
                  If dblAdjustPremTest(1) >= 0.04 Then
                     dblAdjustPremTest(1) = (dblPVFB + 0.036 + 0.25 * dblAdjustPremTest(0)) / (dblAnnuity) 'Line 6
                  End If
               Else
                  dblAdjustPremTest(1) = (dblPVFB + 0.02) / (dblAnnuity - 0.65) 'Line 5 or 7
                  If dblAdjustPremTest(1) >= 0.04 Then
                     dblAdjustPremTest(1) = (dblPVFB + 0.046) / (dblAnnuity) 'Line 7
                  End If
               End If
               m_adblAdjustedPremium(intI) = dblAdjustPremTest(1)
            End If
            m_adblAdjustedPremium(intI) = m_adblAdjustedPremium(intI) * pobjBenefits.DBConstant
'            m_adblCashValue(intI) = pobjBenefits.PVFB(intI)
'            m_adblNSP(intI) = pobjBenefits.PVFB(intI)
            m_adblCashValue(intI) = pobjBenefits.DeathBenefit(intI) * dblDBTiming * pobjMortality.FinalMortality(penuMortalityType, intI) / (1 + sngMyIntRate) + (1 - pobjMortality.FinalMortality(penuMortalityType, intI)) * m_adblCashValue(intI + 1) / (1 + sngMyIntRate)
            m_adblNSP(intI) = pobjBenefits.DeathBenefit(intI) * dblDBTiming * pobjMortality.FinalMortality(penuMortalityType, intI) / (1 + sngMyIntRate) + (1 - pobjMortality.FinalMortality(penuMortalityType, intI)) * m_adblNSP(intI + 1) / (1 + sngMyIntRate)
         Else
            If intI > pobjPremium.PremiumCeaseAge Then
               m_adblAdjustedPremium(intI) = 0
            Else
               m_adblAdjustedPremium(intI) = m_adblAdjustedPremium(intI + 1) 'Adjusted premium will be constant for all durations
            End If
            If intI >= pintBeginAge And intI < pintEndAge Then
               m_adblCashValue(intI) = pobjBenefits.DeathBenefit(intI) * dblDBTiming * pobjMortality.FinalMortality(penuMortalityType, intI) / (1 + sngMyIntRate) - m_adblAdjustedPremium(intI) * dblPremiumTiming + (1 - pobjMortality.FinalMortality(penuMortalityType, intI)) * m_adblCashValue(intI + 1) / (1 + sngMyIntRate)
               m_adblNSP(intI) = pobjBenefits.DeathBenefit(intI) * dblDBTiming * pobjMortality.FinalMortality(penuMortalityType, intI) / (1 + sngMyIntRate) + (1 - pobjMortality.FinalMortality(penuMortalityType, intI)) * m_adblNSP(intI + 1) / (1 + sngMyIntRate)
            End If
         End If
      Next intI
   End If
   If m_blnCalcSC Then
      For intI = pintEndAge To pintBeginAge Step -1
         m_adblCashValue(intI) = m_adblCashValue(intI) - m_asngSC(intI - pintBeginAge)
      Next intI
   End If
'   Debug.Print pintBeginAge, m_adblNSP
   
   'TVCodeTools ErrorHandlerStart
PROC_EXIT:
   Exit Sub
   
PROC_ERR:
   Err.Raise Err.Number
   'TVCodeTools ErrorHandlerEnd
   Resume Next
   Resume
End Sub


Public Sub CalcETICashValue(ByVal pintEndAge As Integer, ByVal pintBeginAge As Integer, _
                        ByVal pfUseExpAllow As Boolean, ByVal pfUseEndow As Boolean, _
                        ByVal poIntRates As SaltIntRates, ByVal pobjBenefits As _
                        SaltBenefits, ByVal pobjPremium As SaltPremium, ByVal _
                        poCommFunctions As SaltCommutationFunctions, ByVal _
                        pobjReserves As SaltReserves, ByVal pobjMortality As _
                        SaltMortality, ByVal penuTiming As TimingType, ByVal _
                        penuMortalityType As MortalityType, penuProdType As eProductType)
   ' Comments  :
   ' Parameters: pintEndAge
   '             pintBeginAge
   '             pfUseExpAllow -
   ' Modified  :
   '
   ' --------------------------------------------------
   'CV
   'TVCodeTools ErrorEnablerStart
   On Error GoTo PROC_ERR
   'TVCodeTools ErrorEnablerEnd
   
Dim intI As Integer
Dim sngMyIntRate As Single
Dim m_dblRc As Double
Dim dblDBTiming As Double
Dim dblPremiumTiming As Double
Dim dblAdjustPremTest(1) As Double
Dim dblPVFB As Double
Dim dblAnnuity As Double
Dim dblG As Double 'From Allan's documentation
Dim dblF As Double 'From Allan's documentation
Dim dblNFF1 As Double 'From Allan's documentation
Dim dblNFF2 As Double 'From Allan's documentation
Dim dblNFF3 As Double 'From Allan's documentation
Dim blnEndowIndicator As Boolean
Dim dblNFF2Num1 As Double 'From Allan's documentation
Dim dblNFF2Num2 As Double 'From Allan's documentation
Dim dblNFF2Den As Double 'From Allan's documentation
   
   
      If poIntRates.InterestRateType(penuMortalityType) = 0 Then 'Constant
         sngMyIntRate = poIntRates.InterestRateConstant(penuMortalityType)
      Else
         sngMyIntRate = poIntRates.InterestRateVary(penuMortalityType, intI - pintBeginAge)
      End If
      If penuTiming = stcurtate Then
         dblDBTiming = 1
         dblPremiumTiming = 1
      Else
         dblDBTiming = sngMyIntRate / Delta(sngMyIntRate)
         If penuTiming = stImmediateClaims Or poCommFunctions.Dx(penuMortalityType, intI) = 0 Then
            dblPremiumTiming = 1
         Else
            dblPremiumTiming = poCommFunctions.DBarx(penuMortalityType, intI) / poCommFunctions.Dx(penuMortalityType, intI)
         End If
      End If
      For intI = pintEndAge To pintBeginAge Step -1
         m_adblAdjustedPremium(intI) = 0
         m_adblCashValue(intI) = pobjBenefits.DeathBenefit(intI) * dblDBTiming * (poCommFunctions.Mx(penuMortalityType, pintBeginAge) - poCommFunctions.Mx(penuMortalityType, intI)) / poCommFunctions.Dx(penuMortalityType, pintBeginAge)
         m_adblPureEndowment(intI) = pobjBenefits.DeathBenefit(pintBeginAge) * poCommFunctions.Dx(penuMortalityType, intI) / poCommFunctions.Dx(penuMortalityType, pintBeginAge)
      Next intI
   
'   Debug.Print pintBeginAge, m_adblNSP
   
   'TVCodeTools ErrorHandlerStart
PROC_EXIT:
   Exit Sub
   
PROC_ERR:
   Err.Raise Err.Number
   'TVCodeTools ErrorHandlerEnd
   Resume Next
   Resume
End Sub



Public Property Get CVMethod() As eCVMethod
   CVMethod = m_enuCVMethod
End Property

Public Property Let CVMethod(ByVal vData As eCVMethod)
   m_enuCVMethod = vData
End Property

Public Property Let CalcCV(ByVal vData As Boolean)
   m_blnCalcCV = vData
End Property

Public Property Let CalcSC(ByVal vData As Boolean)
   m_blnCalcSC = vData
End Property


Public Property Get AAI() As Double
   AAI = m_dblAAI
End Property
Public Property Let AAI(ByVal vData As Double)
   m_dblAAI = vData
End Property

Public Property Get AdjustedPremium(ByVal iIndex1 As Integer) As Double
   AdjustedPremium = m_adblAdjustedPremium(iIndex1)
End Property
Public Property Let AdjustedPremium(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblAdjustedPremium(iIndex1) = vData
End Property

Public Property Get CashValue(ByVal iIndex1 As Integer) As Double
   CashValue = m_adblCashValue(iIndex1)
End Property
'Public Property Get SC(ByVal iIndex1 As Integer) As Single
'   SC = m_adblSC(iIndex1)
'End Property

Public Property Get PureEndowment(ByVal iIndex1 As Integer) As Double
   PureEndowment = m_adblPureEndowment(iIndex1)
End Property

Public Property Let CashValue(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblCashValue(iIndex1) = vData
End Property
Public Property Let SC(ByVal iIndex1 As Integer, ByVal vData As Single)
   m_asngSC(iIndex1) = vData
End Property

Public Property Let PureEndowment(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblPureEndowment(iIndex1) = vData
End Property

Public Property Get EA_CV() As Double
   EA_CV = m_dblEA_CV
End Property
Public Property Let EA_CV(ByVal vData As Double)
   m_dblEA_CV = vData
End Property
