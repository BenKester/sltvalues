VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltCommutationFunctions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private m_adblCx(3, 120) As Double
Private m_adblDx(3, 120) As Double
Private m_adblLx(3, 120) As Double
Private m_adblMx(3, 120) As Double
Private m_adblNx(3, 120) As Double
Private m_adblPx(3, 120) As Double
Private m_adblRx(3, 120) As Double
Private m_adblSx(3, 120) As Double
Private m_adblVx(3, 120) As Double
Private m_adblCBarx(3, 120) As Double
Private m_adblDBarx(3, 120) As Double
Private m_adblMBarx(3, 120) As Double
Private m_adblNBarx(3, 120) As Double
Private m_adblRBarx(3, 120) As Double
Private m_adblDeathx(3, 120) As Double
Private m_adblNxMthly(3, 120) As Double

Private m_objTRCommon As SaltTRCommon
Public Property Get CBarx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer) As Double
   CBarx = m_adblCBarx(iIndicator, iIndex1)
End Property
Public Property Let CBarx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblCBarx(iIndicator, iIndex1) = vData
End Property

Public Property Get Cx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer) As Double
   Cx = m_adblCx(iIndicator, iIndex1)
End Property
Public Property Let Cx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblCx(iIndicator, iIndex1) = vData
End Property

Public Property Get DBarx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer) As Double
   DBarx = m_adblDBarx(iIndicator, iIndex1)
End Property
Public Property Let DBarx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblDBarx(iIndicator, iIndex1) = vData
End Property

Public Property Get Deathx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer) As Double
   Deathx = m_adblDeathx(iIndicator, iIndex1)
End Property
Public Property Let Deathx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblDeathx(iIndicator, iIndex1) = vData
End Property

Public Property Get Dx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer) As Double
   Dx = m_adblDx(iIndicator, iIndex1)
End Property
Public Property Let Dx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblDx(iIndicator, iIndex1) = vData
End Property

Public Property Get lx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer) As Double
   lx = m_adblLx(iIndicator, iIndex1)
End Property
Public Property Let lx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblLx(iIndicator, iIndex1) = vData
End Property

Public Property Get MBarx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer) As Double
   MBarx = m_adblMBarx(iIndicator, iIndex1)
End Property
Public Property Let MBarx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblMBarx(iIndicator, iIndex1) = vData
End Property

Public Property Get Mx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer) As Double
   Mx = m_adblMx(iIndicator, iIndex1)
End Property
Public Property Let Mx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblMx(iIndicator, iIndex1) = vData
End Property

Public Property Get NBarx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer) As Double
   NBarx = m_adblNBarx(iIndicator, iIndex1)
End Property
Public Property Let NBarx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblNBarx(iIndicator, iIndex1) = vData
End Property

Public Property Get Nx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer) As Double
   Nx = m_adblNx(iIndicator, iIndex1)
End Property
Public Property Let Nx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblNx(iIndicator, iIndex1) = vData
End Property

Public Property Get NxMthly(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer) As Double
   NxMthly = m_adblNxMthly(iIndicator, iIndex1)
End Property
Public Property Let NxMthly(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblNxMthly(iIndicator, iIndex1) = vData
End Property

Public Property Get px(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer) As Double
   px = m_adblPx(iIndicator, iIndex1)
End Property
Public Property Let px(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblPx(iIndicator, iIndex1) = vData
End Property

Public Property Get RBarx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer) As Double
   RBarx = m_adblRBarx(iIndicator, iIndex1)
End Property
Public Property Let RBarx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblRBarx(iIndicator, iIndex1) = vData
End Property

Public Property Get Rx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer) As Double
   Rx = m_adblRx(iIndicator, iIndex1)
End Property
Public Property Let Rx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblRx(iIndicator, iIndex1) = vData
End Property

Public Property Get sx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer) As Double
   sx = m_adblSx(iIndicator, iIndex1)
End Property
Public Property Let sx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblSx(iIndicator, iIndex1) = vData
End Property

Public Property Get TRCommon() As SaltTRCommon
   Set TRCommon = m_objTRCommon
End Property
Public Property Set TRCommon(poTRCommon As SaltTRCommon)
   Set m_objTRCommon = poTRCommon
End Property

Public Property Get vx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer) As Double
   vx = m_adblVx(iIndicator, iIndex1)
End Property
Public Property Let vx(ByVal iIndicator As MortalityType, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblVx(iIndicator, iIndex1) = vData
End Property
Public Sub CalcCommFunctions(ByVal pintBeginAge As Integer, ByVal pintEndAge As _
                              Integer, ByVal pobjIntRates As SaltIntRates, ByVal _
                              pobjMortality As SaltMortality, ByVal plngMortalityType _
                              As MortalityType)

Dim dblMyDiscount As Double
Dim sngMyIntRate As Single
Dim intI As Integer

   m_adblLx(plngMortalityType, pintBeginAge) = 10000000
   m_adblDx(plngMortalityType, pintBeginAge) = m_adblLx(plngMortalityType, pintBeginAge)
   dblMyDiscount = 1
   
   For intI = pintBeginAge To pintEndAge
      If pobjIntRates.InterestRateType(plngMortalityType) = 0 Then 'Constant
         dblMyDiscount = dblMyDiscount / (1 + pobjIntRates.InterestRateConstant(plngMortalityType))
         sngMyIntRate = pobjIntRates.InterestRateConstant(plngMortalityType)
      Else
         If pobjIntRates.InterestRateVary(plngMortalityType, intI - pintBeginAge) = 0 Then
            pobjIntRates.InterestRateVary(plngMortalityType, intI - pintBeginAge) = pobjIntRates.InterestRateVary(plngMortalityType, intI - pintBeginAge - 1)
         End If
         dblMyDiscount = dblMyDiscount / (1 + pobjIntRates.InterestRateVary(plngMortalityType, intI - pintBeginAge))
         sngMyIntRate = pobjIntRates.InterestRateVary(plngMortalityType, intI - pintBeginAge)
      End If
      m_adblLx(plngMortalityType, intI + 1) = m_adblLx(plngMortalityType, intI) * (1 - pobjMortality.FinalMortality(plngMortalityType, intI))
      m_adblDx(plngMortalityType, intI + 1) = m_adblLx(plngMortalityType, intI + 1) * dblMyDiscount
      m_adblDBarx(plngMortalityType, intI) = Alpha(CDbl(sngMyIntRate)) * m_adblDx(plngMortalityType, intI) - Beta(CDbl(sngMyIntRate)) * (m_adblDx(plngMortalityType, intI) - m_adblDx(plngMortalityType, intI + 1))
      m_adblCx(plngMortalityType, intI) = m_adblDx(plngMortalityType, intI) * pobjMortality.FinalMortality(plngMortalityType, intI) / (1 + sngMyIntRate)
      m_adblCBarx(plngMortalityType, intI) = m_adblCx(plngMortalityType, intI) * sngMyIntRate / Delta(sngMyIntRate)
   Next intI
   m_adblMBarx(plngMortalityType, pintEndAge + 1) = 0
   m_adblMx(plngMortalityType, pintEndAge + 1) = 0
   m_adblNBarx(plngMortalityType, pintEndAge + 1) = 0
   m_adblNx(plngMortalityType, pintEndAge + 1) = 0
   
   For intI = pintEndAge To pintBeginAge Step -1
      If pobjIntRates.InterestRateType(plngMortalityType) = 0 Then 'Constant
         sngMyIntRate = pobjIntRates.InterestRateConstant(plngMortalityType)
      Else
         sngMyIntRate = pobjIntRates.InterestRateVary(plngMortalityType, intI - pintBeginAge)
      End If
      m_adblMx(plngMortalityType, intI) = m_adblMx(plngMortalityType, intI + 1) + m_adblCx(plngMortalityType, intI)
      m_adblMBarx(plngMortalityType, intI) = m_adblMBarx(plngMortalityType, intI + 1) + m_adblCBarx(plngMortalityType, intI)
      m_adblNx(plngMortalityType, intI) = m_adblNx(plngMortalityType, intI + 1) + m_adblDx(plngMortalityType, intI)
      m_adblNBarx(plngMortalityType, intI) = m_adblNBarx(plngMortalityType, intI + 1) + m_adblDBarx(plngMortalityType, intI)
   Next intI
   
End Sub
