VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MessageBox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'Icon enum
#If False Then
   Dim icoCritical, icoQuestion, icoExclamation, icoInformation
#End If
Public Enum stEnumMBIcon
   icoCritcal = 16
   icoQuestion = 32
   icoExclamation = 48
   icoInformation = 64
End Enum

'Button choice enum
#If False Then
   Dim btnOKOnly, btnOKCancel, btnOKAbortRetryIgnore, btnYesNoCancel, btnYesNo
   Dim btnRetryCancel
#End If
Public Enum stEnumMBButton
   btnOKOnly = 0
   btnOKCancel = 1
   btnOKAbortRetryIgnore = 2
   btnYesNoCancel = 3
   btnYesNo = 4
   btnRetryCancel = 5
End Enum

'Default Button enum
#If False Then
   Dim dfButton1, dfButton2, dfButton3, dfButton4
#End If
Public Enum stEnumMBDefaultButton
   dfButton1 = 0
   dfButton2 = 256
   dfButton3 = 512
   dfButton4 = 768
End Enum

'Modal enum
#If False Then
   Dim mbApplicationModal, mbSystemModal
#End If
Public Enum stEnumMBModal
   mbApplicationModal = 0
   mbSystemModal = 4096
End Enum

'Response enum
#If False Then
   Dim mbReturnNone, mbReturnOK, mbReturnCancel, mbReturnAbort, mbReturnIgnore
   Dim mbReturnYes, mbReturnNo
#End If
Public Enum stEnumMBResponse
   mbReturnNone = 0
   mbReturnOK = 1
   mbReturnCancel = 2
   mbReturnAbort = 3
   mbReturnRetry = 4
   mbReturnIgnore = 5
   mbReturnYes = 6
   mbReturnNo = 7
End Enum

'Private variables to hold property values
Private m_enuIcon As stEnumMBIcon
Private m_enuButtons As stEnumMBButton
Private m_enuDefaultButton As stEnumMBDefaultButton
Private m_enuModal As stEnumMBModal
Private m_enuResponse As stEnumMBResponse
Private m_strMessage As String
Private m_strErrMessage As String
Private m_strTitle As String
Private m_lngHelpContextID As Long
Private m_strHelpFile As String
Public Property Get Buttons() As stEnumMBButton
   Buttons = m_enuButtons
End Property
Public Property Let Buttons(Buttons As stEnumMBButton)
   m_enuButtons = Buttons
End Property

Private Sub Class_Initialize()
On Error GoTo Err_Class_Initialize

   m_enuIcon = icoExclamation
   m_enuButtons = btnOKOnly
   m_enuDefaultButton = dfButton1
   m_enuModal = mbApplicationModal
   m_enuResponse = mbReturnNone
   m_strErrMessage = ""
   m_strTitle = App.EXEName
   m_strHelpFile = ""
   m_lngHelpContextID = 0
   
Exit_Class_Initialize:
   Exit Sub
   
Err_Class_Initialize:
   MsgBox Err.Description
   Resume Exit_Class_Initialize
End Sub

Public Property Get DefaultButton() As stEnumMBDefaultButton
   DefaultButton = m_enuDefaultButton
End Property
Public Property Let DefaultButton(DefaultButton As stEnumMBDefaultButton)
   m_enuDefaultButton = DefaultButton
End Property

Public Property Get ErrMessage() As String
   ErrMessage = m_strErrMessage
End Property
Public Property Let ErrMessage(ErrMessage As String)
   m_strErrMessage = ErrMessage
End Property

Public Property Get HelpContextID() As Long
  HelpContextID = m_lngHelpContextID
End Property

Public Property Let HelpContextID(HelpContextID As Long)
  m_lngHelpContextID = HelpContextID
End Property

Public Property Get HelpFile() As String
  HelpFile = m_strHelpFile
End Property

Public Property Let HelpFile(HelpFile As String)
  m_strHelpFile = HelpFile
End Property

Public Property Get Icon() As stEnumMBIcon
   Icon = m_enuIcon
End Property
Public Property Let Icon(Icon As stEnumMBIcon)
   m_enuIcon = Icon
End Property

Public Property Get Message() As String
   Message = m_strMessage
End Property
Public Property Let Message(Message As String)
   m_strMessage = Message
End Property

Public Property Get Modal() As stEnumMBModal
   Modal = m_enuModal
End Property
Public Property Let Modal(Modal As stEnumMBModal)
   m_enuModal = Modal
End Property
Public Property Get Response() As stEnumMBResponse
   Response = m_enuResponse
End Property
Public Sub RestoreDefaults()
   Call Class_Initialize
End Sub

Public Function Show(Optional ByVal Message As String = "", Optional ByVal Icon As _
                     stEnumMBIcon, Optional ByVal fError As Boolean = True) As stEnumMBResponse
Dim lngOptions As Long, ErrNumber As Long, ErrDescription As String
   
   If fError Then    'We want to get the error information
      ErrDescription = Err.Description
      ErrNumber = Err.Number
   End If
   
On Error GoTo Err_Show
   
'   If IsMissing(Icon) Then
'      Icon = m_enuIcon
'   End If
   Show = mbReturnNone
   If Message = "" Then
      Message = m_strMessage
   End If
   
   lngOptions = Icon + m_enuDefaultButton + m_enuModal + m_enuButtons
   
   If fError Then
      Message = Message & vbNewLine & vbNewLine & _
                "There has been an error in the program." & vbNewLine & _
                vbTab & "Details:" & vbNewLine & _
                vbTab & vbTab & "Error Number:  " & ErrNumber & vbNewLine & _
                vbTab & vbTab & "Error Description:  " & ErrDescription & vbNewLine & _
                vbTab & vbTab & "What to do:  " & m_strErrMessage
   Else
      If Trim(Message) <> "" Then
         Message = Message & ".  " & m_strErrMessage
      End If
   End If
      
   If m_strHelpFile = "" Then
      m_enuResponse = MsgBox(Message, lngOptions, m_strTitle)
   Else
      m_enuResponse = MsgBox(Message, lngOptions, m_strTitle, m_strHelpFile, m_lngHelpContextID)
   End If
   Show = m_enuResponse
   
Exit_Show:
   Exit Function
   
Err_Show:
   MsgBox "Unexpected error." & m_strErrMessage
   Resume Exit_Show
      
End Function

Public Property Get Title() As String
   Title = m_strTitle
End Property
Public Property Let Title(Title As String)
   m_strTitle = Title
End Property

