VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltTRCommon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

   
   
   Private mvarPrintSummary As Boolean
   Private mvarPrintDetail As Boolean
   Private m_VarTiming As TimingType 'local copy
   Private m_VarCVTiming As TimingType
   'Local objects to hold property values to reference other classes
   Private m_oBenefits As SaltBenefits
   Private m_oCommFunctions As SaltCommutationFunctions
   Private m_oCV As SaltCV
   Private m_oDueDeferred As SaltDueDeferred
   Private m_oInsured As SaltInsured
   Private m_oIntRates As SaltIntRates
   Private m_oMortality As SaltMortality
   Private m_oPolicy As SaltPolicy
   Private m_oPremium As SaltPremium
   Private m_oReserves As SaltReserves
   Private m_oSegmentation As SaltSegmentation
   Private m_oIntRate As SaltIntRates
   Private m_oSaltInsuredValues As SaltInsured
   Private m_oSaltBenefitsValues As SaltBenefits
   Private m_oSaltReserves As SaltReserves
   Private m_oSaltCV As SaltCV
   Private m_oSaltCommFns As SaltCommutationFunctions
   Private m_oSaltSegments As SaltSegmentation
   Private m_oSaltDueDef As SaltDueDeferred
   Private m_oSaltPremiumValues As SaltPremium
   Private m_oSaltMortality As SaltMortality
   
   Public Enum MortalityType
      stNonforfeitureMort = 0
      stBasicMort1_2Cx = 1
      stBasicMort = 2
      stMinimumMort = 3
   End Enum
   Public Enum enuTerminalIndicator
      stCashValue = 0
      stBasicUnitary = 1
      stBasicSegmented = 2
      stBasicMax = 3
      stMinimumUnitary = 4
      stMinimumSegmented = 5
      stMinimumMax = 6
   End Enum
   Public Enum TimingType
      stcurtate
      stImmediateClaims
      stContinuous
   End Enum
   Public Enum eRound
      stRoundNearInteger = 0
      stRoundNearCent = 1
      stRoundLowInteger = 2
      stRoundHighInteger = 3
      stRoundNone = 4
   End Enum
   #If False Then
      Dim pfGivenFace, pfGivenPremium
   #End If
   Public Enum ePremiumFace
      pfGivenFace = 0
      pfGivenPremium = 1
   End Enum
' LookupDescription enumeration
   #If False Then
      Dim LookupDesc, LookupClient, LookupDatabase, LookupTableType, LookupUnits
      Dim LookupDivOption, LookupProdType, LookupGender, LookupUnderwriting
      Dim LookupTobacco, LookupState, LookupMode, LookupFormat, LookupValueDefn
      Dim LookupProduct, LookupLives, LookupResValMethod, LookupCVValMethod
      Dim LookupTiming, LookupStatus, LookupPolicyLoanCode, LookupBillingType
      Dim LookupNewspaperCode, LookupProductType, LookupTransactionTypes
      Dim LookupMonths, LookupRiderPlan, LookupNewIssue, LookupUserTypes
      Dim LookupTermType, LookupTransFrame, LookupMiscTrans, LookupPayComm
      Dim LookupYesNo, LookupRound
   #End If
   Public Enum eLookupDescription
      LookupDesc = 0
      LookupClient = 1
      LookupDatabase = 2
      LookupTableType = 3
      LookupUnits = 4
      LookupDivOption = 5
      LookupProdType = 6
      LookupGender = 7
      LookupUnderwriting = 8
      LookupTobacco = 9
      LookupState = 10
      LookupMode = 11
      LookupFormat = 12
      LookupValueDefn = 13
      LookupRider = 14
      LookupReportType = 15
      LookupReportNameBase = 16
      LookupProduct = 17
      LookupLives = 18
      LookupResValMethod = 19
      LookupCVValMethod = 20
      LookupTiming = 21
      LookupStatus = 22
      LookupPolicyLoanCode = 23
      LookupBillingType = 24
      LookupNewspaperCode = 25
      LookupProductType = 26
      LookupTransactionTypes = 27
      LookupMonths = 28
      LookupRiderPlan = 29
      LookupNewIssue = 30
      LookupUserTypes = 31
      LookupTermType = 32
      LookupTransFrame = 33
      LookupMiscTrans = 34
      LookupPayComm = 35
      LookupYesNo = 36
      LookupRound = 37
   End Enum

' Client enumeration
   #If False Then
      Dim ClientRoyalArcanum, ClientSaltTest
   #End If
   Public Enum eClient
      ClientRoyalArcanum = 1
      ClientSaltTest = 2
   End Enum

' TableType enumeration
   #If False Then
      Dim TableConstant, TableAgeDur, TableAttainedAge, TableIssueAge, TableDur
      Dim TableSelectUltimate, TableNone, TableFaceAmount, TableTwoAges, TableToMaturity
   #End If
   Public Enum eTableType
      TableConstant = 0
      TableAgeDur = 1
      TableAttainedAge = 2
      TableIssueAge = 3
      TableDur = 4
      TableSelectUltimate = 5
      TableNone = 6
      TableFaceAmount = 7
      TableTwoAges = 8
      TableToMaturity = 9
   End Enum

' TableUnitDefinition enumeration
   #If False Then
      Dim UnitDuration, UnitAge
   #End If
   Public Enum eTableUnitDefinition
      UnitDuration = 0
      UnitAge = 1
   End Enum

' DividendOptions enumeration
   #If False Then
      Dim DivCash, DivAccum, DivPUA, DivReducePrem
   #End If
   Public Enum eDividendOptions
      DivCash = 0
      DivAccum = 1
      DivReducePrem = 2
      DivPUA = 3
   End Enum

' Gender enumeration
   #If False Then
      Dim GenderUnisex, GenderMale, GenderFemale
   #End If
   Public Enum eGenderConstants
      GenderUnisex = 2
      GenderMale = 0
      GenderFemale = 1
   End Enum

' Underwriting enumeration
   #If False Then
      Dim UndNa, UndStandard, UndSubStandard
   #End If
   Public Enum eUnderwriting
      UndNa = 0
      UndStandard = 1
      UndSubStandard = 2
   End Enum

' Tobacco enumeration
   #If False Then
      Dim TobNA, TobUnismoke, TobTobacco, TobNonTobacco
   #End If
   Public Enum eTobacco
      TobNA = 0
      TobUnismoke = 1
      TobTobacco = 2
      TobNonTobacco = 3
   End Enum

' State enumeration
   ' This enumeration is not currently being used anywhere in the system
   #If False Then
      Dim StateAlabama, StateAlaska, StateArizona, StateArkansas, StateCalifornia
      Dim StateColorado, StateConnecticut, StateDeleware, StateDistrictOfColumbia
      Dim StateFlorida, StateGeorgia, StateHawaii, StateIdaho, StateIllinios
      Dim StateIndiana, StateIowa, StateKansas, StateKentucky, StateLouisiana
      Dim StateMaine, StateMaryland, StateMassachusetts, StateMichigan, StateMinnesota
      Dim StateMississippi, StateMissouri, StateMontana, StateNebraska, StateNevada
      Dim StateNewHampshire, StateNewJersey, StateNewMexico, StateNewYork
      Dim StateNorthCarolina, StateNorthDakota, StateOhio, StateOklahoma, StateOregon
      Dim StatePennsylvania, StateRhodeIsland, StateSouthCarolina, StateSouthDakota
      Dim StateTennessee, StateTexas, StateUtah, StateVermont, StateVirginia
      Dim StateWashington, StateWestVirginia, StateWisconsin, StateWyoming
      Dim StateAmericanSamoa, StateGuam, StatePuertoRico, StateUSVirginIslands
      Dim StateCanada
   #End If
   Public Enum eStates
      StateAlabama = 1
      StateAlaska = 2
      StateArizona = 3
      StateArkansas = 4
      StateCalifornia = 5
      StateColorado = 6
      StateConnecticut = 7
      StateDeleware = 8
      StateDistrictOfColumbia = 9
      StateFlorida = 10
      StateGeorgia = 11
      StateHawaii = 12
      StateIdaho = 13
      StateIllinios = 14
      StateIndiana = 15
      StateIowa = 16
      StateKansas = 17
      StateKentucky = 18
      StateLouisiana = 19
      StateMaine = 20
      StateMaryland = 21
      StateMassachusetts = 22
      StateMichigan = 23
      StateMinnesota = 24
      StateMississippi = 25
      StateMissouri = 26
      StateMontana = 27
      StateNebraska = 28
      StateNevada = 29
      StateNewHampshire = 30
      StateNewJersey = 31
      StateNewMexico = 32
      StateNewYork = 33
      StateNorthCarolina = 34
      StateNorthDakota = 35
      StateOhio = 36
      StateOklahoma = 37
      StateOregon = 38
      StatePennsylvania = 39
      StateRhodeIsland = 40
      StateSouthCarolina = 41
      StateSouthDakota = 42
      StateTennessee = 43
      StateTexas = 44
      StateUtah = 45
      StateVermont = 46
      StateVirginia = 47
      StateWashington = 48
      StateWestVirginia = 49
      StateWisconsin = 50
      StateWyoming = 51
      StateAmericanSamoa = 52
      StateGuam = 53
      StatePuertoRico = 54
      StateUSVirginIslands = 55
      StateCanada = 56
   End Enum

' Mode enumeration
   #If False Then
      Dim ModeNA, ModeAnnual, ModeSemiAnnual, ModeQuarterly, ModeMonthly
      Dim ModeSinglePremium
   #End If
   Public Enum eMode
      ModeNA = 0
      ModeAnnual = 1
      ModeSemiAnnual = 2
      ModeQuarterly = 3
      ModeMonthly = 4
      ModeSinglePremium = 5
   End Enum

' RateFormat enumeration
   ' This enumeration currently is not being used anywhere in the system
   #If False Then
      Dim RateGUT, RateGU, RateGT, RateUT, RateG, RateU, RateT, RateZ
   #End If
   Public Enum eRateFormat
      RateGUT = 1
      RateGU = 2
      RateGT = 3
      RateUT = 4
      RateG = 5
      RateU = 6
      RateT = 7
      RateZ = 8
   End Enum

' ValueDefinition enumeration
   #If False Then
      Dim ValueBasePrem1, ValueBasePrem2, ValueUltimatePrem, ValueWaiverPrem
      Dim ValueADBPrem, ValuePayorPrem, ValueGIRPrem, ValueDeathBenefit
      Dim ValueDividend, ValueGtdCV, ValueTermTo25
      Dim ValueReserve, ValueEndowment
   #End If
   Public Enum eValueDefinition
      ValueBasePrem1 = 1
      ValueBasePrem2 = 2
      ValueUltimatePrem = 3
      ValueWaiverPrem = 4
      ValueADBPrem = 5
      ValuePayorPrem = 6
      ValueGIRPrem = 7
      ValueDeathBenefit = 8
      ValueDividend = 9
      ValueGtdCV = 10
      ValueTermTo25 = 11
      ValueReserve = 12
      ValueEndowment = 13
   End Enum
   
   
   'Rider enumeration
   #If False Then
      Dim RiderADB, RiderWP, RiderPayor, RiderGIPO, RiderTermTo25
   #End If
   Public Enum eRider
      RiderADB = 1
      RiderWP = 2
      RiderGIPO = 3
      RiderPayor = 4
      RiderTermTo25 = 5
   End Enum
   
   'Report Type enumeration
   #If False Then
      Dim ReportCoverPage, ReportTrad, ReportTerm, ReportAnnuity, ReportGradedBenefit, ReportRider
      Dim ReportCoverGB, ReportCoverFS, ReportFS, ReportCoverFE, ReportFE, ReportT25, ReportSPWL
   #End If
   Public Enum eReportType
      ReportCoverPage = 0
      ReportTrad = 1
      ReportTerm = 2
      ReportAnnuity = 3
      ReportGradedBenefit = 4
      ReportRider = 5
      ReportCoverGB = 6
      ReportCoverFS = 7
      ReportFS = 8
      ReportCoverFE = 9
      ReportFE = 10
      ReportT25 = 11
      ReportSPWL = 12
   End Enum
   
   'Report Name Base enumeration
   #If False Then
      Dim ReportNameCover, ReportNameExplanation, ReportNameDefinition, ReportNamePremium, ReportNameDividends, ReportNameDetail, ReportNameSummary
   #End If
   Public Enum eReportNameBase
      ReportNameCover = 0
      ReportNameExplanation = 1
      ReportNameDefinition = 2
      ReportNamePremium = 3
      ReportNameDividends = 4
      ReportNameDetail = 5
      ReportNameSummary = 6
   End Enum
   
' Reserve Method enumeration
   #If False Then
      Dim ResMethodNetLevel
      Dim ResMethodCRVM
      Dim ResMethodIL
      Dim ResMethodNJ
      Dim ResMethodImport
      Dim ResMethodSC
   #End If
   Public Enum eResMethod
      ResMethodNetLevel = 0
      ResMethodCRVM = 1
      ResMethodIL = 2
      ResMethodNJ = 3
      ResMethodImport = 4
      ResMethodSC = 5
   End Enum
' Cash Value Method enumeration
   #If False Then
      Dim CVMethod1980
      Dim CVMethod1941
      Dim CVMethodReserve
      Dim CVMethodImport
   #End If
   Public Enum eCVMethod
      CVMethod1980 = 0
      CVMethod1941 = 1
      CVMethodReserve = 2
      CVMethodImport = 3
   End Enum

' Timing
   #If False Then
      Dim TimingCurtate
      Dim TimingSemiCont
      Dim TimingContinuous
   #End If
   Public Enum eTiming
      TimingCurtate = 0
      TimingSemiCont = 1
      TimingContinuous = 2
   End Enum
   
' ProductType enumeration
   #If False Then
      Dim ProductTraditionalWL, ProductTraditionalOther, ProductTerm, ProductAnnuity
   #End If
   Public Enum eProductType
      ProductTraditionalWL = 0
      ProductTraditionalOther = 1
      ProductTerm = 2
      ProductAnnuity = 3
   End Enum

' eUserTypes enumeration
   #If False Then
      Dim userSA, userCommon, userReadOnly
   #End If
   Public Enum eUserTypes
      userSA = 0
      userCommon = 1
      userReadOnly = 2
   End Enum
   

' Public Events
   Public Event ReportGenerator(ByVal penuMortalityType As MortalityType, ByVal _
            penuTerminalType As enuTerminalIndicator, _
            ByVal pfDetail As Boolean, ByVal pfSummary As Boolean, _
            ByVal poSegmentation As SaltSegmentation, ByVal poReserves _
            As SaltReserves, ByVal poMortality As SaltMortality, _
            ByVal poInsured As SaltInsured, ByVal poBenefits As _
            SaltBenefits, ByVal poCommFunctions As _
            SaltCommutationFunctions, ByVal poCV As SaltCV, ByVal _
            poPremium As SaltPremium)
Public Property Set Segmentation(poSegmentation As SaltSegmentation)
   
   Set m_oSegmentation = poSegmentation
   
End Property
Public Property Get Segmentation() As SaltSegmentation
   
   Set Segmentation = m_oSegmentation
   
End Property

Public Property Set Reserves(poReserves As SaltReserves)
   
   Set m_oReserves = poReserves
   
End Property
Public Property Get Reserves() As SaltReserves
   
   Set Reserves = m_oReserves
   
End Property

Public Property Set Premium(poPremium As SaltPremium)
   
   Set m_oPremium = poPremium
   
End Property
Public Property Get Premium() As SaltPremium
   
   Set Premium = m_oPremium
   
End Property

Public Property Set Policy(poPolicy As SaltPolicy)
   
   Set m_oPolicy = poPolicy
   
End Property
Public Property Get Policy() As SaltPolicy
   
   Set Policy = m_oPolicy
   
End Property

Public Property Set Mortality(poMortality As SaltMortality)
   
   Set m_oMortality = poMortality
   
End Property
Public Property Get Mortality() As SaltMortality
   
   Set Mortality = m_oMortality
   
End Property

Public Property Set IntRates(poIntRates As SaltIntRates)
   
   Set m_oIntRates = poIntRates
   
End Property
Public Property Get IntRates() As SaltIntRates
   
   Set IntRates = m_oIntRates
   
End Property

Public Property Set Insured(poInsured As SaltInsured)
   
   Set m_oInsured = poInsured
   
End Property
Public Property Get Insured() As SaltInsured
   
   Set Insured = m_oInsured
   
End Property

Public Property Set DueDeferred(poDueDeferred As SaltDueDeferred)
   
   Set m_oDueDeferred = poDueDeferred
   
End Property
Public Property Get DueDeferred() As SaltDueDeferred
   
   Set DueDeferred = m_oDueDeferred
   
End Property

Public Property Set CV(poCV As SaltCV)
   
   Set m_oCV = poCV
   
End Property
Public Property Get CV() As SaltCV
   
   Set CV = m_oCV
   
End Property

Public Property Set CommFunctions(poCommFunctions As SaltCommutationFunctions)
   
   Set m_oCommFunctions = poCommFunctions
   
End Property
Public Property Get CommFunctions() As SaltCommutationFunctions
   
   Set CommFunctions = m_oCommFunctions
   
End Property

Public Property Set Benefits(poBenefits As SaltBenefits)
   
   Set m_oBenefits = poBenefits
   
End Property
Public Property Get Benefits() As SaltBenefits
   
   Set Benefits = m_oBenefits
   
End Property

Public Property Get Timing() As Integer
    ' Comments  :
    ' Parameters:  -
    ' Returns   : Integer
    ' Modified  :
    '
    ' --------------------------------------------------
    'used when retrieving value of a property, on the right side of an assignment.
    'Syntax: Debug.Print X.Timing
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Timing = m_VarTiming
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Property
    
PROC_ERR:
    Err.Raise Err.Number
    'TVCodeTools ErrorHandlerEnd
    
End Property

Public Property Get CVTiming() As Integer
    ' Comments  :
    ' Parameters:  -
    ' Returns   : Integer
    ' Modified  :
    '
    ' --------------------------------------------------
    'used when retrieving value of a property, on the right side of an assignment.
    'Syntax: Debug.Print X.Timing
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    CVTiming = m_VarCVTiming
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Property
    
PROC_ERR:
    Err.Raise Err.Number
    'TVCodeTools ErrorHandlerEnd
    
End Property


Public Property Let Timing(ByVal vData As Integer)
    ' Comments  :
    ' Parameters: vData -
    ' Modified  :
    '
    ' --------------------------------------------------
    'used when assigning a value to the property, on the left side of an assignment.
    'Syntax: X.Timing = 5
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    m_VarTiming = vData
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Property
    
PROC_ERR:
    Err.Raise Err.Number
    'TVCodeTools ErrorHandlerEnd
    
End Property

Public Property Let CVTiming(ByVal vData As Integer)
    ' Comments  :
    ' Parameters: vData -
    ' Modified  :
    '
    ' --------------------------------------------------
    'used when assigning a value to the property, on the left side of an assignment.
    'Syntax: X.Timing = 5
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    m_VarCVTiming = vData
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Property
    
PROC_ERR:
    Err.Raise Err.Number
    'TVCodeTools ErrorHandlerEnd
    
End Property


Public Sub CalcFunctions()
    ' Comments  :
    ' Parameters:  -
    ' Modified  :
    '
    ' --------------------------------------------------
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
Dim intEndAge As Integer, enuMortType As MortalityType
Dim fExpAllow As Boolean, enuSegmentType As SegmentType
Dim enuTerminalType As enuTerminalIndicator, fCalcTerminals As Boolean
Dim intEndType As Integer, mintsegcnt As Integer
   
   'items that only need to be done once
   For enuSegmentType = 0 To 1 'Change ending parameter to a 1 if going to do a segmentation calculation (e.g. XXX)
      If m_oReserves.ReserveMethod = stUnitaryMethod And enuSegmentType = stSegmented Then Exit For 'Don't need to do segmented reserves if only unitary is requested
      Call m_oSegmentation.CalcSegments(m_oInsured.IssueAge, m_oMortality, m_oPremium, m_oBenefits, enuSegmentType)
   Next enuSegmentType
   m_oPremium.PremiumCeaseAge = gGlobal.MIN(m_oPremium.PremiumCeaseAge, m_oBenefits.BenefitCeaseAge)
   Call m_oBenefits.CalcDB(m_oInsured.IssueAge)
   
   'Items that repeat for each terminal calculation
   If m_oReserves.ReserveMethodType = ResMethodImport Then
      intEndType = stCashValue
   Else
      intEndType = stMinimumMax
   End If
   For enuTerminalType = stCashValue To intEndType
      If enuTerminalType = stCashValue Then
         enuMortType = stNonforfeitureMort
         enuSegmentType = stUnitary
         If m_oCV.CalcCV Then
            fCalcTerminals = True
         Else
            fCalcTerminals = False
         End If
         fExpAllow = True
      ElseIf enuTerminalType = stBasicUnitary Then
         enuMortType = stBasicMort
         enuSegmentType = stUnitary
         fCalcTerminals = True
      ElseIf enuTerminalType = stBasicSegmented Then
         enuMortType = stBasicMort
         If m_oReserves.ReserveMethod <> stUnitaryMethod Then
            fCalcTerminals = True
         Else
            fCalcTerminals = False
         End If
         enuSegmentType = stSegmented
      ElseIf enuTerminalType = stBasicMax Then
         fCalcTerminals = False
      ElseIf enuTerminalType = stMinimumUnitary Then
         enuMortType = stMinimumMort
         enuSegmentType = stUnitary
         If m_oReserves.ReserveMethod <> stUnitaryMethod Then
            fCalcTerminals = True
         Else
            fCalcTerminals = False
         End If
      ElseIf enuTerminalType = stMinimumSegmented Then
         enuMortType = stMinimumMort
         enuSegmentType = stSegmented
         If m_oReserves.ReserveMethod <> stUnitaryMethod Then
            fCalcTerminals = True
         Else
            fCalcTerminals = False
         End If
      ElseIf enuTerminalType = stMinimumMax Then
         fCalcTerminals = False
      End If
      If fCalcTerminals Then
         intEndAge = m_oMortality.CalcMortality(m_oInsured.IssueAge, m_oInsured.Lives, enuMortType, enuTerminalType, m_oReserves.ReserveMethod, m_oSegmentation, enuSegmentType) 'returns the end of the mortality table
         
         ' m_intVarPremiumCeaseAge = MIN(m_intVarPremiumCeaseAge, m_oBenefits.BenefitCeaseAge)
         'Call m_m_oBenefits.CalcDB
         
         If m_oReserves.ReserveMethod = stSegmentation Or m_oReserves.ReserveMethod = stNY147 Or m_oReserves.ReserveMethod = stXXX1999 Then
             Call m_oCommFunctions.CalcCommFunctions(m_oInsured.IssueAge, intEndAge, m_oIntRates, m_oMortality, enuMortType)
             If m_oCV.CalcCV And enuTerminalType = stCashValue Then
               Call m_oPremium.CalcPVFGrossPrems(m_oBenefits.BenefitCeaseAge, m_oInsured.IssueAge, m_oIntRates, m_oMortality, m_VarCVTiming, enuMortType)
               Call m_oBenefits.CalcPVFB(m_oInsured.IssueAge, m_oBenefits.BenefitCeaseAge, True, m_oIntRates, m_oMortality, m_VarCVTiming, enuMortType)
               Call m_oReserves.CalcPremiums(m_oInsured.IssueAge, m_oBenefits.BenefitCeaseAge, m_oInsured.IssueAge, fExpAllow, True, m_oBenefits, m_oPremium, m_oCommFunctions, m_VarCVTiming, enuMortType)
               'Call m_oReserves.CalcReserve(False, m_oSegmentation.SegmentsGroup(enuSegmentType, mintsegcnt), m_oSegmentation.SegmentsGroup(enuSegmentType, mintsegcnt + 1), fExpAllow, True, m_oMortality, m_oIntRates, m_oBenefits, m_oPremium, m_oCommFunctions, m_VarCVTiming, enuMortType, enuTerminalType)
               If m_oInsured.ETI Then
                  Call m_oCV.CalcETICashValue(m_oBenefits.BenefitCeaseAge, m_oInsured.IssueAge, fExpAllow, True, m_oIntRates, m_oBenefits, m_oPremium, m_oCommFunctions, m_oReserves, m_oMortality, m_VarCVTiming, enuMortType, m_oPolicy.ProductType)
               Else
                  Call m_oCV.CalcCashValue(m_oBenefits.BenefitCeaseAge, m_oInsured.IssueAge, fExpAllow, True, m_oIntRates, m_oBenefits, m_oPremium, m_oCommFunctions, m_oReserves, m_oMortality, m_VarCVTiming, enuMortType, m_oPolicy.ProductType)
               End If
             End If
               
             For mintsegcnt = 1 To m_oSegmentation.SegmentCount(enuSegmentType)
                 If m_oSegmentation.SegmentsGroup(enuSegmentType, mintsegcnt) = m_oBenefits.BenefitCeaseAge Then
                     Call m_oBenefits.CalcPVFB(m_oSegmentation.SegmentsGroup(enuSegmentType, mintsegcnt + 1), m_oSegmentation.SegmentsGroup(enuSegmentType, mintsegcnt), True, m_oIntRates, m_oMortality, m_VarTiming, enuMortType)
                 Else
                     Call m_oBenefits.CalcPVFB(m_oSegmentation.SegmentsGroup(enuSegmentType, mintsegcnt + 1), m_oSegmentation.SegmentsGroup(enuSegmentType, mintsegcnt), False, m_oIntRates, m_oMortality, m_VarTiming, enuMortType)
                 End If
                 Call m_oPremium.CalcPVFGrossPrems(m_oSegmentation.SegmentsGroup(enuSegmentType, mintsegcnt), m_oSegmentation.SegmentsGroup(enuSegmentType, mintsegcnt + 1), m_oIntRates, m_oMortality, m_VarTiming, enuMortType)
                 If mintsegcnt = m_oSegmentation.SegmentCount(enuSegmentType) Then
                     fExpAllow = True
                 Else
                     fExpAllow = False
                 End If
                 If mintsegcnt = 1 Then
                     Call m_oReserves.CalcPremiums(m_oInsured.IssueAge, m_oSegmentation.SegmentsGroup(enuSegmentType, mintsegcnt), m_oSegmentation.SegmentsGroup(enuSegmentType, mintsegcnt + 1), fExpAllow, True, m_oBenefits, m_oPremium, m_oCommFunctions, m_VarTiming, enuMortType)
                 Else
                     Call m_oReserves.CalcPremiums(m_oInsured.IssueAge, m_oSegmentation.SegmentsGroup(enuSegmentType, mintsegcnt), m_oSegmentation.SegmentsGroup(enuSegmentType, mintsegcnt + 1), fExpAllow, False, m_oBenefits, m_oPremium, m_oCommFunctions, m_VarTiming, enuMortType)
                 End If
                 If enuTerminalType = stCashValue Then
                    'Already calculated the CV
                    'Call m_oCV.CalcCashValue(m_oBenefits.BenefitCeaseAge, m_oInsured.IssueAge, fExpAllow, True, m_oIntRates, m_oBenefits, m_oPremium, m_oCommFunctions, m_oReserves, m_oMortality, m_VarTiming, enuMortType, m_oPolicy.ProductType)
                 Else
                    If m_oInsured.ETI Then
                       Call m_oReserves.CalcETIReserve(False, m_oSegmentation.SegmentsGroup(enuSegmentType, mintsegcnt), m_oSegmentation.SegmentsGroup(enuSegmentType, mintsegcnt + 1), fExpAllow, True, m_oMortality, m_oIntRates, m_oBenefits, m_oPremium, m_oCommFunctions, m_VarTiming, enuMortType, enuTerminalType)
                    Else
                       Call m_oReserves.CalcReserve(False, m_oSegmentation.SegmentsGroup(enuSegmentType, mintsegcnt), m_oSegmentation.SegmentsGroup(enuSegmentType, mintsegcnt + 1), fExpAllow, True, m_oMortality, m_oIntRates, m_oBenefits, m_oPremium, m_oCommFunctions, m_VarTiming, enuMortType, enuTerminalType)
                    End If
                 End If
             Next mintsegcnt
         Else
             fExpAllow = True
             
             Call m_oCommFunctions.CalcCommFunctions(m_oInsured.IssueAge, intEndAge, m_oIntRates, m_oMortality, enuMortType)
             If enuTerminalType = stCashValue Then
                Call m_oBenefits.CalcPVFB(m_oInsured.IssueAge, m_oBenefits.BenefitCeaseAge, True, m_oIntRates, m_oMortality, m_VarTiming, enuMortType)
             Else
                Call m_oBenefits.CalcPVFB(m_oInsured.IssueAge, m_oBenefits.BenefitCeaseAge, True, m_oIntRates, m_oMortality, m_VarTiming, enuMortType)
             End If
             Call m_oPremium.CalcPVFGrossPrems(m_oBenefits.BenefitCeaseAge, m_oInsured.IssueAge, m_oIntRates, m_oMortality, m_VarTiming, enuMortType)
             Call m_oReserves.CalcPremiums(m_oInsured.IssueAge, m_oBenefits.BenefitCeaseAge, m_oInsured.IssueAge, fExpAllow, True, m_oBenefits, m_oPremium, m_oCommFunctions, m_VarTiming, enuMortType)
             If enuTerminalType = stCashValue Then
               If m_oInsured.ETI Then
                  Call m_oCV.CalcETICashValue(m_oBenefits.BenefitCeaseAge, m_oInsured.IssueAge, fExpAllow, True, m_oIntRates, m_oBenefits, m_oPremium, m_oCommFunctions, m_oReserves, m_oMortality, m_VarCVTiming, enuMortType, m_oPolicy.ProductType)
               Else
                  Call m_oCV.CalcCashValue(m_oBenefits.BenefitCeaseAge, m_oInsured.IssueAge, fExpAllow, True, m_oIntRates, m_oBenefits, m_oPremium, m_oCommFunctions, m_oReserves, m_oMortality, m_VarCVTiming, enuMortType, m_oPolicy.ProductType)
               End If
             Else
                If m_oInsured.ETI Then
                   Call m_oReserves.CalcETIReserve(False, m_oBenefits.BenefitCeaseAge, m_oInsured.IssueAge, fExpAllow, True, m_oMortality, m_oIntRates, m_oBenefits, m_oPremium, m_oCommFunctions, m_VarTiming, enuMortType, enuTerminalType)
                Else
                   Call m_oReserves.CalcReserve(False, m_oBenefits.BenefitCeaseAge, m_oInsured.IssueAge, fExpAllow, True, m_oMortality, m_oIntRates, m_oBenefits, m_oPremium, m_oCommFunctions, m_VarTiming, enuMortType, enuTerminalType)
                End If
             End If
         End If
         If enuTerminalType <> stCashValue Then
            Call m_oReserves.CalcMidYearReserve(m_oBenefits.BenefitCeaseAge, m_oInsured.IssueAge, enuTerminalType)
         End If
         'If mvarPrintSummary Or mvarPrintDetail And enuTerminalType > stCashValue Then
         If mvarPrintSummary Or mvarPrintDetail Then
            RaiseEvent ReportGenerator(enuMortType, enuTerminalType, _
               mvarPrintDetail, mvarPrintSummary, m_oSegmentation, _
               m_oReserves, m_oMortality, m_oInsured, m_oBenefits, _
               m_oCommFunctions, m_oCV, m_oPremium)
         End If
      End If
      If enuTerminalType = stBasicMax Or enuTerminalType = stMinimumMax Then
         Call m_oReserves.CalcMaxTerminals(m_oBenefits.BenefitCeaseAge, m_oInsured.IssueAge, enuTerminalType)
      End If
      If m_oCV.CalcCV Then Exit For
   Next enuTerminalType
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    gobjMessage.Show "CalcFunctions"
    'MsgBox (Err.Description & ".  Error number " & Err.Number)
    'TVCodeTools ErrorHandlerEnd
    
End Sub
Public Property Let PrintDetail(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PrintDetail = 5
    mvarPrintDetail = vData
End Property


Public Property Let PrintSummary(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PrintDetail = 5
    mvarPrintSummary = vData
End Property



Public Property Get PrintDetail() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PrintDetail
    PrintDetail = mvarPrintDetail
End Property

Public Property Get PrintSummary() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PrintDetail
    PrintSummary = mvarPrintSummary
End Property


Public Property Get IntRateValues() As SaltIntRates
On Error GoTo ErrRtn:
   Set IntRateValues = m_oIntRate
   Exit Property
ErrRtn:
   'Err.Raise Err.Number, App.EXEName + ".IACCalculate.Initialize", Err.Description
   Err.Raise Err.Number, App.EXEName, Err.Description
End Property

Public Property Get SaltInsuredValues() As SaltInsured
On Error GoTo ErrRtn:
   Set SaltInsuredValues = m_oSaltInsuredValues
   Exit Property
ErrRtn:
   'Err.Raise Err.Number, App.EXEName + ".IACCalculate.Initialize", Err.Description
   Err.Raise Err.Number, App.EXEName, Err.Description
End Property


Public Property Get SaltBenefitsValues() As SaltBenefits
On Error GoTo ErrRtn:
   Set SaltBenefitsValues = m_oSaltBenefitsValues
   Exit Property
ErrRtn:
   'Err.Raise Err.Number, App.EXEName + ".IACCalculate.Initialize", Err.Description
   Err.Raise Err.Number, App.EXEName, Err.Description
End Property



Public Property Get SaltReserveValues() As SaltReserves
On Error GoTo ErrRtn:
   Set SaltReserveValues = m_oSaltReserves
   Exit Property
ErrRtn:
   'Err.Raise Err.Number, App.EXEName + ".IACCalculate.Initialize", Err.Description
   Err.Raise Err.Number, App.EXEName, Err.Description
End Property




Public Property Get SaltCVValues() As SaltCV
On Error GoTo ErrRtn:
   Set SaltCVValues = m_oSaltCV
   Exit Property
ErrRtn:
   'Err.Raise Err.Number, App.EXEName + ".IACCalculate.Initialize", Err.Description
   Err.Raise Err.Number, App.EXEName, Err.Description
End Property

Public Property Get SaltCommutationFunctionsValues() As SaltCommutationFunctions
On Error GoTo ErrRtn:
   Set SaltCommutationFunctionsValues = m_oSaltCommFns
   Exit Property
ErrRtn:
   'Err.Raise Err.Number, App.EXEName + ".IACCalculate.Initialize", Err.Description
   Err.Raise Err.Number, App.EXEName, Err.Description
End Property


Public Property Get SaltSegmentationValues() As SaltSegmentation
On Error GoTo ErrRtn:
   Set SaltSegmentationValues = m_oSaltSegments
   Exit Property
ErrRtn:
   'Err.Raise Err.Number, App.EXEName + ".IACCalculate.Initialize", Err.Description
   Err.Raise Err.Number, App.EXEName, Err.Description
End Property


Public Property Get SaltDueDeferredValues() As SaltDueDeferred
On Error GoTo ErrRtn:
   Set SaltDueDeferredValues = m_oSaltDueDef
   Exit Property
ErrRtn:
   'Err.Raise Err.Number, App.EXEName + ".IACCalculate.Initialize", Err.Description
   Err.Raise Err.Number, App.EXEName, Err.Description
End Property

Public Property Get SaltIntRatesValues() As SaltIntRates
On Error GoTo ErrRtn:
   Set SaltIntRatesValues = m_oIntRate
   Exit Property
ErrRtn:
   'Err.Raise Err.Number, App.EXEName + ".IACCalculate.Initialize", Err.Description
   Err.Raise Err.Number, App.EXEName, Err.Description
End Property





Public Property Get SaltPremiumValues() As SaltPremium
On Error GoTo ErrRtn:
   Set SaltPremiumValues = m_oSaltPremiumValues
   Exit Property
ErrRtn:
   'Err.Raise Err.Number, App.EXEName + ".IACCalculate.Initialize", Err.Description
   Err.Raise Err.Number, App.EXEName, Err.Description
End Property




Public Property Get SaltMortalityValues() As SaltMortality
On Error GoTo ErrRtn:
   Set SaltMortalityValues = m_oSaltMortality
   Exit Property
ErrRtn:
   'Err.Raise Err.Number, App.EXEName + ".IACCalculate.Initialize", Err.Description
   Err.Raise Err.Number, App.EXEName, Err.Description
End Property





Private Sub Class_Initialize()

   m_VarTiming = stcurtate
   m_VarCVTiming = stcurtate
   Set m_oBenefits = New SaltBenefits
   Set m_oCommFunctions = New SaltCommutationFunctions
   
   Set m_oCV = New SaltCV
   Set m_oDueDeferred = New SaltDueDeferred
   Set m_oInsured = New SaltInsured
   Set m_oIntRates = New SaltIntRates
   Set m_oMortality = New SaltMortality
   Set m_oPolicy = New SaltPolicy
   Set m_oPremium = New SaltPremium
   Set m_oReserves = New SaltReserves
   Set m_oSegmentation = New SaltSegmentation
   'Set m_oTRCommon = New SaltTRCommon
   
End Sub
