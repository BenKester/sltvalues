VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltBenefits"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variables to hold property values
Private m_adblDeathBenefit(120) As Double
Private m_adblPVFB(120) As Double
Private m_adblPVFB_NoEndow(120) As Double
Private m_adblPVFB_PE(120) As Double
Private m_dblDBConstant As Double
Private m_dblEndowAmount As Double
Private m_intBenefitCeaseAge As Integer
Private m_lngDBType As DBTypeConstants
Private m_objTRCommon As SaltTRCommon

'local variables
Private madblDb(120) As Double

'Public enums
#If False Then
   Dim stDBConstant, stDBVary
#End If
Public Enum DBTypeConstants
   stDBConstant = 0
   stDBVary = 1
End Enum
Public Property Get TRCommon() As SaltTRCommon
   Set TRCommon = m_objTRCommon
End Property
Public Property Set TRCommon(pobjTRCommon As SaltTRCommon)
   Set m_objTRCommon = pobjTRCommon
End Property

Public Property Get DBConstant() As Double
   DBConstant = m_dblDBConstant
End Property
Public Property Let DBConstant(ByVal pdblDBConstant As Double)
   m_dblDBConstant = pdblDBConstant
End Property

Public Property Get DBType() As DBTypeConstants
   DBType = m_lngDBType
End Property
Public Property Let DBType(ByVal plngDBType As DBTypeConstants)
   m_lngDBType = plngDBType
End Property

Public Property Get DeathBenefit(ByVal iIndex1 As Integer) As Double
   DeathBenefit = m_adblDeathBenefit(iIndex1)
End Property
Public Property Let DeathBenefit(ByVal iIndex1 As Integer, ByVal vData As Double)
    m_adblDeathBenefit(iIndex1) = vData
End Property

Public Property Get EndowAmount() As Double
   EndowAmount = m_dblEndowAmount
End Property
Public Property Let EndowAmount(ByVal vData As Double)
   m_dblEndowAmount = vData
End Property

Public Property Get PVFB(ByVal iIndex1 As Integer) As Double
   PVFB = m_adblPVFB(iIndex1)
End Property
Public Property Get PVFB_NoEndow(ByVal iIndex1 As Integer) As Double
   PVFB_NoEndow = m_adblPVFB_NoEndow(iIndex1)
End Property

Public Property Get PVFB_PE(ByVal iIndex1 As Integer) As Double
   PVFB_PE = m_adblPVFB_PE(iIndex1)
End Property


Public Property Let PVFB(ByVal iIndex1 As Integer, ByVal vData As Double)
    m_adblPVFB(iIndex1) = vData
End Property

Public Property Let PVFB_NoEndow(ByVal iIndex1 As Integer, ByVal vData As Double)
    m_adblPVFB_NoEndow(iIndex1) = vData
End Property


Public Property Let PVFB_PE(ByVal iIndex1 As Integer, ByVal vData As Double)
    m_adblPVFB_PE(iIndex1) = vData
End Property



Public Property Get BenefitCeaseAge() As Integer
    BenefitCeaseAge = m_intBenefitCeaseAge
End Property
Public Property Let BenefitCeaseAge(ByVal pintBenefitCeaceAge As Integer)
   m_intBenefitCeaseAge = pintBenefitCeaceAge
End Property

Public Sub CalcDB(pintIssueAge As Integer)

Dim intI As Integer

   If m_lngDBType = stDBConstant Then
      For intI = pintIssueAge To m_intBenefitCeaseAge
         madblDb(intI) = m_dblDBConstant
      Next intI
   Else
      For intI = pintIssueAge To m_intBenefitCeaseAge
         madblDb(intI) = m_adblDeathBenefit(intI)
      Next intI
   End If
    
End Sub
Public Sub CalcPVFB(ByVal pintBeginAge As Integer, ByVal pintEndAge As Integer, ByVal _
                     pfUseEndow As Boolean, ByVal pobjIntRates As SaltIntRates, _
                     ByVal pobjMortality As SaltMortality, ByVal pintTiming As TimingType, _
                     ByVal plngMortalityType As MortalityType)

Dim intI As Integer, sngMyIntRate As Single

   If pfUseEndow Then
      m_dblEndowAmount = m_dblEndowAmount
   Else
      m_dblEndowAmount = 0
   End If
   
   'mvarPVFB(pintEndAge + 1) = 0
   For intI = pintEndAge To pintBeginAge Step -1
      If pobjIntRates.InterestRateType(plngMortalityType) = 0 Then 'Constant
         sngMyIntRate = pobjIntRates.InterestRateConstant(plngMortalityType)
      Else
         sngMyIntRate = pobjIntRates.InterestRateVary(plngMortalityType, intI - pintBeginAge)
      End If
      
      If intI < pintEndAge Then
         m_adblPVFB_PE(intI) = ((1 - pobjMortality.FinalMortality(plngMortalityType, intI)) * m_adblPVFB_PE(intI + 1)) / (1 + sngMyIntRate)
         If pintTiming = stcurtate Then
            m_adblPVFB(intI) = (madblDb(intI) * pobjMortality.FinalMortality(plngMortalityType, intI) + ((1 - pobjMortality.FinalMortality(plngMortalityType, intI)) * m_adblPVFB(intI + 1))) / (1 + sngMyIntRate)
            m_adblPVFB_NoEndow(intI) = (madblDb(intI) * pobjMortality.FinalMortality(plngMortalityType, intI) + ((1 - pobjMortality.FinalMortality(plngMortalityType, intI)) * m_adblPVFB_NoEndow(intI + 1))) / (1 + sngMyIntRate)
         Else
            m_adblPVFB(intI) = (madblDb(intI) * pobjMortality.FinalMortality(plngMortalityType, intI) * (sngMyIntRate / Delta(sngMyIntRate)) + ((1 - pobjMortality.FinalMortality(plngMortalityType, intI)) * m_adblPVFB(intI + 1))) / (1 + sngMyIntRate)
            m_adblPVFB_NoEndow(intI) = (madblDb(intI) * pobjMortality.FinalMortality(plngMortalityType, intI) * (sngMyIntRate / Delta(sngMyIntRate)) + ((1 - pobjMortality.FinalMortality(plngMortalityType, intI)) * m_adblPVFB_NoEndow(intI + 1))) / (1 + sngMyIntRate)
         End If
       '  If m_adblPVFB(intI) > m_adblPVFB(intI + 1) Then Stop
      ElseIf intI = pintEndAge Then
         'mvarPVFB(i) = mvarPVFB(i) + (1 - mvarFinalMortality(i)) * mvarEndowAmount / (1 + MyIntRate)
         m_adblPVFB(intI) = m_dblEndowAmount
         m_adblPVFB_NoEndow(intI) = 0
         m_adblPVFB_PE(intI) = 1000
      End If
   Next intI
    
End Sub
