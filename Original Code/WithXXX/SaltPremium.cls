VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltPremium"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'I am doing this so that the enums will keep their correct case.
#If False Then
   Dim stGPConstant, stGPVary
#End If
Public Enum GPType
   stGPConstant = 0
   stGPVary = 1
End Enum

Private m_adblGrossPremiums(120) As Double
Private m_intPremiumCeaseAge As Integer
Private m_lngGrossPremiumType As GPType
Private m_adblPVFGP(120) As Double
Private m_objTRCommon As SaltTRCommon
Private m_adblAx(120) As Double
Private m_adbBarx(120) As Double
Public Property Get TRCommon() As SaltTRCommon
   Set TRCommon = m_objTRCommon
End Property
Public Property Set TRCommon(poTRCommon As SaltTRCommon)
   Set m_objTRCommon = poTRCommon
End Property

Public Property Get GrossPremiums(ByVal iIndex1 As Integer) As Double
   GrossPremiums = m_adblGrossPremiums(iIndex1)
End Property
Public Property Let GrossPremiums(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblGrossPremiums(iIndex1) = vData
End Property

Public Property Get GrossPremiumType() As Integer
   GrossPremiumType = m_lngGrossPremiumType
End Property
Public Property Let GrossPremiumType(ByVal vData As Integer)
   m_lngGrossPremiumType = vData
End Property

Public Property Get aBarx(ByVal iIndex1 As Integer) As Double
   aBarx = m_adbBarx(iIndex1)
End Property
Public Property Let aBarx(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adbBarx(iIndex1) = vData
End Property

Public Property Get ax(ByVal iIndex1 As Integer) As Double
   ax = m_adblAx(iIndex1)
End Property
Public Property Let ax(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblAx(iIndex1) = vData
End Property

Public Property Get PremiumCeaseAge() As Integer
   PremiumCeaseAge = m_intPremiumCeaseAge
End Property
Public Property Let PremiumCeaseAge(ByVal vData As Integer)
   m_intPremiumCeaseAge = vData
End Property

Public Property Get PVFGP(ByVal iIndex1 As Integer) As Double
   PVFGP = m_adblPVFGP(iIndex1)
End Property
Public Property Let PVFGP(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblPVFGP(iIndex1) = vData
End Property

Public Sub CalcPVFGrossPrems(ByVal pintEndAge As Integer, ByVal pintBeginAge As _
                              Integer, ByVal pobjIntRates As SaltIntRates, ByVal _
                              pobjMortality As SaltMortality, ByVal plngTiming As _
                              TimingType, ByVal plngMortalityType As MortalityType)
   
Dim intI As Integer
Dim intEndAge As Integer
Dim dblPremiumTiming As Double, dblInterestRate As Double, dblDBTiming As Double
   
   For intI = pintEndAge - 1 To pintBeginAge Step -1
      If pobjIntRates.InterestRateType(plngMortalityType) = 0 Then  'Constant
         dblInterestRate = pobjIntRates.InterestRateConstant(plngMortalityType)
      Else
         dblInterestRate = pobjIntRates.InterestRateVary(plngMortalityType, intI - pintBeginAge)
      End If
      If plngTiming = stCurtate Then
         dblDBTiming = 1
         dblPremiumTiming = 1 'See formula 5.8.3 (first two terms on the right side)
      Else
         dblDBTiming = dblInterestRate / Delta(dblInterestRate)
         If plngTiming = stImmediateClaims Then
            dblPremiumTiming = 1 'See formula 5.8.3 (first two terms on the right side)
         Else 'continuous
            dblPremiumTiming = (1 - 1 / (1 + dblInterestRate)) / Delta(dblInterestRate) - Beta(CDbl(dblInterestRate)) * pobjMortality.FinalMortality(plngMortalityType, intI) / (1 + dblInterestRate) 'See formula 5.8.3 (first two terms on the right side)
         End If
      End If
      If intI < m_intPremiumCeaseAge Then
         'Both formulas follow 5.8.4 of Actuarial Mathematics on p209 1st Edition
         If intI = pintEndAge - 1 Then
            m_adblAx(intI) = 1 * dblPremiumTiming
            m_adblPVFGP(intI) = m_adblGrossPremiums(intI - pintBeginAge) * dblPremiumTiming
         Else
            m_adblAx(intI) = 1 * dblPremiumTiming + ((1 - pobjMortality.FinalMortality(plngMortalityType, intI)) * m_adblAx(intI + 1)) / (1 + dblInterestRate)
            m_adblPVFGP(intI) = m_adblGrossPremiums(intI - pintBeginAge) * dblPremiumTiming + ((1 - pobjMortality.FinalMortality(plngMortalityType, intI)) * m_adblPVFGP(intI + 1)) / (1 + dblInterestRate)
         End If
      Else
         m_adblAx(intI) = 0
         m_adblPVFGP(intI) = 0
      End If
'      If intI = 65 Then Stop
      'Debug.Print m_adblPVFGP(intI)
   Next intI
   
End Sub
