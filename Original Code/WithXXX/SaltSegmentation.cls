VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltSegmentation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Public Enum SegmentDefn
   stNoChange = 0
   stSubtract1Pct = 1
   stMultiplyBy_99 = 2
   stAdd1Pct = 3
   stMultiplyBy1_01 = 4
End Enum
Public Enum SegmentType
   stUnitary = 0
   stSegmented = 1
End Enum
Private m_VarSegmentDefn As SegmentDefn 'local copy
Private m_adblVarGPRatios(120) As Double 'local copy
Private m_adblVarMortRatios(120) As Double 'local copy
Private m_aintVarSegments(1, 120) As Integer 'local copy
Private m_aintIntSegmentGroup(1, 120) As Integer
Private m_intSegmentCount(1) As Integer
Private m_oTRCommon As SaltTRCommon

Public Property Get TRCommon() As SaltTRCommon
   Set TRCommon = m_oTRCommon
End Property
Public Property Set TRCommon(poTRCommon As SaltTRCommon)
   Set m_oTRCommon = poTRCommon
End Property

Public Sub CalcSegments(pintBeginAge As Integer, poMortality As SaltMortality, poPremium As SaltPremium, poBenefits As SaltBenefits, penuSegmentType As SegmentType)

Dim intI As Integer
   
   'For intI = m_intVarIssueAge To m_intVarBenefitCeaseAge - 2
   If penuSegmentType = stUnitary Then
      
       m_intSegmentCount(penuSegmentType) = 1
       m_aintIntSegmentGroup(penuSegmentType, m_intSegmentCount(penuSegmentType)) = poBenefits.BenefitCeaseAge
       m_aintVarSegments(penuSegmentType, poBenefits.BenefitCeaseAge) = poBenefits.BenefitCeaseAge
       For intI = poBenefits.BenefitCeaseAge - 1 To pintBeginAge Step -1
            m_aintVarSegments(penuSegmentType, intI) = m_aintVarSegments(penuSegmentType, intI + 1)
       Next intI
   Else
      For intI = pintBeginAge To poBenefits.BenefitCeaseAge - 2
         If intI >= poPremium.PremiumCeaseAge - 1 Then
            m_adblVarGPRatios(intI) = 0
         Else
            m_adblVarGPRatios(intI) = (poPremium.GrossPremiums(intI - pintBeginAge + 1) / poBenefits.DeathBenefit(intI + 1)) / (poPremium.GrossPremiums(intI - pintBeginAge) / poBenefits.DeathBenefit(intI))
         End If
         'Will need to change the mortality to be consistent w/ XXX
         'pomortality.MortalityRate(i,i)
         m_adblVarMortRatios(intI) = poMortality.MortalityRate(penuSegmentType, intI + 1) / poMortality.MortalityRate(penuSegmentType, intI)
         If m_VarSegmentDefn = stAdd1Pct Then
            m_adblVarMortRatios(intI) = m_adblVarMortRatios(intI) + 0.01
         ElseIf m_VarSegmentDefn = stMultiplyBy1_01 Then
            m_adblVarMortRatios(intI) = m_adblVarMortRatios(intI) * 1.01
         ElseIf m_VarSegmentDefn = stSubtract1Pct Then
            m_adblVarMortRatios(intI) = m_adblVarMortRatios(intI) - 0.01
         ElseIf m_VarSegmentDefn = stMultiplyBy_99 Then
            m_adblVarMortRatios(intI) = m_adblVarMortRatios(intI) * 0.99
         End If
      Next intI
   
     
      m_intSegmentCount(penuSegmentType) = 1
      m_aintIntSegmentGroup(penuSegmentType, m_intSegmentCount(penuSegmentType)) = poBenefits.BenefitCeaseAge
      m_aintVarSegments(penuSegmentType, poBenefits.BenefitCeaseAge) = poBenefits.BenefitCeaseAge
      
      For intI = poBenefits.BenefitCeaseAge - 1 To pintBeginAge Step -1
         If m_adblVarGPRatios(intI) > m_adblVarMortRatios(intI) Then
            m_intSegmentCount(penuSegmentType) = m_intSegmentCount(penuSegmentType) + 1
            m_aintIntSegmentGroup(penuSegmentType, m_intSegmentCount(penuSegmentType)) = intI + 1
            m_aintVarSegments(penuSegmentType, intI) = intI + 1
         Else
            m_aintVarSegments(penuSegmentType, intI) = m_aintVarSegments(penuSegmentType, intI + 1)
         End If
      Next intI
   End If
   m_aintIntSegmentGroup(penuSegmentType, m_intSegmentCount(penuSegmentType) + 1) = pintBeginAge
   
End Sub
Public Property Get GPRatios(ByVal iIndex1 As Integer) As Double
   GPRatios = m_adblVarGPRatios(iIndex1)
End Property
Public Property Let GPRatios(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblVarGPRatios(iIndex1) = vData
End Property

Public Property Get MortRatios(ByVal iIndex1 As Integer) As Double
   MortRatios = m_adblVarMortRatios(iIndex1)
End Property
Public Property Let MortRatios(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblVarMortRatios(iIndex1) = vData
End Property

Public Property Let SegmentDefn(ByVal vData As Integer)
   m_VarSegmentDefn = vData
End Property
Public Property Get SegmentDefn() As Integer
   SegmentDefn = m_VarSegmentDefn
End Property

Public Property Get SegmentCount(ByVal iIndicator As SegmentType) As Integer
   SegmentCount = m_intSegmentCount(iIndicator)
End Property

Public Property Get Segments(ByVal iIndicator As SegmentType, ByVal iIndex1 As Integer) As Integer
   Segments = m_aintVarSegments(iIndicator, iIndex1)
End Property
Public Property Let Segments(ByVal iIndicator As SegmentType, ByVal iIndex1 As Integer, ByVal vData As Integer)
   m_aintVarSegments(iIndicator, iIndex1) = vData
End Property

Public Property Get SegmentsGroup(ByVal iIndicator As SegmentType, ByVal iIndex1 As Integer) As Integer
   SegmentsGroup = m_aintIntSegmentGroup(iIndicator, iIndex1)
End Property
Public Property Let SegmentsGroup(ByVal iIndicator As SegmentType, ByVal iIndex1 As Integer, ByVal vData As Integer)
   m_aintIntSegmentGroup(iIndicator, iIndex1) = vData
End Property
