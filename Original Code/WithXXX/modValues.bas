Attribute VB_Name = "modValues"
Option Explicit

Public gobjMessage As New MessageBox
Public gerrHandler As New CErrHandler
Public gSaltTRCommon As New SaltTRCommon
Public gGlobal As New CGlobal
Public m_enuModResDefn As ModResType

Public Function Alpha(ByVal pdblIntRate As Double, Optional ByVal pintMode As Integer) As Double

'5.6.11, p142
Dim dblD As Double, dblIUpperM As Double, dblDUpperM As Double, dblV As Double
   
   dblV = v(pdblIntRate)
   dblD = 1 - dblV
   
   If Delta(pdblIntRate) <> 0 Then
      If pintMode >= 1 Then 'modal
         dblIUpperM = IUpperM(pdblIntRate, pintMode)
         dblDUpperM = DUpperM(pdblIntRate, pintMode)
         Alpha = pdblIntRate * dblD / (dblIUpperM * dblDUpperM)
      Else
         Alpha = pdblIntRate * dblD / (Delta(pdblIntRate) ^ 2)
      End If
   Else
      Alpha = 1
   End If

End Function
Private Sub DueDefPremium(pdatValDate, pdatPaidToDate, pdatIssueDate, pdblGrossPrem, pdblNetPremiumUsed, pintMode)

Dim intPremCount As Integer, datNextAnniv As Date, intCount As Integer
Dim fDeferredNow As Boolean, intDueCount As Integer, intDeferredCount As Integer

   datNextAnniv = pdatIssueDate
   Do Until datNextAnniv > pdatValDate
      datNextAnniv = DateAdd("yyyy", 1, datNextAnniv)
   Loop
   intPremCount = 0
   intDueCount = 0
   fDeferredNow = (pdatPaidToDate >= pdatValDate)
   'If pdatPaidToDate >= pdatValDate Then
   '   fDeferredNow = True
   'Else
   '   fDeferredNow = False
   'End If
   For intCount = 0 To 11 Step pintMode
      'pdatPaidToDate is the "anchor date"
      If DateAdd("m", intCount, pdatPaidToDate) < datNextAnniv Then
         If Not fDeferredNow Then
            If DateAdd("m", intCount, pdatPaidToDate) >= pdatValDate Then
               intDueCount = intPremCount
               fDeferredNow = True
            End If
         End If
         intPremCount = intPremCount + 1
      End If
   Next intCount
   If fDeferredNow Then
      intDeferredCount = intPremCount - intDueCount
   Else
      intDeferredCount = 0
      intDueCount = intPremCount
   End If
   'FinalDueCalc = intDueCount * pdblNetPremiumUsed * pintMode / 12 '/ 100
   'FinalDefCalc = intDeferredCount * pdblNetPremiumUsed * pintMode / 12 ' / 100
   'FinalDueGross = intDueCount * pdblGrossPrem / 100
   'FinalDefGross = intDeferredCount * pdblGrossPrem / 100
   
End Sub
Public Function RoundFn(ByVal pdblInputVariable As Double, ByVal pintDecimalPlaces As _
                        Integer) As Double

   If pdblInputVariable >= 0 Then
      RoundFn = Fix(pdblInputVariable * 10 ^ (CDec(pintDecimalPlaces)) + 0.50001) / (10 ^ (CDec(pintDecimalPlaces)))
   Else
      RoundFn = Fix(pdblInputVariable * 10 ^ (CDec(pintDecimalPlaces)) - 0.50001) / (10 ^ (CDec(pintDecimalPlaces)))
   End If

End Function
Public Function Beta(pdblIntRate As Double, Optional ByVal pintMode As Integer) As Double

'5.6.12, p142
Dim dblD As Double, dblIUpperM As Double, dblDUpperM As Double, dblV As Double
   
   dblV = v(pdblIntRate)
   dblD = 1 - dblV
   
   If Delta(pdblIntRate) <> 0 Then
      If pintMode >= 1 Then 'modal
         dblIUpperM = IUpperM(pdblIntRate, pintMode)
         dblDUpperM = DUpperM(pdblIntRate, pintMode)
         Beta = (pdblIntRate - dblIUpperM) / (dblIUpperM * dblDUpperM)
      ElseIf pintMode = 0 Then 'Continuous
         Beta = (pdblIntRate - Delta(pdblIntRate)) / (Delta(pdblIntRate) ^ 2)
      Else
         Beta = 0
      End If
   End If
   
End Function
Public Function IUpperM(ByVal pdblIntRate As Double, ByVal pintMode As Integer) As Double

  IUpperM = pintMode * ((1 + pdblIntRate) ^ (1 / pintMode) - 1)

End Function
Public Function DUpperM(ByVal pdblIntRate As Double, ByVal pintMode As Integer) As Double

   DUpperM = pintMode * (1 - v(pdblIntRate, (1 / pintMode)))

End Function
Public Function v(ByVal pdblIntRate As Double, Optional ByVal pdblPower As Double) As Double

   If pdblPower <> 0 Then
      v = 1 / ((1 + pdblIntRate) ^ pdblPower)
   Else
      v = 1 / (1 + pdblIntRate)
   End If
   
End Function
Public Function Delta(ByVal pdblIntRate As Double) As Double

   Delta = Log(1 + pdblIntRate)

End Function
