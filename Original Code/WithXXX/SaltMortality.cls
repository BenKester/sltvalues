VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltMortality"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"

Option Explicit

'0. Basic Mortality using 10 Year select for 1/2 cx comparison purposes
'1. Basic Mortality with regular select Factors
'2. Minimum Mortality (Including X Factor)
'3. Nonforfeiture Mortality

Private m_adblMortalityRate(3, 120) As Double
Private m_adblMortalityRate2(3, 120) As Double
Private m_asgl10YearFactor(10) As Single
Private m_aintSelectFactorDur(3) As Integer
Private m_asglSelectFactorConstant(3) As Single
Private m_fUse10YrAfter1Segment(3) As Boolean
Private m_asglVarSelectFactor(3, 30) As Single
Private m_asglVarSelectFactor10Year(3, 10) As Single
Private m_asglXFactor(3, 20) As Single
Private m_adblVarFinalMortality(3, 120) As Double

Private m_lngMortalityIndicator As MortalityType
Private m_objTRCommon As SaltTRCommon
Private Sub Class_Initialize()

Dim intCount As Integer
   
   On Error GoTo Err_Class_Initialize
   
   For intCount = 0 To 3
      m_asglSelectFactorConstant(intCount) = 1
      m_aintSelectFactorDur(intCount) = 0
   Next intCount
   
Exit_Class_Initialize:
   Exit Sub
   
Err_Class_Initialize:
   gobjMessage.Show
   Resume Exit_Class_Initialize
   
End Sub
Public Property Get TRCommon() As SaltTRCommon
   Set TRCommon = m_objTRCommon
End Property
Public Property Set TRCommon(poTRCommon As SaltTRCommon)
   Set m_objTRCommon = poTRCommon
End Property

Public Property Get Use10YrAfter1Segment(ByVal iIndicator As Integer) As Boolean
   Use10YrAfter1Segment = m_fUse10YrAfter1Segment(iIndicator)
End Property
Public Property Let Use10YrAfter1Segment(ByVal iIndicator As Integer, ByVal vData As Boolean)
   m_fUse10YrAfter1Segment(iIndicator) = vData
End Property

Public Property Get SelectFactor10Yr(ByVal iIndex As Integer) As Single
   SelectFactor10Yr = m_asgl10YearFactor(iIndex)
End Property
Public Property Let SelectFactor10Yr(ByVal iIndex As Integer, ByVal vData As Single)
   m_asgl10YearFactor(iIndex) = vData
End Property

Public Property Get MortalityRate(ByVal iIndicator As Integer, ByVal iIndex1 As Integer) As Double
   MortalityRate = m_adblMortalityRate(iIndicator, iIndex1)
End Property
Public Property Get MortalityRate2(ByVal iIndicator As Integer, ByVal iIndex1 As Integer) As Double
   MortalityRate2 = m_adblMortalityRate2(iIndicator, iIndex1)
End Property

Public Property Let MortalityRate(ByVal iIndicator As Integer, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblMortalityRate(iIndicator, iIndex1) = vData
End Property

Public Property Let MortalityRate2(ByVal iIndicator As Integer, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblMortalityRate2(iIndicator, iIndex1) = vData
End Property


Public Property Get FinalMortality(ByVal iIndicator As Integer, ByVal iIndex1 As Integer) As Double
   FinalMortality = m_adblVarFinalMortality(iIndicator, iIndex1)
End Property

Public Property Get SelectFactor(ByVal iIndicator As Integer, ByVal iIndex1 As Integer) As Single
   SelectFactor = m_asglVarSelectFactor(iIndicator, iIndex1)
End Property
Public Property Let SelectFactor(ByVal iIndicator As Integer, ByVal iIndex1 As Integer, ByVal vData As Single)
   m_asglVarSelectFactor(iIndicator, iIndex1) = vData
End Property

Public Property Get SelectFactor10Year(ByVal iIndicator As Integer, ByVal iIndex1 As Integer) As Single
   SelectFactor10Year = m_asglVarSelectFactor10Year(iIndicator, iIndex1)
End Property
Public Property Let SelectFactor10Year(ByVal iIndicator As Integer, ByVal iIndex1 As Integer, ByVal vData As Single)
   m_asglVarSelectFactor10Year(iIndicator, iIndex1) = vData
End Property

Public Property Get SelectFactorConstant(ByVal iIndicator As Integer) As Single
   SelectFactorConstant = m_asglSelectFactorConstant(iIndicator)
End Property
Public Property Let SelectFactorConstant(ByVal iIndicator As Integer, ByVal vData As Single)
   m_asglSelectFactorConstant(iIndicator) = vData
End Property

Public Property Get SelectFactorDur(ByVal iIndicator As Integer) As Integer
   SelectFactorDur = m_aintSelectFactorDur(iIndicator)
End Property
Public Property Let SelectFactorDur(ByVal iIndicator As Integer, ByVal vData As Integer)
   m_aintSelectFactorDur(iIndicator) = vData
End Property

Public Property Get MortalityType() As Integer
   MortalityType = m_lngMortalityIndicator
End Property
Public Property Let MortalityType(ByVal vData As Integer)
   m_lngMortalityIndicator = vData
End Property

Public Property Get XFactor(ByVal iIndicator As Integer, ByVal iIndex1 As Integer) As Single
   XFactor = m_asglXFactor(iIndicator, iIndex1)
End Property
Public Property Let XFactor(ByVal iIndicator As Integer, ByVal iIndex1 As Integer, ByVal vData As Single)
   m_asglXFactor(iIndicator, iIndex1) = vData
End Property

Public Function CalcMortality(ByVal pintIssueAge As Integer, ByVal pintLives As Integer, _
                              ByVal plngIndicator As _
                              MortalityType, ByVal plngTerminalMethod As _
                              enuTerminalIndicator, ByVal plngReserveMethod As _
                              ReserveMethodInput, ByVal pobjSegments As _
                              SaltSegmentation, ByVal plngSegmentType As SegmentType)

Dim intI As Integer, sglAppliedFactor As Single
Dim dblSingleLifeQ(1, 120) As Double, dblValuatHeIsAlive(5, 100) As Double

   'if penuReserveMethod=XXX or NY147 and user requests 10 year factors after the first segment, then use 10 year factors after the first segment
   For intI = pintIssueAge To 120
      sglAppliedFactor = 1
      If plngIndicator = stMinimumMort Then 'X Factors
         'pobjSegments.SegmentsGroup(plngSegmentType, pobjSegments.SegmentCount(plngSegmentType)) is the duration of the first segment of this segment type
         If intI >= pintIssueAge And intI - pintIssueAge + 1 <= 20 And intI < pobjSegments.SegmentsGroup(plngSegmentType, pobjSegments.SegmentCount(plngSegmentType)) Then
            sglAppliedFactor = sglAppliedFactor * m_asglXFactor(plngIndicator, intI - pintIssueAge + 1)
         End If
      End If
      If plngIndicator = stBasicMort Or plngIndicator = stBasicMort1_2Cx Or plngIndicator = stMinimumMort Then '10 Year select Factor after first segment
         If m_fUse10YrAfter1Segment(plngIndicator) And intI >= pobjSegments.SegmentsGroup(plngSegmentType, pobjSegments.SegmentCount(plngSegmentType)) And pobjSegments.SegmentsGroup(plngSegmentType, pobjSegments.SegmentCount(plngSegmentType)) < pintIssueAge + 10 And intI < pintIssueAge + 10 Then
            sglAppliedFactor = sglAppliedFactor * m_asgl10YearFactor(intI - pintIssueAge + 1)
         End If
      End If
      If intI >= pintIssueAge And intI - pintIssueAge + 1 <= m_aintSelectFactorDur(plngIndicator) Then
         sglAppliedFactor = sglAppliedFactor * m_asglSelectFactorConstant(plngIndicator)
      End If
      If intI >= pintIssueAge And intI - pintIssueAge + 1 <= 30 Then
         sglAppliedFactor = sglAppliedFactor * m_asglVarSelectFactor(plngIndicator, intI - pintIssueAge + 1)
      End If
      sglAppliedFactor = 1 ' Hard Code for now - Dave 7/13/2002
      dblSingleLifeQ(0, intI) = m_adblMortalityRate(plngIndicator, intI) * sglAppliedFactor
      If pintLives = 2 Then
         dblSingleLifeQ(1, intI) = m_adblMortalityRate2(plngIndicator, intI) * sglAppliedFactor
      End If
   Next intI
   If pintLives = 1 Then
      For intI = pintIssueAge To 120
         m_adblVarFinalMortality(plngIndicator, intI) = dblSingleLifeQ(0, intI)
      Next intI
   Else 'Frasierized
      dblValuatHeIsAlive(0, 0) = 1
      dblValuatHeIsAlive(1, 0) = 1
      dblValuatHeIsAlive(2, 0) = 1
      dblValuatHeIsAlive(5, 0) = 1
      For intI = pintIssueAge To 120
         If dblSingleLifeQ(0, intI) <> 0 Or dblSingleLifeQ(1, intI) <> 0 Then
            If intI = pintIssueAge Then
               dblValuatHeIsAlive(0, intI) = 1 * (1 - dblSingleLifeQ(0, intI)) 'male is alive
               dblValuatHeIsAlive(1, intI) = 1 * (1 - dblSingleLifeQ(1, intI)) 'female is alive
            Else
               dblValuatHeIsAlive(0, intI) = dblValuatHeIsAlive(0, intI - 1) * (1 - dblSingleLifeQ(0, intI)) 'male is alive
               dblValuatHeIsAlive(1, intI) = dblValuatHeIsAlive(1, intI - 1) * (1 - dblSingleLifeQ(1, intI)) 'female is alive
            End If
            dblValuatHeIsAlive(2, intI) = dblValuatHeIsAlive(0, intI) * dblValuatHeIsAlive(1, intI) 'both alive
            dblValuatHeIsAlive(3, intI) = dblValuatHeIsAlive(0, intI) * (1 - dblValuatHeIsAlive(1, intI)) 'male only alive
            dblValuatHeIsAlive(4, intI) = dblValuatHeIsAlive(1, intI) * (1 - dblValuatHeIsAlive(0, intI)) 'female only alive
            dblValuatHeIsAlive(5, intI) = dblValuatHeIsAlive(2, intI) + dblValuatHeIsAlive(3, intI) + dblValuatHeIsAlive(4, intI) 'at least one is alive
            If intI = pintIssueAge Then
               m_adblVarFinalMortality(plngIndicator, intI) = 1 - dblValuatHeIsAlive(5, intI) / 1
            Else
               If dblValuatHeIsAlive(5, intI - 1) <> 0 Then
                  m_adblVarFinalMortality(plngIndicator, intI) = 1 - dblValuatHeIsAlive(5, intI) / dblValuatHeIsAlive(5, intI - 1)
               Else
                  m_adblVarFinalMortality(plngIndicator, intI) = 1
               End If
            End If
            If pintIssueAge = 20 Then
'               Debug.Print intI, m_adblVarFinalMortality(plngIndicator, intI)
'               Stop
            End If
         Else
            m_adblVarFinalMortality(plngIndicator, intI) = 1
         End If
      Next intI
   End If
   CalcMortality = 120
   For intI = pintIssueAge To 120
      If m_adblVarFinalMortality(plngIndicator, intI) >= 1 Then
         CalcMortality = intI
         Exit For
      ElseIf intI > 50 And m_adblVarFinalMortality(plngIndicator, intI) <= 0 Then
         CalcMortality = intI
         Exit For
      End If
   Next intI

End Function
