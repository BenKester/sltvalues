VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltInsured"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'I am doing this so that the enum values will keep their correct case.
#If False Then
   Dim stMale, stFemale
#End If
Public Enum GenderType
   stMale = 0
   stFemale = 1
End Enum

Private m_intIssueAge As Integer 'local copy
Private m_lngGender As GenderType 'local copy
Private m_objTRCommon As SaltTRCommon
Private m_blnETI As Boolean
Private m_intLives As Integer
Public Property Let ETI(ByVal vData As Boolean)
   m_blnETI = vData
End Property


Public Property Get TRCommon() As SaltTRCommon
   Set TRCommon = m_objTRCommon
End Property
Public Property Get ETI() As Boolean
   ETI = m_blnETI
End Property

Public Property Set TRCommon(poTRCommon As SaltTRCommon)
   Set m_objTRCommon = poTRCommon
End Property

Public Property Get IssueAge() As Integer
   IssueAge = m_intIssueAge
End Property
Public Property Get Lives() As Integer
   Lives = m_intLives
End Property

Public Property Let IssueAge(ByVal vData As Integer)
   m_intIssueAge = vData
End Property

Public Property Let Lives(ByVal vData As Integer)
   m_intLives = vData
End Property


Public Property Get Gender() As GenderType
   Gender = m_lngGender
End Property
Public Property Let Gender(ByVal vData As GenderType)
   m_lngGender = vData
End Property
