VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltPolicy"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private m_datIssueDate As Date
Private m_datValuationDate As Date
Private m_objTRCommon As SaltTRCommon
Private m_enuProdType As eProductType
Public Property Get TRCommon() As SaltTRCommon
   Set TRCommon = m_objTRCommon
End Property
Public Property Set TRCommon(poTRCommon As SaltTRCommon)
   Set m_objTRCommon = poTRCommon
End Property
Public Property Let IssueDate(ByVal vData As Date)
   m_datIssueDate = vData
End Property
Public Property Let ProductType(ByVal vData As eProductType)
   m_enuProdType = vData
End Property

Public Property Get IssueDate() As Date
   IssueDate = m_datIssueDate
End Property
Public Property Get ProductType() As eProductType
   ProductType = m_enuProdType
End Property

Public Property Let ValuationDate(ByVal vData As Date)
   m_datValuationDate = vData
End Property
Public Property Get ValuationDate() As Date
   ValuationDate = m_datValuationDate
End Property
