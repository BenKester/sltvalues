VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltCommutationFunctions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private m_adblCx(130) As Double
Private m_adblDx(130) As Double
Private m_adbl_lx(130) As Double
Private m_adblMx(130) As Double
Private m_adblNx(130) As Double
Private m_adblPx(130) As Double
Private m_adblRx(130) As Double
Private m_adblSx(130) As Double
Private m_adblVx(130) As Double
Private m_adblCBarx(130) As Double
Private m_adblDBarx(130) As Double
Private m_adblMBarx(130) As Double
Private m_adblNBarx(130) As Double
Private m_adblRBarx(130) As Double
Private m_adblDeathx(130) As Double
Private m_adblNxMthly(130) As Double

Public Property Get CBarx(ByVal iIndex1 As Integer) As Double
   CBarx = m_adblCBarx(iIndex1)
End Property
Public Property Get Cx(ByVal iIndex1 As Integer) As Double
   Cx = m_adblCx(iIndex1)
End Property
Public Property Get DBarx(ByVal iIndex1 As Integer) As Double
   DBarx = m_adblDBarx(iIndex1)
End Property
Public Property Get Deathx(ByVal iIndex1 As Integer) As Double
   Deathx = m_adblDeathx(iIndex1)
End Property
Public Property Get Dx(ByVal iIndex1 As Integer) As Double
   Dx = m_adblDx(iIndex1)
End Property
Public Property Get lx(ByVal iIndex1 As Integer) As Double
   lx = m_adbl_lx(iIndex1)
End Property
Public Property Get MBarx(ByVal iIndex1 As Integer) As Double
   MBarx = m_adblMBarx(iIndex1)
End Property
Public Property Get Mx(ByVal iIndex1 As Integer) As Double
   Mx = m_adblMx(iIndex1)
End Property
Public Property Get NBarx(ByVal iIndex1 As Integer) As Double
   NBarx = m_adblNBarx(iIndex1)
End Property
Public Property Get Nx(ByVal iIndex1 As Integer) As Double
   Nx = m_adblNx(iIndex1)
End Property
Public Property Get NxMthly(ByVal iIndex1 As Integer) As Double
   NxMthly = m_adblNxMthly(iIndex1)
End Property
Public Property Get px(ByVal iIndex1 As Integer) As Double
   px = m_adblPx(iIndex1)
End Property
Public Property Get RBarx(ByVal iIndex1 As Integer) As Double
   RBarx = m_adblRBarx(iIndex1)
End Property
Public Property Get Rx(ByVal iIndex1 As Integer) As Double
   Rx = m_adblRx(iIndex1)
End Property
Public Property Get sx(ByVal iIndex1 As Integer) As Double
   sx = m_adblSx(iIndex1)
End Property
Public Property Get vx(ByVal iIndex1 As Integer) As Double
   vx = m_adblVx(iIndex1)
End Property
Public Sub CalcCommFunctions(ByVal pintBeginAge As Integer, ByVal pintEndAge As _
                              Integer)

Dim dblMyDiscount As Double
Dim sngMyIntRate As Single
Dim intI As Integer

   m_adbl_lx(pintBeginAge) = 10000000
   m_adblDx(pintBeginAge) = m_adbl_lx(pintBeginAge)
   dblMyDiscount = 1
   
   For intI = pintBeginAge To pintEndAge
      If gsavIntRate.FinalInterestRate(intI - pintBeginAge) <> 0 Then
         sngMyIntRate = gsavIntRate.FinalInterestRate(intI - pintBeginAge)
      End If
      'Debug.Print intI, gsavIntRate.FinalInterestRate(intI - pintBeginAge)
      'If intI = 20 Then Stop
      'If pobjIntRates.InterestRateType = 0 Then 'Constant
         dblMyDiscount = dblMyDiscount / (1 + sngMyIntRate)
      '   sngMyIntRate = pobjIntRates.InterestRateConstant
      'Else
      '   dblMyDiscount = dblMyDiscount / (1 + sngMyIntRate)
      '   sngMyIntRate = pobjIntRates.InterestRateVary(intI - pintBeginAge)
      'End If
      m_adbl_lx(intI + 1) = m_adbl_lx(intI) * (1 - gsavMortality.FinalMortality(intI))
      m_adblDx(intI + 1) = m_adbl_lx(intI + 1) * dblMyDiscount
      m_adblDBarx(intI) = Alpha(CDbl(sngMyIntRate)) * m_adblDx(intI) - Beta(CDbl(sngMyIntRate)) * (m_adblDx(intI) - m_adblDx(intI + 1))
      m_adblCx(intI) = m_adblDx(intI) * gsavMortality.FinalMortality(intI) / (1 + sngMyIntRate)
      m_adblCBarx(intI) = m_adblCx(intI) * sngMyIntRate / Delta(sngMyIntRate)
   Next intI
   m_adblMBarx(pintEndAge + 1) = 0
   m_adblMx(pintEndAge + 1) = 0
   m_adblNBarx(pintEndAge + 1) = 0
   m_adblNx(pintEndAge + 1) = 0
   
   For intI = pintEndAge To pintBeginAge Step -1
      sngMyIntRate = gsavIntRate.FinalInterestRate(intI - pintBeginAge)
      'If pobjIntRates.InterestRateType = 0 Then 'Constant
      '   sngMyIntRate = pobjIntRates.InterestRateConstant
      'Else
      '   sngMyIntRate = pobjIntRates.InterestRateVary(intI - pintBeginAge)
      'End If
      m_adblMx(intI) = m_adblMx(intI + 1) + m_adblCx(intI)
      m_adblMBarx(intI) = m_adblMBarx(intI + 1) + m_adblCBarx(intI)
      m_adblNx(intI) = m_adblNx(intI + 1) + m_adblDx(intI)
      m_adblNBarx(intI) = m_adblNBarx(intI + 1) + m_adblDBarx(intI)
   Next intI
   
End Sub
Public Sub ResetValues()

   Erase m_adbl_lx, m_adblCBarx, m_adblCx, m_adblDBarx, m_adblDeathx, m_adblDx
   Erase m_adblMBarx, m_adblMx, m_adblNBarx, m_adblNx, m_adblNxMthly, m_adblPx
   Erase m_adblRBarx, m_adblRx, m_adblSx, m_adblVx
   
End Sub
