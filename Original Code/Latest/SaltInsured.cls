VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltInsured"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'I am doing this so that the enum values will keep their correct case.
#If False Then
   Dim stMale, stFemale
#End If
Public Enum GenderType
   stMale = 0
   stFemale = 1
End Enum

Private m_intIssueAge As Integer 'local copy
Private m_intIssueAge2 As Integer
Private m_enuGender As GenderType 'local copy
Private m_blnETI As Boolean
Private m_enuLives As eNumberOfLives
Private m_enuGender2 As GenderType
Public Property Get ETI() As Boolean
   ETI = m_blnETI
End Property
Public Property Let ETI(ByVal vData As Boolean)
   m_blnETI = vData
End Property

Public Property Get IssueAge() As Integer
   IssueAge = m_intIssueAge
End Property
Public Property Let IssueAge(ByVal vData As Integer)
   m_intIssueAge = vData
End Property

Public Property Get IssueAge2() As Integer
   IssueAge2 = m_intIssueAge2
End Property
Public Property Let IssueAge2(ByVal pintData As Integer)
   m_intIssueAge2 = pintData
End Property

Public Property Get Lives() As eNumberOfLives
   Lives = m_enuLives
End Property
Public Property Let Lives(ByVal vData As eNumberOfLives)
   m_enuLives = vData
End Property

Public Property Get Gender() As GenderType
   Gender = m_enuGender
End Property
Public Property Get Gender2() As GenderType
   Gender2 = m_enuGender2
End Property

Public Property Let Gender(ByVal vData As GenderType)
   m_enuGender = vData
End Property
Public Property Let Gender2(ByVal vData As GenderType)
   m_enuGender2 = vData
End Property

Public Sub ResetValues()
   
   m_blnETI = False
   m_enuGender = 0
   m_enuGender2 = 0
   m_enuLives = 0
   m_intIssueAge = 0
   m_intIssueAge2 = 0
   
End Sub
