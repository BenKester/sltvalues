VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltMortality"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********  Select Factors  ******************
'Note that the 1st dimension is for the number of lives (0 to 1 for lives 1 to 2)
Private m_asngSelectFactor(0 To 1, 0 To 130) As Single   ' Ben added on 7/2/2003
Private m_asngSelectFactorMin(0 To 1, 0 To 130) As Single   ' Dave added on 8/1/2003
Private m_asngSelectFactorTabCOI(0 To 1, 0 To 130) As Single   'Dave added on 8/1/2003

'This is a select factor that is the same for all of the mortality types if it is used
Private m_asngRenewalSegment10YrSelect(0 To 1, 1 To 10) As Single
'Determines when the array above is used
Private m_enuRenewalSegment10YrSelect As BasicMinimumOptions
'X Factors
Private m_asngXFactor(20) As Single
'**********  End of Select Factors  ******************

'**********  Mortality Rates  ************************
'Note that the 1st dimension is for the segment type (0 to 1 for initial segment or renewal segment)
Private m_adblMortalityRate(1, 130) As Double
Private m_adblMortalityRate2(1, 130) As Double
Private m_adblMortalityRateMin(1, 130) As Double 'Dave added on 8/1/2003
Private m_adblMortalityRate2Min(1, 130) As Double 'Dave added on 8/1/2003
Private m_adblMortalityRateTabCOI(1, 130) As Double 'Dave added on 8/1/2003
Private m_adblMortalityRate2TabCOI(1, 130) As Double 'Dave added on 8/1/2003

'Final Mortality rates
Private m_adblVarFinalMortality(130) As Double
'**********  End of Mortality Rates  ************************

Private m_enuMortalityIndicator As MortalityType

Public Property Get SelFactor(ByVal pintLifeNum As Integer, ByVal pintDur _
                                 As Integer) As Single
   SelFactor = m_asngSelectFactor(pintLifeNum, pintDur)
End Property
Public Property Let SelFactor(ByVal pintLifeNum As Integer, ByVal pintDur _
                                 As Integer, ByVal psngValue As Single)
   m_asngSelectFactor(pintLifeNum, pintDur) = psngValue
End Property

Public Property Get SelFactorMinimum(ByVal pintLifeNum As Integer, ByVal pintDur _
                                 As Integer) As Single
   SelFactorMinimum = m_asngSelectFactorMin(pintLifeNum, pintDur)
End Property
Public Property Get SelFactorTabCOI(ByVal pintLifeNum As Integer, ByVal pintDur _
                                 As Integer) As Single
   SelFactorTabCOI = m_asngSelectFactorTabCOI(pintLifeNum, pintDur)
End Property

Public Property Let SelFactorMinimum(ByVal pintLifeNum As Integer, ByVal pintDur _
                                 As Integer, ByVal psngValue As Single)
   m_asngSelectFactorMin(pintLifeNum, pintDur) = psngValue
End Property
Public Property Let SelFactorTabCOI(ByVal pintLifeNum As Integer, ByVal pintDur _
                                 As Integer, ByVal psngValue As Single)
   m_asngSelectFactorTabCOI(pintLifeNum, pintDur) = psngValue
End Property

Private Sub Class_Initialize()

Dim intCount As Integer
   
   On Error GoTo Err_Class_Initialize
   
   For intCount = 0 To 130
      m_asngSelectFactor(0, intCount) = 1
      m_asngSelectFactor(1, intCount) = 1
   Next intCount
   
Exit_Class_Initialize:
   Exit Sub
   
Err_Class_Initialize:
   gobjMessage.Show
   Resume Exit_Class_Initialize
   
End Sub
Public Sub ResetValues()

Dim intCount As Integer

   For intCount = 0 To 130
      m_asngSelectFactor(0, intCount) = 1
      m_asngSelectFactor(1, intCount) = 1
   Next intCount
   Erase m_adblMortalityRate, m_adblMortalityRate2, m_adblMortalityRate2Min
   Erase m_adblMortalityRate2TabCOI, m_adblMortalityRateMin
   Erase m_adblMortalityRateTabCOI, m_adblVarFinalMortality
   Erase m_asngRenewalSegment10YrSelect, m_asngSelectFactorMin
   Erase m_asngSelectFactorTabCOI, m_asngXFactor
   
   m_enuMortalityIndicator = 0
   m_enuRenewalSegment10YrSelect = 0
   
End Sub
Public Property Get RenewalSegment10YrSelect() As BasicMinimumOptions
   RenewalSegment10YrSelect = m_enuRenewalSegment10YrSelect
End Property
Public Property Let RenewalSegment10YrSelect(ByVal vData As BasicMinimumOptions)
   m_enuRenewalSegment10YrSelect = vData
End Property

Public Property Get RenewalSegment10YrSelectFactor(ByVal pintLifeNum As Integer, ByVal pintDur _
                                 As Integer) As Single
   RenewalSegment10YrSelectFactor = m_asngRenewalSegment10YrSelect(pintLifeNum, pintDur)
End Property
Public Property Let RenewalSegment10YrSelectFactor(ByVal pintLifeNum As Integer, ByVal pintDur As Integer, _
  ByVal vData As Single)

   m_asngRenewalSegment10YrSelect(pintLifeNum, pintDur) = vData
End Property

Public Property Get MortalityRate(ByVal enuSegIndicator As SegmentIndicator, ByVal iIndex1 As Integer) As Double
   MortalityRate = m_adblMortalityRate(enuSegIndicator, iIndex1)
End Property
Public Property Get MortalityRateTabCOI(ByVal enuSegIndicator As SegmentIndicator, ByVal iIndex1 As Integer) As Double
   MortalityRateTabCOI = m_adblMortalityRateTabCOI(enuSegIndicator, iIndex1)
End Property

Public Property Get MortalityRateMin(ByVal enuSegIndicator As SegmentIndicator, ByVal iIndex1 As Integer) As Double
   MortalityRateMin = m_adblMortalityRateMin(enuSegIndicator, iIndex1)
End Property

Public Property Get MortalityRate2(ByVal enuSegIndicator As SegmentIndicator, ByVal iIndex1 As Integer) As Double
   MortalityRate2 = m_adblMortalityRate2(enuSegIndicator, iIndex1)
End Property
Public Property Get MortalityRate2Min(ByVal enuSegIndicator As SegmentIndicator, ByVal iIndex1 As Integer) As Double
   MortalityRate2Min = m_adblMortalityRate2Min(enuSegIndicator, iIndex1)
End Property
Public Property Get MortalityRate2TabCOI(ByVal enuSegIndicator As SegmentIndicator, ByVal iIndex1 As Integer) As Double
   MortalityRate2TabCOI = m_adblMortalityRate2TabCOI(enuSegIndicator, iIndex1)
End Property

Public Property Let MortalityRate(ByVal enuSegIndicator As SegmentIndicator, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblMortalityRate(enuSegIndicator, iIndex1) = vData
End Property
Public Property Let MortalityRateTabCOI(ByVal enuSegIndicator As SegmentIndicator, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblMortalityRateTabCOI(enuSegIndicator, iIndex1) = vData
End Property

Public Property Let MortalityRateMin(ByVal enuSegIndicator As SegmentIndicator, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblMortalityRateMin(enuSegIndicator, iIndex1) = vData
End Property


Public Property Let MortalityRate2(ByVal enuSegIndicator As SegmentIndicator, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblMortalityRate2(enuSegIndicator, iIndex1) = vData
End Property
Public Property Let MortalityRate2TabCOI(ByVal enuSegIndicator As SegmentIndicator, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblMortalityRate2TabCOI(enuSegIndicator, iIndex1) = vData
End Property

Public Property Let MortalityRate2Min(ByVal enuSegIndicator As SegmentIndicator, ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblMortalityRate2Min(enuSegIndicator, iIndex1) = vData
End Property


Public Property Get FinalMortality(ByVal iIndex1 As Integer) As Double
   FinalMortality = m_adblVarFinalMortality(iIndex1)
End Property



Public Property Get MortalityType() As MortalityType
   MortalityType = m_enuMortalityIndicator
End Property
Public Property Let MortalityType(ByVal vData As MortalityType)
   m_enuMortalityIndicator = vData
End Property

Public Property Get XFactor(ByVal iIndex1 As Integer) As Single
   XFactor = m_asngXFactor(iIndex1)
End Property
Public Property Let XFactor(ByVal iIndex1 As Integer, ByVal vData As Single)
   m_asngXFactor(iIndex1) = vData
End Property

Public Function CalcMortality(ByVal pintIssueAge As Integer, ByVal penuTerminalMethod As _
                              enuTerminalIndicator, ByVal pintIssueAge2 As Integer, ByVal _
                              strReportHeader As String)

Dim intAge As Integer, sngSegment1SelectFactor As Single, intAge2 As Integer
Dim dblSingleLifeQ1(130) As Double, dblValuatHeIsAlive(5, 130) As Double
Dim intDur As Integer, dblSingleLifeQ2(130) As Double
Dim sngRenewalSegmentFactor As Single
Dim intLifeIndicator As Integer
Dim blnDebugOn As Boolean, dblMortRateTemp As Double
Dim blnUseRenewalSegment As Boolean
Dim enuSegmentIndicator As SegmentIndicator

blnDebugOn = False 'True
   
   
   'if penuReserveMethod=XXX or NY147 and user requests 10 year factors after the first segment, then use 10 year factors after the first segment
'm_enuMortalityIndicator is an input item to this function
For intLifeIndicator = 0 To gsavInsured.Lives - 1
   For intAge = pintIssueAge To 129
'      If intAge = 100 Then Stop
      sngSegment1SelectFactor = 1
      intAge2 = MIN(intAge + (pintIssueAge2 - pintIssueAge), 130)
      intDur = intAge - pintIssueAge + 1
'Get the select factor - split into 1st segments and renewal segments
      
      'Determine which Segment
      If m_enuMortalityIndicator = stBasicMort1_2Cx Then
         If intDur <= 10 Then 'And gsavMidYear.MidYearFactors = st10YrSelMortFactors - Don't think I need
            If intAge < gsavSegmentation.SegmentsGroup(gsavSegmentation.SegmentCount) Then 'Still in 1st segment
               enuSegmentIndicator = stFirstSegment
               sngSegment1SelectFactor = m_asngSelectFactorTabCOI(intLifeIndicator, intDur)
               sngRenewalSegmentFactor = 1
            Else
               enuSegmentIndicator = stRenewalSegment
               sngSegment1SelectFactor = 1
               sngRenewalSegmentFactor = m_asngSelectFactorTabCOI(intLifeIndicator, intDur)
            End If
         Else
            enuSegmentIndicator = stRenewalSegment
            sngSegment1SelectFactor = 1
            sngRenewalSegmentFactor = 1
         End If
      Else
         If intAge < gsavSegmentation.SegmentsGroup(gsavSegmentation.SegmentCount) Then 'Still in 1st segment
            enuSegmentIndicator = stFirstSegment
            If m_enuMortalityIndicator = stMinimumMort Or m_enuMortalityIndicator = stSegmentedMort Then
               sngSegment1SelectFactor = m_asngSelectFactorMin(intLifeIndicator, intDur)
               If m_enuMortalityIndicator = stMinimumMort Then 'X Factors
                  'gsavSegmentation.SegmentsGroup(plngSegmentType, gsavSegmentation.SegmentCount(plngSegmentType)) is the duration of the first segment of this segment type
                  If intDur <= 20 Then
                     sngSegment1SelectFactor = sngSegment1SelectFactor * m_asngXFactor(intAge - pintIssueAge + 1)
                  End If
               End If
            Else
               sngSegment1SelectFactor = m_asngSelectFactor(intLifeIndicator, intDur)
            End If
            sngRenewalSegmentFactor = 1
         Else 'Renewal Segment
            sngSegment1SelectFactor = 1
            enuSegmentIndicator = stRenewalSegment
            blnUseRenewalSegment = False
            If penuTerminalMethod = stBasicSegmented Or penuTerminalMethod = stMinimumSegmented Or m_enuMortalityIndicator = stBasicMort1_2Cx Then
               If intAge >= gsavSegmentation.SegmentsGroup(gsavSegmentation.SegmentCount) _
                 And gsavSegmentation.SegmentsGroup(gsavSegmentation.SegmentCount) < pintIssueAge + 10 And _
                 intDur <= 10 Then
                  If penuTerminalMethod = stBasicSegmented And (m_enuRenewalSegment10YrSelect = stBasicOnly Or m_enuRenewalSegment10YrSelect = stBasicMinimum) Then
                     blnUseRenewalSegment = True
                  ElseIf penuTerminalMethod = stMinimumSegmented And (m_enuRenewalSegment10YrSelect = stMinimumOnly Or m_enuRenewalSegment10YrSelect = stBasicMinimum) Then
                     blnUseRenewalSegment = True
                  End If
               End If
            End If
            
            If blnUseRenewalSegment Then
               sngRenewalSegmentFactor = m_asngRenewalSegment10YrSelect(intLifeIndicator, intDur)
            Else
               sngRenewalSegmentFactor = 1
            End If
         End If
      End If
'         If m_enuMortalityIndicator = stMinimumMort Or m_enuMortalityIndicator = stSegmentation Then
'               If intAge >= gsavSegmentation.SegmentsGroup(plngSegmentType, gsavSegmentation.SegmentCount(plngSegmentType)) _
'                 And gsavSegmentation.SegmentsGroup(plngSegmentType, gsavSegmentation.SegmentCount(plngSegmentType)) < pintIssueAge + 10 And _
'                 intDur <= 10 Then
'                  sngRenewalSegmentFactor = m_asngRenewalSegment10YrSelect(intLifeIndicator, intDur)
'               Else
'                  sngRenewalSegmentFactor = 1
'               End If
'            Else
'               sngRenewalSegmentFactor = 1
'            End If
'         Else
'            sngRenewalSegmentFactor = 1
'         End If
'      sngSegment1SelectFactor = m_asngSelectFactor(intLifeIndicator, intDur)
      If intLifeIndicator = 0 Then
         If m_enuMortalityIndicator = stBasicMort1_2Cx Then
            dblMortRateTemp = m_adblMortalityRateTabCOI(enuSegmentIndicator, intAge)
         ElseIf m_enuMortalityIndicator = stMinimumMort Then
            dblMortRateTemp = m_adblMortalityRateMin(enuSegmentIndicator, intAge)
         Else
            dblMortRateTemp = m_adblMortalityRate(enuSegmentIndicator, intAge)
         End If
         dblSingleLifeQ1(intAge) = dblMortRateTemp * _
         sngSegment1SelectFactor * sngRenewalSegmentFactor
         If blnDebugOn And intDur <= 21 Then
            Debug.Print intAge, dblSingleLifeQ1(intAge), m_adblMortalityRate(enuSegmentIndicator, intAge), sngSegment1SelectFactor, sngRenewalSegmentFactor
         End If
      Else
         If m_enuMortalityIndicator = stBasicMort1_2Cx Then
            dblMortRateTemp = m_adblMortalityRate2TabCOI(enuSegmentIndicator, intAge2)
         ElseIf m_enuMortalityIndicator = stMinimumMort Then
            dblMortRateTemp = m_adblMortalityRate2Min(enuSegmentIndicator, intAge2)
         Else
            dblMortRateTemp = m_adblMortalityRate2(enuSegmentIndicator, intAge2)
         End If
         dblSingleLifeQ2(intAge) = dblMortRateTemp _
            * sngSegment1SelectFactor * sngRenewalSegmentFactor
         If blnDebugOn Then
            Debug.Print intAge, intAge2, dblSingleLifeQ2(intAge), m_adblMortalityRate2(enuSegmentIndicator, intAge2), sngSegment1SelectFactor, sngRenewalSegmentFactor
         End If
      End If
   Next intAge
   If gsavInsured.Lives = Onelife Then Exit For
Next intLifeIndicator
If gsavInsured.Lives = Onelife Then
   For intAge = pintIssueAge To 129
      m_adblVarFinalMortality(intAge) = dblSingleLifeQ1(intAge)
   Next intAge
Else 'Frasierized
   Call FrasierizeMort(pintIssueAge, dblSingleLifeQ1, dblSingleLifeQ2, _
      m_adblVarFinalMortality, False)
'''      dblValuatHeIsAlive(0, 0) = 1
'''      dblValuatHeIsAlive(1, 0) = 1
'''      dblValuatHeIsAlive(2, 0) = 1
'''      dblValuatHeIsAlive(5, 0) = 1
'''      For intAge = pintIssueAge To 130
'''         If dblSingleLifeQ1(intAge) <> 0 Or dblSingleLifeQ2(intAge) <> 0 Then
'''            If intAge = pintIssueAge Then
'''               dblValuatHeIsAlive(0, intAge) = 1 * (1 - dblSingleLifeQ1(intAge)) 'male is alive
'''               dblValuatHeIsAlive(1, intAge) = 1 * (1 - dblSingleLifeQ2(intAge)) 'female is alive
'''            Else
'''               dblValuatHeIsAlive(0, intAge) = dblValuatHeIsAlive(0, intAge - 1) * (1 - dblSingleLifeQ1(intAge)) 'male is alive
'''               dblValuatHeIsAlive(1, intAge) = dblValuatHeIsAlive(1, intAge - 1) * (1 - dblSingleLifeQ2(intAge)) 'female is alive
'''            End If
'''            dblValuatHeIsAlive(2, intAge) = dblValuatHeIsAlive(0, intAge) * dblValuatHeIsAlive(1, intAge) 'both alive
'''            dblValuatHeIsAlive(3, intAge) = dblValuatHeIsAlive(0, intAge) * (1 - dblValuatHeIsAlive(1, intAge)) 'male only alive
'''            dblValuatHeIsAlive(4, intAge) = dblValuatHeIsAlive(1, intAge) * (1 - dblValuatHeIsAlive(0, intAge)) 'female only alive
'''            dblValuatHeIsAlive(5, intAge) = dblValuatHeIsAlive(2, intAge) + dblValuatHeIsAlive(3, intAge) + dblValuatHeIsAlive(4, intAge) 'at least one is alive
'''            If intAge = pintIssueAge Then
'''               m_adblVarFinalMortality( intAge) = 1 - dblValuatHeIsAlive(5, intAge) / 1
'''            Else
'''               If dblValuatHeIsAlive(5, intAge - 1) <> 0 Then
'''                  m_adblVarFinalMortality( intAge) = 1 - dblValuatHeIsAlive(5, intAge) / dblValuatHeIsAlive(5, intAge - 1)
'''               Else
'''                  m_adblVarFinalMortality( intAge) = 1
'''               End If
'''            End If
'''            If pintIssueAge = 20 Then
''''               Debug.Print intAge, m_adblVarFinalMortality( intAge)
''''               Stop
'''            End If
'''         Else
'''            m_adblVarFinalMortality( intAge) = 1
'''         End If
'''      Next intAge
End If
CalcMortality = 130
For intAge = pintIssueAge To 130
   If m_adblVarFinalMortality(intAge) >= 1 Then
      m_adblVarFinalMortality(intAge) = 1
      CalcMortality = intAge
      For intDur = intAge + 1 To 130
         m_adblVarFinalMortality(intDur) = 1
      Next intDur
      Exit For
   ElseIf intAge > 50 And m_adblVarFinalMortality(intAge) <= 0 Then
      CalcMortality = intAge
      Exit For
   End If
Next intAge

End Function
Public Sub FrasierizeMort(ByVal pintIssueAge As Integer, ByRef padblMort1() As _
                           Double, ByRef padblMort2() As Double, ByRef _
                           padblOutput() As Double, Optional ByVal pblnRedimArray As _
                           Boolean = True)

Dim intAge As Integer
Dim adblAlive1() As Double, adblAlive2() As Double, adblAliveBoth() As Double
Dim adblAliveOnly1() As Double, adblAliveOnly2() As Double
Dim adblAliveEither() As Double, intLBound As Integer, intUBound As Integer
Dim dblMort1 As Double, dblMort2 As Double
Dim blnProcessMort As Boolean
   
   intLBound = LBound(padblMort1)
   intUBound = UBound(padblMort1)
   If pblnRedimArray Then
      ReDim padblOutput(intLBound To intUBound)
   End If
   ReDim adblAlive1(intLBound To intUBound)
   ReDim adblAlive2(intLBound To intUBound)
   ReDim adblAliveBoth(intLBound To intUBound)
   ReDim adblAliveOnly1(intLBound To intUBound)
   ReDim adblAliveOnly2(intLBound To intUBound)
   ReDim adblAliveEither(intLBound To intUBound)
   For intAge = 0 To pintIssueAge - 1
      padblOutput(intAge) = 0
   Next intAge
   For intAge = pintIssueAge To MIN(129, UBound(padblOutput))
      blnProcessMort = False
      If intAge <= intUBound Then
         blnProcessMort = True
         dblMort1 = padblMort1(intAge)
      Else
         dblMort1 = 1
      End If
      If intAge <= UBound(padblMort2) Then
         blnProcessMort = True
         dblMort2 = padblMort2(intAge)
      Else
         dblMort2 = 1
      End If
      If blnProcessMort Then
         If gsavInsured.Lives = TwoLivesFirstToDie Then 'p233, Formula 8.2.8 of Bowers
            padblOutput(intAge) = MIN(1, dblMort1 + dblMort2 - dblMort1 * dblMort2)
         Else
            If intAge = pintIssueAge Then
               adblAlive1(intAge) = 1 * (1 - dblMort1)
               adblAlive2(intAge) = 1 * (1 - dblMort2)
            Else
               adblAlive1(intAge) = adblAlive1(intAge - 1) * (1 - dblMort1)
               adblAlive2(intAge) = adblAlive2(intAge - 1) * (1 - dblMort2)
            End If
            adblAliveBoth(intAge) = adblAlive1(intAge) * adblAlive2(intAge)
            adblAliveOnly1(intAge) = adblAlive1(intAge) * (1 - adblAlive2(intAge))
            adblAliveOnly2(intAge) = adblAlive2(intAge) * (1 - adblAlive1(intAge))
            adblAliveEither(intAge) = adblAliveBoth(intAge) + adblAliveOnly1(intAge) _
               + adblAliveOnly2(intAge)
            If intAge = pintIssueAge Then
               padblOutput(intAge) = 1 - adblAliveEither(intAge) / 1
            Else
               If adblAliveEither(intAge - 1) <> 0 Then
                  padblOutput(intAge) = 1 - adblAliveEither(intAge) / _
                     adblAliveEither(intAge - 1)
               Else
                  padblOutput(intAge) = 1
               End If
            End If
         End If
      Else
         padblOutput(intAge) = 1
      End If
   Next intAge

End Sub
