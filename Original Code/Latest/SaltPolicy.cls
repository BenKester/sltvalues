VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltPolicy"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private m_datIssueDate As Date
Private m_datValuationDate As Date
Private m_enuProdType As eProductType
Private m_CalcType As CalcTypeDefn
Public Property Get IssueDate() As Date
   IssueDate = m_datIssueDate
End Property
Public Property Let IssueDate(ByVal vData As Date)
   m_datIssueDate = vData
End Property

Public Property Get ProductType() As eProductType
   ProductType = m_enuProdType
End Property
Public Property Get CalcType() As CalcTypeDefn
   CalcType = m_CalcType
End Property

Public Property Let ProductType(ByVal vData As eProductType)
   m_enuProdType = vData
End Property
Public Property Let CalcType(ByVal vData As CalcTypeDefn)
   m_CalcType = vData
End Property

Public Sub ResetValues()

   m_datIssueDate = 0
   m_datValuationDate = 0
   m_enuProdType = 0

End Sub
Public Property Get ValuationDate() As Date
   ValuationDate = m_datValuationDate
End Property
Public Property Let ValuationDate(ByVal vData As Date)
   m_datValuationDate = vData
End Property

' Purpose      : Calculates the duration of a policy
' Inputs       :
'  pdatIssueDate - the issue date of the policy
'  pdatValuationDate - the valuation date used to determine the duration
' Source       : Dave Kester
' Created      : 8/24/2001 By Dave Kester
' Modified     :
Public Function Duration() As Integer

   Duration = DateDiff("m", IssueDate, ValuationDate)
   If Day(IssueDate) > Day(ValuationDate) Then
      Duration = Duration - 1
   End If
   Duration = Fix(Duration / 12) + 1
   
End Function

' Purpose      : Interpolates the valuation date between policy anniversaries on an exact day basis
' Inputs       :
'  IssueDate - the issue date of the policy
'  ValuationDate - the valuation date used to determine the duration
' Source       : Ben Kester
' Created      : 8/20/2003 By Ben Kester
' Modified     :
Public Function ExactDayInterpolation() As Double

Dim datVal2 As Date, intDays As Integer, intTotalDays As Integer
   
   datVal2 = DateSerial(Year(ValuationDate), Month(IssueDate), Day(IssueDate))
   If datVal2 < ValuationDate Then
      datVal2 = DateAdd("yyyy", 1, datVal2)
   End If
   intDays = DateDiff("d", ValuationDate, datVal2)
   intTotalDays = DateDiff("d", DateAdd("yyyy", -1, datVal2), datVal2)
   ExactDayInterpolation = intDays / intTotalDays

End Function
' Purpose      : Calculates the duration of a policy
' Inputs       :
'  pdatIssueDate - the issue date of the policy
'  pdatValuationDate - the valuation date used to determine the duration
' Source       : Dave Kester
' Created      : 8/24/2001 By Dave Kester
' Modified     :
Public Function MonthDuration() As Integer

   MonthDuration = DateDiff("m", IssueDate, ValuationDate)
   If Day(IssueDate) > Day(ValuationDate) Then
      MonthDuration = MonthDuration - 1
   End If
   MonthDuration = (MonthDuration Mod 12) + 1
   
End Function

