VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltCV"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private m_adblAdjustedPremium(130) As Double
Private m_adblCashValue(130) As Double
Private m_asngSC(130) As Single
'Private m_adblSC(130) As Double
Private m_adblPureEndowment(130) As Double
Private m_dblEA_CV As Double
Private m_dblAAI As Double
Private m_blnCalcCV As Boolean
Private m_blnCalcSC As Boolean
Private m_enuCVMethod As eCVMethod
Private m_enuCVMethodFinal As eCVMethod
Private m_adblNSP(130) As Double
Private m_blnCVMessage(0) As Boolean
'Private m_enuModResDefn As ModResType
Private m_dblFinalTerminalCV As Double
Private m_adblPVFAdjPrem(130) As Double
Private m_adblUnusualCV(130) As Double
Private m_blnTestForUnusualCV As Boolean 'F3
Private m_blnCompareToCV As Boolean 'F4
Public Property Get FinalTerminalCV() As Double
    FinalTerminalCV = m_dblFinalTerminalCV
End Property

Public Property Get PVFAdjPrem(ByVal iIndex1 As Integer) As Double
    PVFAdjPrem = m_adblPVFAdjPrem(iIndex1)
End Property

Public Property Let FinalTerminalCV(ByVal vData As Double)
    m_dblFinalTerminalCV = vData
End Property
Public Property Let PVFAdjPrem(ByVal iIndex1 As Integer, ByVal vData As Double)
    m_adblPVFAdjPrem(iIndex1) = vData
End Property

Public Property Get NSP(ByVal pintAttAge As Integer) As Double
   NSP = m_adblNSP(pintAttAge)
End Property

Public Property Get CalcCV() As Boolean
   CalcCV = m_blnCalcCV
End Property
Public Property Let CalcCV(ByVal vData As Boolean)
   m_blnCalcCV = vData
End Property


Public Property Get CalcSC() As Boolean
   CalcSC = m_blnCalcSC
End Property

Public Sub CalcCashValue(ByVal pintEndAge As Integer, ByVal pintBeginAge As _
                        Integer, ByVal pfUseExpAllow As Boolean, ByVal _
                        pfUseEndow As Boolean, ByVal penuTiming As TimingType, _
                        ByVal penuProdType As eProductType, pintMortEndAge As _
                        Integer)
   ' Comments  :
   ' Parameters: pintEndAge
   '             pintBeginAge
   '             pfUseExpAllow -
   ' Modified  :
   '
   ' --------------------------------------------------
   'CV
   'TVCodeTools ErrorEnablerStart
   On Error GoTo PROC_ERR
   'TVCodeTools ErrorEnablerEnd
   
Dim intI As Integer
Dim sngMyIntRate As Single
Dim m_dblRc As Double
Dim dblDBTiming As Double
Dim dblPremiumTiming As Double
Dim dblAdjustPremTest(1) As Double
Dim dblPVFB As Double
Dim dblAnnuity As Double
Dim dblG As Double 'From Allan's documentation
Dim dblF As Double 'From Allan's documentation
Dim dblNFF1 As Double 'From Allan's documentation
Dim dblNFF2 As Double 'From Allan's documentation
Dim dblNFF3 As Double 'From Allan's documentation
Dim blnEndowIndicator As Boolean
Dim dblNFF2Num1 As Double 'From Allan's documentation
Dim dblNFF2Num2 As Double 'From Allan's documentation
Dim dblNFF2Den As Double 'From Allan's documentation
Dim dblNetPremium As Double
   
'   dblG = 1000 * (gsavCommFns.Mx(pintBeginAge) - _
'     gsavCommFns.Mx(pintEndAge) + _
'     gsavCommFns.Dx(pintEndAge) * -1 * blnEndowIndicator) / _
'     (gsavCommFns.Nx(pintBeginAge) - _
'     gsavCommFns.Nx(gsavPremium.PremiumCeaseAge))
'   dblF = 1000 * (gsavCommFns.Mx(pintBeginAge)) / _
'     (gsavCommFns.Nx(pintBeginAge) - _
'     gsavCommFns.Nx(pintBeginAge + 20))
'   If dblG <= dblF Then
'      dblNFF1 = 1000 * (gsavCommFns.Mx(pintBeginAge) - _
'     gsavCommFns.Mx(pintBeginAge + 1)) / _
'     (gsavCommFns.Dx(pintBeginAge))
'   Else
'      dblNFF1 = 0
'   End If
'   If dblG <= dblF Then
'      dblNFF2 = 1000 * (gsavCommFns.Mx(pintBeginAge + 1) - _
'     gsavCommFns.Mx(pintEndAge) + _
'     gsavCommFns.Dx(pintEndAge) * -1 * blnEndowIndicator) / _
'     (gsavCommFns.Nx(pintBeginAge + 1) - _
'     gsavCommFns.Nx(gsavPremium.PremiumCeaseAge))
'   Else
'      dblNFF2Num1 = 1000 * (gsavCommFns.Mx(pintBeginAge + 1)) / _
'        (gsavCommFns.Nx(pintBeginAge + 1) - _
'        gsavCommFns.Nx(pintBeginAge + 20))
'      dblNFF2Num2 = 1000 * (gsavCommFns.Mx(pintBeginAge) - _
'        gsavCommFns.Mx(pintBeginAge + 1)) / _
'        (gsavCommFns.Dx(pintBeginAge))
'      If gsavPremium.PremiumCeaseAge - pintBeginAge >= 20 Then
'         dblNFF2Den = ((gsavCommFns.Nx(pintBeginAge) - _
'           gsavCommFns.Nx(pintBeginAge + 20)) / _
'           gsavCommFns.Dx(pintBeginAge))
'      Else
'         dblNFF2Den = ((gsavCommFns.Nx(pintBeginAge) - _
'           gsavCommFns.Nx(gsavPremium.PremiumCeaseAge)) / _
'           gsavCommFns.Dx(pintBeginAge))
'      End If
'      dblNFF2 = dblG + (dblNFF2Num1 - dblNFF2Num2) / dblNFF2Den
'   End If
'   If dblG <= dblF Then
'      dblNFF3 = 1000 * (gsavCommFns.Mx(pintBeginAge + 1) - _
'     gsavCommFns.Mx(pintEndAge) + _
'     gsavCommFns.Dx(pintEndAge) * -1 * blnEndowIndicator) / _
'     (gsavCommFns.Nx(pintBeginAge + 1) - _
'     gsavCommFns.Nx(gsavPremium.PremiumCeaseAge))
'   Else
'      dblNFF3 = dblG
'   End If
   If m_enuCVMethodFinal = CVMethodReserve Then
      sngMyIntRate = gsavIntRate.FinalInterestRate(intI - pintBeginAge)
      'If poIntRates.InterestRateType = 0 Then 'Constant
      '   sngMyIntRate = poIntRates.InterestRateConstant
      'Else
      '   sngMyIntRate = poIntRates.InterestRateVary(intI - pintBeginAge)
      'End If
      If penuTiming = stCurtate Then
         dblDBTiming = 1
         dblPremiumTiming = 1
      Else
         dblDBTiming = sngMyIntRate / Delta(sngMyIntRate)
         If penuTiming = stSemiContinuous Or gsavCommFns.Dx(intI) = 0 Then
            dblPremiumTiming = 1
         Else
            dblPremiumTiming = gsavCommFns.DBarx(intI) / gsavCommFns.Dx(intI)
         End If
      End If
      For intI = pintEndAge To pintBeginAge Step -1
         'm_adblAdjustedPremium(intI) = m_adblModifiedNetPremium(intI)
         'm_adblCashValue(intI) = m_adblModifiedReserves(intI)
         'm_adblNSP(intI) = gsavBenefits.DeathBenefit(intI) * dblDBTiming * gsavMortality.FinalMortality(intI) / (1 + sngMyIntRate) + (1 - gsavMortality.FinalMortality(intI)) * m_adblNSP(intI + 1) / (1 + sngMyIntRate)
         If gsavReserves.ModRes = stNetLevel Then
            m_adblAdjustedPremium(intI) = gsavReserves.NetAnnualPremiums(pintBeginAge)
         ElseIf gsavReserves.ModRes = stUnitary Or gsavReserves.ModRes = stILStandard Then
            m_adblAdjustedPremium(intI) = gsavReserves.ModifiedNetPremium(intI)
         Else
            If Not m_blnCVMessage(0) Then
               MsgBox "Modified (adjusted) premium is not available"
               m_blnCVMessage(0) = True
            End If
         End If
         If intI >= pintBeginAge And intI < pintEndAge Then
            m_adblCashValue(intI) = gsavBenefits.DeathBenefit(intI) * dblDBTiming * gsavMortality.FinalMortality(intI) / (1 + sngMyIntRate) + (1 - gsavMortality.FinalMortality(intI)) * m_adblCashValue(intI + 1) / (1 + sngMyIntRate)
            If intI < gsavPremium.PremiumCeaseAge Then
               m_adblCashValue(intI) = m_adblCashValue(intI) - m_adblAdjustedPremium(intI) * dblPremiumTiming
            End If
         ElseIf intI = pintEndAge Then
            m_adblCashValue(intI) = gsavBenefits.PVFB(intI)
         End If
      Next intI
   
   
'         For intI = pintEndAge To pintBeginAge Step -1
'            If poIntRates.InterestRateType = 0 Then 'Constant
'               sngMyIntRate = poIntRates.InterestRateConstant
'            Else
'               sngMyIntRate = poIntRates.InterestRateVary(intI - pintBeginAge)
'            End If
'            If penuTiming = stcurtate Then
'               dblDBTiming = 1
'               dblPremiumTiming = 1
'            Else
'               If Delta(sngMyIntRate) <> 0 Then
'                  dblDBTiming = sngMyIntRate / Delta(sngMyIntRate)
'               Else
'                  dblDBTiming = 1
'               End If
'               If penuTiming = stSemiContinuous Or gsavCommFns.Dx(intI) = 0 Then
'                  dblPremiumTiming = 1
'               Else
'                  dblPremiumTiming = gsavCommFns.DBarx(intI) / gsavCommFns.Dx(intI)
'               End If
'            End If
'         Next intI
'
   ElseIf m_enuCVMethodFinal = CVMethodNone Then
      For intI = pintEndAge To pintBeginAge Step -1
         m_adblCashValue(intI) = 0
         m_adblNSP(intI) = 0
      Next intI
   ElseIf m_enuCVMethodFinal = CVMethod1980 Then
      m_dblAAI = 0
      If gsavBenefits.DBType = stDBConstant Then
         m_dblAAI = gsavBenefits.DBConstant
      Else
         For intI = 1 To 10
            m_dblAAI = m_dblAAI + gsavBenefits.DeathBenefit(pintBeginAge + intI - 1)
         Next intI
         m_dblAAI = m_dblAAI / 10
      End If
      'gsavReserves.NetAnnualPremiums(pintBeginAge) = gsavBenefits.PVFB(pintBeginAge) / (gsavCommFns.Nx(pintBeginAge) - gsavCommFns.Nx(pintEndAge)) / gsavCommFns.Dx(pintBeginAge)
      '    If gsavBenefits.PVFB(pintBeginAge) / ((gsavCommFns.Nx(pintBeginAge) - gsavCommFns.Nx(pintEndAge)) / gsavCommFns.Dx(pintBeginAge)) < 0.04 * m_dblAAI Then   'Formula 15.2.7, p438
      '        m_dblEA_CV = 1.25 * gsavBenefits.PVFB(pintBeginAge) / ((gsavCommFns.Nx(pintBeginAge) - gsavCommFns.Nx(pintEndAge)) / gsavCommFns.Dx(pintBeginAge)) + 0.01 * m_dblAAI
      '    Else
      '        m_dblEA_CV = 0.06 * m_dblAAI
      '    End If
      'Formula 15.2.6 on page 437
      dblNetPremium = gsavBenefits.PVFB(pintBeginAge) / ((gsavCommFns.Nx(pintBeginAge) - gsavCommFns.Nx(gsavPremium.PremiumCeaseAge)) / gsavCommFns.Dx(pintBeginAge))
      If dblNetPremium < 0.04 * m_dblAAI Then  'Formula 15.2.7, p438
         m_dblEA_CV = 1.25 * dblNetPremium + 0.01 * m_dblAAI
      Else
         m_dblEA_CV = 0.06 * m_dblAAI
      End If
      If pfUseExpAllow Then
         m_dblRc = (gsavBenefits.PVFB(pintBeginAge) + m_dblEA_CV) / gsavPremium.PVFGP(pintBeginAge)
      Else
         m_dblRc = (gsavBenefits.PVFB(pintBeginAge)) / gsavPremium.PVFGP(pintBeginAge)
      End If
      For intI = pintEndAge To pintBeginAge Step -1
         sngMyIntRate = gsavIntRate.FinalInterestRate(intI - pintBeginAge)
         'If poIntRates.InterestRateType = 0 Then 'Constant
         '   sngMyIntRate = poIntRates.InterestRateConstant
         'Else
         '   sngMyIntRate = poIntRates.InterestRateVary(intI - pintBeginAge)
         'End If
         If penuTiming = stCurtate Then
            dblDBTiming = 1
            dblPremiumTiming = 1
         Else
            dblDBTiming = sngMyIntRate / Delta(sngMyIntRate)
            If penuTiming = stSemiContinuous Or gsavCommFns.Dx(intI) = 0 Then
               dblPremiumTiming = 1
            Else
               dblPremiumTiming = gsavCommFns.DBarx(intI) / gsavCommFns.Dx(intI)
            End If
         End If
         If intI >= pintBeginAge And intI < pintEndAge Then
            m_adblAdjustedPremium(intI) = m_dblRc * gsavPremium.FinalGrossPremiums(intI - pintBeginAge)
            'm_adblCashValue(intI) = gsavBenefits.DeathBenefit(intI) * dblDBTiming * gsavMortality.FinalMortality(intI) / (1 + sngMyIntRate) - m_adblAdjustedPremium(intI) * dblPremiumTiming + (1 - gsavMortality.FinalMortality(intI)) * m_adblCashValue(intI + 1) / (1 + sngMyIntRate)
            m_adblPVFAdjPrem(intI) = m_adblAdjustedPremium(intI) * dblPremiumTiming + (1 - gsavMortality.FinalMortality(intI)) * m_adblPVFAdjPrem(intI + 1) / (1 + sngMyIntRate)
            m_adblCashValue(intI) = gsavBenefits.PVFB(intI) - m_adblPVFAdjPrem(intI)
            m_adblNSP(intI) = gsavBenefits.DeathBenefit(intI) * dblDBTiming * gsavMortality.FinalMortality(intI) / (1 + sngMyIntRate) + (1 - gsavMortality.FinalMortality(intI)) * m_adblNSP(intI + 1) / (1 + sngMyIntRate)
         ElseIf intI = pintEndAge Then
            m_adblCashValue(intI) = gsavBenefits.PVFB(intI)
            m_adblPVFAdjPrem(intI) = 0
            m_adblNSP(intI) = gsavBenefits.PVFB(intI)
         End If
         'Debug.Print intI, gsavBenefits.PVFB(intI)
      Next intI
   ElseIf m_enuCVMethodFinal = CVMethod1941 Then 'Assume that it is a constant premium and death benefit
      For intI = pintEndAge To pintBeginAge Step -1
         sngMyIntRate = gsavIntRate.FinalInterestRate(intI - pintBeginAge)
         'If poIntRates.InterestRateType = 0 Then 'Constant
         '   sngMyIntRate = poIntRates.InterestRateConstant
         'Else
         '   sngMyIntRate = poIntRates.InterestRateVary(intI - pintBeginAge)
         'End If
         If penuTiming = stCurtate Then
            dblDBTiming = 1
            dblPremiumTiming = 1
         Else
            dblDBTiming = sngMyIntRate / Delta(sngMyIntRate)
            If penuTiming = stSemiContinuous Or gsavCommFns.Dx(intI) = 0 Then
               dblPremiumTiming = 1
            Else
               dblPremiumTiming = gsavCommFns.DBarx(intI) / gsavCommFns.Dx(intI)
            End If
         End If
         'If intI >= gsavPremium.PremiumCeaseAge Then
         '   dblPremiumTiming = 0
         'End If
         If intI = gsavPremium.PremiumCeaseAge Then
            'Adjusted premium calculations found on page 436 of Bowers 1, table 15.2
            dblPVFB = gsavCommFns.Mx(pintBeginAge) / gsavCommFns.Dx(pintBeginAge)
            dblAnnuity = (gsavCommFns.Nx(pintBeginAge)) / gsavCommFns.Dx(pintBeginAge)
            dblAdjustPremTest(0) = (dblPVFB + 0.02) / (dblAnnuity - 0.65) 'Line 1 or 2 if plan is WL
            If dblAdjustPremTest(0) > 0.04 Then 'Recalculate
               dblAdjustPremTest(0) = (dblPVFB + 0.046) / dblAnnuity 'Line 2 if plan is WL
            End If
            If pintEndAge >= pintMortEndAge And gsavPremium.PremiumCeaseAge >= pintMortEndAge Then
            'If penuProdType = ProductTraditionalWL Then '"Whole Life": Line 1 or 2
               m_adblAdjustedPremium(intI) = dblAdjustPremTest(0)
            Else 'Other
               dblPVFB = gsavBenefits.PVFB(pintBeginAge) / gsavBenefits.DBConstant
               If penuTiming <> stContinuous Then
                  dblAnnuity = (gsavCommFns.Nx(pintBeginAge) - gsavCommFns.Nx(gsavPremium.PremiumCeaseAge)) / gsavCommFns.Dx(pintBeginAge)
               Else
                  dblAnnuity = (gsavCommFns.NBarx(pintBeginAge) - gsavCommFns.NBarx(gsavPremium.PremiumCeaseAge)) / gsavCommFns.Dx(pintBeginAge)
               End If
               If dblAdjustPremTest(0) < 0.04 Then
                  dblAdjustPremTest(1) = (dblPVFB + 0.02) / (dblAnnuity - 0.65) 'Line 3 or 4 or 6
                  If dblAdjustPremTest(1) > dblAdjustPremTest(0) Then
                     dblAdjustPremTest(1) = (dblPVFB + 0.02 + 0.25 * dblAdjustPremTest(0)) / (dblAnnuity - 0.4) 'Line 4 or 6
                  End If
                  If dblAdjustPremTest(1) >= 0.04 Then
                     dblAdjustPremTest(1) = (dblPVFB + 0.036 + 0.25 * dblAdjustPremTest(0)) / (dblAnnuity) 'Line 6
                  End If
               Else
                  dblAdjustPremTest(1) = (dblPVFB + 0.02) / (dblAnnuity - 0.65) 'Line 5 or 7
                  If dblAdjustPremTest(1) >= 0.04 Then
                     dblAdjustPremTest(1) = (dblPVFB + 0.046) / (dblAnnuity) 'Line 7
                  End If
               End If
               m_adblAdjustedPremium(intI) = dblAdjustPremTest(1)
            End If
            m_adblAdjustedPremium(intI) = m_adblAdjustedPremium(intI) * gsavBenefits.DBConstant
'            m_adblCashValue(intI) = gsavBenefits.PVFB(intI)
'            m_adblNSP(intI) = gsavBenefits.PVFB(intI)
            If intI >= pintBeginAge And intI < pintEndAge Then
               m_adblCashValue(intI) = gsavBenefits.DeathBenefit(intI) * dblDBTiming * gsavMortality.FinalMortality(intI) / (1 + sngMyIntRate) + (1 - gsavMortality.FinalMortality(intI)) * m_adblCashValue(intI + 1) / (1 + sngMyIntRate)
               m_adblNSP(intI) = gsavBenefits.DeathBenefit(intI) * dblDBTiming * gsavMortality.FinalMortality(intI) / (1 + sngMyIntRate) + (1 - gsavMortality.FinalMortality(intI)) * m_adblNSP(intI + 1) / (1 + sngMyIntRate)
            Else
               m_adblCashValue(intI) = gsavBenefits.PVFB(intI)
               m_adblNSP(intI) = gsavBenefits.PVFB(intI)
            End If
         Else
            If intI > gsavPremium.PremiumCeaseAge Then
               m_adblAdjustedPremium(intI) = 0
            Else
               m_adblAdjustedPremium(intI) = m_adblAdjustedPremium(intI + 1) 'Adjusted premium will be constant for all durations
            End If
            If intI >= pintBeginAge And intI < pintEndAge Then
               m_adblCashValue(intI) = gsavBenefits.DeathBenefit(intI) * dblDBTiming * gsavMortality.FinalMortality(intI) / (1 + sngMyIntRate) - m_adblAdjustedPremium(intI) * dblPremiumTiming + (1 - gsavMortality.FinalMortality(intI)) * m_adblCashValue(intI + 1) / (1 + sngMyIntRate)
               m_adblNSP(intI) = gsavBenefits.DeathBenefit(intI) * dblDBTiming * gsavMortality.FinalMortality(intI) / (1 + sngMyIntRate) + (1 - gsavMortality.FinalMortality(intI)) * m_adblNSP(intI + 1) / (1 + sngMyIntRate)
            Else
               m_adblCashValue(intI) = gsavBenefits.PVFB(intI)
               m_adblNSP(intI) = gsavBenefits.PVFB(intI)
            End If
         End If
      Next intI
   End If
   If m_blnCalcSC Then
      For intI = pintEndAge To pintBeginAge Step -1
         m_adblCashValue(intI) = m_adblCashValue(intI) - m_asngSC(intI - pintBeginAge)
      Next intI
   End If
'   Debug.Print pintBeginAge, m_adblNSP
   
   'TVCodeTools ErrorHandlerStart
PROC_EXIT:
   Exit Sub
   
PROC_ERR:
   Err.Raise Err.Number
   'TVCodeTools ErrorHandlerEnd
   Resume Next
   Resume
End Sub

Public Sub CalcETICashValue(ByVal pintEndAge As Integer, ByVal pintBeginAge As _
                           Integer, ByVal pfUseExpAllow As Boolean, ByVal _
                           pfUseEndow As Boolean, ByVal penuTiming As TimingType, _
                           ByVal penuProdType As eProductType)
   ' Comments  :
   ' Parameters: pintEndAge
   '             pintBeginAge
   '             pfUseExpAllow -
   ' Modified  :
   '
   ' --------------------------------------------------
   'CV
   'TVCodeTools ErrorEnablerStart
   On Error GoTo PROC_ERR
   'TVCodeTools ErrorEnablerEnd
   
Dim intI As Integer
Dim sngMyIntRate As Single
Dim m_dblRc As Double
Dim dblDBTiming As Double
Dim dblPremiumTiming As Double
Dim dblAdjustPremTest(1) As Double
Dim dblPVFB As Double
Dim dblAnnuity As Double
Dim dblG As Double 'From Allan's documentation
Dim dblF As Double 'From Allan's documentation
Dim dblNFF1 As Double 'From Allan's documentation
Dim dblNFF2 As Double 'From Allan's documentation
Dim dblNFF3 As Double 'From Allan's documentation
Dim blnEndowIndicator As Boolean
Dim dblNFF2Num1 As Double 'From Allan's documentation
Dim dblNFF2Num2 As Double 'From Allan's documentation
Dim dblNFF2Den As Double 'From Allan's documentation
   
   
      For intI = pintEndAge To pintBeginAge Step -1
         sngMyIntRate = gsavIntRate.FinalInterestRate(intI - pintBeginAge)
         'If poIntRates.InterestRateType = 0 Then 'Constant
         '   sngMyIntRate = poIntRates.InterestRateConstant
         'Else
         '   sngMyIntRate = poIntRates.InterestRateVary(intI - pintBeginAge)
         'End If
         If penuTiming = stCurtate Then
            dblDBTiming = 1
            dblPremiumTiming = 1
         Else
            dblDBTiming = sngMyIntRate / Delta(sngMyIntRate)
            If penuTiming = stSemiContinuous Or gsavCommFns.Dx(intI) = 0 Then
               dblPremiumTiming = 1
            Else
               dblPremiumTiming = gsavCommFns.DBarx(intI) / gsavCommFns.Dx(intI)
            End If
         End If
         m_adblAdjustedPremium(intI) = 0
         m_adblCashValue(intI) = gsavBenefits.DeathBenefit(intI) * dblDBTiming * (gsavCommFns.Mx(pintBeginAge) - gsavCommFns.Mx(intI)) / gsavCommFns.Dx(pintBeginAge)
         m_adblPureEndowment(intI) = gsavBenefits.DeathBenefit(pintBeginAge) * gsavCommFns.Dx(intI) / gsavCommFns.Dx(pintBeginAge)
      Next intI
   
'   Debug.Print pintBeginAge, m_adblNSP
   
   'TVCodeTools ErrorHandlerStart
PROC_EXIT:
   Exit Sub
   
PROC_ERR:
   Err.Raise Err.Number
   'TVCodeTools ErrorHandlerEnd
   Resume Next
   Resume
End Sub

Public Sub CalcUnusualCV(ByVal pintEndAge As Integer, ByVal pintBeginAge As _
                        Integer, ByVal pfUseExpAllow As Boolean, ByVal pfUseEndow _
                        As Boolean, ByVal penuTiming As TimingType, ByVal _
                        penuProdType As eProductType)
   ' Comments  :
   ' Parameters: pintEndAge
   '             pintBeginAge
   '             pfUseExpAllow -
   ' Modified  :
   '
   ' --------------------------------------------------
   'CV
   'TVCodeTools ErrorEnablerStart
   On Error GoTo PROC_ERR
   'TVCodeTools ErrorEnablerEnd
   
Dim intI As Integer
Dim sngMyIntRate As Single
Dim m_dblRc As Double
Dim dblDBTiming As Double
Dim dblPremiumTiming As Double
Dim dblAdjustPremTest(1) As Double
Dim dblPVFB As Double
Dim dblAnnuity As Double
Dim dblG As Double 'From Allan's documentation
Dim dblF As Double 'From Allan's documentation
Dim dblNFF1 As Double 'From Allan's documentation
Dim dblNFF2 As Double 'From Allan's documentation
Dim dblNFF3 As Double 'From Allan's documentation
Dim blnEndowIndicator As Boolean
Dim dblNFF2Num1 As Double 'From Allan's documentation
Dim dblNFF2Num2 As Double 'From Allan's documentation
Dim dblNFF2Den As Double 'From Allan's documentation
Dim dblUnusualCVTest(130) As Double
   
   For intI = pintEndAge To pintBeginAge Step -1
      sngMyIntRate = gsavIntRate.FinalInterestRate(intI - pintBeginAge)
      'If poIntRates.InterestRateType = 0 Then 'Constant
      '   sngMyIntRate = poIntRates.InterestRateConstant
      'Else
      '   sngMyIntRate = poIntRates.InterestRateVary(intI - pintBeginAge)
      'End If
      If penuTiming = stCurtate Then
         dblDBTiming = 1
         dblPremiumTiming = 1
      Else
         dblDBTiming = sngMyIntRate / Delta(sngMyIntRate)
         If penuTiming = stSemiContinuous Or gsavCommFns.Dx(intI) = 0 Then
            dblPremiumTiming = 1
         Else
            dblPremiumTiming = gsavCommFns.DBarx(intI) / gsavCommFns.Dx(intI)
         End If
      End If
      m_adblAdjustedPremium(intI) = 0
      m_adblCashValue(intI) = gsavBenefits.DeathBenefit(intI) * dblDBTiming * (gsavCommFns.Mx(pintBeginAge) - gsavCommFns.Mx(intI)) / gsavCommFns.Dx(pintBeginAge)
      m_adblPureEndowment(intI) = gsavBenefits.DeathBenefit(pintBeginAge) * gsavCommFns.Dx(intI) / gsavCommFns.Dx(pintBeginAge)
   Next intI
   
'   Debug.Print pintBeginAge, m_adblNSP
   
   'TVCodeTools ErrorHandlerStart
PROC_EXIT:
   Exit Sub
   
PROC_ERR:
   Err.Raise Err.Number
   'TVCodeTools ErrorHandlerEnd
   Resume Next
   Resume
End Sub




Public Property Get CVMethod() As eCVMethod
   CVMethod = m_enuCVMethod
End Property
Public Property Get CVMethodFinal() As eCVMethod
   CVMethodFinal = m_enuCVMethodFinal
End Property

Public Property Let CVMethodFinal(ByVal vData As eCVMethod)
   m_enuCVMethodFinal = vData
End Property
Public Property Let CVMethod(ByVal vData As eCVMethod)
   m_enuCVMethod = vData
End Property

Public Property Get TestForUnusualCV() As Boolean
   TestForUnusualCV = m_blnTestForUnusualCV
End Property

Public Property Let TestForUnusualCV(ByVal vData As Boolean)
   m_blnTestForUnusualCV = vData
End Property

Public Property Get CompareToCV() As Boolean
   CompareToCV = m_blnCompareToCV
End Property

Public Property Let CompareToCV(ByVal vData As Boolean)
   m_blnCompareToCV = vData
End Property

Public Property Let CalcSC(ByVal vData As Boolean)
   m_blnCalcSC = vData
End Property


Public Property Get AAI() As Double
   AAI = m_dblAAI
End Property
Public Property Let AAI(ByVal vData As Double)
   m_dblAAI = vData
End Property

Public Property Get AdjustedPremium(ByVal iIndex1 As Integer) As Double
   AdjustedPremium = m_adblAdjustedPremium(iIndex1)
End Property
Public Property Let AdjustedPremium(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblAdjustedPremium(iIndex1) = vData
End Property

'Public Property Get SC(ByVal iIndex1 As Integer) As Single
'   SC = m_adblSC(iIndex1)
'End Property
Public Property Get SC(ByVal iIndex1 As Integer) As Single
   SC = m_asngSC(iIndex1)
End Property

Public Property Get PureEndowment(ByVal iIndex1 As Integer) As Double
   PureEndowment = m_adblPureEndowment(iIndex1)
End Property
Public Property Get CashValue(ByVal iIndex1 As Integer) As Double
   CashValue = m_adblCashValue(iIndex1)
End Property

Public Property Let CashValue(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblCashValue(iIndex1) = vData
End Property
Public Property Get UnusualCV(ByVal iIndex1 As Integer) As Double
   UnusualCV = m_adblUnusualCV(iIndex1)
End Property

Public Property Let UnusualCV(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblUnusualCV(iIndex1) = vData
End Property
Public Property Let SC(ByVal iIndex1 As Integer, ByVal vData As Single)
   m_asngSC(iIndex1) = vData
End Property

Public Property Let PureEndowment(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblPureEndowment(iIndex1) = vData
End Property

Public Property Get EA_CV() As Double
   EA_CV = m_dblEA_CV
End Property
Public Property Let EA_CV(ByVal vData As Double)
   m_dblEA_CV = vData
End Property
Public Sub ResetValues()

   Erase m_adblAdjustedPremium, m_adblCashValue, m_adblNSP, m_adblPureEndowment
   Erase m_adblPVFAdjPrem, m_adblUnusualCV, m_asngSC, m_blnCVMessage
   
   m_blnCalcCV = False
   m_blnCalcSC = False
   m_blnCompareToCV = False
   m_blnTestForUnusualCV = False
   m_dblAAI = 0
   m_dblEA_CV = 0
   m_enuCVMethod = 0
   m_enuCVMethodFinal = 0
   m_dblFinalTerminalCV = 0
   
End Sub
