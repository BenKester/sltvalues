VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltPremium"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'I am doing this so that the enums will keep their correct case.
#If False Then
   Dim stGPConstant, stGPVary
#End If
Public Enum GPType
   stGPConstant = 0
   stGPVary = 1
End Enum
Private m_asngPolicyFee(130) As Double
Private m_adblGrossPremiums(130) As Double
Private m_adblFinalGrossPremiums(130) As Double
Private m_intPremiumCeaseAge As Integer
Private m_lngGrossPremiumType As GPType
Private m_adblPVFGP(130) As Double
Private m_adbl_ax(130) As Double
Private m_adbBarx(130) As Double
Private m_enuPolicyFeeDefn As BasicMinimumOptions

Public Sub ResetValues()

   Erase m_adbl_ax, m_adblFinalGrossPremiums, m_adblGrossPremiums, m_adblPVFGP
   Erase m_asngPolicyFee
   
   m_enuPolicyFeeDefn = 0
   m_intPremiumCeaseAge = 0
   m_lngGrossPremiumType = 0
   
End Sub
Public Property Get GrossPremiums(ByVal iIndex1 As Integer) As Double
   GrossPremiums = m_adblGrossPremiums(iIndex1)
End Property
Public Property Get FinalGrossPremiums(ByVal iIndex1 As Integer) As Double
   FinalGrossPremiums = m_adblFinalGrossPremiums(iIndex1)
End Property

Public Property Get PolicyFee(ByVal iIndex1 As Integer) As Single
   PolicyFee = m_asngPolicyFee(iIndex1)
End Property

Public Property Let GrossPremiums(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblGrossPremiums(iIndex1) = vData
End Property
Public Property Let FinalGrossPremiums(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblFinalGrossPremiums(iIndex1) = vData
End Property

Public Property Let PolicyFee(ByVal iIndex1 As Integer, ByVal vData As Single)
   m_asngPolicyFee(iIndex1) = vData
End Property


Public Property Get GrossPremiumType() As Integer
   GrossPremiumType = m_lngGrossPremiumType
End Property
Public Property Get PolicyFeeDefn() As BasicMinimumOptions
   PolicyFeeDefn = m_enuPolicyFeeDefn
End Property

Public Property Let GrossPremiumType(ByVal vData As Integer)
   m_lngGrossPremiumType = vData
End Property
Public Property Let PolicyFeeDefn(ByVal vData As BasicMinimumOptions)
   m_enuPolicyFeeDefn = vData
End Property

Public Property Get aBarx(ByVal iIndex1 As Integer) As Double
   aBarx = m_adbBarx(iIndex1)
End Property
Public Property Let aBarx(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adbBarx(iIndex1) = vData
End Property

Public Property Get ax(ByVal iIndex1 As Integer) As Double
   ax = m_adbl_ax(iIndex1)
End Property
Public Property Let ax(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adbl_ax(iIndex1) = vData
End Property

Public Property Get PremiumCeaseAge() As Integer
   PremiumCeaseAge = m_intPremiumCeaseAge
End Property
Public Property Let PremiumCeaseAge(ByVal vData As Integer)
   m_intPremiumCeaseAge = vData
End Property

Public Property Get PVFGP(ByVal iIndex1 As Integer) As Double
   PVFGP = m_adblPVFGP(iIndex1)
End Property
Public Property Let PVFGP(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblPVFGP(iIndex1) = vData
End Property

Public Sub CalcPVFGrossPrems(ByVal pintEndAge As Integer, ByVal pintBeginAge As _
                              Integer, ByVal plngTiming As TimingType)
   
Dim intI As Integer
Dim intEndAge As Integer
Dim dblPremiumTiming As Double, dblInterestRate As Double, dblDBTiming As Double
   
   For intI = pintEndAge - 1 To pintBeginAge Step -1
      dblInterestRate = gsavIntRate.FinalInterestRate(intI - gsavInsured.IssueAge)
'      If pobjIntRates.InterestRateType = 0 Then  'Constant
'         dblInterestRate = pobjIntRates.InterestRateConstant
'      Else
'         dblInterestRate = pobjIntRates.InterestRateVary(intI - gsavInsured.IssueAge)
'      End If
      If plngTiming = stCurtate Then
         dblDBTiming = 1
         dblPremiumTiming = 1 'See formula 5.8.3 (first two terms on the right side)
      Else
         dblDBTiming = dblInterestRate / Delta(dblInterestRate)
         If plngTiming = stSemiContinuous Then
            dblPremiumTiming = 1 'See formula 5.8.3 (first two terms on the right side)
         Else 'continuous
            dblPremiumTiming = (1 - 1 / (1 + dblInterestRate)) / Delta(dblInterestRate) - Beta(CDbl(dblInterestRate)) * gsavMortality.FinalMortality(intI) / (1 + dblInterestRate)  'See formula 5.8.3 (first two terms on the right side)
         End If
      End If
      If intI < m_intPremiumCeaseAge Then
         'Both formulas follow 5.8.4 of Actuarial Mathematics on p209 1st Edition
         If intI = pintEndAge - 1 Then
            m_adbl_ax(intI) = 1 * dblPremiumTiming
            'Dave changed on 11/18/03
            'm_adblPVFGP(intI) = m_adblGrossPremiums(intI - gsavInsured.IssueAge) * dblPremiumTiming
            m_adblPVFGP(intI) = m_adblFinalGrossPremiums(intI - gsavInsured.IssueAge) * dblPremiumTiming
         Else
            m_adbl_ax(intI) = 1 * dblPremiumTiming + ((1 - gsavMortality.FinalMortality(intI)) * m_adbl_ax(intI + 1)) / (1 + dblInterestRate)
            'Dave changed on 11/18/03
            'm_adblPVFGP(intI) = m_adblGrossPremiums(intI - gsavInsured.IssueAge) * dblPremiumTiming + ((1 - gsavMortality.FinalMortality(intI)) * m_adblPVFGP(intI + 1)) / (1 + dblInterestRate)
            m_adblPVFGP(intI) = m_adblFinalGrossPremiums(intI - gsavInsured.IssueAge) * dblPremiumTiming + ((1 - gsavMortality.FinalMortality(intI)) * m_adblPVFGP(intI + 1)) / (1 + dblInterestRate)
         End If
      Else
         m_adbl_ax(intI) = 0
         m_adblPVFGP(intI) = 0
      End If
'      If intI = 65 Then Stop
      'Debug.Print m_adblPVFGP(intI)
   Next intI
   
End Sub
Public Sub CalcFinalGrossPrems(ByVal pintEndAge As Integer, ByVal pintBeginAge As _
                              Integer, ByVal penuBasicMinimum As BasicMinimumOptions)
   
Dim intI As Integer

   
   For intI = 0 To pintEndAge - pintBeginAge - 1
      If penuBasicMinimum = stMinimumOnly Then
         If m_enuPolicyFeeDefn = stBasicMinimum Or m_enuPolicyFeeDefn = stMinimumOnly Then
            m_adblFinalGrossPremiums(intI) = m_adblGrossPremiums(intI) + m_asngPolicyFee(intI)
         Else
            m_adblFinalGrossPremiums(intI) = m_adblGrossPremiums(intI)
         End If
      Else
         If m_enuPolicyFeeDefn = stBasicMinimum Or m_enuPolicyFeeDefn = stBasicOnly Then
            m_adblFinalGrossPremiums(intI) = m_adblGrossPremiums(intI) + m_asngPolicyFee(intI)
         Else
            m_adblFinalGrossPremiums(intI) = m_adblGrossPremiums(intI)
         End If
      End If
   Next intI
   
End Sub
