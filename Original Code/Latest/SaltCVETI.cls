VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltCVETI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'Option Explicit
'
'Public Sub GetETI(ByVal pintAge As Integer, ByVal pintDuration As Integer, _
'                   pdblCV As Double, pdblFA As Double, pintEndAge As Integer, _
'                   pdblPureEndow As Double, intYears As Integer, intDays As Integer)
''ByVal pintMaturityAge As Integer,
'Dim intAge As Integer
'Dim dblCV As Double
'Dim intAttAge As Integer
'Dim dblInterpolatedM As Double, dblMore As Double, strSql As String
'Dim dblLess As Double, datMaturity As Date, blnPastMaturity As Boolean
'Dim lngDays As Long
'Dim datTemp As Date, rsaProduct As New ADODB.Recordset
'Dim asngMortality(130) As Single
'Dim i As Integer, dblEndowPV As Double
'Dim enuCVMethod As eCVMethod
'
'   On Error GoTo GetETI_Err
'
''   rsaProduct.Close
''   rsaProduct.Open "Select * from tbl06Products where ProdID = " & mrsInquiryNow("ProdID"), gcnnMain, adOpenKeyset, adLockOptimistic
''   If rsaProduct.EOF Then
''      MsgBox "Could not find product record when calculating the ETI (Product ID#" & mrsInquiryNow("ProdID") & ")"
''      GoTo Exit_GetETI
''   End If
'   'enuCVMethod = rsaProduct("CVMethod")
'
'   'datMaturity = frmInquiry.txtExpiryDate
'   intAttAge = pintAge + pintDuration ' - 1
'
'   'oSaltCommon.IntRates.InterestRateConstant(stNonforfeitureMort) = _
'      rsProduct("CVInterest")
'   'oSaltCommon.CV.CVMethod = enuCVMethod - Not needed
'   'Cash value Mortality
'
'   'oSaltCommon.IntRates.InterestRateConstant(0) = rsaProduct("CVInterest")
'   'oSaltCommon.CV.CVMethod = enuCVMethod
'
'   'Cash value Mortality
'   'Call ReadAgeDur(eTableType.TableAttainedAge, "tbl07Insurance", gcnnMain, _
'   "tbl" & rsaProduct("ETIMort"), "AA", 0, asngMortality(), 0, 115)
'
'   For i = 0 To UBound(asngMortality)
'      oSaltCommon.Mortality.MortalityRate(0, i) = asngMortality(i) / 1000
'   Next i
'
''   Call oSaltCommon.Mortality.CalcMortality(intAge, stNonforfeitureMort, _
'      -1, -1, oSaltCommon.Segmentation, stSegmented)
'
'   With oSaltCommon.CommFunctions
'      Call .CalcCommFunctions(0, pintEndAge, oSaltCommon.IntRates, _
'         oSaltCommon.Mortality, stNonforfeitureMort)
'
'      'dblCV = Me.txtResultingCV
'      dblCV = pdblCV
'
'      If dblCV > 0 Then
'         dblInterpolatedM = Round(.Mx(stNonforfeitureMort, intAttAge) - _
'            (dblCV * .Dx(stNonforfeitureMort, intAttAge) / _
'            pdblFA))
'
'         If dblInterpolatedM <= 0 Then
'            pdatExpiry = Me.dtpETIExpiryDate
'            intYears = pintEndAge - (intAttAge) - 1
'            If .Dx(stNonforfeitureMort, intAttAge + intYears) > 0 Then
'               pdblPureEndow = (dblCV * .Dx(stNonforfeitureMort, intAttAge) - pdblFA * (.Mx(stNonforfeitureMort, intAttAge) - .Mx(stNonforfeitureMort, intAttAge + intYears))) / .Dx(stNonforfeitureMort, intAttAge + intYears)
'            Else
'               pdblPureEndow = 0
'            End If
'            Exit Sub
'         End If
'         For intAge = intAttAge To pintEndAge
'            If .Mx(stNonforfeitureMort, intAge) < dblInterpolatedM Then
'               intYears = intAge - (intAttAge) - 1
'               Exit For
'            End If
'         Next intAge
'         dblLess = .Mx(stNonforfeitureMort, intAttAge + intYears + 1)
'         dblMore = .Mx(stNonforfeitureMort, intAttAge + intYears) - _
'            dblLess
'         dblInterpolatedM = dblInterpolatedM - dblLess + 0 * .Mx(stNonforfeitureMort, _
'            intAttAge + intYears - 1)
'         ' Update the ETI days
'         lngDays = RoundUp(365 - (dblInterpolatedM / dblMore) * 365)
'         ' Make sure that the Expiry date is less than the maturity date
'         pdatExpiry = DateAdd("d", lngDays, DateAdd("yyyy", _
'            intYears, Me.dtpDate))
'         If pdatExpiry < datMaturity Then
'            pdblPureEndow = 0
'         Else
'            ' Get the pure endowment
'            'pdblPureEndow = dblCV - _
'               (.Mx(stNonforfeitureMort, intAttAge) - .Mx(stNonforfeitureMort, intAttAge)) / .Dx(stNonforfeitureMort, intAttAge)
'            ' = (CV - (Mx(maturity) - Mx(AttAge)) / Dx(AttAge))/nEx
'            'Me.txtETIPureEndowment = (Me.txtResultingCV - _
'               (.Mx(stNonforfeitureMort, intAttAge + intYears) - _
'               .Mx(stNonforfeitureMort, intAttAge)) / _
'               .Dx(stNonforfeitureMort, intAttAge)) / nEx
'            'Dave changed on 5/29/03
'            If .Dx(stNonforfeitureMort, intAttAge + intYears) > 0 Then
'               pdblPureEndow = (dblCV * .Dx(stNonforfeitureMort, intAttAge) - pdblFA * (.Mx(stNonforfeitureMort, intAttAge) - .Mx(stNonforfeitureMort, intAttAge + intYears))) / .Dx(stNonforfeitureMort, intAttAge + intYears)
'            Else
'               pdblPureEndow = 0
'            End If
'         End If
'         'pdblPureEndow = pdblPureEndow * .Dx(stNonforfeitureMort, intAttAge) / .Dx(stNonforfeitureMort, intAttAge + intYears)
'
'      Else
'      End If
'
'   End With
'
'Exit_GetETI:
'   gerrHandler.Pop
'   Exit Sub
'
'GetETI_Err:
'   MsgBox Err.Number & vbNewLine & _
'          Err.Description, vbInformation + vbOKOnly, "ETI CV Calculation"
'   Resume Exit_GetETI
'   Resume
'End Sub
