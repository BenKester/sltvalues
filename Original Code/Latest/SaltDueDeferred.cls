VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltDueDeferred"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private m_dblDeferredPremium As Double
Private m_dblDuePremium As Double

Public Sub ResetValues()

   m_dblDeferredPremium = 0
   m_dblDuePremium = 0
   
End Sub
Public Property Get DuePremium() As Double
   DuePremium = m_dblDuePremium
End Property
Public Property Let DuePremium(ByVal vData As Double)
   m_dblDuePremium = vData
End Property

Public Property Get DeferredPremium() As Double
   DeferredPremium = m_dblDeferredPremium
End Property
Public Property Let DeferredPremium(ByVal vData As Double)
   m_dblDeferredPremium = vData
End Property
