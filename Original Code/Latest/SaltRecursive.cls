VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltRecursive"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mdblNP As Double
Private mdblEOYValue As Double
Private mdblEndow As Double
Private mblnImmediateBenefit As Boolean
Private mblnContinuousPayment As Boolean
Private mintPremMode As Integer
Private mdblIntRate As Double
Private mdblMortalityRate As Double
Private mdblMortalityRateX_1 As Double
Private mdblBenefit As Double
Private mintType As Integer

Public Property Get dblEOYValue() As Double
   dblEOYValue = mdblEOYValue
End Property
Public Property Let dblEOYValue(ByVal vData As Double)
   mdblEOYValue = vData
End Property

Public Property Get dblBenefit() As Double
   dblBenefit = mdblBenefit
End Property
Public Property Let dblBenefit(ByVal vData As Double)
   mdblBenefit = vData
End Property

Public Property Get dblMortalityRate() As Double
   dblMortalityRate = mdblMortalityRate
End Property
Public Property Let dblMortalityRate(ByVal vData As Double)
   mdblMortalityRate = vData
End Property

Public Property Let dblMortalityRateX_1(ByVal vData As Double)
   mdblMortalityRateX_1 = vData
End Property
Public Property Get dblMortalityRateX_1() As Double
   dblMortalityRateX_1 = mdblMortalityRateX_1
End Property

Public Property Get dblIntRate() As Double
   dblIntRate = mdblIntRate
End Property
Public Property Let dblIntRate(ByVal vData As Double)
   mdblIntRate = vData
End Property

Public Property Get intPremMode() As Integer
   intPremMode = mintPremMode
End Property
Public Property Let intPremMode(ByVal vData As Integer)
   mintPremMode = vData
End Property

Public Property Get intType() As Integer
   intType = mintType
End Property
Public Property Let intType(ByVal vData As Integer)
   mintType = vData
End Property

Public Property Get blnImmediateBenefit() As Boolean
   blnImmediateBenefit = mblnImmediateBenefit
End Property
Public Property Let blnImmediateBenefit(ByVal vData As Boolean)
   mblnImmediateBenefit = vData
End Property

Public Property Get blnContinuousPayment() As Boolean
   blnContinuousPayment = mblnContinuousPayment
End Property
Public Property Let blnContinuousPayment(ByVal vData As Boolean)
   mblnContinuousPayment = vData
End Property

Public Property Get dblEndow() As Double
   dblEndow = mdblEndow
End Property
Public Property Let dblEndow(ByVal vData As Double)
   mdblEndow = vData
End Property

Public Property Get dblNP() As Double
   dblNP = mdblNP
End Property
Public Property Let dblNP(ByVal vData As Double)
   mdblNP = vData
End Property
Public Function CalcReserve(ByVal pdblOther As Double, ByVal pblnForward As Boolean) As Double

Dim dblPx As Double, dblV As Double
   
   dblV = v(mdblIntRate)
   
   If pblnForward Then
      dblPx = 1 - mdblMortalityRateX_1
      CalcReserve = mdblEndow + (pdblOther - DBAdjust * mdblBenefit * _
         mdblMortalityRateX_1 * dblV + PremAdjust(True) * mdblNP) / (dblPx * dblV)
   Else
      dblPx = 1 - mdblMortalityRate
      CalcReserve = (pdblOther + mdblEndow) * (dblPx) * dblV _
         + DBAdjust * mdblBenefit * mdblMortalityRate * dblV _
         - PremAdjust(True) * mdblNP
   End If
   
End Function
Public Function CalcLifeAnnuity(ByVal pdblOther As Double, ByVal pblnForward As Boolean) As Double

Dim dblPx As Double, dblV As Double
   
   dblV = v(mdblIntRate)
   
   If pblnForward Then
      dblPx = 1 - mdblMortalityRateX_1
      CalcLifeAnnuity = (pdblOther - PremAdjust(True) * mdblNP) / (dblPx * dblV)
   Else
      dblPx = 1 - mdblMortalityRate
      CalcLifeAnnuity = (pdblOther * dblPx * dblV) + PremAdjust(True) * mdblNP
   End If
   
End Function
Public Function CalcNSP(ByVal pdblOther As Double, ByVal pblnForward As Boolean) As Double

Dim dblPx As Double, dblV As Double
   
   dblV = v(mdblIntRate)
   
   If pblnForward Then
      dblPx = 1 - mdblMortalityRateX_1
      CalcNSP = mdblEndow + _
      (pdblOther - DBAdjust * mdblBenefit * dblMortalityRateX_1 * dblV) / (dblPx * dblV)
   Else
      dblPx = 1 - mdblMortalityRate
      CalcNSP = (pdblOther + mdblEndow) * (dblPx) * dblV _
      + DBAdjust * mdblBenefit * mdblMortalityRate * dblV
   End If
   
End Function
Public Function CalcIntAnnuity(ByVal pdblOther As Double, ByVal pblnForward As Boolean) As Double

Dim dblV As Double
   
   dblV = v(mdblIntRate)
   
   If pblnForward Then
      CalcIntAnnuity = (pdblOther * dblV) + PremAdjust(False) * mdblNP
   Else
      CalcIntAnnuity = (pdblOther - PremAdjust(False) * mdblNP) / dblV
   End If
   
End Function
Public Function Calc_lx(ByVal pdblOther As Double) As Double

Dim dblPx As Double
   
   dblPx = 1 - dblMortalityRateX_1
   Calc_lx = pdblOther * dblPx
   
End Function
Public Function Calc_Dx(ByVal pdblOtherCurtate As Double) As Double

Dim dblPx As Double, dblV As Double
   
   dblV = v(mdblIntRate)
   dblPx = 1 - dblMortalityRateX_1
   Calc_Dx = PremAdjust(True) * pdblOtherCurtate * dblPx * dblV
   
End Function
Public Function Calc_Cx(ByVal pdblDx_Curtate As Double) As Double

Dim dblPx As Double, dblV As Double
   
   dblV = v(mdblIntRate)
   Calc_Cx = DBAdjust * pdblDx_Curtate * mdblMortalityRate * dblV
   
End Function
Public Function Calc_Nx(ByVal pdblOther As Double, ByVal pdblDx As Double) As Double
   Calc_Nx = pdblOther + pdblDx
End Function
Public Function Calc_Mx(ByVal pdblOther As Double, ByVal pdblCx As Double) As Double
   Calc_Mx = pdblOther + pdblCx 'Backward only
End Function
Public Function Calc_Rx(ByVal pdblOther As Double, ByVal pdblMx As Double) As Double
   Calc_Rx = pdblOther + pdblMx 'Backward only
End Function
Public Function Calc_Sx(ByVal pdblOther As Double, ByVal pblnForward As Boolean, pdblNx As Double) As Double
   Calc_Sx = pdblOther + pdblNx
End Function
Public Function Calc_Dx_Curtate(ByVal pdblOther As Double) As Double

Dim dblPx As Double, dblV As Double
   
   dblV = v(mdblIntRate)
   dblPx = 1 - dblMortalityRateX_1
   Calc_Dx_Curtate = pdblOther * dblPx * dblV
   
End Function
Public Function DBAdjust() As Double
   
   If mblnImmediateBenefit Then
      DBAdjust = mdblIntRate / Log(1 + mdblIntRate)
   Else
      DBAdjust = 1
   End If
   
End Function
Public Function PremAdjust(pblnILC As Boolean) As Double

Dim dblPx As Double, dblV As Double
   
   dblV = v(mdblIntRate)
   dblPx = 1 - mdblMortalityRate
   
   If Not pblnILC Then 'Interest Annuity
      PremAdjust = DUpperM(mdblIntRate, 1) / DUpperM(mdblIntRate, mintPremMode)
   Else
      If blnContinuousPayment Then 'Continuous
         PremAdjust = Alpha(mdblIntRate, 0) - Beta(mdblIntRate, 0) * (1 - dblPx * dblV)
      Else
         If mintPremMode <> 1 Then 'Not Annual
            PremAdjust = Alpha(mdblIntRate, mintPremMode) - Beta(mdblIntRate, mintPremMode) * (1 - dblPx * dblV)
         Else
            PremAdjust = 1
         End If
      End If
   End If
   
End Function
Public Function CalcRecursive() As Double

Dim dblPx As Double, dblV As Double
   
   dblV = v(mdblIntRate)
   dblPx = 1 - mdblMortalityRate
   
   CalcRecursive = (mdblEOYValue + mdblEndow) * (dblPx) * dblV _
      + DBAdjust * mdblBenefit * mdblMortalityRate * dblV _
      - PremAdjust(True) * mdblNP
   
End Function
Public Sub ResetValues()
   
   mdblNP = 0
   mdblEOYValue = 0
   mdblEndow = 0
   mblnImmediateBenefit = 0
   mblnContinuousPayment = 0
   mintPremMode = 0
   mdblIntRate = 0
   mdblMortalityRate = 0
   mdblMortalityRateX_1 = 0
   mdblBenefit = 0
   mintType = 0
   
End Sub
