VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltOutput"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Dave changed on 11-19-2003
Private m_enuUnitDBRound As eRound
Private m_enuUnitTerminalCVRound As eRound
Private m_enuUnitMeanCVRound As eRound
Private m_enuUnitAdjustedPremRound As eRound
Private m_enuUnitTerminalReserveRound As eRound
Private m_enuUnitMeanReserveRound As eRound
Private m_enuUnitNetPremRound As eRound
Private m_enuTotalDBRound As eRound
Private m_enuTotalTerminalCVRound As eRound
Private m_enuTotalMeanCVRound As eRound
Private m_enuTotalAdjustedPremRound As eRound
Private m_enuTotalTerminalReserveRound As eRound
Private m_enuTotalMeanReserveRound As eRound
Private m_enuTotalNetPremRound As eRound

Private m_intUnitDBRound As Integer
Private m_intUnitTerminalCVRound As Integer
Private m_intUnitMeanCVRound As Integer
Private m_intUnitAdjustedPremRound As Integer
Private m_intUnitTerminalReserveRound As Integer
Private m_intUnitMeanReserveRound As Integer
Private m_intUnitNetPremRound As Integer
Private m_intTotalDBRound As Integer
Private m_intTotalTerminalCVRound As Integer
Private m_intTotalMeanCVRound As Integer
Private m_intTotalAdjustedPremRound As Integer
Private m_intTotalTerminalReserveRound As Integer
Private m_intTotalMeanReserveRound As Integer
Private m_intTotalNetPremRound As Integer

Public Property Get UnitDBRound() As Integer
   UnitDBRound = m_intUnitDBRound
End Property
Public Property Let UnitDBRound(ByVal vNewValue As Integer)
   m_intUnitDBRound = vNewValue
End Property

Public Property Get UnitTerminalCVRound() As Integer
   UnitTerminalCVRound = m_intUnitTerminalCVRound
End Property

Public Property Let UnitTerminalCVRound(ByVal vNewValue As Integer)
   m_intUnitTerminalCVRound = vNewValue
End Property

Public Property Get UnitMeanCVRound() As Integer
   UnitMeanCVRound = m_intUnitMeanCVRound
End Property

Public Property Let UnitMeanCVRound(ByVal vNewValue As Integer)
   m_intUnitMeanCVRound = vNewValue
End Property

Public Property Get UnitAdjustedPremRound() As Integer
   UnitAdjustedPremRound = m_intUnitAdjustedPremRound
End Property

Public Property Let UnitAdjustedPremRound(ByVal vNewValue As Integer)
   m_intUnitAdjustedPremRound = vNewValue
End Property

Public Property Get UnitTerminalReserveRound() As Integer
   UnitTerminalReserveRound = m_intUnitTerminalReserveRound
End Property

Public Property Let UnitTerminalReserveRound(ByVal vNewValue As Integer)
   m_intUnitTerminalReserveRound = vNewValue
End Property

Public Property Get UnitMeanReserveRound() As Integer
   UnitMeanReserveRound = m_intUnitMeanReserveRound
End Property

Public Property Let UnitMeanReserveRound(ByVal vNewValue As Integer)
   m_intUnitMeanReserveRound = vNewValue
End Property

Public Property Get UnitNetPremRound() As Integer
   UnitNetPremRound = m_intUnitNetPremRound
End Property

Public Property Let UnitNetPremRound(ByVal vNewValue As Integer)
   m_intUnitNetPremRound = vNewValue
End Property

Public Property Get TotalDBRound() As Integer
   TotalDBRound = m_intTotalDBRound
End Property

Public Property Let TotalDBRound(ByVal vNewValue As Integer)
   m_intTotalDBRound = vNewValue
End Property

Public Property Get TotalTerminalCVRound() As Integer
   TotalTerminalCVRound = m_intTotalTerminalCVRound
End Property

Public Property Let TotalTerminalCVRound(ByVal vNewValue As Integer)
   m_intTotalTerminalCVRound = vNewValue
End Property

Public Property Get TotalMeanCVRound() As Integer
   TotalMeanCVRound = m_intTotalMeanCVRound
End Property

Public Property Let TotalMeanCVRound(ByVal vNewValue As Integer)
   m_intTotalMeanCVRound = vNewValue
End Property

Public Property Get TotalAdjustedPremRound() As Integer
   TotalAdjustedPremRound = m_intTotalAdjustedPremRound
End Property

Public Property Let TotalAdjustedPremRound(ByVal vNewValue As Integer)
   m_intTotalAdjustedPremRound = vNewValue
End Property

Public Property Get TotalTerminalReserveRound() As Integer
   TotalTerminalReserveRound = m_intTotalTerminalReserveRound
End Property

Public Property Let TotalTerminalReserveRound(ByVal vNewValue As Integer)
   m_intTotalTerminalReserveRound = vNewValue
End Property

Public Property Get TotalMeanReserveRound() As Integer
   TotalMeanReserveRound = m_intTotalMeanReserveRound
End Property

Public Property Let TotalMeanReserveRound(ByVal vNewValue As Integer)
   m_intTotalMeanReserveRound = vNewValue
End Property

Public Property Get TotalNetPremRound() As Integer
   TotalNetPremRound = m_intTotalNetPremRound
End Property

Public Property Let TotalNetPremRound(ByVal vNewValue As Integer)
   m_intTotalNetPremRound = vNewValue
End Property

Public Property Get eUnitDBRound() As eRound
   eUnitDBRound = m_enuUnitDBRound
End Property

Public Property Let eUnitDBRound(ByVal vNewValue As eRound)
   m_enuUnitDBRound = vNewValue
End Property

Public Property Get eUnitTerminalCVRound() As eRound
   eUnitTerminalCVRound = m_enuUnitTerminalCVRound
End Property

Public Property Let eUnitTerminalCVRound(ByVal vNewValue As eRound)
   m_enuUnitTerminalCVRound = vNewValue
End Property

Public Property Get eUnitMeanCVRound() As eRound
   eUnitMeanCVRound = m_enuUnitMeanCVRound
End Property

Public Property Let eUnitMeanCVRound(ByVal vNewValue As eRound)
   m_enuUnitMeanCVRound = vNewValue
End Property

Public Property Get eUnitAdjustedPremRound() As eRound
   eUnitAdjustedPremRound = m_enuUnitAdjustedPremRound
End Property

Public Property Let eUnitAdjustedPremRound(ByVal vNewValue As eRound)
   m_enuUnitAdjustedPremRound = vNewValue
End Property

Public Property Get eUnitTerminalReserveRound() As eRound
   eUnitTerminalReserveRound = m_enuUnitTerminalReserveRound
End Property

Public Property Let eUnitTerminalReserveRound(ByVal vNewValue As eRound)
   m_enuUnitTerminalReserveRound = vNewValue
End Property

Public Property Get eUnitMeanReserveRound() As eRound
   eUnitMeanReserveRound = m_enuUnitMeanReserveRound
End Property

Public Property Let eUnitMeanReserveRound(ByVal vNewValue As eRound)
   m_enuUnitMeanReserveRound = vNewValue
End Property

Public Property Get eUnitNetPremRound() As eRound
   eUnitNetPremRound = m_enuUnitNetPremRound
End Property

Public Property Let eUnitNetPremRound(ByVal vNewValue As eRound)
   m_enuUnitNetPremRound = vNewValue
End Property

Public Property Get eTotalDBRound() As eRound
   eTotalDBRound = m_enuTotalDBRound
End Property

Public Property Let eTotalDBRound(ByVal vNewValue As eRound)
   m_enuTotalDBRound = vNewValue
End Property

Public Property Get eTotalTerminalCVRound() As eRound
   eTotalTerminalCVRound = m_enuTotalTerminalCVRound
End Property

Public Property Let eTotalTerminalCVRound(ByVal vNewValue As eRound)
   m_enuTotalTerminalCVRound = vNewValue
End Property

Public Property Get eTotalMeanCVRound() As eRound
   eTotalMeanCVRound = m_enuTotalMeanCVRound
End Property

Public Property Let eTotalMeanCVRound(ByVal vNewValue As eRound)
   m_enuTotalMeanCVRound = vNewValue
End Property

Public Property Get eTotalAdjustedPremRound() As eRound
   eTotalAdjustedPremRound = m_enuTotalAdjustedPremRound
End Property

Public Property Let eTotalAdjustedPremRound(ByVal vNewValue As eRound)
   m_enuTotalAdjustedPremRound = vNewValue
End Property

Public Property Get eTotalTerminalReserveRound() As eRound
   eTotalTerminalReserveRound = m_enuTotalTerminalReserveRound
End Property

Public Property Let eTotalTerminalReserveRound(ByVal vNewValue As eRound)
   m_enuTotalTerminalReserveRound = vNewValue
End Property

Public Property Get eTotalMeanReserveRound() As eRound
   eTotalMeanReserveRound = m_enuTotalMeanReserveRound
End Property

Public Property Let eTotalMeanReserveRound(ByVal vNewValue As eRound)
   m_enuTotalMeanReserveRound = vNewValue
End Property

Public Property Get eTotalNetPremRound() As eRound
   eTotalNetPremRound = m_enuTotalNetPremRound
End Property

Public Property Let eTotalNetPremRound(ByVal vNewValue As eRound)
   m_enuTotalNetPremRound = vNewValue
End Property
