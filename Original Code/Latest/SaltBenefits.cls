VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltBenefits"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variables to hold property values
Private m_adblDeathBenefit(130) As Double
Private m_adblPVFB(130) As Double
Private m_adblPVFB_NoEndow(130) As Double
Private m_adblPVFB_PE(130) As Double
Private m_dblDBConstant As Double
Private m_adblEndowAmount(130) As Double  ' Ben changed on 7/2/2003 from single value to array
Private m_intBenefitCeaseAge As Integer
Private m_dblUnits As Double 'Dave added on 11-19-2003
Private m_lngDBType As DBTypeConstants

'local variables
Private madblDb(130) As Double

'Public enums
#If False Then
   Dim stDBConstant, stDBVary
#End If
Public Enum DBTypeConstants
   stDBConstant = 0
   stDBVary = 1
End Enum
Public Property Let DBType(ByVal penuData As DBTypeConstants)
   m_lngDBType = penuData
End Property
Public Property Get DBType() As DBTypeConstants
   DBType = m_lngDBType
End Property

Public Property Get DBConstant() As Double
   DBConstant = m_dblDBConstant
End Property
Public Property Get BenefitUnits() As Double
   BenefitUnits = m_dblUnits 'Dave added on 11-19-2003
End Property

Public Property Let DBConstant(ByVal pdblDBConstant As Double)
   m_dblDBConstant = pdblDBConstant
End Property
Public Property Let BenefitUnits(ByVal pdblUnits As Double)
   m_dblUnits = pdblUnits 'Dave added on 11-19-2003
End Property

Public Property Get DeathBenefit(ByVal iIndex1 As Integer) As Double
   DeathBenefit = m_adblDeathBenefit(iIndex1)
End Property
Public Property Let DeathBenefit(ByVal iIndex1 As Integer, ByVal vData As Double)
    m_adblDeathBenefit(iIndex1) = vData
End Property

Public Property Get EndowAmount(ByVal pintAge As Integer) As Double
   EndowAmount = m_adblEndowAmount(pintAge)
End Property
Public Property Let EndowAmount(ByVal pintAge As Integer, ByVal vData As Double)
   m_adblEndowAmount(pintAge) = vData
End Property

Public Property Get PVFB(ByVal iIndex1 As Integer) As Double
   PVFB = m_adblPVFB(iIndex1)
End Property
Public Property Get PVFB_NoEndow(ByVal iIndex1 As Integer) As Double
   PVFB_NoEndow = m_adblPVFB_NoEndow(iIndex1)
End Property

Public Property Get PVFB_PE(ByVal iIndex1 As Integer) As Double
   PVFB_PE = m_adblPVFB_PE(iIndex1)
End Property


Public Property Let PVFB(ByVal iIndex1 As Integer, ByVal vData As Double)
    m_adblPVFB(iIndex1) = vData
End Property

Public Property Let PVFB_NoEndow(ByVal iIndex1 As Integer, ByVal vData As Double)
    m_adblPVFB_NoEndow(iIndex1) = vData
End Property


Public Property Let PVFB_PE(ByVal iIndex1 As Integer, ByVal vData As Double)
    m_adblPVFB_PE(iIndex1) = vData
End Property

Public Property Get BenefitCeaseAge() As Integer
    BenefitCeaseAge = m_intBenefitCeaseAge
End Property
Public Property Let BenefitCeaseAge(ByVal pintBenefitCeaseAge As Integer)
   m_intBenefitCeaseAge = pintBenefitCeaseAge
End Property

Public Sub CalcDB(pintIssueAge As Integer)
Dim intI As Integer

   If m_lngDBType = stDBConstant Then
      For intI = pintIssueAge To m_intBenefitCeaseAge - 1
         madblDb(intI) = m_dblDBConstant
         m_adblDeathBenefit(intI) = m_dblDBConstant
      Next intI
   Else
      For intI = pintIssueAge To m_intBenefitCeaseAge - 1
         madblDb(intI) = m_adblDeathBenefit(intI)
      Next intI
   End If
   
End Sub
Public Sub CalcPVFB(ByVal pintBeginAge As Integer, ByVal pintEndAge As Integer, _
                     ByVal pintTiming As TimingType, _
                     ByVal penuTerminalType As enuTerminalIndicator)

Dim intI As Integer, sngMyIntRate As Single, blnEndowCV As Boolean, blnEndowBasicUnitary As Boolean
Dim blnEndowMinUnitary As Boolean

   ' Ben changed on 7/2/2003
   'If pfUseEndow Then
   '   m_dblEndowAmount = m_dblEndowAmount
   'Else
   '   m_dblEndowAmount = 0
   'End If
   blnEndowCV = False
   blnEndowBasicUnitary = False
   blnEndowMinUnitary = False
   
   If (gsavReserves.ModRes = stXXX1999 Or gsavReserves.ModRes = stNY147 Or gsavReserves.ModRes = stXXX1995) Then
      If gsavReserves.EndowOptionXXX = stNotUsed Then
      Else
         If (penuTerminalType = stBasicSegmented And _
            (gsavReserves.EndowChoiceXXX = stBasicMinimum Or gsavReserves.EndowChoiceXXX = stBasicOnly)) _
            Or (penuTerminalType = stMinimumSegmented And _
            (gsavReserves.EndowChoiceXXX = stBasicMinimum Or gsavReserves.EndowChoiceXXX = stMinimumOnly)) Then
            If gsavReserves.EndowOptionXXX = stEndowCV Then
               blnEndowCV = True
            ElseIf penuTerminalType = stBasicSegmented Then
               blnEndowBasicUnitary = True
            ElseIf penuTerminalType = stMinimumSegmented Then
               blnEndowMinUnitary = True
            End If
         End If
      End If
   End If
   'Dave added endowment amount that varies by duration on 7/2/03
   'mvarPVFB(pintEndAge + 1) = 0
   For intI = pintEndAge To pintBeginAge Step -1
      sngMyIntRate = gsavIntRate.FinalInterestRate(intI - pintBeginAge) ' padblFinalInterestRate(intI - pintBeginAge)
      'If pobjIntRates.InterestRateType = 0 Then 'Constant
      '   sngMyIntRate = pobjIntRates.InterestRateConstant
      'Else
      '   sngMyIntRate = pobjIntRates.InterestRateVary(intI - pintBeginAge)
      'End If
      
      If intI < pintEndAge Then
         If intI > pintBeginAge Then
            m_adblPVFB_PE(intI) = m_adblEndowAmount(intI - 1) + (m_adblPVFB_PE(intI + 1)) * (1 - gsavMortality.FinalMortality(intI)) / (1 + sngMyIntRate)
         Else
            m_adblPVFB_PE(intI) = (m_adblPVFB_PE(intI + 1)) * (1 - gsavMortality.FinalMortality(intI)) / (1 + sngMyIntRate)
         End If
         If pintTiming = stCurtate Then
'            m_adblPVFB(intI) = (madblDb(intI) * gsavMortality.FinalMortality(intI) + ((1 - gsavMortality.FinalMortality(intI)) * m_adblPVFB(intI + 1))) / (1 + sngMyIntRate)
            m_adblPVFB_NoEndow(intI) = (madblDb(intI) * gsavMortality.FinalMortality(intI) + ((1 - gsavMortality.FinalMortality(intI)) * m_adblPVFB_NoEndow(intI + 1))) / (1 + sngMyIntRate)
         Else
'            m_adblPVFB(intI) = (madblDb(intI) * gsavMortality.FinalMortality(intI) * (sngMyIntRate / Delta(sngMyIntRate)) + ((1 - gsavMortality.FinalMortality(intI)) * m_adblPVFB(intI + 1))) / (1 + sngMyIntRate)
            m_adblPVFB_NoEndow(intI) = (madblDb(intI) * gsavMortality.FinalMortality(intI) * (sngMyIntRate / Delta(sngMyIntRate)) + ((1 - gsavMortality.FinalMortality(intI)) * m_adblPVFB_NoEndow(intI + 1))) / (1 + sngMyIntRate)
         End If
'         m_adblPVFB(intI) = m_adblPVFB(intI) ' + m_adblEndowAmount(intI)
       '  If m_adblPVFB(intI) > m_adblPVFB(intI + 1) Then Stop
      ElseIf intI = pintEndAge Then
         'mvarPVFB(i) = mvarPVFB(i) + (1 - mvarFinalMortality(i)) * mvarEndowAmount / (1 + MyIntRate)
'         m_adblPVFB(intI) = m_adblEndowAmount(intI)
         m_adblPVFB_NoEndow(intI) = 0
         If intI = m_intBenefitCeaseAge Then
            m_adblPVFB_PE(intI) = m_adblEndowAmount(pintEndAge - 1)
         Else
            If blnEndowCV Then
               m_adblPVFB_PE(intI) = gsavCV.CashValue(pintEndAge)
            ElseIf blnEndowMinUnitary Then
               m_adblPVFB_PE(intI) = gsavReserves.TerminalReserveMinimumUnitary(pintEndAge)
            ElseIf blnEndowBasicUnitary Then
               m_adblPVFB_PE(intI) = gsavReserves.BasicUnitary(pintEndAge)
            Else
               m_adblPVFB_PE(intI) = m_adblEndowAmount(pintEndAge - 1)
            End If
         End If
      End If
      m_adblPVFB(intI) = m_adblPVFB_NoEndow(intI) + m_adblPVFB_PE(intI)
   Next intI
   If pintEndAge > 0 Then
      m_adblPVFB_PE(pintEndAge) = m_adblEndowAmount(pintEndAge - 1)
      m_adblPVFB(pintEndAge) = m_adblPVFB_PE(pintEndAge)
   End If
End Sub
Public Sub ResetValues()

   Erase m_adblDeathBenefit, m_adblEndowAmount, m_adblPVFB, m_adblPVFB_NoEndow
   Erase m_adblPVFB_PE
   m_dblDBConstant = 0
   m_intBenefitCeaseAge = 0
   m_lngDBType = stDBConstant
   
End Sub
