VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltIntRates"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'I am doing this so that the enum values will keep their correct case.
#If False Then
   Dim stConstant, stVaryByDuration
#End If
Public Enum InterestRateType
   stConstant = 0
   stVaryByDuration = 1
End Enum

Private m_enuInterestRateType As InterestRateType 'local copy
Private m_adblInterestRateConstant As Double 'local copy
Private m_adblInterestRateVary(130) As Double 'local copy
Private m_adblMinimumInterestRate(130) As Double
Private m_adblFinalInterestRate(130) As Double
Private m_adblNonforfeitureInterestRate As Double 'Only to be used in a reserve calculation where they need a CV interest rate (e.g. XXX Test for unusual CV's)
Public Function ResetValues() As Boolean

   On Error GoTo Err_ResetValues
   Erase m_adblFinalInterestRate, m_adblInterestRateVary, m_adblMinimumInterestRate
   
   m_enuInterestRateType = 0
   m_adblInterestRateConstant = 0
   m_adblNonforfeitureInterestRate = 0
   ResetValues = True
   
Exit_ResetValues:
   Exit Function
   
Err_ResetValues:
   gobjSAVE.FoundError Err.Description, Err.Number, "SaltIntRates.ResetValues"
   Resume Exit_ResetValues
   Resume
   
End Function
Public Property Get InterestRateType() As Integer
   InterestRateType = m_enuInterestRateType
End Property
Public Property Let InterestRateType(ByVal vData As Integer)
   m_enuInterestRateType = vData
End Property

Public Property Get InterestRateConstant() As Double
   InterestRateConstant = m_adblInterestRateConstant
End Property
Public Property Get NonforfeitureInterestRate() As Double
   NonforfeitureInterestRate = m_adblNonforfeitureInterestRate
End Property

Public Property Let InterestRateConstant(ByVal vData As Double)
   m_adblInterestRateConstant = vData
End Property
Public Property Let NonforfeitureInterestRate(ByVal vData As Double)
   m_adblNonforfeitureInterestRate = vData
   
End Property


Public Property Get InterestRateVary(ByVal iIndex1 As Integer) As Double
   InterestRateVary = m_adblInterestRateVary(iIndex1)
End Property

Public Property Let InterestRateVary(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblInterestRateVary(iIndex1) = vData
End Property

Public Property Get MinimumInterestRate(ByVal iIndex1 As Integer) As Double
   MinimumInterestRate = m_adblMinimumInterestRate(iIndex1)
End Property

Public Property Let MinimumInterestRate(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblMinimumInterestRate(iIndex1) = vData
End Property

Public Property Get FinalInterestRate(ByVal iIndex1 As Integer) As Double
   FinalInterestRate = m_adblFinalInterestRate(iIndex1)
End Property

Public Property Let FinalInterestRate(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblFinalInterestRate(iIndex1) = vData
End Property

Public Function CalcInterestRate(ByVal pintIssueAge As Integer, pintEndAge As _
                                 Integer, ByVal penuTerminalMethod As _
                                 enuTerminalIndicator, ByVal strReportHeader As _
                                 String) As Boolean

Dim intIndex As Integer
   
   On Error GoTo Err_CalcInterestRate
   For intIndex = 0 To pintEndAge - pintIssueAge - 1 'Final interest rate starts at index value of 0 for duration 1
      If penuTerminalMethod = stMinimumOneHalfCx Or penuTerminalMethod = stMinimumSegmented Or _
        penuTerminalMethod = stMinimumUnitary Then
         m_adblFinalInterestRate(intIndex) = m_adblMinimumInterestRate(intIndex)
      Else
         If m_enuInterestRateType = stConstant Then 'Constant
            m_adblFinalInterestRate(intIndex) = m_adblInterestRateConstant
         Else
            m_adblFinalInterestRate(intIndex) = m_adblInterestRateVary(intIndex)
         End If
      End If
   Next intIndex
   For intIndex = pintEndAge - pintIssueAge To 130 'Need to go to the end of the mortality table for certain calculations (e.g. 19 Pay premium test)
      m_adblFinalInterestRate(intIndex) = m_adblFinalInterestRate(intIndex - 1)
   Next intIndex
   CalcInterestRate = True
   
Exit_CalcInterestRate:
   Exit Function
   
Err_CalcInterestRate:
   gobjSAVE.FoundError Err.Description, Err.Number, "SaltIntRates.CalcInterestRate"
   Resume Exit_CalcInterestRate
   Resume
   
End Function
