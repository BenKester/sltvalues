VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaltSegmentation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
Private m_VarSegmentDefn As SegmentDefn 'local copy
Private m_adblVarGPRatios(130) As Double 'local copy
Private m_adblGrossPremiumSegment(130) As Double 'local copy
Private m_adblVarMortRatios(130) As Double 'local copy
Private m_aintVarSegments(130) As Integer  'local copy
Private m_aintIntSegmentGroup(130) As Integer
Private m_intSegmentCount As Integer
Private m_blnSegmentUsePolicyFee As Boolean
Public Property Get GPRatios(ByVal iIndex1 As Integer) As Double
   GPRatios = m_adblVarGPRatios(iIndex1)
End Property
Public Property Let GPRatios(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblVarGPRatios(iIndex1) = vData
End Property
Public Property Get GrossPremiumSegment(ByVal iIndex1 As Integer) As Double
   GrossPremiumSegment = m_adblGrossPremiumSegment(iIndex1)
End Property

Public Property Let GrossPremiumSegment(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblGrossPremiumSegment(iIndex1) = vData
End Property

Public Property Get MortRatios(ByVal iIndex1 As Integer) As Double
   MortRatios = m_adblVarMortRatios(iIndex1)
End Property
Public Property Let MortRatios(ByVal iIndex1 As Integer, ByVal vData As Double)
   m_adblVarMortRatios(iIndex1) = vData
End Property

Public Sub ResetValues()
   m_VarSegmentDefn = 0
   Erase m_adblVarGPRatios, m_adblGrossPremiumSegment
   Erase m_adblVarMortRatios, m_aintVarSegments, m_aintIntSegmentGroup
   m_intSegmentCount = 0
   m_blnSegmentUsePolicyFee = False
End Sub
Public Property Let SegmentDefn(ByVal vData As Integer)
   m_VarSegmentDefn = vData
End Property
Public Property Get SegmentDefn() As Integer
   SegmentDefn = m_VarSegmentDefn
End Property

Public Property Get SegmentCount() As Integer
   SegmentCount = m_intSegmentCount
End Property
Public Property Get SegmentUsePolicyFee() As Boolean
   SegmentUsePolicyFee = m_blnSegmentUsePolicyFee
End Property

Public Property Let SegmentCount(ByVal vData As Integer)
   m_intSegmentCount = vData
End Property
Public Property Let SegmentUsePolicyFee(ByVal vData As Boolean)
   m_blnSegmentUsePolicyFee = vData
End Property

Public Property Get Segments(ByVal iIndex1 As Integer) As Integer
   Segments = m_aintVarSegments(iIndex1)
End Property
Public Property Let Segments(ByVal iIndex1 As Integer, ByVal vData As Integer)
   m_aintVarSegments(iIndex1) = vData
End Property

Public Property Get SegmentsGroup(ByVal iIndex1 As Integer) As Integer
   SegmentsGroup = m_aintIntSegmentGroup(iIndex1)
End Property
Public Property Let SegmentsGroup(ByVal iIndex1 As Integer, ByVal vData As Integer)
   m_aintIntSegmentGroup(iIndex1) = vData
End Property

Public Sub CalcSegments(pintBeginAge As Integer, penuSegmentType As SegmentType)

Dim intI As Integer
Dim adblPrem(1) As Double
   
   'For intI = m_intVarIssueAge To m_intVarBenefitCeaseAge - 2
   If penuSegmentType = stUnitaryMethod Then
      
      m_intSegmentCount = 1
      m_aintIntSegmentGroup(m_intSegmentCount) = gsavBenefits.BenefitCeaseAge
      m_aintVarSegments(gsavBenefits.BenefitCeaseAge) = gsavBenefits.BenefitCeaseAge
      For intI = gsavBenefits.BenefitCeaseAge - 1 To pintBeginAge Step -1
         m_aintVarSegments(intI) = m_aintVarSegments(intI + 1)
         m_adblGrossPremiumSegment(intI) = 0
      Next intI
   Else
      For intI = pintBeginAge To gsavBenefits.BenefitCeaseAge - 2
         If intI >= gsavPremium.PremiumCeaseAge - 1 Then
            m_adblVarGPRatios(intI) = 0
         Else
            adblPrem(0) = gsavPremium.GrossPremiums(intI - pintBeginAge)
            adblPrem(1) = gsavPremium.GrossPremiums(intI - pintBeginAge + 1)
            If m_blnSegmentUsePolicyFee Then
               adblPrem(0) = adblPrem(0) + gsavPremium.PolicyFee(intI - pintBeginAge)
               adblPrem(1) = adblPrem(1) + gsavPremium.PolicyFee(intI - pintBeginAge + 1)
            End If
            If adblPrem(1) = 0 Then
               m_adblVarGPRatios(intI) = 0 ' 9999
            ElseIf adblPrem(0) = 0 Then
               m_adblVarGPRatios(intI) = 9999
            ElseIf gsavBenefits.DeathBenefit(intI + 1) = 0 Or gsavBenefits.DeathBenefit(intI) = 0 Then 'Not sure if this is correct but need to do something so it doesn't error out.
               m_adblVarGPRatios(intI) = 0 '9999
            Else
               m_adblVarGPRatios(intI) = (adblPrem(1) / gsavBenefits.DeathBenefit(intI + 1)) / _
                 (adblPrem(0) / gsavBenefits.DeathBenefit(intI))
            End If
         End If
         m_adblGrossPremiumSegment(intI) = adblPrem(0)
         If intI = gsavBenefits.BenefitCeaseAge - 2 Then
            m_adblGrossPremiumSegment(intI + 1) = adblPrem(1)
            m_adblVarGPRatios(intI + 1) = 1
            m_adblVarMortRatios(intI + 1) = 1
         End If
         'Will need to change the mortality to be consistent w/ XXX
         'pomortality.MortalityRate(i,i)
         'm_adblVarMortRatios(intI) = gsavMortality.MortalityRate(intI + 1) / gsavMortality.MortalityRate(intI)
         m_adblVarMortRatios(intI) = gsavMortality.FinalMortality(intI + 1) / gsavMortality.FinalMortality(intI)
        ' m_VarSegmentDefn = stMultiplyBy1_01
         If m_VarSegmentDefn = stAdd1Pct Then
            m_adblVarMortRatios(intI) = m_adblVarMortRatios(intI) + 0.01
         ElseIf m_VarSegmentDefn = stMultiplyBy1_01 Then
            m_adblVarMortRatios(intI) = m_adblVarMortRatios(intI) * 1.01
         ElseIf m_VarSegmentDefn = stSubtract1Pct Then
            m_adblVarMortRatios(intI) = m_adblVarMortRatios(intI) - 0.01
         ElseIf m_VarSegmentDefn = stMultiplyBy_99 Then
            m_adblVarMortRatios(intI) = m_adblVarMortRatios(intI) * 0.99
         End If
         m_adblVarMortRatios(intI) = MAX(m_adblVarMortRatios(intI), 1)
      Next intI
   
     
      m_intSegmentCount = 1
      m_aintIntSegmentGroup(m_intSegmentCount) = gsavBenefits.BenefitCeaseAge
      m_aintVarSegments(gsavBenefits.BenefitCeaseAge) = gsavBenefits.BenefitCeaseAge
      
      For intI = gsavBenefits.BenefitCeaseAge - 1 To pintBeginAge Step -1
         If m_adblVarGPRatios(intI) > m_adblVarMortRatios(intI) Then
            m_intSegmentCount = m_intSegmentCount + 1
            m_aintIntSegmentGroup(m_intSegmentCount) = intI + 1
            m_aintVarSegments(intI) = intI + 1
         Else
            m_aintVarSegments(intI) = m_aintVarSegments(intI + 1)
         End If
      Next intI
   End If
   m_aintIntSegmentGroup(m_intSegmentCount + 1) = pintBeginAge
   
End Sub
