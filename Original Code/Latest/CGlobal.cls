VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGlobal"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mcnnMain As ADODB.Connection
Private Function SetFormat(ByVal pintType As Integer, ByVal pintGender As eGenderConstants, _
   pintTobacco As eTobacco, ByVal pintUW As eUnderwriting)
   If pintType = 8 Then
      SetFormat = "Z"
   ElseIf pintType = 7 Then
      SetFormat = "T" & pintTobacco
   ElseIf pintType = 6 Then
      SetFormat = "U" & pintUW
   ElseIf pintType = 5 Then
      SetFormat = "G" & pintGender
   ElseIf pintType = 4 Then
      SetFormat = "U" & pintUW & "T" & pintTobacco
   ElseIf pintType = 3 Then
      SetFormat = "G" & pintGender & "T" & pintTobacco
   ElseIf pintType = 2 Then
      SetFormat = "G" & pintGender & "U" & pintUW
   ElseIf pintType = 1 Then
      SetFormat = "G" & pintGender & "U" & pintUW & "T" & pintTobacco
   End If
End Function
Private Function GetEnteredValue(ByVal pfldField As ADODB.Field, _
                                Optional ByVal pvntIfNull As Variant = "", _
                                Optional ByVal pblnIsFormatted As Boolean = False) As Variant
   If IsNull(pfldField.Value) Then
      If pfldField.Type = adDate Then
         GetEnteredValue = Format(Now(), "MM/DD/YYYY")
      Else
         GetEnteredValue = pvntIfNull
      End If
   Else
      If pblnIsFormatted = True Then
         GetEnteredValue = Format(pfldField.Value, "#0.00##")
      Else
         GetEnteredValue = pfldField.Value
      End If
   End If
End Function
' Purpose  : Populates an array with values from a table
' Inputs   :
'  pstrTableName - String specifying the name of the table or query to use
'  pdbInput - DAO Database object specifying the database that pstrTableName is in
'  pstrDataFieldName - String specifying the name of the data field (ie - "Age 0")
'  pstrIndexFieldName - String specifying the name of the index field (ie - duration,
'     age, ID)
'  pintInitialArrayIndex - Integer specifying the index at which to begin populating
'     the array
'  pasngOutput() - Variant array specifying the array to be populated
'  plngFirstValue - Long specifying the first index value to retrieve
'  plngLastValue - Long specifying the last index value to retrieve (-999 or omit to get
'     all of the indexes)
'  pintProdID - Integer specifying the product ID to get (-999 or omit to not check the
'     product ID)
'  pintSubCode - Integer specifying the sub code to get (-999 or omit to not check the
'     sub code)
' Returns  :
' Details  :
'  Source  : Salt Solutions
'  Created : 10/10/2001 1:37 PM   By Ben Kester
'  Modified:
Private Function ReadAgeDur(ByVal pintValueVariation As eTableType, _
      ByVal pstrTableName As String, ByVal pcnnInput As ADODB.Connection, _
      ByVal pstrDataFieldName As String, ByVal pstrIndexFieldName As String, _
      ByVal pintInitialArrayIndex As Integer, ByRef pasngOutput() As Single, _
      ByVal plngFirstValue As Long, Optional ByVal plngLastValue As Long _
      = -999, Optional ByVal pintProdID As Integer = -999, Optional ByVal _
      pintSubCode As Integer = -999) As Boolean
 
Dim intIndex As Integer, rsInput As New ADODB.Recordset, strSQL As String
Dim strWhere As String, i As Integer
   
   ' If an error occurrs, False will be returned
   ReadAgeDur = False
   strWhere = ""
   If plngLastValue <> -999 Then
      strWhere = strWhere & " AND ((" & pstrTableName & "." & pstrIndexFieldName & _
         ")<=" & MAX(plngLastValue, plngFirstValue) & ")"
   End If
   If pintProdID <> -999 Then
      strWhere = strWhere & " AND ((" & pstrTableName & ".ProdID)=" & pintProdID & ")"
   End If
   If pintSubCode <> -999 Then
      strWhere = strWhere & " AND ((" & pstrTableName & ".SubCode)=" & pintSubCode & ")"
   End If
   strSQL = "SELECT [" & pstrDataFieldName & "] FROM " & pstrTableName & vbNewLine & _
      "WHERE (((" & pstrTableName & "." & pstrIndexFieldName & ")>=" & _
         plngFirstValue & ")" & strWhere & ")" & vbNewLine & _
      "ORDER BY " & pstrIndexFieldName & ";"
   ' Open up the recordset
   rsInput.Open strSQL, mcnnMain, adOpenForwardOnly, adLockReadOnly
   'Set rsInput = New ADODB.Recordset
   'rsInput.Open strSQL, pdbInput, adOpenForwardOnly, adLockReadOnly
   With rsInput
      If Not .EOF Then
         If .BOF Then
            .MoveNext
         End If
         intIndex = pintInitialArrayIndex
         If pintValueVariation = TableIssueAge Then
            plngLastValue = MIN(plngLastValue, 100)
            For i = plngFirstValue To plngLastValue
               pasngOutput(intIndex) = GetEnteredValue(.Fields(pstrDataFieldName), "")
               intIndex = intIndex + 1
            Next i
         Else
            Do Until .EOF
               If IsNull(.Fields(pstrDataFieldName)) Then
                  pasngOutput(intIndex) = 0
               Else
                  pasngOutput(intIndex) = GetEnteredValue(.Fields(pstrDataFieldName), "")
               End If
               intIndex = intIndex + 1
               .MoveNext
            Loop
         End If
      Else
         MsgBox ("Could not find rate for sql:" & vbNewLine & strSQL)
      End If
   End With
   ReadAgeDur = True

End Function
Public Property Get oSaltCommon() As SaltTRCommon
   If gobjSAVE Is Nothing Then
      Set gobjSAVE = New SaltTRCommon
   End If
   Set oSaltCommon = gobjSAVE
End Property

Public Function MAX(pvarVal1 As Variant, pvarVal2 As Variant) As Variant
 
   MAX = modValues.MAX(pvarVal1, pvarVal2)
End Function
Public Function MIN(ByVal pvarVal1 As Variant, ByVal pvarVal2 As Variant) As Variant

   MIN = modValues.MIN(pvarVal1, pvarVal2)
   
End Function

