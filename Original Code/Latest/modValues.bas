Attribute VB_Name = "modValues"
Option Explicit

Public gobjMessage As New MessageBox
Public gobjSAVE As New SaltTRCommon
Public gobjGlobal As New CGlobal
Public Const gcblnMidYearMaxAt0 = False 'Dave added 7/8/03
Public Const gcblnIncludeEndowInAlpha = True 'Dave added 7/8/03
Public Property Get gsavBenefits() As SaltBenefits
   Set gsavBenefits = gobjSAVE.Benefits
End Property
Public Property Get gsavCommFns() As SaltCommutationFunctions
   Set gsavCommFns = gobjSAVE.CommFunctions
End Property
Public Property Get gsavCV() As SaltCV
   Set gsavCV = gobjSAVE.CV
End Property
Public Property Get gsavCVETI() As SaltCVETI
   Set gsavCVETI = gobjSAVE.CVETI
End Property
Public Property Get gsavDueDeferred() As SaltDueDeferred
   Set gsavDueDeferred = gobjSAVE.DueDeferred
End Property
Public Property Get gsavInsured() As SaltInsured
   Set gsavInsured = gobjSAVE.Insured
End Property
Public Property Get gsavIntRate() As SaltIntRates
   Set gsavIntRate = gobjSAVE.IntRates
End Property
Public Property Get gsavMidYear() As SaltMidYear
   Set gsavMidYear = gobjSAVE.MidYear
End Property
Public Property Get gsavMortality() As SaltMortality
   Set gsavMortality = gobjSAVE.Mortality
End Property
Public Property Get gsavPolicy() As SaltPolicy
   Set gsavPolicy = gobjSAVE.Policy
End Property
Public Property Get gsavOutput() As SaltOutput
   Set gsavOutput = gobjSAVE.OutputClass
End Property

Public Property Get gsavPremium() As SaltPremium
   Set gsavPremium = gobjSAVE.Premium
End Property
Public Property Get gsavRecursive() As SaltRecursive
   Set gsavRecursive = gobjSAVE.Recursive
End Property
Public Property Get gsavReserves() As SaltReserves
   Set gsavReserves = gobjSAVE.Reserves
End Property
Public Property Get gsavSegmentation() As SaltSegmentation
   Set gsavSegmentation = gobjSAVE.Segmentation
End Property

Public Function MAX(pvarVal1 As Variant, pvarVal2 As Variant) As Variant
   If pvarVal1 > pvarVal2 Then
      MAX = pvarVal1
   Else
      MAX = pvarVal2
   End If
End Function
Public Function MIN(pvarVal1 As Variant, pvarVal2 As Variant) As Variant
   If pvarVal1 < pvarVal2 Then
      MIN = pvarVal1
   Else
      MIN = pvarVal2
   End If
End Function
Public Function Alpha(ByVal pdblIntRate As Double, Optional ByVal pintMode As Integer) As Double

'5.6.11, p142
Dim dblD As Double, dblIUpperM As Double, dblDUpperM As Double, dblV As Double
   
   dblV = v(pdblIntRate)
   dblD = 1 - dblV
   
   If Delta(pdblIntRate) <> 0 Then
      If pintMode >= 1 Then 'modal
         dblIUpperM = IUpperM(pdblIntRate, pintMode)
         dblDUpperM = DUpperM(pdblIntRate, pintMode)
         Alpha = pdblIntRate * dblD / (dblIUpperM * dblDUpperM)
      Else
         Alpha = pdblIntRate * dblD / (Delta(pdblIntRate) ^ 2)
      End If
   Else
      Alpha = 1
   End If

End Function
Private Sub DueDefPremium(pdatValDate, pdatPaidToDate, pdatIssueDate, pdblGrossPrem, pdblNetPremiumUsed, pintMode)

Dim intPremCount As Integer, datNextAnniv As Date, intCount As Integer
Dim fDeferredNow As Boolean, intDueCount As Integer, intDeferredCount As Integer

   datNextAnniv = pdatIssueDate
   Do Until datNextAnniv > pdatValDate
      datNextAnniv = DateAdd("yyyy", 1, datNextAnniv)
   Loop
   intPremCount = 0
   intDueCount = 0
   fDeferredNow = (pdatPaidToDate >= pdatValDate)
   'If pdatPaidToDate >= pdatValDate Then
   '   fDeferredNow = True
   'Else
   '   fDeferredNow = False
   'End If
   For intCount = 0 To 11 Step pintMode
      'pdatPaidToDate is the "anchor date"
      If DateAdd("m", intCount, pdatPaidToDate) < datNextAnniv Then
         If Not fDeferredNow Then
            If DateAdd("m", intCount, pdatPaidToDate) >= pdatValDate Then
               intDueCount = intPremCount
               fDeferredNow = True
            End If
         End If
         intPremCount = intPremCount + 1
      End If
   Next intCount
   If fDeferredNow Then
      intDeferredCount = intPremCount - intDueCount
   Else
      intDeferredCount = 0
      intDueCount = intPremCount
   End If
   'FinalDueCalc = intDueCount * pdblNetPremiumUsed * pintMode / 12 '/ 100
   'FinalDefCalc = intDeferredCount * pdblNetPremiumUsed * pintMode / 12 ' / 100
   'FinalDueGross = intDueCount * pdblGrossPrem / 100
   'FinalDefGross = intDeferredCount * pdblGrossPrem / 100
   
End Sub
Public Function RoundFn(ByVal pdblInputVariable As Double, ByVal pintDecimalPlaces As Integer, _
                        Optional ByVal pintRounding As eRound) As Double

   ' For simplicity, assume that there are 0 decimal places
   If pintDecimalPlaces = 0 Then
      If pintRounding = stRoundDown Then
         RoundFn = Fix(pdblInputVariable)
      ElseIf pintRounding = stRoundHighCent Then
         RoundFn = RoundFn(pdblInputVariable, 2, stRoundUp)
      ElseIf pintRounding = stRoundHighInteger Then
         If Fix(pdblInputVariable) = pdblInputVariable Then
            RoundFn = pdblInputVariable
         Else
            RoundFn = Fix(pdblInputVariable) + 1
         End If
      ElseIf pintRounding = stRoundLowCent Then
         RoundFn = RoundFn(pdblInputVariable, 2, stRoundDown)
      ElseIf pintRounding = stRoundLowInteger Then
         RoundFn = RoundFn(pdblInputVariable, 0, stRoundDown)
      ElseIf pintRounding = stRoundNear Then
         RoundFn = Round(pdblInputVariable, 0)
      ElseIf pintRounding = stRoundNearCent Then
         RoundFn = Round(pdblInputVariable, 2)
      ElseIf pintRounding = stRoundNone Then
         RoundFn = pdblInputVariable
      ElseIf pintRounding = stRoundUp Then
         RoundFn = RoundFn(pdblInputVariable, 0, stRoundHighInteger)
      ElseIf pintRounding = stRoundNearInteger Then
         RoundFn = Round(pdblInputVariable, 0)
      End If
   Else
      Select Case pintRounding
         Case stRoundDown, stRoundNear, stRoundUp
            RoundFn = RoundFn(pdblInputVariable * (10 ^ (pintDecimalPlaces)), _
               0, pintRounding) / (10 ^ pintDecimalPlaces)
         Case Else
            RoundFn = RoundFn(pdblInputVariable, 0, pintRounding)
      End Select
   End If

   
   'If pdblInputVariable >= 0 Then
   '   RoundFn = Fix(pdblInputVariable * 10 ^ (CDec(pintDecimalPlaces)) + 0.50001) / (10 ^ (CDec(pintDecimalPlaces)))
   'Else
   '   RoundFn = Fix(pdblInputVariable * 10 ^ (CDec(pintDecimalPlaces)) - 0.50001) / (10 ^ (CDec(pintDecimalPlaces)))
   'End If


End Function
Public Function Beta(pdblIntRate As Double, Optional ByVal pintMode As Integer) As Double

'5.6.12, p142
Dim dblD As Double, dblIUpperM As Double, dblDUpperM As Double, dblV As Double
   
   dblV = v(pdblIntRate)
   dblD = 1 - dblV
   
   If Delta(pdblIntRate) <> 0 Then
      If pintMode >= 1 Then 'modal
         dblIUpperM = IUpperM(pdblIntRate, pintMode)
         dblDUpperM = DUpperM(pdblIntRate, pintMode)
         Beta = (pdblIntRate - dblIUpperM) / (dblIUpperM * dblDUpperM)
      ElseIf pintMode = 0 Then 'Continuous
         Beta = (pdblIntRate - Delta(pdblIntRate)) / (Delta(pdblIntRate) ^ 2)
      Else
         Beta = 0
      End If
   End If
   
End Function
Public Function IUpperM(ByVal pdblIntRate As Double, ByVal pintMode As Integer) As Double

  IUpperM = pintMode * ((1 + pdblIntRate) ^ (1 / pintMode) - 1)

End Function
Public Function DUpperM(ByVal pdblIntRate As Double, ByVal pintMode As Integer) As Double

   DUpperM = pintMode * (1 - v(pdblIntRate, (1 / pintMode)))

End Function
Public Function v(ByVal pdblIntRate As Double, Optional ByVal pdblPower As Double) As Double

   If pdblPower <> 0 Then
      v = 1 / ((1 + pdblIntRate) ^ pdblPower)
   Else
      v = 1 / (1 + pdblIntRate)
   End If
   
End Function
Public Function Delta(ByVal pdblIntRate As Double) As Double

   Delta = Log(1 + pdblIntRate)

End Function
