﻿using System;
namespace SLTValues
{
    public class SaltPolicy
    {
        public DateTime IssueDate { get; set; }
        public eProductType ProductType { get; set; }
        public CalcTypeDef CalcType { get; set; }
        public DateTime ValuationDate { get; set; }
        private static int MonthDifference(DateTime lValue, DateTime rValue)
        {
            return (lValue.Month - rValue.Month) + 12 * (lValue.Year - rValue.Year);
        }
        public int Duration()
        {
            int ret = MonthDifference(ValuationDate, IssueDate);
            if (IssueDate.Day > ValuationDate.Day) ret -= 1;
            return (int)Math.Truncate(ret / 12.0) + 1;
        }
        public double ExactDayInterpolation()
        {
            DateTime d = new DateTime(ValuationDate.Year, IssueDate.Month, IssueDate.Day);
            if (d < ValuationDate) d = d.AddYears(1);
            return (double)((d - ValuationDate).Days) / (d - d.AddYears(-1)).Days;
        }
        public int MonthDuration()
        {
            int ret = MonthDifference(ValuationDate, IssueDate);
            if (IssueDate.Day > ValuationDate.Day) ret -= 1;
            return (ret % 12) + 1;
        }
    }
}
