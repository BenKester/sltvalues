﻿using System;

namespace SLTValues
{
    public class SaltIntRates
    {
        public enum InterestRateType { Constant = 0, VaryByDuration = 1 };
        public InterestRateType IntRateType { get; set; }
        public double InterestRateConstant { get; set; }
        public double[] InterestRateVary { get; set; }
        public double[] MinimumInterestRate { get; set; }
        public double[] FinalInterestRate { get; set; }
        public double NonforfeitureInterestRate { get; set; }
        public SaltIntRates()
        {
            InterestRateVary = new double[131];
            MinimumInterestRate = new double[131];
            FinalInterestRate = new double[131];
        }
        public bool CalcInterestRate(int issueAge, int endAge, TerminalIndicator terminalMethod, string reportHeader)
        {
            for (int index = 0; index <= endAge - issueAge - 1; index++)
                if (terminalMethod == TerminalIndicator.MinimumOneHalfCx || terminalMethod == TerminalIndicator.MinimumSegmented
                        || terminalMethod == TerminalIndicator.MinimumUnitary)
                    FinalInterestRate[index] = MinimumInterestRate[index];
                else
                 if (IntRateType == InterestRateType.Constant)
                    FinalInterestRate[index] = InterestRateConstant;
                else
                    FinalInterestRate[index] = InterestRateVary[index];
            for (int index = endAge - issueAge; index <= 130; index++)
                FinalInterestRate[index] = FinalInterestRate[index - 1];
            return true;
        }
    }
}
