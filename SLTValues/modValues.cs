﻿using System;
namespace SLTValues
{
    public class modValues
    {
        public const bool gcblnMidYearMaxAt0 = false;
        public const bool gcblnIncludeEndowInAlpha = true;
        public static CGlobal gobjGlobal = new CGlobal();
        public static SaltTRCommon gobjSave = new SaltTRCommon();

        public static SaltBenefits Benefits { get { return gobjSave.Benefits; } }
        public static SaltCommutationFunctions CommFns { get { return gobjSave.CommFunctions; } }
        public static SaltCV CV { get { return gobjSave.CV; } }
        public static SaltDueDeferred DueDeferred { get { return gobjSave.DueDeferred; } }
        public static SaltInsured Insured { get { return gobjSave.Insured; } }
        public static SaltIntRates IntRate { get { return gobjSave.IntRates; } }
        public static SaltMidYear MidYear { get { return gobjSave.MidYear; } }
        public static SaltMortality Mortality { get { return gobjSave.Mortality; } }
        public static SaltOutput OutputClass { get { return gobjSave.OutputClass; } }
        public static SaltPolicy Policy { get { return gobjSave.Policy; } }
        public static SaltPremium Premium { get { return gobjSave.Premium; } }
        public static SaltRecursive Recursive { get { return gobjSave.Recursive; } }
        public static SaltReserves Reserves { get { return gobjSave.Reserves; } }
        public static SaltSegmentation Segmentation { get { return gobjSave.Segmentation; } }
        public static double Alpha(double intRate, int mode = 0)
        {
            double d, V;
            V = v(intRate);
            d = 1 - V;
            if (Delta(intRate) != 0)
            {
                if (mode >= 1)   //modal
                    return intRate * d / (IUpperM(intRate, mode) * DUpperM(intRate, mode));
                else
                    return intRate * d / (Math.Pow(Delta(intRate), 2));
            }
            else
                return 1;
        }
        public static void DueDefPremium(DateTime valDate, DateTime paidToDate, DateTime issueDate, double grossPrem, double netPremiumUsed, int mode)
        {
            // looks like this method doesn't do anything
            int premCount, dueCount, deferredCount;
            DateTime nextAnniv;
            bool deferredNow;
            nextAnniv = issueDate;
            while (nextAnniv <= valDate)
                nextAnniv = new DateTime(nextAnniv.Year + 1, nextAnniv.Month, nextAnniv.Day);
            premCount = 0; dueCount = 0;
            deferredNow = (paidToDate >= valDate);
            for (int count = 0; count < 12; count += mode)
            {
                DateTime d = new DateTime(paidToDate.Year, paidToDate.Month + count, paidToDate.Day);
                if (d < nextAnniv)
                {
                    if (!deferredNow && d >= valDate)
                    {
                        dueCount = premCount;
                        deferredNow = true;
                    }
                    premCount += 1;
                }
            }
            if (deferredNow)
                deferredCount = premCount - dueCount;
            else
            {
                deferredCount = 0;
                dueCount = premCount;
            }
        }
        public static double Beta(double intRate, int mode = 0)
        {
            if (Delta(intRate) != 0)
            {
                if (mode >= 1)  // modal
                {
                    double iUpperM = IUpperM(intRate, mode);
                    return (intRate - iUpperM) / (iUpperM * DUpperM(intRate, mode));
                }
                else if (mode == 0) // continuous
                    return (intRate - Delta(intRate)) / Math.Pow(Delta(intRate), 2);
            }
            return 0;
        }
        public static double IUpperM(double intRate, int mode)
        {
            return mode * (Math.Pow(1 + intRate, 1.0 / mode) - 1);
        }
        public static double DUpperM(double intRate, int mode)
        {
            return mode * (1 - v(intRate, 1.0 / mode));
        }
        public static double v(double intRate, double power = 0)
        {
            if (power != 0)
                return 1 / Math.Pow(1 + intRate, power);
            else
                return 1 / (1 + intRate);
        }
        public static double Delta(double intRate)
        {
            return Math.Log(1 + intRate);
        }
    }
}
