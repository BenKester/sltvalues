﻿using System;
namespace SLTValues
{
    public class SaltSegmentation
    {
        public SegmentDefn SegmentDefn { get; set; }
        public double[] GPRatios { get; set; }
        public double[] GrossPremiumSegment { get; set; }
        public double[] MortRatios { get; set; }
        public int[] Segments { get; set; }
        public int[] SegmentsGroup { get; set; }
        public int SegmentCount { get; set; }
        public bool SegmentUsePolicyFee { get; set; }
        public SaltSegmentation()
        {
            GPRatios = new double[131];
            GrossPremiumSegment = new double[131];
            MortRatios = new double[131];
            Segments = new int[131];
            SegmentsGroup = new int[131];
        }
        
        public void CalcSegments(int beginAge, SegmentType segmentType)
        {
            double[] prem = new double[1];
            int benCeaseAge = modValues.Benefits.BenefitCeaseAge;
            if (segmentType == SegmentType.UnitaryMethod)
            {
                SegmentCount = 1;
                SegmentsGroup[SegmentCount] = benCeaseAge;
                Segments[benCeaseAge] = benCeaseAge;
                for (int i = benCeaseAge - 1; i > beginAge; i--)
                {
                    Segments[i] = Segments[i + 1];
                    GrossPremiumSegment[i] = 0;
                }
            }
            else
            {
                for (int i = beginAge; i < benCeaseAge - 2; i++)
                {
                    if (i >= modValues.Premium.PremiumCeaseAge - 1)
                        GPRatios[i] = 0;
                    else
                    {
                        prem[0] = modValues.Premium.GrossPremiums[i - beginAge];
                        prem[1] = modValues.Premium.GrossPremiums[i - beginAge + 1];
                        if (SegmentUsePolicyFee)
                        {
                            prem[0] += modValues.Premium.PolicyFee[i - beginAge];
                            prem[1] += modValues.Premium.PolicyFee[i - beginAge + 1];
                        }
                        if (prem[1] == 0)
                            GPRatios[i] = 0;
                        else if (prem[0] == 0)
                            GPRatios[i] = 9999;
                        else if (modValues.Benefits.DeathBenefit[i + 1] == 0 || modValues.Benefits.DeathBenefit[i] == 0)
                            GPRatios[i] = 0;
                        else
                            GPRatios[i] = (prem[1] / modValues.Benefits.DeathBenefit[i + 1]) / (prem[0] / modValues.Benefits.DeathBenefit[i]);

                    }
                    GrossPremiumSegment[i] = prem[0];
                    if (i == benCeaseAge - 1)
                    {
                        GrossPremiumSegment[i + 1] = prem[1];
                        GPRatios[i + 1] = 1;
                        MortRatios[i + 1] = 1;
                    }
                    MortRatios[i] = modValues.Mortality.FinalMortality[i + 1] / modValues.Mortality.FinalMortality[i];
                    if (SegmentDefn == SegmentDefn.Add1Pct)
                        MortRatios[i] += 0.01;
                    else if (SegmentDefn == SegmentDefn.MultiplyBy1_01)
                        MortRatios[i] *= 1.01;
                    else if (SegmentDefn == SegmentDefn.Subtract1Pct)
                        MortRatios[i] -= 0.01;
                    else if (SegmentDefn == SegmentDefn.MultiplyBy_99)
                        MortRatios[i] *= 0.99;
                    MortRatios[i] = Math.Max(1, MortRatios[i]);
                }

                SegmentCount = 1;
                SegmentsGroup[SegmentCount] = benCeaseAge;
                Segments[benCeaseAge] = benCeaseAge;
                for (int i = benCeaseAge - 1; i > beginAge; i--)
                    if (GPRatios[i] > MortRatios[i])
                    {
                        SegmentCount += 1;
                        SegmentsGroup[SegmentCount] = i + 1;
                        Segments[i] = i + 1;
                    }
                    else
                        Segments[i] = Segments[i + 1];
            }
            SegmentsGroup[SegmentCount + 1] = beginAge;
        }
    }
}
