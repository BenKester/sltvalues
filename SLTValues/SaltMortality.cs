﻿using System;

namespace SLTValues
{
    public class SaltMortality
    {
        //For select factors, the 1st dimension is for the number of lives (0 to 1 for lives 1 to 2)
        //For mortality rates, the 1st dimension is for the segment type (0 to 1 for initial segment || renewal segment)
        public double[,] SelFactor { get; set; }
        public double[,] SelFactorMinimum { get; set; }
        public double[,] SelFactorTabCOI { get; set; }
        public BasicMinimumOptions RenewalSegment10YrSelect { get; set; }
        public double[,] RenewalSegment10YrSelectFactor { get; set; }
        public double[,] MortalityRate { get; set; }
        public double[,] MortalityRateTabCOI { get; set; }
        public double[,] MortalityRateMin { get; set; }
        public double[,] MortalityRate2 { get; set; }
        public double[,] MortalityRate2Min { get; set; }
        public double[,] MortalityRate2TabCOI { get; set; }
        public double[] FinalMortality { get; set; }
        public MortalityType MortalityType { get; set; }
        public double[] XFactor { get; set; }
        public SaltMortality()
        {
            SelFactor = new double[2, 131];
            SelFactorMinimum = new double[2, 131];
            SelFactorTabCOI = new double[2, 131];
            RenewalSegment10YrSelectFactor = new double[2, 131];
            MortalityRate = new double[2, 131];
            MortalityRateTabCOI = new double[2, 131];
            MortalityRateMin = new double[2, 131];
            MortalityRate2 = new double[2, 131];
            MortalityRate2Min = new double[2, 131];
            MortalityRate2TabCOI = new double[2, 131];
            FinalMortality = new double[131];
            XFactor = new double[21];
            for (int i = 0; i < 131; i++)
            {
                SelFactor[0, i] = 1;
                SelFactor[1, i] = 1;
            }
        }
        public int CalcMortality(int issueAge, TerminalIndicator terminalMethod, int issueAge2, string reportHeader)
        {
            int age2, ret, dur;
            double segment1SelectFactor, renewalSegmentFactor, mortRateTemp;
            bool useRenewalSegment;
            double[] singleLifeQ1 = new double[131], singleLifeQ2 = new double[131];
            double[,] valueatHeIsAlive = new double[6, 131];
            SegmentIndicator segmentIndicator;

            for (int lifeIndicator = 0; lifeIndicator <= (int)modValues.Insured.Lives - 1; lifeIndicator++)
            {
                for (int age = issueAge; age < 130; age++)
                {
                    segment1SelectFactor = 1;
                    age2 = Math.Min(age + (issueAge2 - issueAge), 130);
                    dur = age - issueAge + 1;
                    if (MortalityType == MortalityType.BasicMort1_2Cx)
                    {
                        if (dur <= 10)
                        {
                            if (age < modValues.Segmentation.SegmentsGroup[modValues.Segmentation.SegmentCount])
                            {
                                segmentIndicator = SegmentIndicator.FirstSegment;
                                segment1SelectFactor = SelFactorTabCOI[lifeIndicator, dur];
                                renewalSegmentFactor = 1;
                            }
                            else
                            {
                                segmentIndicator = SegmentIndicator.RenewalSegment;
                                segment1SelectFactor = 1;
                                renewalSegmentFactor = SelFactorTabCOI[lifeIndicator, dur];
                            }
                        }
                        else
                        {
                            segmentIndicator = SegmentIndicator.RenewalSegment;
                            segment1SelectFactor = 1;
                            renewalSegmentFactor = 1;
                        }
                    }
                    else
                    {
                        if (age < modValues.Segmentation.SegmentsGroup[modValues.Segmentation.SegmentCount])
                        {
                            segmentIndicator = SegmentIndicator.FirstSegment;
                            if (MortalityType == MortalityType.MinimumMort || MortalityType == MortalityType.SegmentedMort)
                            {
                                segment1SelectFactor = SelFactorMinimum[lifeIndicator, dur];
                                if (MortalityType == MortalityType.MinimumMort)
                                    if (dur <= 20)
                                        segment1SelectFactor = segment1SelectFactor * XFactor[age - issueAge + 1];
                            }
                            else
                                segment1SelectFactor = SelFactor[lifeIndicator, dur];
                            renewalSegmentFactor = 1;
                        }
                        else
                        {
                            segment1SelectFactor = 1;
                            segmentIndicator = SegmentIndicator.RenewalSegment;
                            useRenewalSegment = false;
                            if (terminalMethod == TerminalIndicator.BasicSegmented || terminalMethod == TerminalIndicator.MinimumSegmented || MortalityType == MortalityType.BasicMort1_2Cx)
                                if (age >= modValues.Segmentation.SegmentsGroup[modValues.Segmentation.SegmentCount]
                                                     && modValues.Segmentation.SegmentsGroup[modValues.Segmentation.SegmentCount] < issueAge + 10 && dur <= 10)
                                {
                                    if (terminalMethod == TerminalIndicator.BasicSegmented && (RenewalSegment10YrSelect == BasicMinimumOptions.BasicOnly || RenewalSegment10YrSelect == BasicMinimumOptions.BasicMinimum))
                                        useRenewalSegment = true;
                                    else if (terminalMethod == TerminalIndicator.MinimumSegmented && (RenewalSegment10YrSelect == BasicMinimumOptions.MinimumOnly || RenewalSegment10YrSelect == BasicMinimumOptions.BasicMinimum))
                                        useRenewalSegment = true;
                                }
                            if (useRenewalSegment)
                                renewalSegmentFactor = RenewalSegment10YrSelectFactor[lifeIndicator, dur];
                            else
                                renewalSegmentFactor = 1;
                        }
                    }
                    if (lifeIndicator == 0)
                    {
                        if (MortalityType == MortalityType.BasicMort1_2Cx)
                            mortRateTemp = MortalityRateTabCOI[(int)segmentIndicator, age];
                        else if (MortalityType == MortalityType.MinimumMort)
                            mortRateTemp = MortalityRateMin[(int)segmentIndicator, age];
                        else
                            mortRateTemp = MortalityRate[(int)segmentIndicator, age];
                        singleLifeQ1[age] = mortRateTemp * segment1SelectFactor * renewalSegmentFactor;
                    }
                    else
                    {
                        if (MortalityType == MortalityType.BasicMort1_2Cx)
                            mortRateTemp = MortalityRate2TabCOI[(int)segmentIndicator, age2];
                        else if (MortalityType == MortalityType.MinimumMort)
                            mortRateTemp = MortalityRate2Min[(int)segmentIndicator, age2];
                        else
                            mortRateTemp = MortalityRate2[(int)segmentIndicator, age2];
                        singleLifeQ2[age] = mortRateTemp * segment1SelectFactor * renewalSegmentFactor;
                    }
                }
                if (modValues.Insured.Lives == eNumberOfLives.OneLife) break;
            }
            if (modValues.Insured.Lives == eNumberOfLives.OneLife)
                for (int age = issueAge; age <= 129; age++)
                    FinalMortality[age] = singleLifeQ1[age];
            else
                FrasierizeMort(issueAge, singleLifeQ1, singleLifeQ2, FinalMortality);
            ret = 130;
            for (int age = issueAge; age <= 130; age++)
                if (FinalMortality[age] >= 1)
                {
                    FinalMortality[age] = 1;
                    ret = age;
                    for (dur = age + 1; dur <= 130; dur++)
                        FinalMortality[dur] = 1;
                    break;
                }
                else if (age > 50 && FinalMortality[age] <= 0)
                {
                    ret = age;
                    break;
                }
            return ret;
        }
        public void FrasierizeMort(int issueAge, double[] mort1, double[] mort2, double[] output)
        {
            double[] alive1, alive2, aliveBoth, aliveOnly1, aliveOnly2, aliveEither;
            int ubound;
            double m1, m2;
            bool processMort;
            ubound = mort1.GetUpperBound(0);
            alive1 = new double[ubound + 1];
            alive2 = new double[ubound + 1];
            aliveBoth = new double[ubound + 1];
            aliveOnly1 = new double[ubound + 1];
            aliveOnly2 = new double[ubound + 1];
            aliveEither = new double[ubound + 1];
            for (int age = 0; age <= issueAge - 1; age++)
                output[age] = 0;
            for (int age = issueAge; age <= Math.Min(129, output.GetUpperBound(0)); age++)
            {
                processMort = false;
                if (age <= ubound)
                {
                    processMort = true;
                    m1 = mort1[age];
                }
                else
                    m1 = 1;
                if (age <= mort2.GetUpperBound(0))
                {
                    processMort = true;
                    m2 = mort2[age];
                }
                else
                    m2 = 1;
                if (processMort)
                {
                    if (modValues.Insured.Lives == eNumberOfLives.TwoLivesFirstToDie)
                        output[age] = Math.Min(1, m1 + m2 - m1 * m2);
                    else
                    {
                        if (age == issueAge)
                        {
                            alive1[age] = 1 * (1 - m1);
                            alive2[age] = 1 * (1 - m2);
                        }
                        else
                        {
                            alive1[age] = alive1[age - 1] * (1 - m1);
                            alive2[age] = alive2[age - 1] * (1 - m2);
                        }
                        aliveBoth[age] = alive1[age] * alive2[age];
                        aliveOnly1[age] = alive1[age] * (1 - alive2[age]);
                        aliveOnly2[age] = alive2[age] * (1 - alive1[age]);
                        aliveEither[age] = aliveBoth[age] + aliveOnly1[age] + aliveOnly2[age];
                        if (age == issueAge)
                            output[age] = 1 - aliveEither[age] / 1;
                        else
                        {
                            if (aliveEither[age - 1] != 0)
                                output[age] = 1 - aliveEither[age] / aliveEither[age - 1];
                            else
                                output[age] = 1;
                        }
                    }
                }
                else
                    output[age] = 1;
            }

        }
    }
}
