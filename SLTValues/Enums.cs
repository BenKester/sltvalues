﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SLTValues
{
    public enum eSegmentRatioRule { NoChangeRatios = 0, IncreaseAllRatios1 = 1, DecreaseAllRatios1 = 2 };
    public enum SegmentDefn { NoChange = 0, Subtract1Pct = 1, MultiplyBy_99 = 2, Add1Pct = 3, MultiplyBy1_01 = 4 };
    public enum EndowOptionXXX { NotUsed = 0, EndowUnitaryReserve = 1, EndowCV = 2 };
    public enum BasicMinimumOptions { None = 0, BasicOnly = 1, MinimumOnly = 2, BasicMinimum = 3 };
    public enum ComparisonRuleReserve { CompareTermials = 0, CompareMidYear = 1 };
    public enum MinimumMeanReserveDefn { CalculatedNetPremium = 0, GPIfLessNP = 1 };
    public enum eAdjNetLevelPrem { DivideByAnnuityDollar1 = 0, DivideByConstPercentGross = 1 };
    public enum eTermReserveDefn { NoCompareCV = 0, UseHighTermResAndCV = 1 };
    public enum eTermReserveDur { TerminalPremDefDurZero = 0, NegExpAllowDurZero = 1 };
    public enum eNetPremFirstYr { ReduceByCRVMExpAllow = 0, NoReduce = 1 };
    public enum MidYearRoundingRule { RoundTerminalsNetFirst = 0, RoundMYFirst = 1 };
    public enum MidYearDefn { Mean = 0, MidTerminals = 1 };
    public enum MidYearRule { CompareOneHalfCx = 0, NoComparison = 1 };
    public enum eMidYrSelFactor { NoSelMortFactors = 0, TenYrSelMortFactors = 1 };
    public enum MinDefn { MinReserves = 0, DeficiencyRes = 1 };
    public enum MortalityType { NonforfeitureMort = 0, BasicMort1_2Cx = 1, BasicMort = 2, MinimumMort = 3, SegmentedMort = 4 };
    public enum TerminalIndicator : int
    {
        NA = -1, CashValue = 0, BasicUnitary = 1, BasicSegmented = 2, BasicOneHalfCx = 3,
        MinimumUnitary = 4, MinimumSegmented = 5, MinimumOneHalfCx = 6, SegmentedTerminal = 7, Finished = 999
    };
    public enum TimingType { Curtate = 0, SemiContinuous = 1, Continuous = 2 };
    public enum eRound
    {
        RoundNearInteger = 0, RoundNearCent = 1, RoundLowInteger = 2, RoundHighInteger = 3, RoundLowCent = 4,
        RoundHighCent = 5, RoundUp = 6, RoundNear = 7, RoundDown = 8, RoundNone = 9
    };
    public enum ePremiumFace { GivenFace = 0, GivenPremium = 1 };
    public enum eLookupDescription
    {
        LookupDesc = 0, LookupClient = 1, LookupDatabase = 2, LookupTableType = 3, LookupUnits = 4, LookupDivOption = 5,
        LookupProdType = 6, LookupGender = 7, LookupUnderwriting = 8, LookupTobacco = 9, LookupState = 10, LookupMode = 11, LookupFormat = 12,
        LookupValueDefn = 13, LookupRider = 14, LookupReportType = 15, LookupReportNameBase = 16, LookupProduct = 17, LookupLives = 18,
        LookupResValMethod = 19, LookupCVValMethod = 20, LookupTiming = 21, LookupStatus = 22, LookupPolicyLoanCode = 23, LookupBillingType = 24,
        LookupNewspaperCode = 25, LookupProductType = 26, LookupTransactionTypes = 27, LookupMonths = 28, LookupRiderPlan = 29, LookupNewIssue = 30,
        LookupUserTypes = 31, LookupTermType = 32, LookupTransFrame = 33, LookupMiscTrans = 34, LookupPayComm = 35, LookupYesNo = 36, LookupRound = 37,
        LookupMonthlyCOIConv = 39, LookupMeanAssumption = 40, LookupDBOption = 41, Lookup7702Test = 42, LookupSaveSwitch = 43
    };
    public enum eClient { ClientRoyalArcanum = 1, ClientSaltTest = 2 };
    public enum eTableType { TableConstant = 0, TableAgeDur = 1, TableAttainedAge = 2, TableIssueAge = 3, TableDur = 4, TableSelectUltimate = 5, TableNone = 6, TableFaceAmount = 7, TableTwoAges = 8, TableToMaturity = 9, TableUltimate = 10, TableZero = 11, TableOnePayment = 12, TableMortTable = 13, TableAge = 14 };
    public enum eTableUnitDefinition { UnitDuration, UnitAge };
    public enum eDividendOptions { DivCash = 0, DivAccum = 1, DivReducePrem = 2, DivPUA = 3 };
    public enum eGenderConstants { GenderUnisex = 2, GenderMale = 0, GenderFemale = 1, Gender80Male20Female = 3, Gender60Male40Female = 4, Gender50Male50Female = 5, Gender40Male60Female = 6, Gender20Male80Female = 7 };
    public enum eUnderwriting { UndNa = 0, UndStandard = 1, UndSubStandard = 2 };
    public enum eTobacco { TobNA = 0, TobUnismoke = 1, TobTobacco = 2, TobNonTobacco = 3 };
    public enum eMode { ModeNA = 0, ModeAnnual = 1, ModeSemiAnnual = 2, ModeQuarterly = 3, ModeMonthly = 4, ModeSinglePremium = 5 };
    public enum eValueDefinition
    {
        ValueBasePrem1 = 1, ValueBasePrem2 = 2, ValueUltimatePrem = 3, ValueWaiverPrem = 4, ValueADBPrem = 5,
        ValuePayorPrem = 6, ValueGIRPrem = 7, ValueDeathBenefit = 8, ValueDividend = 9, ValueGtdCV = 10, ValueTermTo25 = 11, ValueReserve = 12,
        ValueEndowment = 13, ValueGIRPrem2 = 14, ValueADBBenefit = 15, ValuePureEndow = 16, ValueETIExpiring = 17, ValueNSP = 18,
        ValueRenewalPrem = 19, ValueScMembers = 20, ValueMEL = 21, ValuePremExpLoad = 22, ValuePolicyFee = 23, ValueMECLimit = 24,
        ValueValIntRate = 25, ValueGtdIntRate = 26
    };
    public enum eRider { RiderADB = 1, RiderWP = 2, RiderGIPO = 3, RiderPayor = 4, RiderTermTo25 = 5 };
    public enum eReportType
    {
        ReportCoverPage = 0, ReportTrad = 1, ReportTerm = 2, ReportAnnuity = 3, ReportGradedBenefit = 4, ReportRider = 5,
        ReportCoverGB = 6, ReportCoverFS = 7, ReportFS = 8, ReportCoverFE = 9, ReportFE = 10, ReportT25 = 11, ReportSPWL = 12
    };
    public enum eReportNameBase
    {
        ReportNameCover = 0, ReportNameExplanation = 1, ReportNameDefinition = 2, ReportNamePremium = 3,
        ReportNameDividends = 4, ReportNameDetail = 5, ReportNameSummary = 6
    };
    public enum eProductType { ProductTraditionalWL = 0, ProductTraditionalOther = 1, ProductTerm = 2, ProductAnnuity = 3 };
    public enum eUserTypes { userSA = 0, userCommon = 1, userReadOnly = 2 };
    public enum ModResType
    {
        NetLevel = 0, Unitary = 1, ILStandard = 2, NJStandard = 3, Imported = 4, FPT = 5, GradedUnitary = 6, TabularCOI = 7,
        SC = 8, NY147 = 9, XXX1999 = 10, OptionalYrtArt = 11, OptionalExemptionFromUnitary = 12, SegmentedMethod = 13, XXX1995 = 14
    };
    public enum eCVMethod
    {
        CVMethod1980 = 0, CVMethod1941 = 1, CVMethodReserve = 2, CVMethodImport = 3, CVMethodNone = 4, CVMethodNetLevel = 100,
        CVMethodUnitary = 101, CVMethodILStandard = 102, CVMethodNJStandard = 103, CVMethodFPT = 105, CVMethodGradedUnitary = 106,
        CVMethodTabularCOI = 107, CVMethodSC = 108, CVMethodNY147 = 109, CVMethodXXX1999 = 110, CVMethodOptionalYrtArt = 111,
        CVMethodOptionalExemptionFromUnitary = 112, CVMethodSegmentedMethod = 113, CVMethodXXX1995 = 114
    };
    public enum PremiumType { LowPremium = 0, HighPremium = 1 };
    public enum SegmentType { UnitaryMethod = 0, Segmentation = 1 };
    public enum MeanInterp { HalfYear = 0, NextMonth = 1, ExactDay = 2 };
    public enum eNumberOfLives { PeriodCertainOnly = 0, OneLife = 1, TwoLives = 2, TwoLivesFirstToDie = 3 };
    public enum DB19Pay { ELRA = 0, AverageDB = 1 };
    public enum CalcTypeDef { Reserves = 0, CV = 1 };
    public enum Dur1NetPremForDefRes { Alpha = 0, Beta = 1 };
    public enum RenewalSegmentMortality { Female10YearSelect = 47, Male10YearSelect = 48, _20Male10YearSelect = 1015, _40Male10YearSelect = 1016, _50Male10YearSelect = 1017, _60Male10YearSelect = 1018, _80Male10YearSelect = 1019 };
    public enum SegmentIndicator { FirstSegment = 0, RenewalSegment = 1 };
}
