﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SLTValues
{
    public class VBArray<T>
    {
        private T[] data;
        public int LBound { get; set; }
        public VBArray(int lbound, int ubound)
        {
            LBound = lbound;
            data = new T[ubound - lbound];
        }
        public T this[int index]
        {
            get
            {
                return data[index - LBound];
            }
            set
            {
                data[index - LBound] = value;
            }
        }
    }
    // This class was commented out
    public class SaltCVETI { }
}
