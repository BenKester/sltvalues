﻿using System;

namespace SLTValues
{
    public class SaltInsured
    {
        public enum GenderType { Male, Female };
        public int IssueAge { get; set; }
        public int IssueAge2 { get; set; }
        public GenderType Gender { get; set; }
        public bool ETI { get; set; }
        public eNumberOfLives Lives { get; set; }
        public GenderType Gender2 { get; set; }
    }
}
