﻿using System.Windows.Forms;
namespace SLTValues
{
    public class SaltCV
    {
        public double FinalTerminalCV { get; set; }
        public double[] PVFAdjPrem { get; set; }
        public double[] NSP { get; set; }
        public bool CalcCV { get; set; }
        public bool CalcSC { get; set; }
        public eCVMethod CVMethod { get; set; }
        public eCVMethod CVMethodFinal { get; set; }
        public bool TestForUnusualCV { get; set; }
        public bool CompareToCV { get; set; }
        public double AAI { get; set; }
        public double[] AdjustedPremium { get; set; }
        public double[] SC { get; set; }
        public double[] PureEndowment { get; set; }
        public double[] CashValue { get; set; }
        public double[] UnusualCV { get; set; }
        public double EA_CV { get; set; }
        private bool cvMessage = false;
        public SaltCV()
        {
            PVFAdjPrem = new double[131];
            NSP = new double[131];
            AdjustedPremium = new double[131];
            SC = new double[131];
            PureEndowment = new double[131];
            CashValue = new double[131];
            UnusualCV = new double[131];
        }
        public void CalcCashValue(int endAge, int beginAge, bool useExpAllow, bool useAllow, TimingType timing,
                                eProductType prodType, int pintMortEndAge)
        {
            double myIntRate, rc, dbTiming, premiumTiming, pvfb, annuity, netPremium;
            double[] adjustPremTest = new double[1];

            if (CVMethodFinal == eCVMethod.CVMethodReserve)
            {
                myIntRate = modValues.IntRate.FinalInterestRate[0 - beginAge];
                if (timing == TimingType.Curtate)
                {
                    dbTiming = 1;
                    premiumTiming = 1;
                }
                else
                {
                    dbTiming = myIntRate / modValues.Delta(myIntRate);
                    if (timing == TimingType.SemiContinuous || modValues.CommFns.Dx[0] == 0)
                        premiumTiming = 1;
                    else
                        premiumTiming = modValues.CommFns.DBarx[0] / modValues.CommFns.Dx[0];
                }
                for (int i = endAge; i >= beginAge; i--)
                {
                    if (modValues.Reserves.ModRes == ModResType.NetLevel)
                        AdjustedPremium[i] = modValues.Reserves.NetAnnualPremiums[beginAge];
                    else if (modValues.Reserves.ModRes == ModResType.Unitary || modValues.Reserves.ModRes == ModResType.ILStandard)
                        AdjustedPremium[i] = modValues.Reserves.ModifiedNetPremium[i];
                    else
            if (!cvMessage)
                    {
                        MessageBox.Show("Modified (adjusted) premium is not available");
                        cvMessage = true;
                    }
                    if (i >= beginAge && i < endAge)
                    {
                        CashValue[i] = modValues.Benefits.DeathBenefit[i] * dbTiming * modValues.Mortality.FinalMortality[i] / (1 + myIntRate) + (1 - modValues.Mortality.FinalMortality[i]) * CashValue[i + 1] / (1 + myIntRate);
                        if (i < modValues.Premium.PremiumCeaseAge)
                            CashValue[i] = CashValue[i] - AdjustedPremium[i] * premiumTiming;
                    }
                    else if (i == endAge)
                        CashValue[i] = modValues.Benefits.PVFB[i];
                }
            }
            else if (CVMethodFinal == eCVMethod.CVMethodNone)
                for (int i = endAge; i >= beginAge; i--)
                {
                    CashValue[i] = 0;
                    NSP[i] = 0;
                }
            else if (CVMethodFinal == eCVMethod.CVMethod1980)
            {
                AAI = 0;
                if (modValues.Benefits.DBType == SaltBenefits.DBTypeConstants.DBConstant)
                    AAI = modValues.Benefits.DBConstant;
                else
                {
                    for (int i = 1; i <= 10; i++)
                        AAI = AAI + modValues.Benefits.DeathBenefit[beginAge + i - 1];
                    AAI = AAI / 10;
                }
                netPremium = modValues.Benefits.PVFB[beginAge] / ((modValues.CommFns.Nx[beginAge] - modValues.CommFns.Nx[modValues.Premium.PremiumCeaseAge]) / modValues.CommFns.Dx[beginAge]);
                if (netPremium < 0.04 * AAI)
                    EA_CV = 1.25 * netPremium + 0.01 * AAI;
                else
                    EA_CV = 0.06 * AAI;
                if (useExpAllow)
                    rc = (modValues.Benefits.PVFB[beginAge] + EA_CV) / modValues.Premium.PVFGP[beginAge];
                else
                    rc = (modValues.Benefits.PVFB[beginAge]) / modValues.Premium.PVFGP[beginAge];
                for (int i = endAge; i >= beginAge; i--)
                {
                    myIntRate = modValues.IntRate.FinalInterestRate[i - beginAge];
                    if (timing == TimingType.Curtate)
                    {
                        dbTiming = 1;
                        premiumTiming = 1;
                    }
                    else
                    {
                        dbTiming = myIntRate / modValues.Delta(myIntRate);
                        if (timing == TimingType.SemiContinuous || modValues.CommFns.Dx[i] == 0)
                            premiumTiming = 1;
                        else
                            premiumTiming = modValues.CommFns.DBarx[i] / modValues.CommFns.Dx[i];
                    }
                    if (i >= beginAge && i < endAge)
                    {
                        AdjustedPremium[i] = rc * modValues.Premium.FinalGrossPremiums[i - beginAge];
                        PVFAdjPrem[i] = AdjustedPremium[i] * premiumTiming + (1 - modValues.Mortality.FinalMortality[i]) * PVFAdjPrem[i + 1] / (1 + myIntRate);
                        CashValue[i] = modValues.Benefits.PVFB[i] - PVFAdjPrem[i];
                        NSP[i] = modValues.Benefits.DeathBenefit[i] * dbTiming * modValues.Mortality.FinalMortality[i] / (1 + myIntRate) +
                            (1 - modValues.Mortality.FinalMortality[i]) * NSP[i + 1] / (1 + myIntRate);
                    }
                    else if (i == endAge)
                    {
                        CashValue[i] = modValues.Benefits.PVFB[i];
                        PVFAdjPrem[i] = 0;
                        NSP[i] = modValues.Benefits.PVFB[i];
                    }
                }
            }
            else if (CVMethodFinal == eCVMethod.CVMethod1941)
                for (int i = endAge; i >= beginAge; i--)
                {
                    myIntRate = modValues.IntRate.FinalInterestRate[i - beginAge];
                    if (timing == TimingType.Curtate)
                    {
                        dbTiming = 1;
                        premiumTiming = 1;
                    }
                    else
                    {
                        dbTiming = myIntRate / modValues.Delta(myIntRate);
                        if (timing == TimingType.SemiContinuous || modValues.CommFns.Dx[i] == 0)
                            premiumTiming = 1;
                        else
                            premiumTiming = modValues.CommFns.DBarx[i] / modValues.CommFns.Dx[i];
                    }
                    if (i == modValues.Premium.PremiumCeaseAge)
                    {
                        pvfb = modValues.CommFns.Mx[beginAge] / modValues.CommFns.Dx[beginAge];
                        annuity = (modValues.CommFns.Nx[beginAge]) / modValues.CommFns.Dx[beginAge];
                        adjustPremTest[0] = (pvfb + 0.02) / (annuity - 0.65);
                        if (adjustPremTest[0] > 0.04)
                            adjustPremTest[0] = (pvfb + 0.046) / annuity;
                        if (endAge >= pintMortEndAge && modValues.Premium.PremiumCeaseAge >= pintMortEndAge)
                            AdjustedPremium[i] = adjustPremTest[0];
                        else
                        {
                            pvfb = modValues.Benefits.PVFB[beginAge] / modValues.Benefits.DBConstant;
                            if (timing != TimingType.Continuous)
                                annuity = (modValues.CommFns.Nx[beginAge] - modValues.CommFns.Nx[modValues.Premium.PremiumCeaseAge]) /
                                    modValues.CommFns.Dx[beginAge];
                            else
                                annuity = (modValues.CommFns.NBarx[beginAge] - modValues.CommFns.NBarx[modValues.Premium.PremiumCeaseAge]) /
                                    modValues.CommFns.Dx[beginAge];
                            if (adjustPremTest[0] < 0.04)
                            {
                                adjustPremTest[1] = (pvfb + 0.02) / (annuity - 0.65);
                                if (adjustPremTest[1] > adjustPremTest[0])
                                    adjustPremTest[1] = (pvfb + 0.02 + 0.25 * adjustPremTest[0]) / (annuity - 0.4);
                                if (adjustPremTest[1] >= 0.04)
                                    adjustPremTest[1] = (pvfb + 0.036 + 0.25 * adjustPremTest[0]) / (annuity);
                            }
                            else
                            {
                                adjustPremTest[1] = (pvfb + 0.02) / (annuity - 0.65);
                                if (adjustPremTest[1] >= 0.04)
                                    adjustPremTest[1] = (pvfb + 0.046) / (annuity);
                            }
                            AdjustedPremium[i] = adjustPremTest[1];
                        }
                        AdjustedPremium[i] = AdjustedPremium[i] * modValues.Benefits.DBConstant;
                        if (i >= beginAge && i < endAge)
                        {
                            CashValue[i] = modValues.Benefits.DeathBenefit[i] * dbTiming * modValues.Mortality.FinalMortality[i] /
                                (1 + myIntRate) + (1 - modValues.Mortality.FinalMortality[i]) * CashValue[i + 1] / (1 + myIntRate);
                            NSP[i] = modValues.Benefits.DeathBenefit[i] * dbTiming * modValues.Mortality.FinalMortality[i] /
                                (1 + myIntRate) + (1 - modValues.Mortality.FinalMortality[i]) * NSP[i + 1] / (1 + myIntRate);
                        }
                        else
                        {
                            CashValue[i] = modValues.Benefits.PVFB[i];
                            NSP[i] = modValues.Benefits.PVFB[i];
                        }
                    }
                    else
            if (i > modValues.Premium.PremiumCeaseAge)
                        AdjustedPremium[i] = 0;
                    else
                        AdjustedPremium[i] = AdjustedPremium[i + 1];
                    if (i >= beginAge && i < endAge)
                    {
                        CashValue[i] = modValues.Benefits.DeathBenefit[i] * dbTiming * modValues.Mortality.FinalMortality[i] /
                            (1 + myIntRate) - AdjustedPremium[i] * premiumTiming + (1 - modValues.Mortality.FinalMortality[i]) * CashValue[i + 1] /
                            (1 + myIntRate);
                        NSP[i] = modValues.Benefits.DeathBenefit[i] * dbTiming * modValues.Mortality.FinalMortality[i] /
                            (1 + myIntRate) + (1 - modValues.Mortality.FinalMortality[i]) * NSP[i + 1] / (1 + myIntRate);
                    }
                    else
                    {
                        CashValue[i] = modValues.Benefits.PVFB[i];
                        NSP[i] = modValues.Benefits.PVFB[i];
                    }
                }
            if (CalcSC)
                for (int i = endAge; i >= beginAge; i--)
                    CashValue[i] = CashValue[i] - SC[i - beginAge];
        }

        public void CalcETICashValue(int endAge, int beginAge, bool useExpAllow, bool useAllow, TimingType timing, eProductType prodType)
        {
            double myIntRate, dbTiming, premiumTiming;
            for (int i = endAge; i >= beginAge; i--)
            {
                myIntRate = modValues.IntRate.FinalInterestRate[i - beginAge];
                if (timing == TimingType.Curtate)
                {
                    dbTiming = 1;
                    premiumTiming = 1;
                }
                else
                {
                    dbTiming = myIntRate / modValues.Delta(myIntRate);
                    if (timing == TimingType.SemiContinuous || modValues.CommFns.Dx[i] == 0)
                        premiumTiming = 1;
                    else
                        premiumTiming = modValues.CommFns.DBarx[i] / modValues.CommFns.Dx[i];
                }
                AdjustedPremium[i] = 0;
                CashValue[i] = modValues.Benefits.DeathBenefit[i] * dbTiming * (modValues.CommFns.Mx[beginAge] - modValues.CommFns.Mx[i]) / modValues.CommFns.Dx[beginAge];
                PureEndowment[i] = modValues.Benefits.DeathBenefit[beginAge] * modValues.CommFns.Dx[i] / modValues.CommFns.Dx[beginAge];
            }


        }
        public void CalcUnusualCV(int endAge, int beginAge, bool useExpAllow, bool useAllow, TimingType timing, eProductType prodType)
        {
            double myIntRate, dbTiming, premiumTiming;
            for (int i = endAge; i >= beginAge; i--)
            {
                myIntRate = modValues.IntRate.FinalInterestRate[i - beginAge];
                if (timing == TimingType.Curtate)
                {
                    dbTiming = 1;
                    premiumTiming = 1;
                }
                else
                {
                    dbTiming = myIntRate / modValues.Delta(myIntRate);
                    if (timing == TimingType.SemiContinuous || modValues.CommFns.Dx[i] == 0)
                        premiumTiming = 1;
                    else
                        premiumTiming = modValues.CommFns.DBarx[i] / modValues.CommFns.Dx[i];
                }
                AdjustedPremium[i] = 0;
                CashValue[i] = modValues.Benefits.DeathBenefit[i] * dbTiming * (modValues.CommFns.Mx[beginAge] - modValues.CommFns.Mx[i]) / modValues.CommFns.Dx[beginAge];
                PureEndowment[i] = modValues.Benefits.DeathBenefit[beginAge] * modValues.CommFns.Dx[i] / modValues.CommFns.Dx[beginAge];
            }
        }
    }
}
