﻿using System;

namespace SLTValues
{
    public class SaltDueDeferred
    {
        public double DeferredPremium { get; set; }
        public double DuePremium { get; set; }
    }
}
