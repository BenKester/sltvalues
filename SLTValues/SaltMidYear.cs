﻿using System;
namespace SLTValues
{
    public class SaltMidYear
    {
        public double[] MYBasicUnitary { get; set; }
        public double[] MYBasicSegmented { get; set; }
        public double[] MYMinimumUnitary { get; set; }
        public double[] MYMinimumSegmented { get; set; }
        public double[] MYBasicOneHalfCx { get; set; }
        public double[] MYBasicMean { get; set; }
        public double[] MYMinimumMean { get; set; }
        public double[] MYMinimumFinal { get; set; }
        public double[] MYMinimumDeficiencyFinal { get; set; }
        public double[] MYBasicUnusualCVTest { get; set; }
        public int MYReserveRounding { get; set; }
        public MidYearRoundingRule MYRoundingRule { get; set; }
        public MidYearRule MYRule { get; set; }
        public eTermReserveDur TermReserveDur { get; set; }
        public MinDefn MinDefn { get; set; }
        public ComparisonRuleReserve ComparisonRule { get; set; }
        public eNetPremFirstYr NetPremFirstYr { get; set; }
        public double[] BasicArt_MidYearReserve { get; set; }
        public double[] BasicArt_MidYearDeficiencyReserve { get; set; }
        public double[] TerminalBasicUnitary { get; set; }
        public double[] TerminalBasicSegmented { get; set; }
        public double[] TerminalBasicTerminal { get; set; }
        public double[] TerminalMinimumUnitary { get; set; }
        public double[] TerminalMinimumSegmented { get; set; }
        public double[] TerminalMinimumTerminal { get; set; }
        public double[] TerminalDeficiencyTerminal { get; set; }
        public double[] TerminalDeficiencyTerminalDef { get; set; }
        public double TerminalDeficiencyOutput;
        public double[] NP_BasicUnitary { get; set; }
        public double[] NP_BasicSegmented { get; set; }
        public double[] NP_BasicFinal { get; set; }
        public double[] NP_MinimumUnitary { get; set; }
        public double[] NP_MinimumSegmented { get; set; }
        public double[] NP_MinimumFinal { get; set; }
        public double[] UnusualCV_TestValue { get; set; }
        public bool[] UnusualCV_Test { get; set; }
        public int[] UnusualCV_Segments { get; set; }
        public double[] UnusualCV_PVSegmentPrems { get; set; }
        public double[] UnusualCV_PVSegmentBenefits { get; set; }
        public double[] UnusualCV_Ratio { get; set; }
        public double[] UnusualCV_NetPremium { get; set; }
        public double[] UnusualCV_TerminalReserve { get; set; }
        public double[] UnusualCV_MeanReserve { get; set; }
        public double[] UnusualCV_ComparedToCV { get; set; }
        public MinimumMeanReserveDefn MinimumMeanReserveDefn { get; set; }
        public MinimumMeanReserveDefn NetPremMinimumDefn { get; set; }
        public MeanInterp MeanInterpolation { get; set; }
        public double FinalMeanReserve { get; set; }
        public double FinalMeanCV { get; set; }
        public double MYFinalDeficiencyMean { get; set; }
        public MidYearDefn MidYear { get; set; }
        public MidYearDefn MidYearFactors { get; set; }
        private double InterpFactor { get; set; }
        public SaltMidYear()
        {
            MYBasicUnitary = new double[131];
            MYBasicSegmented = new double[131];
            MYMinimumUnitary = new double[131];
            MYMinimumSegmented = new double[131];
            MYBasicOneHalfCx = new double[131];
            MYBasicMean = new double[131];
            MYMinimumMean = new double[131];
            MYMinimumFinal = new double[131];
            MYMinimumDeficiencyFinal = new double[131];
            MYBasicUnusualCVTest = new double[131];
            BasicArt_MidYearReserve = new double[131];
            BasicArt_MidYearDeficiencyReserve = new double[131];
            TerminalBasicUnitary = new double[131];
            TerminalBasicSegmented = new double[131];
            TerminalBasicTerminal = new double[131];
            TerminalMinimumUnitary = new double[131];
            TerminalMinimumSegmented = new double[131];
            TerminalMinimumTerminal = new double[131];
            TerminalDeficiencyTerminal = new double[131];
            TerminalDeficiencyTerminalDef = new double[131];
            NP_BasicUnitary = new double[131];
            NP_BasicSegmented = new double[131];
            NP_BasicFinal = new double[131];
            NP_MinimumUnitary = new double[131];
            NP_MinimumSegmented = new double[131];
            NP_MinimumFinal = new double[131];
            UnusualCV_TestValue = new double[131];
            UnusualCV_Test = new bool[131];
            UnusualCV_Segments = new int[131];
            UnusualCV_PVSegmentPrems = new double[131];
            UnusualCV_PVSegmentBenefits = new double[131];
            UnusualCV_Ratio = new double[131];
            UnusualCV_NetPremium = new double[131];
            UnusualCV_TerminalReserve = new double[131];
            UnusualCV_MeanReserve = new double[131];
            UnusualCV_ComparedToCV = new double[131];
        }
        public void CalcMY_MidYearReserve(int endAge, int beginAge, TerminalIndicator terminalType)
        {
            bool segmented = modValues.Reserves.XXX || modValues.Reserves.ModRes == ModResType.SegmentedMethod;
            if (MeanInterpolation == MeanInterp.HalfYear)
                InterpFactor = 0.5;
            else if (MeanInterpolation == MeanInterp.NextMonth)
                InterpFactor = 1 - modValues.gobjSave.Policy.MonthDuration() / 12.0;
            else if (MeanInterpolation == MeanInterp.ExactDay)
                InterpFactor = modValues.gobjSave.Policy.ExactDayInterpolation();
            if (terminalType == TerminalIndicator.BasicOneHalfCx || terminalType == TerminalIndicator.BasicUnitary || terminalType == TerminalIndicator.CashValue)
            {
                CalcTm_BasicUnitary(endAge, beginAge, terminalType);
                if (segmented) CalcTm_BasicSegmented(endAge, beginAge);
                if (terminalType == TerminalIndicator.BasicOneHalfCx) CalcBasicArt_MidYear(endAge, beginAge);
                CalcMY_BasicUnitary(endAge, beginAge, terminalType);
                if (segmented) CalcMY_BasicSegmented(endAge, beginAge);
                if (terminalType == TerminalIndicator.BasicOneHalfCx) CalcMY_BasicOneHalfCx(endAge, beginAge);
                if (modValues.Reserves.XXX) CalcMY_BasicUnusualCV(endAge, beginAge);
                CalcMY_BasicMean(endAge, beginAge, terminalType);
                CalcNP_BasicUnitary(endAge, beginAge, terminalType);
                if (segmented) CalcNP_BasicSegmented(endAge, beginAge);
                CalcNP_BasicFinal(endAge, beginAge, terminalType);
                CalcTm_BasicTerminal(endAge, beginAge, terminalType);
            }
            else
            {
                CalcMY_MinimumUnitary(endAge, beginAge);
                if (segmented) CalcMY_MinimumSegmented(endAge, beginAge);
                CalcMY_MinimumMean(endAge, beginAge);
                CalcMY_MinimumFinal(endAge, beginAge);
                CalcNP_MinimumUnitary(endAge, beginAge);
                if (segmented) CalcNP_MinimumSegmented(endAge, beginAge);
                CalcNP_MinimumFinal(endAge, beginAge);
                CalcTm_MinimumUnitary(endAge, beginAge);
                if (segmented) CalcTm_MinimumSegmented(endAge, beginAge);
                CalcTm_MinimumTerminal(endAge, beginAge);
                CalcTm_DeficiencyTerminal(endAge, beginAge);
                CalcTm_Deficiency(endAge, beginAge);
            }

        }
        public void CalcMY_BasicUnitary(int endAge, int beginAge, TerminalIndicator terminalType)
        {
            double temp, temp1;

            for (int i = beginAge + 1; i <= endAge; i++)
            {
                if (MidYear == MidYearDefn.Mean)
                {
                    if (terminalType == TerminalIndicator.CashValue || ((int)modValues.CV.CVMethod < 100 && modValues.Policy.CalcType == CalcTypeDef.CV))
                        temp1 = modValues.CV.AdjustedPremium[i - 1];
                    else
                        temp1 = modValues.Reserves.NetPremBasicUnitary[i - 1];
                }
                else
                    temp1 = 0;
                if (terminalType == TerminalIndicator.CashValue || ((int)modValues.CV.CVMethod < 100 && modValues.Policy.CalcType == CalcTypeDef.CV))
                {
                    if (MYRoundingRule == MidYearRoundingRule.RoundTerminalsNetFirst)
                        temp = InterpFactor * Round(modValues.CV.CashValue[i - 1], modValues.OutputClass.UnitTerminalCVRound, modValues.OutputClass.eUnitTerminalCVRound) +
                                    (1 - InterpFactor) * Round(modValues.CV.CashValue[i], modValues.OutputClass.UnitTerminalCVRound, modValues.OutputClass.eUnitTerminalCVRound) +
                                                    InterpFactor * Round(temp1, modValues.OutputClass.UnitAdjustedPremRound, modValues.OutputClass.eUnitAdjustedPremRound);
                    else
                        temp = InterpFactor * modValues.CV.CashValue[i - 1] + (1 - InterpFactor) * modValues.CV.CashValue[i] + InterpFactor * temp1;
                    MYBasicUnitary[i] = Round(temp, modValues.OutputClass.UnitMeanCVRound, modValues.OutputClass.eUnitMeanCVRound);
                }
                else
                {
                    if (MYRoundingRule == MidYearRoundingRule.RoundTerminalsNetFirst)
                        temp = InterpFactor * Round(modValues.Reserves.BasicUnitary[i - 1], modValues.OutputClass.UnitTerminalReserveRound, modValues.OutputClass.eUnitTerminalReserveRound) +
                                    (1 - InterpFactor) * Round(modValues.Reserves.BasicUnitary[i], modValues.OutputClass.UnitTerminalReserveRound, modValues.OutputClass.eUnitTerminalReserveRound) +
                                                            InterpFactor * Round(temp1, modValues.OutputClass.UnitNetPremRound, modValues.OutputClass.eUnitNetPremRound);
                    else
                        temp = InterpFactor * modValues.Reserves.BasicUnitary[i - 1] + (1 - InterpFactor) * modValues.Reserves.BasicUnitary[i] + InterpFactor * temp1;
                    MYBasicUnitary[i] = Round(temp, modValues.OutputClass.UnitMeanReserveRound, modValues.OutputClass.eUnitMeanReserveRound);
                }
            }
        }
        public void CalcMY_BasicSegmented(int endAge, int beginAge)
        {
            double temp, temp1;

            for (int i = beginAge + 1; i <= endAge; i++)
            {
                if (!modValues.Reserves.XXX)
                    MYBasicSegmented[i] = 0;
                else
                {
                    if (MidYear == MidYearDefn.Mean)
                        temp1 = modValues.Reserves.NetPremBasicSegmented[i - 1];
                    else
                        temp1 = 0;
                    if (MYRoundingRule == MidYearRoundingRule.RoundTerminalsNetFirst)
                        temp = InterpFactor * Round(modValues.Reserves.SegmentedBasic[i - 1], modValues.OutputClass.UnitTerminalReserveRound, modValues.OutputClass.eUnitTerminalReserveRound) +
                                    (1 - InterpFactor) * Round(modValues.Reserves.SegmentedBasic[i], modValues.OutputClass.UnitTerminalReserveRound, modValues.OutputClass.eUnitTerminalReserveRound) +
                                            InterpFactor * Round(temp1, modValues.OutputClass.UnitNetPremRound, modValues.OutputClass.eUnitNetPremRound);
                    else
                        temp = InterpFactor * modValues.Reserves.SegmentedBasic[i - 1] +
                                    (1 - InterpFactor) * modValues.Reserves.SegmentedBasic[i] + InterpFactor * temp1;
                    MYBasicSegmented[i] = Round(temp, modValues.OutputClass.UnitMeanReserveRound, modValues.OutputClass.eUnitMeanReserveRound);
                }
            }

        }
        public void CalcMY_BasicOneHalfCx(int endAge, int beginAge)
        {
            if (modValues.Reserves.ModRes == ModResType.OptionalYrtArt)
                for (int i = beginAge + 1; i <= endAge; i++)
                    MYBasicOneHalfCx[i] = 0;
            else
                for (int i = beginAge + 1; i <= endAge; i++)
                    MYBasicOneHalfCx[i] = Round(InterpFactor * modValues.Reserves.NetPrem_CoiArt[i - 1], modValues.OutputClass.UnitMeanReserveRound,
                        modValues.OutputClass.eUnitMeanReserveRound);
        }
        public void CalcBasicArt_MidYear(int endAge, int beginAge)
        {
            double temp1, temp2, temp3, temp4;

            for (int i = beginAge + 1; i <= endAge; i++)
            {
                if (MidYear == MidYearDefn.Mean)
                {
                    temp1 = Round(modValues.Reserves.NetPrem_CoiArt[i - 1], modValues.OutputClass.UnitNetPremRound,
                        modValues.OutputClass.eUnitNetPremRound);
                    temp3 = modValues.Reserves.NetPrem_CoiArt[i - 1];
                }
                else
                {
                    temp1 = 0;
                    temp3 = 0;
                }
                temp2 = Round(InterpFactor * modValues.Reserves.NetPrem_CoiArt[i - 1], modValues.OutputClass.UnitNetPremRound,
                    modValues.OutputClass.eUnitNetPremRound);
                temp4 = InterpFactor * modValues.Reserves.NetPrem_CoiArt[i - 1];
                if (MYRoundingRule == MidYearRoundingRule.RoundTerminalsNetFirst)
                {
                    BasicArt_MidYearReserve[i] = InterpFactor * (Round(modValues.Reserves.BasicArt_TerminalReserve[i - 1],
                        modValues.OutputClass.UnitTerminalReserveRound, modValues.OutputClass.eUnitTerminalReserveRound) + temp1) + (1 - InterpFactor) *
                        Round(modValues.Reserves.BasicArt_TerminalReserve[i], modValues.OutputClass.UnitTerminalReserveRound,
                        modValues.OutputClass.eUnitTerminalReserveRound);
                    BasicArt_MidYearDeficiencyReserve[i] = InterpFactor * (Round(modValues.Reserves.BasicArt_TerminalReserve[i - 1],
                        modValues.OutputClass.UnitTerminalReserveRound, modValues.OutputClass.eUnitTerminalReserveRound) + temp1) - temp2 +
                        (1 - InterpFactor) * Round(modValues.Reserves.BasicArt_TerminalReserve[i], modValues.OutputClass.UnitTerminalReserveRound,
                        modValues.OutputClass.eUnitTerminalReserveRound);
                }
                else
                {
                    BasicArt_MidYearReserve[i] = (1 - InterpFactor) * modValues.Reserves.BasicArt_TerminalReserve[i] +
                        InterpFactor * (modValues.Reserves.BasicArt_TerminalReserve[i - 1] + temp3);
                    BasicArt_MidYearDeficiencyReserve[i] = (1 - InterpFactor) * modValues.Reserves.BasicArt_TerminalReserve[i] +
                        InterpFactor * (modValues.Reserves.BasicArt_TerminalReserve[i - 1] + temp3) - temp4;
                }
            }

        }
        public void CalcMY_BasicUnusualCV(int endAge, int beginAge)
        {
            for (int i = beginAge + 1; i <= endAge; i++)
                if (modValues.Reserves.ModRes == ModResType.OptionalYrtArt || !modValues.Reserves.XXX)
                    MYBasicUnusualCVTest[i] = 0;
                else
                    MYBasicUnusualCVTest[i] = UnusualCV_ComparedToCV[i];
        }
        public void CalcMY_BasicMean(int endAge, int beginAge, TerminalIndicator terminalType)
        {
            double temp0, temp1, temp2, temp3;

            for (int i = beginAge + 1; i <= endAge; i++)
            {
                if (MYRule == MidYearRule.CompareOneHalfCx && terminalType != TerminalIndicator.CashValue)
                    temp0 = MYBasicOneHalfCx[i];
                else
                    temp0 = -999999999;
                if ((!modValues.Reserves.XXX && modValues.Reserves.ModRes != ModResType.SegmentedMethod) || (terminalType == TerminalIndicator.CashValue
                                || ((int)modValues.CV.CVMethod < 100 && modValues.Policy.CalcType == CalcTypeDef.CV)))
                    temp1 = MYBasicUnitary[i];
                else if (modValues.Reserves.ModRes == ModResType.SegmentedMethod) //not currently supported
                    temp1 = 0; //Need to change when supported
                else if (modValues.Reserves.ModRes == ModResType.OptionalYrtArt)
                    temp1 = Round(modValues.Reserves.NetPrem_CoiArt[i] * InterpFactor, modValues.OutputClass.UnitMeanReserveRound, modValues.OutputClass.eUnitMeanReserveRound);
                else
                {
                    if (ComparisonRule == ComparisonRuleReserve.CompareMidYear)
                        temp2 = Math.Max(MYBasicUnitary[i], MYBasicSegmented[i]);
                    else if (modValues.Reserves.BasicUnitary[i] >= modValues.Reserves.SegmentedBasic[i])
                        temp2 = MYBasicUnitary[i];
                    else
                        temp2 = MYBasicSegmented[i];
                    if ((modValues.CV.TestForUnusualCV || modValues.CV.CompareToCV) && modValues.Reserves.ModRes != ModResType.OptionalYrtArt)
                        temp3 = MYBasicUnusualCVTest[i];
                    else
                        temp3 = 0;
                    temp1 = Math.Max(temp2, temp3);
                }
                MYBasicMean[i] = Math.Max(temp0, temp1);
            }
        }
        public void CalcTm_BasicUnitary(int endAge, int beginAge, TerminalIndicator terminalType)
        {
            if (modValues.Reserves.ModRes == ModResType.OptionalYrtArt)
                for (int i = beginAge; i <= endAge; i++)
                    TerminalBasicUnitary[i] = 0;
            else if (terminalType == TerminalIndicator.CashValue || ((int)modValues.CV.CVMethod < 100 &&
                            modValues.Policy.CalcType == CalcTypeDef.CV))
                for (int i = beginAge; i <= endAge; i++)
                    TerminalBasicUnitary[i] = modValues.CV.CashValue[i];
            else
                for (int i = beginAge; i <= endAge; i++)
                    if (TermReserveDur == eTermReserveDur.TerminalPremDefDurZero && i == beginAge)
                        TerminalBasicUnitary[i] = 0;
                    else
                        TerminalBasicUnitary[i] = modValues.Reserves.BasicUnitary[i];

        }
        public void CalcNP_BasicUnitary(int endAge, int beginAge, TerminalIndicator terminalType)
        {
            if (modValues.Reserves.ModRes == ModResType.OptionalYrtArt)
                for (int i = beginAge; i <= endAge - 1; i++)
                    NP_BasicUnitary[i] = 0;
            else
                if (terminalType == TerminalIndicator.CashValue || ((int)modValues.CV.CVMethod < 100 &&
                modValues.Policy.CalcType == CalcTypeDef.CV))
                for (int i = beginAge; i <= endAge - 1; i++) //Already have expense allowance removed
                    NP_BasicUnitary[i] = modValues.CV.AdjustedPremium[i];
            else
                for (int i = beginAge; i <= endAge - 1; i++) //Already have expense allowance removed
                    NP_BasicUnitary[i] = modValues.Reserves.NetPremBasicUnitary[i];
        }
        public void CalcNP_MinimumUnitary(int endAge, int beginAge)
        {
            if (modValues.Reserves.ModRes == ModResType.OptionalYrtArt)
                for (int i = beginAge; i <= endAge - 1; i++)
                    NP_MinimumUnitary[i] = 0;
            else
                for (int i = beginAge; i <= endAge - 1; i++)
                    if (NetPremMinimumDefn == MinimumMeanReserveDefn.GPIfLessNP)
                        NP_MinimumUnitary[i] = Math.Min(modValues.Reserves.NetPremMinimumUnitary[i], modValues.Premium.FinalGrossPremiums[i - beginAge]);
                    else
                        if (NetPremFirstYr == eNetPremFirstYr.NoReduce)
                        NP_MinimumUnitary[i] = modValues.Reserves.NetPremMinimumUnitary[i];
                    else
                        NP_MinimumUnitary[i] = modValues.Reserves.NetPremMinimumUnitary[i];

        }
        public void CalcNP_MinimumSegmented(int endAge, int beginAge)
        {
            if (modValues.Reserves.ModRes == ModResType.OptionalYrtArt)
                for (int i = beginAge; i <= endAge - 1; i++)
                    NP_MinimumSegmented[i] = 0;
            else
                for (int i = beginAge; i <= endAge - 1; i++)
                    if (NetPremMinimumDefn == MinimumMeanReserveDefn.GPIfLessNP)
                        NP_MinimumSegmented[i] = Math.Min(modValues.Reserves.NetPremMinimumSegmented[i], modValues.Premium.FinalGrossPremiums[i - beginAge]);
                    else
                        if (NetPremFirstYr == eNetPremFirstYr.NoReduce)
                        NP_MinimumSegmented[i] = modValues.Reserves.NetPremMinimumSegmented[i];
                    else
                        NP_MinimumSegmented[i] = modValues.Reserves.NetPremMinimumSegmented[i];
        }
        public void CalcNP_BasicSegmented(int endAge, int beginAge)
        {
            if (modValues.Reserves.ModRes == ModResType.OptionalYrtArt || !modValues.Reserves.XXX)
                for (int i = beginAge; i <= endAge - 1; i++)
                    NP_BasicSegmented[i] = 0;
            else
                for (int i = beginAge; i <= endAge - 1; i++)
                    if (NetPremFirstYr == eNetPremFirstYr.ReduceByCRVMExpAllow)
                        NP_BasicSegmented[i] = modValues.Reserves.NetPremBasicSegmented[i];
                    else
                        NP_BasicSegmented[i] = modValues.Reserves.NetPremBasicSegmented[i];

        }
        public void CalcNP_BasicFinal(int endAge, int beginAge, TerminalIndicator terminalType)
        {
            for (int i = beginAge; i <= endAge - 1; i++)
            {
                if ((!modValues.Reserves.XXX && modValues.Reserves.ModRes != ModResType.SegmentedMethod) ||
                    (terminalType == TerminalIndicator.CashValue || ((int)modValues.CV.CVMethod < 100 &&
                    modValues.Policy.CalcType == CalcTypeDef.CV)))
                    NP_BasicFinal[i] = NP_BasicUnitary[i];
                else if (modValues.Reserves.ModRes == ModResType.OptionalExemptionFromUnitary ||
                    modValues.Reserves.ModRes == ModResType.SegmentedMethod)
                    NP_BasicFinal[i] = NP_BasicSegmented[i];
                else if (modValues.Reserves.ModRes == ModResType.OptionalYrtArt)
                    NP_BasicFinal[i] = modValues.Reserves.NetPrem_CoiArt[i];
                else
                {
                    if (ComparisonRule == ComparisonRuleReserve.CompareMidYear)
                    {
                        if (MYBasicSegmented[i] >= MYBasicUnitary[i])
                            NP_BasicFinal[i] = NP_BasicSegmented[i];
                        else
                            NP_BasicFinal[i] = NP_BasicUnitary[i];
                    }
                    else
                    {
                        if (TerminalBasicSegmented[i] >= TerminalBasicUnitary[i])
                            NP_BasicFinal[i] = NP_BasicSegmented[i];
                        else
                            NP_BasicFinal[i] = NP_BasicUnitary[i];
                    }
                }
                NP_BasicFinal[i] = Round(NP_BasicFinal[i], modValues.OutputClass.UnitNetPremRound, modValues.OutputClass.eUnitNetPremRound);
            }
        }
        public void CalcNP_MinimumFinal(int endAge, int beginAge)
        {
            for (int i = beginAge; i <= endAge - 1; i++)
            {
                if (modValues.Reserves.MinUnitary_DeficiencyReserve[i] > 0 || modValues.Reserves.MinSegmented_DeficiencyReserve[i] > 0)
                {
                    if (!modValues.Reserves.XXX && modValues.Reserves.ModRes != ModResType.SegmentedMethod)
                        NP_MinimumFinal[i] = NP_MinimumUnitary[i];
                    else if (modValues.Reserves.ModRes == ModResType.OptionalExemptionFromUnitary ||
                        modValues.Reserves.ModRes == ModResType.SegmentedMethod)
                        NP_MinimumFinal[i] = NP_MinimumSegmented[i];
                    else if (modValues.Reserves.ModRes == ModResType.OptionalYrtArt)
                    {
                        if (NetPremMinimumDefn == MinimumMeanReserveDefn.GPIfLessNP)
                            NP_MinimumFinal[i] = Math.Min(modValues.Reserves.NetPrem_CoiArt[i], modValues.Premium.FinalGrossPremiums[i - beginAge]);
                        else
                            NP_MinimumFinal[i] = modValues.Reserves.NetPrem_CoiArt[i];
                    }
                    else
                    {
                        if (ComparisonRule == ComparisonRuleReserve.CompareMidYear)
                        {
                            if (MYBasicSegmented[i] >= MYBasicUnitary[i])
                                NP_MinimumFinal[i] = NP_MinimumSegmented[i];
                            else
                                NP_MinimumFinal[i] = NP_MinimumUnitary[i];
                        }
                        else
                        {
                            if (TerminalBasicSegmented[i] >= TerminalBasicUnitary[i])
                                NP_MinimumFinal[i] = NP_MinimumSegmented[i];
                            else
                                NP_MinimumFinal[i] = NP_MinimumUnitary[i];
                        }
                    }
                    NP_MinimumFinal[i] = Round(NP_MinimumFinal[i], modValues.OutputClass.UnitNetPremRound, modValues.OutputClass.eUnitNetPremRound);
                }
                else
                    NP_MinimumFinal[i] = NP_BasicFinal[i];
            }
        }
        public void CalcTm_MinimumUnitary(int endAge, int beginAge)
        {
            if (modValues.Reserves.ModRes == ModResType.OptionalYrtArt)
                for (int i = beginAge; i <= endAge; i++)
                    TerminalMinimumUnitary[i] = modValues.Reserves.MinUnitary_DeficiencyReserve[i];
            else
                for (int i = beginAge; i <= endAge; i++)
                    if (TermReserveDur == eTermReserveDur.TerminalPremDefDurZero && i == beginAge)
                        TerminalMinimumUnitary[i] = modValues.Reserves.MinUnitary_DeficiencyReserve[i];
                    else
                        TerminalMinimumUnitary[i] = modValues.Reserves.MinUnitary_MinimumUnitary[i];
        }
        public void CalcTm_MinimumSegmented(int endAge, int beginAge)
        {
            if (modValues.Reserves.ModRes == ModResType.OptionalYrtArt)
                for (int i = beginAge; i <= endAge; i++)
                    TerminalMinimumSegmented[i] = modValues.Reserves.MinSegmented_DeficiencyReserve[i];
            else
                for (int i = beginAge; i <= endAge; i++)
                    if (TermReserveDur == eTermReserveDur.TerminalPremDefDurZero && i == beginAge)
                        TerminalMinimumSegmented[i] = modValues.Reserves.MinSegmented_DeficiencyReserve[i];
                    else
                        TerminalMinimumSegmented[i] = modValues.Reserves.MinSegmented_MinimumSegmented[i];
        }
        public void CalcTm_MinimumTerminal(int endAge, int beginAge)
        {
            for (int i = beginAge; i <= endAge; i++)
            {
                if (modValues.Reserves.ModRes == ModResType.OptionalExemptionFromUnitary || modValues.Reserves.ModRes == ModResType.SegmentedMethod)
                    TerminalMinimumTerminal[i] = TerminalMinimumSegmented[i];
                else if (!modValues.Reserves.XXX)
                    TerminalMinimumTerminal[i] = TerminalMinimumUnitary[i];
                else
            if (ComparisonRule == ComparisonRuleReserve.CompareMidYear)
                {
                    if (i > beginAge)
                    {
                        if (MYBasicSegmented[i] >= MYBasicUnitary[i])
                            TerminalMinimumTerminal[i] = TerminalMinimumSegmented[i];
                        else
                            TerminalMinimumTerminal[i] = TerminalMinimumUnitary[i];
                    }
                    else
                if (MYBasicSegmented[i + 1] >= MYBasicUnitary[i + 1])
                        TerminalMinimumTerminal[i] = TerminalMinimumSegmented[i];
                    else
                        TerminalMinimumTerminal[i] = TerminalMinimumUnitary[i];
                }
                else
            if (Round(TerminalBasicSegmented[i], modValues.OutputClass.UnitTerminalReserveRound, modValues.OutputClass.eUnitTerminalReserveRound) >=
                Round(TerminalBasicUnitary[i], modValues.OutputClass.UnitTerminalReserveRound, modValues.OutputClass.eUnitTerminalReserveRound))
                    TerminalMinimumTerminal[i] = TerminalMinimumSegmented[i];
                else
                    TerminalMinimumTerminal[i] = TerminalMinimumUnitary[i];
                TerminalMinimumTerminal[i] = Round(TerminalMinimumTerminal[i], modValues.OutputClass.UnitTerminalReserveRound, modValues.OutputClass.eUnitTerminalReserveRound);
            }
        }
        public void CalcTm_DeficiencyTerminal(int endAge, int beginAge)
        {
            for (int i = beginAge; i <= endAge; i++)
                if (TerminalBasicTerminal[i] < 0 && TerminalMinimumTerminal[i] < 0 && MinDefn == MinDefn.DeficiencyRes)
                {
                    TerminalDeficiencyTerminal[i] = 0;
                    TerminalDeficiencyTerminalDef[i] = 0;
                }
                else if (modValues.Reserves.ModRes == ModResType.OptionalYrtArt)
                {
                    TerminalDeficiencyTerminal[i] = modValues.Reserves.BasicArt_TerminalReserve[i];
                    TerminalDeficiencyTerminalDef[i] = Math.Max(modValues.Reserves.BasicArt_TerminalReserve[i], modValues.Reserves.NetPrem_CoiArt[i]);
                }
                else
                {
                    TerminalDeficiencyTerminal[i] = TerminalMinimumTerminal[i];
                    TerminalDeficiencyTerminalDef[i] = Math.Max(0, TerminalMinimumTerminal[i] - TerminalBasicTerminal[i]);
                }
        }
        public void CalcTm_Deficiency(int endAge, int beginAge)
        {
            int i = 0;
            if (TerminalBasicTerminal[beginAge] < 0 && TerminalMinimumTerminal[beginAge] < 0)
                TerminalDeficiencyOutput = 0;
            else if (modValues.Reserves.ModRes == ModResType.OptionalYrtArt)
                TerminalDeficiencyOutput = Math.Max(0, modValues.Reserves.BasicArt_TerminalReserve[i] - modValues.Reserves.NetPrem_CoiArt[i]);
            else
                TerminalDeficiencyOutput = Math.Max(0, TerminalMinimumTerminal[beginAge] - TerminalBasicTerminal[beginAge]);
        }
        public void CalcTm_BasicSegmented(int endAge, int beginAge)
        {
            if (modValues.Reserves.ModRes == ModResType.OptionalYrtArt || !modValues.Reserves.XXX)
                for (int i = beginAge; i <= endAge; i++)
                    TerminalBasicSegmented[i] = 0;
            else
                for (int i = beginAge; i <= endAge; i++)
                    if (TermReserveDur == eTermReserveDur.TerminalPremDefDurZero && i == beginAge)
                        TerminalBasicSegmented[i] = 0;
                    else
                        TerminalBasicSegmented[i] = modValues.Reserves.SegmentedBasic[i];
        }
        public void CalcTm_BasicTerminal(int endAge, int beginAge, TerminalIndicator terminalType)
        {
            if (modValues.Reserves.ModRes == ModResType.OptionalYrtArt)
                for (int i = beginAge; i <= endAge; i++)
                    TerminalBasicTerminal[i] = 0;
            else
                for (int i = beginAge; i <= endAge; i++)
                {
                    if (TermReserveDur == eTermReserveDur.TerminalPremDefDurZero && i == beginAge)
                        TerminalBasicTerminal[i] = 0;
                    else if (modValues.Reserves.ModRes == ModResType.OptionalExemptionFromUnitary || modValues.Reserves.ModRes == ModResType.SegmentedMethod)
                        TerminalBasicTerminal[i] = TerminalBasicSegmented[i];
                    else if (terminalType == TerminalIndicator.CashValue || ((int)modValues.CV.CVMethod < 100 && modValues.Policy.CalcType == CalcTypeDef.CV))
                        TerminalBasicTerminal[i] = TerminalBasicUnitary[i];
                    else if (!modValues.Reserves.XXX)
                        TerminalBasicTerminal[i] = TerminalBasicUnitary[i];
                    else
            if (ComparisonRule == ComparisonRuleReserve.CompareMidYear)
                    {
                        if (MYBasicSegmented[i] >= MYBasicUnitary[i])
                            TerminalBasicTerminal[i] = TerminalBasicSegmented[i];
                        else
                            TerminalBasicTerminal[i] = TerminalBasicUnitary[i];
                    }
                    else
                if (TerminalBasicSegmented[i] >= TerminalBasicUnitary[i])
                        TerminalBasicTerminal[i] = TerminalBasicSegmented[i];
                    else
                        TerminalBasicTerminal[i] = TerminalBasicUnitary[i];
                    TerminalBasicTerminal[i] = Round(TerminalBasicTerminal[i], modValues.OutputClass.UnitTerminalReserveRound, modValues.OutputClass.eUnitTerminalReserveRound);
                }
        }

        public void CalcUnusualCV()
        {
            double temp, temp1, adjustedNLPrem, expenseAllowance = 0, _19PayPremium, FirstYearBenefitPremium, dbTiming, premTiming;

            //Note that this sub is called after the basic assumptions are run.  Thus, it is using basic interest rates && mortality
            //Also, I am starting my loops at issue age + 1.  VE starts at issue age but it is end of year value (reserves) so they
            // should be attained age which is what I am doing.  It does result in some tricky indexing.  Nearly all of my input items
            // need to subtract 1 from the index.  Also, sometimes I am comparing to the previous year's cash value && must reference
            // the cash value for the index - 2.

            if (MeanInterpolation == MeanInterp.HalfYear)
                InterpFactor = 0.5;
            else if (MeanInterpolation == MeanInterp.NextMonth)
                InterpFactor = 1 - modValues.gobjSave.Policy.MonthDuration() / 12.0;
            else if (MeanInterpolation == MeanInterp.ExactDay)
                InterpFactor = modValues.gobjSave.Policy.ExactDayInterpolation();

            if (modValues.CV.TestForUnusualCV)
            {
                UnusualCV_Segments[modValues.Benefits.BenefitCeaseAge + 1] = 0;
                for (int i = modValues.Benefits.BenefitCeaseAge; i >= modValues.Insured.IssueAge + 1; i--)
                {
                    //****** Note that this timing adjustment can only be used for one duration.    ************
                    //****** Otherwise it is not correct if the interest rate varies by duration!!! ************
                    if (modValues.gobjSave.Timing == TimingType.Curtate || modValues.IntRate.FinalInterestRate[i - modValues.Insured.IssueAge - 1] == 0)
                        dbTiming = 1;
                    else
                        dbTiming = modValues.IntRate.FinalInterestRate[i - modValues.Insured.IssueAge - 1] / modValues.Delta(modValues.IntRate.FinalInterestRate[i - modValues.Insured.IssueAge - 1]);
                    if (i == modValues.Insured.IssueAge + 1)
                    {
                        if (UnusualCV_PVSegmentPrems[i + 1] > 0)
                            adjustedNLPrem = modValues.Premium.FinalGrossPremiums[0] * UnusualCV_PVSegmentBenefits[i + 1] / UnusualCV_PVSegmentPrems[i + 1];
                        else
                            adjustedNLPrem = 0;
                        _19PayPremium = modValues.Reserves.Ren19PayPrem * modValues.Reserves.ELRA;
                        FirstYearBenefitPremium = modValues.Mortality.FinalMortality[i - 1] * modValues.Benefits.DeathBenefit[i - 1] * dbTiming / (1 + modValues.IntRate.FinalInterestRate[0]);
                        if (modValues.gobjSave.Timing == TimingType.Continuous)
                            FirstYearBenefitPremium = FirstYearBenefitPremium / (modValues.CommFns.DBarx[i - 1] / modValues.CommFns.Dx[i - 1]);
                        expenseAllowance = (Math.Min(adjustedNLPrem, _19PayPremium) - FirstYearBenefitPremium);
                        if (modValues.gobjSave.Timing == TimingType.Continuous && !modValues.gobjSave.Reserves.ExpenseAllowCurtate)
                            expenseAllowance = expenseAllowance * (modValues.CommFns.DBarx[i - 1] / modValues.CommFns.Dx[i - 1]);
                    }
                    else
                        expenseAllowance = 0;

                    if (i > modValues.Insured.IssueAge && i > 1)
                        temp = modValues.CV.CashValue[i - 2];
                    else
                        temp = 0;
                    UnusualCV_TestValue[i] = temp +
                            1.1 * modValues.Premium.FinalGrossPremiums[i - modValues.Insured.IssueAge - 1] +
                            (1.1 * modValues.IntRate.NonforfeitureInterestRate *
                            (modValues.CV.CashValue[i - 2] + modValues.Premium.FinalGrossPremiums[i - modValues.Insured.IssueAge - 1]))
                            + 0.05 * modValues.CV.SC[1];

                    UnusualCV_Test[i] = (modValues.CV.CashValue[i - 1] > UnusualCV_TestValue[i]);
                    if (UnusualCV_Test[i])
                        UnusualCV_Segments[i] = i - modValues.Insured.IssueAge;
                    else
                        UnusualCV_Segments[i] = UnusualCV_Segments[i + 1];

                    if (UnusualCV_Segments[i] > 0)
                        UnusualCV_PVSegmentPrems[i] = (modValues.CommFns.Dx[i - 1] * modValues.Premium.PVFGP[i - 1] -
                        modValues.CommFns.Dx[modValues.Insured.IssueAge + UnusualCV_Segments[i]] * modValues.Premium.PVFGP[modValues.Insured.IssueAge + UnusualCV_Segments[i]])
                        / modValues.CommFns.Dx[i - 1];
                    else
                        UnusualCV_PVSegmentPrems[i] = 0;

                    if (modValues.CommFns.Dx[i - 1] > 0 && UnusualCV_Segments[i] > 0)
                    {
                        UnusualCV_PVSegmentBenefits[i] = (modValues.CommFns.Dx[i - 1] * modValues.Benefits.PVFB_NoEndow[i - 1]
                        - modValues.CommFns.Dx[modValues.Insured.IssueAge + UnusualCV_Segments[i]] * modValues.Benefits.PVFB_NoEndow[modValues.Insured.IssueAge + (int)UnusualCV_Segments[i]]) / modValues.CommFns.Dx[i - 1];
                        UnusualCV_PVSegmentBenefits[i] = UnusualCV_PVSegmentBenefits[i] + expenseAllowance +
                            modValues.CV.CashValue[modValues.Insured.IssueAge + UnusualCV_Segments[i] - 1] *
                            modValues.CommFns.Dx[modValues.Insured.IssueAge + 1 + UnusualCV_Segments[i] - 1] / modValues.CommFns.Dx[i - 1];
                    }
                    else
                        UnusualCV_PVSegmentBenefits[i] = 0;
                }

                UnusualCV_TerminalReserve[modValues.Insured.IssueAge] = -expenseAllowance;
                for (int i = modValues.Insured.IssueAge + 1; i <= modValues.Benefits.BenefitCeaseAge; i++)
                {
                    if (modValues.gobjSave.Timing == TimingType.Curtate || modValues.IntRate.FinalInterestRate[i - modValues.Insured.IssueAge - 1] == 0)
                    {
                        dbTiming = 1;
                        premTiming = 1;
                    }
                    else
                    {
                        dbTiming = modValues.IntRate.FinalInterestRate[i - modValues.Insured.IssueAge - 1] / modValues.Delta(modValues.IntRate.FinalInterestRate[i - modValues.Insured.IssueAge - 1]);
                        if (modValues.gobjSave.Timing == TimingType.Continuous)
                            premTiming = modValues.CommFns.DBarx[i - 1] / modValues.CommFns.Dx[i - 1];
                        else
                            premTiming = 1;
                    }
                    if (UnusualCV_PVSegmentPrems[i] > 0)
                    {
                        if (UnusualCV_Segments[i] == UnusualCV_Segments[i - 1])
                        {
                            if (i > modValues.Insured.IssueAge)
                                UnusualCV_Ratio[i] = UnusualCV_Ratio[i - 1];
                            else
                                UnusualCV_Ratio[i] = 0;
                        }
                        else
                        if (i > modValues.Insured.IssueAge + 1)
                            UnusualCV_Ratio[i] = (UnusualCV_PVSegmentBenefits[i] - modValues.CV.CashValue[i - 2]) / UnusualCV_PVSegmentPrems[i];
                        else
                            UnusualCV_Ratio[i] = (UnusualCV_PVSegmentBenefits[i]) / UnusualCV_PVSegmentPrems[i];
                    }
                    else
                        UnusualCV_Ratio[i] = 0;

                    UnusualCV_NetPremium[i] = UnusualCV_Ratio[i] * modValues.Premium.FinalGrossPremiums[i - modValues.Insured.IssueAge - 1];

                    if (UnusualCV_Segments[i] > 0)
                    {
                        UnusualCV_TerminalReserve[i] = (UnusualCV_TerminalReserve[i - 1] *
                            (1 + modValues.IntRate.FinalInterestRate[i - modValues.Insured.IssueAge - 1]) +
                            (UnusualCV_NetPremium[i] * premTiming *
                            (1 + modValues.IntRate.FinalInterestRate[i - modValues.Insured.IssueAge - 1])) -
                            dbTiming * modValues.Mortality.FinalMortality[i - 1] * modValues.Benefits.DeathBenefit[i - 1]);
                        if (modValues.Mortality.FinalMortality[i - 1] < 1)
                            UnusualCV_TerminalReserve[i] = UnusualCV_TerminalReserve[i] / (1 - modValues.Mortality.FinalMortality[i - 1]);
                    }
                    else
                        UnusualCV_TerminalReserve[i] = 0;

                    if (MidYear == MidYearDefn.Mean)
                        temp = Round(UnusualCV_NetPremium[i], modValues.OutputClass.UnitNetPremRound, modValues.OutputClass.eUnitNetPremRound);
                    else
                        temp = 0;
                    UnusualCV_MeanReserve[i] = InterpFactor * (temp +
                        Round(UnusualCV_TerminalReserve[i - 1], modValues.OutputClass.UnitTerminalReserveRound, modValues.OutputClass.eUnitTerminalReserveRound))
                        + (1 - InterpFactor) * Round(UnusualCV_TerminalReserve[i], modValues.OutputClass.UnitTerminalReserveRound, modValues.OutputClass.eUnitTerminalReserveRound);
                    UnusualCV_MeanReserve[i] = Round(UnusualCV_MeanReserve[i], modValues.OutputClass.UnitMeanReserveRound, modValues.OutputClass.eUnitMeanReserveRound);
                    if (modValues.CV.TestForUnusualCV)
                        temp = UnusualCV_MeanReserve[i];
                    else
                        temp = 0;

                    if (modValues.CV.CompareToCV)
                    {
                        if (i - 2 >= 0)
                            temp1 = (1 - InterpFactor) * modValues.CV.CashValue[i - 1] + InterpFactor * modValues.CV.CashValue[i - 2];
                        else
                            temp1 = (1 - InterpFactor) * modValues.CV.CashValue[i - 1];
                    }
                    else
                        temp1 = 0;

                    UnusualCV_ComparedToCV[i] = Math.Max(temp, temp1);
                }
            }
            else
                for (int i = modValues.Insured.IssueAge + 1; i <= modValues.Benefits.BenefitCeaseAge; i++)
                    UnusualCV_ComparedToCV[i] = 0;
        }


        public void CalcMY_MinimumUnitary(int endAge, int beginAge)
        {
            double temp1 = 0, temp = 0;
            for (int i = beginAge + 1; i <= endAge; i++)
                if (modValues.Reserves.ModRes == ModResType.OptionalYrtArt)
                    MYMinimumUnitary[i] = 0;
                else
                {
                    if (MidYear == MidYearDefn.Mean)
                    {
                        if (MinimumMeanReserveDefn == MinimumMeanReserveDefn.CalculatedNetPremium)
                            temp1 = modValues.Reserves.NetPremMinimumUnitary[i - 1];
                        else if (MinimumMeanReserveDefn == MinimumMeanReserveDefn.GPIfLessNP)
                            temp1 = Math.Min(modValues.Reserves.NetPremMinimumUnitary[i - 1], modValues.Premium.FinalGrossPremiums[i - beginAge - 1]);
                    }
                    else
                        temp1 = 0;
                    if (MYRoundingRule == MidYearRoundingRule.RoundTerminalsNetFirst)
                        temp = InterpFactor * (
                            Round(modValues.Reserves.MinUnitary_MinimumUnitary[i - 1], modValues.OutputClass.UnitTerminalReserveRound, modValues.OutputClass.eUnitTerminalReserveRound) +
                            Round(modValues.Reserves.NetPremMinimumUnitary[i - 1], modValues.OutputClass.UnitNetPremRound, modValues.OutputClass.eUnitNetPremRound)) +
                            (1 - InterpFactor) * Round(modValues.Reserves.MinUnitary_MinimumUnitary[i], modValues.OutputClass.UnitTerminalReserveRound, modValues.OutputClass.eUnitTerminalReserveRound);
                    else
                        temp = InterpFactor * (modValues.Reserves.MinUnitary_MinimumUnitary[i - 1] +
                            modValues.Reserves.NetPremMinimumUnitary[i - 1]) +
                            (1 - InterpFactor) * modValues.Reserves.MinUnitary_MinimumUnitary[i];
                    MYMinimumUnitary[i] = Round(temp, modValues.OutputClass.UnitMeanReserveRound, modValues.OutputClass.eUnitMeanReserveRound);
                }

        }
        public void CalcMY_MinimumSegmented(int endAge, int beginAge)
        {
            double temp, temp1 = 0;
            for (int i = beginAge + 1; i <= endAge; i++)
            {
                if (MidYear == MidYearDefn.Mean)
                {
                    if (MinimumMeanReserveDefn == MinimumMeanReserveDefn.CalculatedNetPremium)
                        temp1 = modValues.Reserves.NetPremBasicSegmented[i - 1];
                    else if (MinimumMeanReserveDefn == MinimumMeanReserveDefn.GPIfLessNP)
                        temp1 = Math.Min(modValues.Reserves.NetPremBasicSegmented[i - 1], modValues.Premium.FinalGrossPremiums[i - beginAge - 1]);
                }
                else
                    temp1 = 0;
                if (MYRoundingRule == MidYearRoundingRule.RoundTerminalsNetFirst)
                    temp = InterpFactor * (
            Round(modValues.Reserves.MinSegmented_MinimumSegmented[i - 1], modValues.OutputClass.UnitTerminalReserveRound, modValues.OutputClass.eUnitTerminalReserveRound) +
            Round(temp1, modValues.OutputClass.UnitNetPremRound, modValues.OutputClass.eUnitNetPremRound)) +
            (1 - InterpFactor) * Round(modValues.Reserves.MinSegmented_MinimumSegmented[i], modValues.OutputClass.UnitTerminalReserveRound, modValues.OutputClass.eUnitTerminalReserveRound);
                else
                    temp = InterpFactor * (modValues.Reserves.MinSegmented_MinimumSegmented[i - 1] +
                    temp1) + (1 - InterpFactor) * modValues.Reserves.MinSegmented_MinimumSegmented[i];
                MYMinimumSegmented[i] = Round(temp, modValues.OutputClass.UnitMeanReserveRound, modValues.OutputClass.eUnitMeanReserveRound);
            }

        }
        public void CalcMY_MinimumMean(int endAge, int beginAge)
        {
            for (int i = beginAge + 1; i <= endAge; i++)
                if (modValues.Reserves.MinUnitary_DeficiencyReserve[beginAge] > 0 || modValues.Reserves.MinSegmented_DeficiencyReserve[beginAge] > 0)
                {
                    if (!modValues.Reserves.XXX && modValues.Reserves.ModRes != ModResType.SegmentedMethod)
                        MYMinimumMean[i] = MYMinimumUnitary[i];
                    else if (modValues.Reserves.ModRes == ModResType.SegmentedMethod)
                        MYMinimumMean[i] = MYMinimumSegmented[i];
                    else if (modValues.Reserves.ModRes == ModResType.OptionalYrtArt)
                        MYMinimumMean[i] = 0;
                    else
                        if (ComparisonRule == ComparisonRuleReserve.CompareMidYear)
                    {
                        if (MYBasicSegmented[i] >= MYBasicUnitary[i])
                            MYMinimumMean[i] = MYMinimumSegmented[i];
                        else
                            MYMinimumMean[i] = MYMinimumUnitary[i];
                    }
                    else
                    if (Round(TerminalBasicSegmented[i], modValues.OutputClass.UnitTerminalReserveRound, modValues.OutputClass.eUnitTerminalReserveRound) >=
                            Round(TerminalBasicUnitary[i], modValues.OutputClass.UnitTerminalReserveRound, modValues.OutputClass.eUnitTerminalReserveRound))
                        MYMinimumMean[i] = MYMinimumSegmented[i];
                    else
                        MYMinimumMean[i] = MYMinimumUnitary[i];
                }
                else
                    MYMinimumMean[i] = MYBasicMean[i];
        }

        public void CalcMY_MinimumFinal(int endAge, int beginAge)
        {
            for (int i = beginAge + 1; i <= endAge; i++)
                if (MYMinimumFinal[i] < 0 && MYBasicMean[i] < 0)
                {
                    MYMinimumFinal[i] = 0;
                    MYMinimumDeficiencyFinal[i] = 0;
                }
                else if (modValues.Reserves.ModRes == ModResType.OptionalYrtArt)
                {
                    MYMinimumFinal[i] = BasicArt_MidYearReserve[i];
                    MYMinimumDeficiencyFinal[i] = BasicArt_MidYearDeficiencyReserve[i];
                }
                else
                {
                    MYMinimumFinal[i] = MYMinimumMean[i];
                    MYMinimumDeficiencyFinal[i] = Math.Max(0, MYMinimumMean[i] - MYBasicMean[i]);
                }
        }
        private double Round(double x, int places, eRound rule)
        {
            if (places == 0)
                switch (rule)
                {
                    case eRound.RoundDown:
                    case eRound.RoundLowInteger:
                        return Math.Floor(x);
                    case eRound.RoundHighCent:
                        return Round(x, 2, eRound.RoundUp);
                    case eRound.RoundHighInteger:
                    case eRound.RoundUp:
                        return Math.Ceiling(x);
                    case eRound.RoundLowCent:
                        return Round(x, 2, eRound.RoundDown);
                    case eRound.RoundNear:
                    case eRound.RoundNearInteger:
                        return Math.Round(x, 0);
                    case eRound.RoundNone:
                        return x;
                    case eRound.RoundNearCent:
                        return Math.Round(x, 2);
                    default:
                        throw new ArgumentException("Rounding rule " + rule + " not supported");
                }
            else
                if (rule == eRound.RoundDown || rule == eRound.RoundNear || rule == eRound.RoundUp)
                    return Round(x * Math.Pow(10, places), 0, rule) / Math.Pow(10, places);
                else
                    return Round(x, 0, rule);
        }
    }
}
