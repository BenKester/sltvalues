﻿namespace SLTValues
{
    public class SaltPremium
    {
        public enum GPType { GPConstant = 0, GPVary = 1};
        public double[] PolicyFee { get; set; }
        public double[] GrossPremiums { get; set; }
        public double[] FinalGrossPremiums { get; set; }
        public int PremiumCeaseAge { get; set; }
        public GPType GrossPremiumType { get; set; }
        public double[] PVFGP { get; set; }
        public double[] ax { get; set; }
        public double[] Barx { get; set; }
        public BasicMinimumOptions PolicyFeeDefn { get; set; }
        public SaltPremium()
        {
            PolicyFee = new double[131];
            GrossPremiums = new double[131];
            FinalGrossPremiums = new double[131];
            PVFGP = new double[131];
            ax = new double[131];
            Barx = new double[131];
        }
        public void CalcPVFGrossPrems(int endAge, int beginAge, TimingType timing)
        {
            double premiumTiming, dbTiming;
            for (int i = endAge - 1; i > beginAge; i--)
            {
                double interestRate = modValues.IntRate.FinalInterestRate[i - modValues.Insured.IssueAge];
                if (timing == TimingType.Curtate)
                {
                    dbTiming = 1; premiumTiming = 1;
                }
                else
                {
                    dbTiming = interestRate / modValues.Delta(interestRate);
                    if (timing == TimingType.SemiContinuous)
                        premiumTiming = 1;
                    else
                        premiumTiming = (1 - 1 / (1 + interestRate)) / modValues.Delta(interestRate) - modValues.Beta(interestRate) * modValues.Mortality.FinalMortality[i] / (1 + interestRate);
                }
                if (i < PremiumCeaseAge)
                {
                    if (i == endAge - 1)
                    {
                        ax[i] = 1 * premiumTiming;
                        PVFGP[i] = FinalGrossPremiums[i - modValues.Insured.IssueAge] * premiumTiming;
                    }
                    else
                    {
                        ax[i] = 1 * premiumTiming + ((1 - modValues.Mortality.FinalMortality[i]) * ax[i + 1]) / (1 + interestRate);
                        PVFGP[i] = FinalGrossPremiums[i - modValues.Insured.IssueAge] * premiumTiming + ((1 - modValues.Mortality.FinalMortality[i]) * PVFGP[i + 1]) / (1 + interestRate);
                    }
                }
                else
                {
                    ax[i] = 0; PVFGP[i] = 0;
                }
            }

        }
        public void CalcFinalGrossPrems(int endAge, int beginAge, BasicMinimumOptions basicMinimum)
        {
            for (int i = 0; i < endAge - beginAge - 1; i++)
                if (basicMinimum == BasicMinimumOptions.MinimumOnly)
                {
                    if (PolicyFeeDefn == BasicMinimumOptions.BasicMinimum || PolicyFeeDefn == BasicMinimumOptions.MinimumOnly)
                        FinalGrossPremiums[i] = GrossPremiums[i] + PolicyFee[i];
                     else
                        FinalGrossPremiums[i] = GrossPremiums[i];
                }
                else
                {
                    if (PolicyFeeDefn == BasicMinimumOptions.BasicMinimum || PolicyFeeDefn == BasicMinimumOptions.BasicOnly)
                        FinalGrossPremiums[i] = GrossPremiums[i] + PolicyFee[i];
                    else
                        FinalGrossPremiums[i] = GrossPremiums[i];
                }
        }

    }
}
