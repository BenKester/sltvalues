﻿using System;
namespace SLTValues
{
    public class SaltReserves
    {
        private double rn;
        public bool NLPConstantPercent { get; set; }
        public ModResType ModRes { get; set; }
        public double[] MidTermReserves { get; set; }
        public double[] BasicUnitary { get; set; }
        public double[] SegmentedBasic { get; set; }
        public double[] MinUnitary_MinimumUnitary { get; set; }
        public double[] SegmentedMinimum { get; set; }
        public double[] PVFNetPrem { get; set; }
        public double[] PVFModPrem { get; set; }
        public PremiumType PremiumType { get; set; }
        public Dur1NetPremForDefRes Dur1NetPremForDR { get; set; }
        public int CRVMYears { get; set; }
        public int GradedYears { get; set; }
        public double BetaCRVM { get; set; }
        public double ExpenseAllow { get; set; }
        public double BetaFPT { get; set; }
        public double[] DefModReserves { get; set; }
        public double[] DefNLReserves { get; set; }
        public double ELRA { get; set; }
        public bool ExpenseAllowCurtate { get; set; }
        public bool XXX { get; set; }
        public bool ImportNetPremiums { get; set; }
        public double[] ModifiedNetPremium { get; set; }
        public double[] ModifiedReserves { get; set; }
        public double[] NetAnnualPremiums { get; set; }
        public VBArray<double> NetLevelReserves { get; set; }
        public double rc { get; set; }
        public double Ren19PayPrem { get; set; }
        public SegmentType SegmentTypeDefined { get; set; }
        public EndowOptionXXX EndowOptionXXX { get; set; }
        public BasicMinimumOptions EndowChoiceXXX { get; set; }
        public DB19Pay DB19PayOption { get; set; }
        public double rf { get; set; }
        public double FinalTerminalReserve { get; set; }
        public double FinalTerminalReserveDef { get; set; }
        public double[] TotalNLReserves { get; set; }
        public double[] BasicReserveArt { get; set; }
        public double[,] TotalModReserves { get; set; }
        public double[] NetPrem_CoiArt { get; set; }
        public double[] MinimumOneHalfCx { get; set; }
        public double[] NetPremMinimumOneHalfCx { get; set; }
        public double[] NetPremBasicUnitary { get; set; }
        public double[] NetPremBasicSegmented { get; set; }
        public double[] NetPremMinimumUnitary { get; set; }
        public double[] NetPremMinimumSegmented { get; set; }
        public double[] MinUnitary_PremDeficiency { get; set; }
        public double[] BasicArt_PremDeficiency { get; set; }
        public double[] BasicArt_TerminalReserve { get; set; }
        public double[] BasicArt_PVPremDeficiency { get; set; }
        public double[] BasicArt_DeficiencyReserve { get; set; }
        public double[] MinUnitary_PVPremDeficiency { get; set; }
        public double[] MinUnitary_DeficiencyReserve { get; set; }
        public double[] MinSegmented_PremDeficiency { get; set; }
        public double[] MinSegmented_PVPremDeficiency { get; set; }
        public double[] MinSegmented_DeficiencyReserve { get; set; }
        public double[] MinSegmented_MinimumSegmented { get; set; }
        public double[] DeficiencyReserveMinimumSegmented { get; set; }
        public double[] TerminalReserveMinimumUnitary { get; set; }
        public int TerminalReserveRounding { get; set; }
        public int PremiumCeaseAge { get; set; }
        public int NetPremiumRounding { get; set; }
        public SaltReserves()
        {
            ExpenseAllowCurtate = true;
            EndowOptionXXX = EndowOptionXXX.NotUsed;
            EndowChoiceXXX = BasicMinimumOptions.None;
            MidTermReserves = new double[131];
            BasicUnitary = new double[131];
            SegmentedBasic = new double[131];
            MinUnitary_MinimumUnitary = new double[131];
            SegmentedMinimum = new double[131];
            PVFNetPrem = new double[131];
            PVFModPrem = new double[131];
            DefModReserves = new double[131];
            DefNLReserves = new double[131];
            ModifiedNetPremium = new double[131];
            ModifiedReserves = new double[131];
            NetAnnualPremiums = new double[131];
            NetLevelReserves = new VBArray<double>(-1, 131);
            TotalModReserves = new double[7, 31];
            TotalNLReserves = new double[131];
            BasicReserveArt = new double[131];
            NetPrem_CoiArt = new double[131];
            MinimumOneHalfCx = new double[131];
            NetPremMinimumOneHalfCx = new double[131];
            NetPremBasicUnitary = new double[131];
            NetPremBasicSegmented = new double[131];
            NetPremMinimumUnitary = new double[131];
            NetPremMinimumUnitary = new double[131];
            MinUnitary_PremDeficiency = new double[131];
            BasicArt_PremDeficiency = new double[131];
            BasicArt_PVPremDeficiency = new double[131];
            BasicArt_TerminalReserve = new double[131];
            BasicArt_DeficiencyReserve = new double[131];
            MinUnitary_PVPremDeficiency = new double[131];
            MinUnitary_DeficiencyReserve = new double[131];
            MinSegmented_PremDeficiency = new double[131];
            MinSegmented_PVPremDeficiency = new double[131];
            MinSegmented_DeficiencyReserve = new double[131];
            MinSegmented_MinimumSegmented = new double[131];
            DeficiencyReserveMinimumSegmented = new double[131];
            TerminalReserveMinimumUnitary = new double[131];
        }
        public double CalcAlpha(double previousReserves, double dxNum, double db,
                           double cx, double dx, double endow, double dx_1)
        {
            return (previousReserves * dxNum + db * cx + endow * dx_1) / dx;
        }
        public void CalcPremiums(int issueAge, int endAge, int beginAge, bool useExpAllow, bool useEndow, TimingType timing,
                     TerminalIndicator terminalType)
        {
            double pvfnp, annuityFactor, gradedPrem = 0, pvfcrvm, year1Benefit, pvEndowBenefit;
            bool useIL = false;
            if (!ImportNetPremiums)
            {
                if (modValues.Premium.PVFGP[beginAge] != 0)
                    rn = (modValues.Benefits.PVFB[beginAge]) / modValues.Premium.PVFGP[beginAge];
                else
                    rn = 0;
                if (NLPConstantPercent)
                    for (int i = endAge - 1; i >= beginAge; i--)
                        if (i >= modValues.Premium.PremiumCeaseAge)
                            NetAnnualPremiums[i] = 0;
                        else
                            NetAnnualPremiums[i] = rn * modValues.Premium.FinalGrossPremiums[i - modValues.Insured.IssueAge];
                else
                    for (int i = endAge - 1; i >= beginAge; i--)
                        if (modValues.Premium.ax[beginAge] == 0 || i >= modValues.Premium.PremiumCeaseAge)
                            NetAnnualPremiums[i] = 0;
                        else
                            NetAnnualPremiums[i] = modValues.Benefits.PVFB[beginAge] / modValues.Premium.ax[beginAge];
                if (useExpAllow && (beginAge + 1) < endAge)
                {
                    if (ModRes == ModResType.TabularCOI || terminalType == TerminalIndicator.BasicOneHalfCx)
                        for (int i = endAge - 1; i >= beginAge; i--)
                        {
                            if (timing == TimingType.Continuous)
                                pvEndowBenefit = modValues.Benefits.EndowAmount[i] * modValues.CommFns.Dx[i + 1] / modValues.CommFns.DBarx[i];
                            else
                                pvEndowBenefit = modValues.Benefits.EndowAmount[i] * modValues.CommFns.Dx[i + 1] / modValues.CommFns.Dx[i];
                            if (timing == TimingType.Curtate)
                                ModifiedNetPremium[i] = modValues.Benefits.DeathBenefit[i] * modValues.CommFns.Cx[i] / modValues.CommFns.Dx[i];
                            else if (timing == TimingType.SemiContinuous)
                                ModifiedNetPremium[i] = modValues.Benefits.DeathBenefit[i] * modValues.CommFns.CBarx[i] / modValues.CommFns.Dx[i];
                            else
                                ModifiedNetPremium[i] = modValues.Benefits.DeathBenefit[i] * modValues.CommFns.CBarx[i] / modValues.CommFns.DBarx[i];
                            ModifiedNetPremium[i] = ModifiedNetPremium[i] + pvEndowBenefit;
                            ExpenseAllow = 0;
                        }
                    else if (ModRes == ModResType.NetLevel)
                    {
                        for (int i = endAge - 1; i >= beginAge; i--)
                            ModifiedNetPremium[i] = NetAnnualPremiums[i];
                        ExpenseAllow = 0;
                    }
                    else
                    {
                        if (timing == TimingType.Curtate || ExpenseAllowCurtate)
                            Ren19PayPrem = modValues.CommFns.Mx[beginAge + 1] / (modValues.CommFns.Nx[beginAge + 1] - modValues.CommFns.Nx[beginAge + 20]);
                        else if (timing == TimingType.SemiContinuous)
                            Ren19PayPrem = modValues.CommFns.MBarx[beginAge + 1] / (modValues.CommFns.Nx[beginAge + 1] - modValues.CommFns.Nx[beginAge + 20]);
                        else
                            Ren19PayPrem = modValues.CommFns.MBarx[beginAge + 1] / (modValues.CommFns.NBarx[beginAge + 1] - modValues.CommFns.NBarx[beginAge + 20]);
                        if (modValues.Premium.ax[beginAge + 1] != 0)
                            BetaFPT = modValues.Benefits.PVFB[beginAge + 1] / modValues.Premium.ax[beginAge + 1];
                        else
                            BetaFPT = 0;
                        if (modValues.Benefits.DBType == SaltBenefits.DBTypeConstants.DBConstant)
                            ELRA = modValues.Benefits.DeathBenefit[beginAge];
                        else
                           if (DB19PayOption == DB19Pay.AverageDB)
                        {
                            if (modValues.Benefits.DBType == SaltBenefits.DBTypeConstants.DBConstant)
                                ELRA = modValues.Benefits.DeathBenefit[beginAge];
                            else
                            {
                                ELRA = 0;
                                int j;
                                for (j = 2; j <= Math.Min(10, modValues.Premium.PremiumCeaseAge - modValues.Insured.IssueAge); j++)
                                    ELRA = ELRA + modValues.Benefits.DeathBenefit[beginAge + j - 1];
                                if (j > 2) ELRA = ELRA / (j - 2);
                            }
                        }
                        else
                            ELRA = modValues.Benefits.PVFB_NoEndow[beginAge + 1] / ((modValues.CommFns.Mx[beginAge + 1] - modValues.CommFns.Mx[endAge]) /
                                            modValues.CommFns.Dx[beginAge + 1]);


                        if (modValues.Premium.PVFGP[beginAge + 1] != 0)
                            rf = (modValues.Benefits.PVFB[beginAge + 1]) / modValues.Premium.PVFGP[beginAge + 1];
                        else
                            rf = 0;
                        if (rf * modValues.Premium.FinalGrossPremiums[0] <= ELRA * Ren19PayPrem || ModRes == ModResType.FPT)
                        {
                            PremiumType = PremiumType.LowPremium;
                            for (int i = endAge - 1; i >= beginAge + 1; i--)
                                ModifiedNetPremium[i] = rf * modValues.Premium.FinalGrossPremiums[i - beginAge];
                        }
                        else
                        {
                            PremiumType = PremiumType.HighPremium;
                            if (timing == TimingType.Curtate || ExpenseAllowCurtate)
                                year1Benefit = modValues.Benefits.DeathBenefit[beginAge] * modValues.CommFns.Cx[beginAge] / modValues.CommFns.Dx[beginAge];
                            else
                                year1Benefit = modValues.Benefits.DeathBenefit[beginAge] * modValues.CommFns.CBarx[beginAge] / modValues.CommFns.Dx[beginAge];
                            if (modValues.gcblnIncludeEndowInAlpha && useEndow && modValues.Benefits.EndowAmount[beginAge] > 0)
                                year1Benefit = year1Benefit + modValues.Benefits.EndowAmount[beginAge] * modValues.CommFns.Dx[beginAge + 1] / modValues.CommFns.Dx[beginAge];
                            if (ModRes == ModResType.ILStandard && modValues.Premium.PremiumCeaseAge - issueAge > 20)
                            {
                                ExpenseAllow = (ELRA * Ren19PayPrem - year1Benefit) / (modValues.Premium.PVFGP[beginAge] - modValues.Premium.PVFGP[beginAge + 20] * modValues.CommFns.Dx[beginAge + 20] / modValues.CommFns.Dx[beginAge]);
                                useIL = true;
                            }
                            else
                                ExpenseAllow = (ELRA * Ren19PayPrem - year1Benefit) / modValues.Premium.PVFGP[beginAge];
                            rc = (modValues.Benefits.PVFB[beginAge]) / modValues.Premium.PVFGP[beginAge] + ExpenseAllow;
                            for (int i = endAge - 1; i >= beginAge + 1; i--)
                                if (useIL && i >= beginAge + 20)
                                    ModifiedNetPremium[i] = NetAnnualPremiums[i];
                                else
                                    ModifiedNetPremium[i] = rc * modValues.Premium.FinalGrossPremiums[i - beginAge];
                        }

                        if (PremiumType == PremiumType.LowPremium)
                        {
                            if (timing == TimingType.Curtate)
                                ModifiedNetPremium[beginAge] = modValues.Benefits.DeathBenefit[beginAge] * modValues.CommFns.Cx[beginAge] / modValues.CommFns.Dx[beginAge];
                            else if (timing == TimingType.SemiContinuous)
                                ModifiedNetPremium[beginAge] = modValues.Benefits.DeathBenefit[beginAge] * modValues.CommFns.CBarx[beginAge] / modValues.CommFns.Dx[beginAge];
                            else
                                ModifiedNetPremium[beginAge] = modValues.Benefits.DeathBenefit[beginAge] * modValues.CommFns.CBarx[beginAge] / modValues.CommFns.DBarx[beginAge];
                            if (useEndow && modValues.Benefits.EndowAmount[beginAge] > 0)
                            {
                                if (timing == TimingType.Curtate || timing == TimingType.SemiContinuous)
                                    ModifiedNetPremium[beginAge] = ModifiedNetPremium[beginAge] + modValues.Benefits.EndowAmount[beginAge] * modValues.CommFns.Cx[beginAge + 1] / modValues.CommFns.Dx[beginAge];
                                else
                                    ModifiedNetPremium[beginAge] = ModifiedNetPremium[beginAge] + modValues.Benefits.EndowAmount[beginAge] * modValues.CommFns.Cx[beginAge + 1] / modValues.CommFns.DBarx[beginAge];
                            }
                            ExpenseAllow = ModifiedNetPremium[beginAge + 1] - ModifiedNetPremium[beginAge];
                        }
                        if (ModifiedNetPremium[beginAge] > ModifiedNetPremium[beginAge + 1])
                        {
                            for (int i = endAge - 1; i >= beginAge; i--)
                                ModifiedNetPremium[i] = NetAnnualPremiums[i];
                            ExpenseAllow = 0;
                        }
                        if (ModRes == ModResType.GradedUnitary || ModRes == ModResType.NJStandard)
                        {
                            if (ModRes == ModResType.NJStandard)
                            {
                                CRVMYears = 1;
                                GradedYears = 19;
                            }
                            if (timing == TimingType.Continuous)
                            {
                                pvfcrvm = (modValues.CommFns.NBarx[issueAge + 1] - modValues.CommFns.NBarx[issueAge + CRVMYears]) / modValues.CommFns.Dx[issueAge + 1] * ModifiedNetPremium[issueAge + 1];
                                pvfnp = (modValues.CommFns.NBarx[issueAge + CRVMYears + GradedYears] - modValues.CommFns.NBarx[modValues.Premium.PremiumCeaseAge]) / modValues.CommFns.Dx[issueAge + 1] * NetAnnualPremiums[Math.Min(issueAge + CRVMYears + GradedYears, modValues.Premium.PremiumCeaseAge - 1)];
                                annuityFactor = (modValues.CommFns.NBarx[issueAge + CRVMYears] - modValues.CommFns.NBarx[issueAge + CRVMYears + GradedYears]) / modValues.CommFns.Dx[issueAge + 1];
                            }
                            else
                            {
                                pvfcrvm = (modValues.CommFns.Nx[issueAge + 1] - modValues.CommFns.Nx[issueAge + CRVMYears]) / modValues.CommFns.Dx[issueAge + 1] * ModifiedNetPremium[issueAge + 1];
                                pvfnp = (modValues.CommFns.Nx[issueAge + CRVMYears + GradedYears] - modValues.CommFns.Nx[modValues.Premium.PremiumCeaseAge]) / modValues.CommFns.Dx[issueAge + 1] * NetAnnualPremiums[Math.Min(issueAge + CRVMYears + GradedYears, modValues.Premium.PremiumCeaseAge - 1)];
                                annuityFactor = (modValues.CommFns.Nx[issueAge + CRVMYears] - modValues.CommFns.Nx[issueAge + CRVMYears + GradedYears]) / modValues.CommFns.Dx[issueAge + 1];
                            }
                            if (annuityFactor != 0)
                                gradedPrem = (modValues.Benefits.PVFB[issueAge + 1] - pvfnp - pvfcrvm) / annuityFactor;
                            for (int i = endAge - 1; i >= beginAge; i--)
                                if (i >= issueAge + CRVMYears && i < issueAge + CRVMYears + GradedYears && i < modValues.Premium.PremiumCeaseAge)
                                    ModifiedNetPremium[i] = gradedPrem;
                                else if (i >= issueAge + CRVMYears + GradedYears && i < modValues.Premium.PremiumCeaseAge)
                                    ModifiedNetPremium[i] = NetAnnualPremiums[i];
                        }
                    }
                    if (ModRes == ModResType.TabularCOI || terminalType == TerminalIndicator.BasicOneHalfCx)
                    { }
                    else
                    {
                        for (int i = endAge - 1; i >= beginAge + 1; i--)
                            ModifiedNetPremium[i] = Math.Max(ModifiedNetPremium[i], NetAnnualPremiums[i]);
                        ModifiedNetPremium[beginAge] = Math.Min(ModifiedNetPremium[beginAge], NetAnnualPremiums[beginAge]);
                    }
                }
                else
                    for (int i = endAge - 1; i >= beginAge; i--)
                        ModifiedNetPremium[i] = NetAnnualPremiums[i];
            }
            if (modValues.MidYear.TermReserveDur == eTermReserveDur.NegExpAllowDurZero && terminalType != TerminalIndicator.BasicOneHalfCx)
                if (ModifiedNetPremium.GetUpperBound(0) >= beginAge + 1)
                    if (beginAge == issueAge)
                        if (ModifiedNetPremium[beginAge + 1] > 0)
                            if (ModRes == ModResType.FPT || ModRes == ModResType.ILStandard || ModRes == ModResType.NJStandard || ModRes == ModResType.NY147 || ModRes == ModResType.OptionalExemptionFromUnitary ||
                                  ModRes == ModResType.SegmentedMethod || ModRes == ModResType.Unitary || ModRes == ModResType.XXX1995 || ModRes == ModResType.XXX1999)
                                ModifiedNetPremium[beginAge] = Math.Max(ModifiedNetPremium[beginAge + 1], ModifiedNetPremium[beginAge]);
        }
        public void CalcReserve(bool reCalc, int endAge, int beginAge, bool useExpAllow, bool useEndow, TimingType timing, TerminalIndicator terminalType)
        {
            double myIntRate, dbTiming, premiumTiming;
            for (int i = endAge; i >= beginAge; i--)
            {
                myIntRate = modValues.IntRate.FinalInterestRate[i - beginAge];
                if (timing == TimingType.Curtate)
                {
                    dbTiming = 1;
                    premiumTiming = 1;
                }
                else
                {
                    if (modValues.Delta(myIntRate) != 0)
                        dbTiming = myIntRate / modValues.Delta(myIntRate);
                    else
                        dbTiming = 1;
                    if (timing == TimingType.SemiContinuous || modValues.CommFns.Dx[i] == 0)
                        premiumTiming = 1;
                    else
                        premiumTiming = modValues.CommFns.DBarx[i] / modValues.CommFns.Dx[i];
                }
                if (ModRes == ModResType.TabularCOI || terminalType == TerminalIndicator.BasicOneHalfCx)
                    ModifiedReserves[i] = 0;
                else
                {
                    if (i >= beginAge && i < endAge)
                    {
                        PVFNetPrem[i] = NetAnnualPremiums[i] * premiumTiming + (1 - modValues.Mortality.FinalMortality[i]) * PVFNetPrem[i + 1] / (1 + myIntRate);
                        if (PremiumType == PremiumType.HighPremium && i == beginAge && useExpAllow)
                        {
                            if (timing == TimingType.Curtate)
                                ModifiedNetPremium[beginAge] = CalcAlpha(Math.Max(ModifiedReserves[i + 1], 0), modValues.CommFns.Dx[beginAge + 1], modValues.Benefits.DeathBenefit[beginAge], modValues.CommFns.Cx[beginAge], modValues.CommFns.Dx[beginAge], modValues.Benefits.EndowAmount[beginAge], modValues.CommFns.Dx[beginAge + 1]);
                            else if (timing == TimingType.SemiContinuous)
                                ModifiedNetPremium[beginAge] = CalcAlpha(Math.Max(ModifiedReserves[i + 1], 0), modValues.CommFns.Dx[beginAge + 1], modValues.Benefits.DeathBenefit[beginAge], modValues.CommFns.CBarx[beginAge], modValues.CommFns.Dx[beginAge], modValues.Benefits.EndowAmount[beginAge], modValues.CommFns.Dx[beginAge + 1]);
                            else
                                ModifiedNetPremium[beginAge] = CalcAlpha(Math.Max(ModifiedReserves[i + 1], 0), modValues.CommFns.Dx[beginAge + 1], modValues.Benefits.DeathBenefit[beginAge], modValues.CommFns.CBarx[beginAge], modValues.CommFns.DBarx[beginAge], modValues.Benefits.EndowAmount[beginAge], modValues.CommFns.Dx[beginAge + 1]);
                            if (ModifiedNetPremium[beginAge] > ModifiedNetPremium[beginAge + 1] && !reCalc)
                            {
                                for (int j = PremiumCeaseAge; j >= beginAge; j--)
                                    ModifiedNetPremium[j] = NetAnnualPremiums[j];
                                CalcReserve(true, endAge, beginAge, useExpAllow, useEndow, timing, terminalType);
                            }
                        }
                        PVFModPrem[i] = ModifiedNetPremium[i] * premiumTiming + (1 - modValues.Mortality.FinalMortality[i]) * PVFModPrem[i + 1] / (1 + myIntRate);


                        if (terminalType == TerminalIndicator.MinimumUnitary || terminalType == TerminalIndicator.MinimumSegmented)
                        {
                            if (NetAnnualPremiums[i] > modValues.Premium.FinalGrossPremiums[i - beginAge])
                                DefNLReserves[i] = (NetAnnualPremiums[i] - modValues.Premium.FinalGrossPremiums[i - beginAge]) * modValues.Mortality.FinalMortality[i] / (1 + myIntRate) + (1 - modValues.Mortality.FinalMortality[i]) * DefNLReserves[i + 1] / (1 + myIntRate);
                            else
                                DefNLReserves[i] = (1 - modValues.Mortality.FinalMortality[i]) * DefNLReserves[i + 1] / (1 + myIntRate);
                            if (ModifiedNetPremium[i] > modValues.Premium.FinalGrossPremiums[i - beginAge])
                                DefModReserves[i] = (ModifiedNetPremium[i] - modValues.Premium.FinalGrossPremiums[i - beginAge]) * modValues.Mortality.FinalMortality[i] / (1 + myIntRate) + (1 - modValues.Mortality.FinalMortality[i]) * DefModReserves[i + 1] / (1 + myIntRate);
                            else
                                DefModReserves[i] = (1 - modValues.Mortality.FinalMortality[i]) * DefModReserves[i + 1] / (1 + myIntRate);
                        }
                        else
                            DefModReserves[i] = 0;
                    }
                    else if (i == endAge)
                    {
                        NetLevelReserves[i] = modValues.Benefits.PVFB[i];
                        ModifiedReserves[i] = modValues.Benefits.PVFB[i];
                        PVFModPrem[i] = 0;
                        PVFNetPrem[i] = 0;
                        DefNLReserves[i] = 0;
                        TotalNLReserves[i] = 0;
                        TotalModReserves[(int)terminalType, i] = 0;
                    }
                }
                NetLevelReserves[i] = modValues.Benefits.PVFB[i] - PVFNetPrem[i];
                if (ModRes == ModResType.TabularCOI || terminalType == TerminalIndicator.BasicOneHalfCx)
                    ModifiedReserves[i] = 0;
                else
                    ModifiedReserves[i] = modValues.Benefits.PVFB[i] - PVFModPrem[i];
                TotalNLReserves[i] = NetLevelReserves[i] + DefNLReserves[i];
                TotalModReserves[(int)terminalType, i] = ModifiedReserves[i] + DefModReserves[i];
                if (terminalType == TerminalIndicator.BasicUnitary)
                {
                    BasicUnitary[i] = ModifiedReserves[i];
                    NetPremBasicUnitary[i] = ModifiedNetPremium[i];
                }
                else if (terminalType == TerminalIndicator.BasicSegmented)
                {
                    SegmentedBasic[i] = ModifiedReserves[i];
                    NetPremBasicSegmented[i] = ModifiedNetPremium[i];
                }
                else if (terminalType == TerminalIndicator.BasicOneHalfCx)
                {
                    BasicReserveArt[i] = ModifiedReserves[i];
                    NetPrem_CoiArt[i] = ModifiedNetPremium[i];
                }
                else if (terminalType == TerminalIndicator.MinimumUnitary)
                {
                    TerminalReserveMinimumUnitary[i] = ModifiedReserves[i];
                    NetPremMinimumUnitary[i] = ModifiedNetPremium[i];
                }
                else if (terminalType == TerminalIndicator.MinimumSegmented)
                {
                    SegmentedMinimum[i] = ModifiedReserves[i];
                    NetPremMinimumSegmented[i] = ModifiedNetPremium[i];
                }
                else if (terminalType == TerminalIndicator.MinimumOneHalfCx)
                {
                    MinimumOneHalfCx[i] = ModifiedReserves[i];
                    NetPremMinimumOneHalfCx[i] = ModifiedNetPremium[i];
                }
            }
        }
        public void CalcETIReserve(bool reCalc, int endAge, int beginAge, bool useExpAllow, bool useEndow, TimingType timing,
                                   TerminalIndicator terminalType)
        {
            double myIntRate, dbTiming;
            for (int i = endAge; i >= beginAge; i--)
            {
                myIntRate = modValues.IntRate.FinalInterestRate[i - beginAge];
                if (timing == TimingType.Curtate)
                    dbTiming = 1;
                else
                 if (modValues.Delta(myIntRate) != 0)
                    dbTiming = myIntRate / modValues.Delta(myIntRate);
                else
                    dbTiming = 1;
                NetLevelReserves[i] = modValues.Benefits.DeathBenefit[i] * dbTiming * (modValues.CommFns.Mx[beginAge] - modValues.CommFns.Mx[i]) / modValues.CommFns.Dx[beginAge];
                ModifiedReserves[i] = NetLevelReserves[i];
                DefNLReserves[i] = 0;
                DefModReserves[i] = 0;
                TotalNLReserves[i] = NetLevelReserves[i] + DefNLReserves[i];
                TotalModReserves[(int)terminalType, i] = ModifiedReserves[i] + DefModReserves[i];
            }
            NetLevelReserves[-1] = modValues.Benefits.DeathBenefit[beginAge] * modValues.CommFns.Dx[endAge] / modValues.CommFns.Dx[beginAge];


        }
        public void CalcMaxTerminals(int endAge, int beginAge, TerminalIndicator terminalType)
        { }

        public void CalcDeficiencyReserve(int endAge, int beginAge, TerminalIndicator terminalType)
        {
            double myIntRate, dbTiming, premiumTiming;
            //The interest rate && mortality still should be that was used for the minimum calculations unless doing the YRT method which will use basic
            MinUnitary_DeficiencyReserve[0] = 0;
            for (int i = endAge - 1; i >= beginAge; i--)
            {
                myIntRate = modValues.IntRate.FinalInterestRate[i - beginAge];
                if (modValues.gobjSave.Timing == TimingType.Curtate)
                {
                    dbTiming = 1;
                    premiumTiming = 1;
                }
                else
                {
                    if (modValues.Delta(myIntRate) != 0)
                        dbTiming = myIntRate / modValues.Delta(myIntRate);
                    else
                        dbTiming = 1;
                    if (modValues.gobjSave.Timing == TimingType.SemiContinuous || modValues.CommFns.Dx[i] == 0)
                        premiumTiming = 1;
                    else
                        premiumTiming = modValues.CommFns.DBarx[i] / modValues.CommFns.Dx[i];
                }
                if (terminalType == TerminalIndicator.BasicOneHalfCx)
                    BasicArt_PremDeficiency[i] = Math.Max(NetPrem_CoiArt[i] - modValues.Premium.FinalGrossPremiums[i - beginAge], 0);
                else
         if (i - modValues.Insured.IssueAge + 1 <= modValues.Segmentation.SegmentsGroup[modValues.Segmentation.SegmentCount] - modValues.Insured.IssueAge &&
                    modValues.Segmentation.SegmentsGroup[modValues.Segmentation.SegmentCount] - modValues.Insured.IssueAge <= 5 &&
                    (ModRes == ModResType.NY147 || ModRes == ModResType.XXX1995))
                {
                    MinUnitary_PremDeficiency[i] = 0;
                    MinSegmented_PremDeficiency[i] = 0;
                }
                else
                {
                    MinUnitary_PremDeficiency[i] = Math.Max(NetPremMinimumUnitary[i] - modValues.Premium.FinalGrossPremiums[i - beginAge], 0);
                    MinSegmented_PremDeficiency[i] = Math.Max(NetPremMinimumSegmented[i] - modValues.Premium.FinalGrossPremiums[i - beginAge], 0);
                }
                if (terminalType == TerminalIndicator.BasicOneHalfCx)
                    BasicArt_PVPremDeficiency[i] = BasicArt_PremDeficiency[i] * premiumTiming * modValues.CommFns.Dx[i] / modValues.CommFns.Dx[beginAge];
                else
                {
                    MinUnitary_PVPremDeficiency[i] = MinUnitary_PremDeficiency[i] * premiumTiming * modValues.CommFns.Dx[i] / modValues.CommFns.Dx[beginAge];
                    MinSegmented_PVPremDeficiency[i] = MinSegmented_PremDeficiency[i] * premiumTiming * modValues.CommFns.Dx[i] / modValues.CommFns.Dx[beginAge];
                }
                if (terminalType == TerminalIndicator.BasicOneHalfCx)
                {
                    if (i == endAge - 1)
                        BasicArt_DeficiencyReserve[beginAge] = BasicArt_PVPremDeficiency[i];
                    else
                        BasicArt_DeficiencyReserve[beginAge] = BasicArt_DeficiencyReserve[beginAge] + BasicArt_PVPremDeficiency[i];
                }
                else
         if (i == endAge - 1)
                {
                    MinUnitary_DeficiencyReserve[beginAge] = MinUnitary_PVPremDeficiency[i];
                    MinSegmented_DeficiencyReserve[beginAge] = MinSegmented_PVPremDeficiency[i];
                }
                else
                {
                    MinUnitary_DeficiencyReserve[beginAge] = MinUnitary_DeficiencyReserve[beginAge] + MinUnitary_PVPremDeficiency[i];
                    MinSegmented_DeficiencyReserve[beginAge] = MinSegmented_DeficiencyReserve[beginAge] + MinSegmented_PVPremDeficiency[i];
                }
            }
            for (int i = beginAge; i <= endAge; i++)
            {
                myIntRate = modValues.IntRate.FinalInterestRate[i - beginAge];
                if (modValues.gobjSave.Timing == TimingType.Curtate)
                {
                    dbTiming = 1;
                    premiumTiming = 1;
                }
                else
                {
                    if (modValues.Delta(myIntRate) != 0)
                        dbTiming = myIntRate / modValues.Delta(myIntRate);
                    else
                        dbTiming = 1;
                    if (modValues.gobjSave.Timing == TimingType.SemiContinuous || modValues.CommFns.Dx[i] == 0 || i == beginAge)
                        premiumTiming = 1;
                    else
                        premiumTiming = modValues.CommFns.DBarx[i - 1] / modValues.CommFns.Dx[i - 1];
                }
                if (i > beginAge)
                {
                    if (modValues.Mortality.FinalMortality[i - 1] < 1)
                        if (terminalType == TerminalIndicator.BasicOneHalfCx)
                            BasicArt_DeficiencyReserve[i] = (BasicArt_DeficiencyReserve[i - 1] * (1 + myIntRate) -
                                (BasicArt_PremDeficiency[i - 1] * (1 + myIntRate) * premiumTiming)) / (1 - modValues.Mortality.FinalMortality[i - 1]);
                        else
                        {
                            MinUnitary_DeficiencyReserve[i] = (MinUnitary_DeficiencyReserve[i - 1] * (1 + myIntRate) -
                                (MinUnitary_PremDeficiency[i - 1] * (1 + myIntRate) * premiumTiming)) / (1 - modValues.Mortality.FinalMortality[i - 1]);
                            MinSegmented_DeficiencyReserve[i] = (MinSegmented_DeficiencyReserve[i - 1] * (1 + myIntRate) -
                                (MinSegmented_PremDeficiency[i - 1] * (1 + myIntRate) * premiumTiming)) / (1 - modValues.Mortality.FinalMortality[i - 1]);
                        }
                    else
                    {
                        if (terminalType == TerminalIndicator.BasicOneHalfCx)
                            BasicArt_DeficiencyReserve[i] = 0;
                        else
                        {
                            MinUnitary_DeficiencyReserve[i] = 0;
                            MinSegmented_DeficiencyReserve[i] = 0;
                        }
                    }
                }

                if (terminalType == TerminalIndicator.BasicOneHalfCx)
                    BasicArt_TerminalReserve[i] = BasicArt_DeficiencyReserve[i];
                else
                {
                    MinUnitary_MinimumUnitary[i] = TerminalReserveMinimumUnitary[i] + MinUnitary_DeficiencyReserve[i];
                    MinSegmented_MinimumSegmented[i] = SegmentedMinimum[i] + MinSegmented_DeficiencyReserve[i];
                }
            }
        }

    }
}
