﻿using System;
namespace SLTValues
{
    public class SaltCommutationFunctions
    {
        public double[] Cx { get; set; }
        public double[] Dx { get; set; }
        public double[] lx { get; set; }
        public double[] Mx { get; set; }
        public double[] Nx { get; set; }
        public double[] Px { get; set; }
        public double[] Rx { get; set; }
        public double[] Sx { get; set; }
        public double[] Vx { get; set; }
        public double[] CBarx { get; set; }
        public double[] DBarx { get; set; }
        public double[] MBarx { get; set; }
        public double[] NBarx { get; set; }
        public double[] RBarx { get; set; }
        public double[] Deathx { get; set; }
        public double[] NxMthly { get; set; }
        public SaltCommutationFunctions()
        {
            Cx = new double[131];
            Dx = new double[131];
            lx = new double[131];
            Mx = new double[131];
            Nx = new double[131];
            Px = new double[131];
            Rx = new double[131];
            Sx = new double[131];
            Vx = new double[131];
            CBarx = new double[131];
            DBarx = new double[131];
            MBarx = new double[131];
            NBarx = new double[131];
            RBarx = new double[131];
            Deathx = new double[131];
            NxMthly = new double[131];
        }
        public void CalcCommFunctions(int beginAge, int endAge)
        {
            double myDiscount, myIntRate = 0;
            lx[beginAge] = 10000000;
            Dx[beginAge] = lx[beginAge];
            myDiscount = 1;
            for (int i = beginAge; i < endAge; i++)
            {
                if (modValues.IntRate.FinalInterestRate[i - beginAge] != 0)
                    myIntRate = modValues.IntRate.FinalInterestRate[i - beginAge];
                myDiscount = myDiscount / (1 + myIntRate);
                lx[i + 1] = lx[i] * (1 - modValues.Mortality.FinalMortality[i]);
                Dx[i + 1] = lx[i + 1] * myDiscount;
                DBarx[i] = modValues.Alpha(myIntRate) * Dx[i] - modValues.Beta(myIntRate) * (Dx[i] - Dx[i + 1]);
                Cx[i] = Dx[i] * modValues.Mortality.FinalMortality[i] / (1 + myIntRate);
                CBarx[i] = Cx[i] * myIntRate / modValues.Delta(myIntRate);
            }
            MBarx[endAge + 1] = 0;
            Mx[endAge + 1] = 0;
            NBarx[endAge + 1] = 0;
            Nx[endAge + 1] = 0;

            for (int i = endAge; i > beginAge; i--)
            {
                myIntRate = modValues.IntRate.FinalInterestRate[i - beginAge];
                Mx[i] = Mx[i + 1] + Cx[i];
                MBarx[i] = MBarx[i + 1] + CBarx[i];
                Nx[i] = Nx[i + 1] + Dx[i];
                NBarx[i] = NBarx[i + 1] + DBarx[i];
            }
        }
    }
}