﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace SLTValues
{
    public class SaltTRCommon
    {
        public SaltBenefits Benefits { get; set; }
        public SaltCommutationFunctions CommFunctions { get; set; }
        public SaltCV CV { get; set; }
        public SaltCVETI CVETI { get; set; }
        public SaltDueDeferred DueDeferred { get; set; }
        public SaltInsured Insured { get; set; }
        public SaltIntRates IntRates { get; set; }
        public SaltMidYear MidYear { get; set; }
        public SaltMortality Mortality { get; set; }
        public SaltPolicy Policy { get; set; }
        public SaltOutput OutputClass { get; set; }
        public SaltPremium Premium { get; set; }
        public SaltRecursive Recursive { get; set; }
        public SaltReserves Reserves { get; set; }
        public SaltSegmentation Segmentation { get; set; }
        public TimingType Timing { get; set; }
        public TimingType CVTiming { get; set; }
        public bool PrintDetail { get; set; }
        public bool PrintSummary { get; set; }
        public SaltIntRates IntRateValues { get; set; }
        public delegate void ReportGeneratorHandler(object sender, TerminalIndicator terminalType);
        public event ReportGeneratorHandler ReportGenerator;
        public SaltTRCommon()
        {
            Timing = TimingType.Curtate;
            CVTiming = TimingType.Curtate;
            Benefits = new SaltBenefits();
            CommFunctions = new SaltCommutationFunctions();
            CV = new SaltCV();
            DueDeferred = new SaltDueDeferred();
            Insured = new SaltInsured();
            IntRates = new SaltIntRates();
            Mortality = new SaltMortality();
            Policy = new SaltPolicy();
            Premium = new SaltPremium();
            Reserves = new SaltReserves();
            Segmentation = new SaltSegmentation();
            CVETI = new SaltCVETI();
            MidYear = new SaltMidYear();
            OutputClass = new SaltOutput();
        }
        public void CalcFunctions()
        {
            int endAge = 0;
            MortalityType mortType = MortalityType.BasicMort;
            bool expAllow, calcTerminals = false, recalcQ;
            SegmentType segmentType = SegmentType.Segmentation;
            TerminalIndicator terminalType;
            string header = "", lastHeader;
            BasicMinimumOptions terminalGroup, terminalGroupPrevious = BasicMinimumOptions.None;
            if ((new[] { ModResType.XXX1999, ModResType.NY147, ModResType.XXX1995, ModResType.OptionalExemptionFromUnitary,
                ModResType.OptionalYrtArt }).Contains(modValues.Reserves.ModRes))
            {
                Mortality.MortalityType = MortalityType.SegmentedMort;
                Segmentation.SegmentsGroup[1] = Benefits.BenefitCeaseAge;
                Segmentation.SegmentCount = 1;
                endAge = Mortality.CalcMortality(Insured.IssueAge, TerminalIndicator.SegmentedTerminal,
                  Insured.IssueAge2, "Segmented Assumptions");
                Segmentation.CalcSegments(Insured.IssueAge, SegmentType.Segmentation);
                ReportGenerator(this, TerminalIndicator.NA);
            }
            else
            {
                Mortality.MortalityType = MortalityType.BasicMort;
                Segmentation.SegmentsGroup[1] = Benefits.BenefitCeaseAge;
                Segmentation.SegmentCount = 1;
            }
            Premium.PremiumCeaseAge = Math.Min(Premium.PremiumCeaseAge, Benefits.BenefitCeaseAge);
            Benefits.CalcDB(Insured.IssueAge);


            lastHeader = "";
            terminalGroup = BasicMinimumOptions.None;
            for (terminalType = TerminalIndicator.CashValue; terminalType <= TerminalIndicator.MinimumOneHalfCx; terminalType++)
            {
                recalcQ = true;
                if (terminalType == TerminalIndicator.CashValue)
                {
                    if (modValues.Policy.CalcType == CalcTypeDef.Reserves)
                        calcTerminals = false;
                    else if ((int)modValues.CV.CVMethod >= 100)
                    {
                        calcTerminals = true;
                        modValues.CV.CVMethodFinal = eCVMethod.CVMethodReserve;
                        modValues.Reserves.ModRes = (ModResType)( (int)modValues.CV.CVMethod - 100);
                    }
                    else
                    {
                        calcTerminals = true;
                        modValues.CV.CVMethodFinal = modValues.CV.CVMethod;
                    }
                    mortType = MortalityType.BasicMort;
                    segmentType = SegmentType.UnitaryMethod;
                    if (calcTerminals)
                    {
                        lastHeader = header;
                        header = "Basic unitary assumptions";
                        terminalGroupPrevious = terminalGroup;
                        terminalGroup = BasicMinimumOptions.BasicOnly;
                    }
                    recalcQ = calcTerminals;
                }
                else if (terminalType == TerminalIndicator.BasicUnitary)
                {
                    if (modValues.Policy.CalcType == CalcTypeDef.Reserves)
                        calcTerminals = true;
                    else if ((int)modValues.CV.CVMethod >= 100)
                        calcTerminals = true;
                    else
                        calcTerminals = false;
                    mortType = MortalityType.BasicMort;
                    segmentType = SegmentType.UnitaryMethod;
                    if (calcTerminals)
                    {
                        lastHeader = header;
                        header = "Basic unitary assumptions";
                        terminalGroupPrevious = terminalGroup;
                        terminalGroup = BasicMinimumOptions.BasicOnly;
                    }
                    recalcQ = calcTerminals;
                }
                else if (terminalType == TerminalIndicator.BasicSegmented)
                {
                    mortType = MortalityType.BasicMort;
                    if ((new[] { ModResType.XXX1999, ModResType.XXX1995, ModResType.NY147 }).Contains(modValues.Reserves.ModRes))
                        calcTerminals = true;
                    else
                        calcTerminals = false;
                    segmentType = SegmentType.Segmentation;
                    if (calcTerminals)
                    {
                        lastHeader = header;
                        header = "Basic segmented assumptions";
                        terminalGroupPrevious = terminalGroup;
                        terminalGroup = BasicMinimumOptions.BasicOnly;
                        if (lastHeader == "Basic unitary assumptions")
                            recalcQ = false;
                    }
                }
                else if (terminalType == TerminalIndicator.BasicOneHalfCx)
                {
                    if (modValues.MidYear.MYRule == MidYearRule.CompareOneHalfCx)
                        calcTerminals = true;
                    else
                        calcTerminals = false;
                    if (modValues.Reserves.XXX)
                    {
                        recalcQ = true;
                        mortType = MortalityType.BasicMort1_2Cx;
                    }
                    else
                    {
                        recalcQ = false;
                        mortType = MortalityType.BasicMort;
                    }
                    segmentType = SegmentType.UnitaryMethod;
                    if (calcTerminals)
                    {
                        lastHeader = header;
                        header = "Basic 1/2 Cx assumptions";
                        terminalGroupPrevious = terminalGroup;
                        terminalGroup = BasicMinimumOptions.BasicOnly;
                    }
                }
                else if (terminalType == TerminalIndicator.MinimumUnitary)
                {
                    mortType = MortalityType.MinimumMort;
                    segmentType = SegmentType.UnitaryMethod;
                    if ((new[] { ModResType.XXX1999, ModResType.XXX1995, ModResType.NY147 }).Contains(modValues.Reserves.ModRes))
                        calcTerminals = true;
                    else
                        calcTerminals = false;
                    if (calcTerminals)
                    {
                        lastHeader = header;
                        header = "Minimum unitary assumptions";
                        terminalGroupPrevious = terminalGroup;
                        terminalGroup = BasicMinimumOptions.MinimumOnly;
                    }
                }
                else if (terminalType == TerminalIndicator.MinimumSegmented)
                {
                    mortType = MortalityType.MinimumMort;
                    segmentType = SegmentType.Segmentation;
                    if ((new[] { ModResType.XXX1999, ModResType.XXX1995, ModResType.NY147 }).Contains(modValues.Reserves.ModRes))
                        calcTerminals = true;
                    else
                        calcTerminals = false;
                    if (calcTerminals)
                    {
                        lastHeader = header;
                        header = "Minimum segmented assumptions";
                        terminalGroupPrevious = terminalGroup;
                        terminalGroup = BasicMinimumOptions.MinimumOnly;
                        if (lastHeader == "Minimum unitary assumptions")
                            recalcQ = false;
                    }
                }
                else if (terminalType == TerminalIndicator.MinimumOneHalfCx)
                {
                    calcTerminals = false;
                    recalcQ = false;
                    segmentType = SegmentType.UnitaryMethod;
                    lastHeader = header;
                    header = "Minimum 1/2 Cx assumptions";
                    terminalGroupPrevious = terminalGroup;
                    terminalGroup = BasicMinimumOptions.MinimumOnly;
                }
                if (calcTerminals)
                {
                    Mortality.MortalityType = mortType;
                    if (recalcQ)
                        endAge = Mortality.CalcMortality(Insured.IssueAge, terminalType,
                           Insured.IssueAge2, header);
                    if (terminalGroup == BasicMinimumOptions.BasicOnly && terminalGroupPrevious == BasicMinimumOptions.None)
                    {
                        Premium.CalcFinalGrossPrems(Benefits.BenefitCeaseAge, Insured.IssueAge, terminalGroup);
                        IntRates.CalcInterestRate(Insured.IssueAge, Benefits.BenefitCeaseAge, terminalType, header);
                    }
                    else if (terminalGroup == BasicMinimumOptions.MinimumOnly && (terminalGroupPrevious == BasicMinimumOptions.None || terminalGroupPrevious == BasicMinimumOptions.BasicOnly))
                    {
                        Premium.CalcFinalGrossPrems(Benefits.BenefitCeaseAge, Insured.IssueAge, terminalGroup);
                        IntRates.CalcInterestRate(Insured.IssueAge, Benefits.BenefitCeaseAge, terminalType, header);
                    }
                    if (segmentType == SegmentType.Segmentation)
                    {
                        if (recalcQ)
                            CommFunctions.CalcCommFunctions(Insured.IssueAge, endAge);
                        for (int mintsegcnt = 1; mintsegcnt <= Segmentation.SegmentCount; mintsegcnt++)
                        {
                            Benefits.CalcPVFB(Segmentation.SegmentsGroup[mintsegcnt + 1], Segmentation.SegmentsGroup[mintsegcnt], Timing, terminalType);
                            Premium.CalcPVFGrossPrems(Segmentation.SegmentsGroup[mintsegcnt], Segmentation.SegmentsGroup[mintsegcnt + 1], Timing);
                            expAllow = mintsegcnt == Segmentation.SegmentCount;
                            if (mintsegcnt == 1)
                                Reserves.CalcPremiums(Insured.IssueAge, Segmentation.SegmentsGroup[mintsegcnt], Segmentation.SegmentsGroup[mintsegcnt + 1], expAllow, true, Timing, terminalType);
                            else
                                Reserves.CalcPremiums(Insured.IssueAge, Segmentation.SegmentsGroup[mintsegcnt], Segmentation.SegmentsGroup[mintsegcnt + 1], expAllow, false, Timing, terminalType);
                            if (Insured.ETI)
                                Reserves.CalcETIReserve(false, Segmentation.SegmentsGroup[mintsegcnt], Segmentation.SegmentsGroup[mintsegcnt + 1], expAllow, true, Timing, terminalType);
                            else
                                Reserves.CalcReserve(false, Segmentation.SegmentsGroup[mintsegcnt], Segmentation.SegmentsGroup[mintsegcnt + 1], expAllow, true, Timing, terminalType);
                        }
                    }
                    else {
                        expAllow = true;
                        CommFunctions.CalcCommFunctions(Insured.IssueAge, endAge);
                        Benefits.CalcPVFB(Insured.IssueAge, Benefits.BenefitCeaseAge, Timing, terminalType);
                        Premium.CalcPVFGrossPrems(Benefits.BenefitCeaseAge, Insured.IssueAge, Timing);
                        Reserves.CalcPremiums(Insured.IssueAge, Benefits.BenefitCeaseAge, Insured.IssueAge, expAllow, true, Timing, terminalType);
                        if (terminalType == TerminalIndicator.CashValue)
                        {
                            if (Insured.ETI)
                                CV.CalcETICashValue(Benefits.BenefitCeaseAge, Insured.IssueAge, expAllow, true, CVTiming, Policy.ProductType);
                            else
                                CV.CalcCashValue(Benefits.BenefitCeaseAge, Insured.IssueAge, expAllow, true, CVTiming, Policy.ProductType, endAge);
                        }
                        else
                        {
                            if (Insured.ETI)
                                Reserves.CalcETIReserve(false, Benefits.BenefitCeaseAge, Insured.IssueAge, expAllow, true, Timing, terminalType);
                            else
                                Reserves.CalcReserve(false, Benefits.BenefitCeaseAge, Insured.IssueAge, expAllow, true, Timing, terminalType);
                        }
                    }


                    if ((modValues.Reserves.XXX || modValues.Reserves.ModRes == ModResType.TabularCOI ||
                          modValues.Reserves.ModRes == ModResType.OptionalExemptionFromUnitary || modValues.Reserves.ModRes == ModResType.OptionalYrtArt) &&
                          (terminalType == TerminalIndicator.MinimumSegmented || terminalType == TerminalIndicator.BasicOneHalfCx))
                        modValues.Reserves.CalcDeficiencyReserve(Benefits.BenefitCeaseAge, Insured.IssueAge, terminalType);
                    if ((modValues.Reserves.ModRes == ModResType.XXX1999 || modValues.Reserves.ModRes == ModResType.NY147 ||
                                   modValues.Reserves.ModRes == ModResType.XXX1995) && terminalType == TerminalIndicator.BasicUnitary)
                        modValues.MidYear.CalcUnusualCV();
                }
                if (terminalType == TerminalIndicator.CashValue && calcTerminals)
                {
                    if (!modValues.Reserves.XXX && modValues.MidYear.MYRule == MidYearRule.NoComparison)
                    {
                        modValues.MidYear.CalcMY_MidYearReserve(Benefits.BenefitCeaseAge, Insured.IssueAge, terminalType);
                        ReportGenerator(this, terminalType + 1);
                        continue;
                    }
                    ReportGenerator(this, terminalType + 1);
                }
                else if (terminalType == TerminalIndicator.BasicUnitary && calcTerminals)
                {
                    if (modValues.Reserves.XXX = false && modValues.MidYear.MYRule == MidYearRule.NoComparison)
                    {
                        modValues.MidYear.CalcMY_MidYearReserve(Benefits.BenefitCeaseAge, Insured.IssueAge, terminalType);
                        ReportGenerator(this, terminalType);
                        continue;
                    }
                    ReportGenerator(this, terminalType);
                }
                else if (terminalType == TerminalIndicator.BasicSegmented)
                {
                    if (modValues.Reserves.XXX) ReportGenerator(this, terminalType);
                }
                else if (terminalType == TerminalIndicator.BasicOneHalfCx)
                {
                    modValues.MidYear.CalcMY_MidYearReserve(Benefits.BenefitCeaseAge, Insured.IssueAge, terminalType);
                    ReportGenerator(this, terminalType);
                    if (!modValues.Reserves.XXX) continue;
                }
                else if (terminalType == TerminalIndicator.MinimumUnitary)
                {
                    if (modValues.Reserves.XXX) ReportGenerator(this, terminalType);
                }
                else if (terminalType == TerminalIndicator.MinimumSegmented)
                {
                    modValues.MidYear.CalcMY_MidYearReserve(Benefits.BenefitCeaseAge, Insured.IssueAge, terminalType);
                    ReportGenerator(this, terminalType);
                    continue;
                }
            }

            if (modValues.Reserves.XXX)
            {
                modValues.Reserves.FinalTerminalReserve = Math.Max(modValues.MidYear.TerminalBasicTerminal[modValues.Insured.IssueAge + modValues.Policy.Duration()],
                  modValues.MidYear.TerminalDeficiencyTerminal[modValues.Insured.IssueAge + modValues.Policy.Duration()]);
                modValues.MidYear.FinalMeanReserve = Math.Max(modValues.MidYear.MYBasicMean[modValues.Insured.IssueAge + modValues.Policy.Duration()],
                     modValues.MidYear.MYMinimumFinal[modValues.Insured.IssueAge + modValues.Policy.Duration()]);
                modValues.Reserves.FinalTerminalReserveDef = modValues.MidYear.TerminalDeficiencyTerminalDef[modValues.Insured.IssueAge + modValues.Policy.Duration()];
                modValues.MidYear.MYFinalDeficiencyMean = modValues.MidYear.MYMinimumDeficiencyFinal[modValues.Insured.IssueAge + modValues.Policy.Duration()];
            }
            else if (modValues.Policy.CalcType == CalcTypeDef.Reserves)
            {
                modValues.Reserves.FinalTerminalReserve = modValues.MidYear.TerminalBasicTerminal[modValues.Insured.IssueAge + modValues.Policy.Duration()];
                modValues.MidYear.FinalMeanReserve = modValues.MidYear.MYBasicMean[modValues.Insured.IssueAge + modValues.Policy.Duration()];
            }
            else
            {
                modValues.CV.FinalTerminalCV = modValues.MidYear.TerminalBasicTerminal[modValues.Insured.IssueAge + modValues.Policy.Duration()];
                modValues.MidYear.FinalMeanCV = modValues.MidYear.MYBasicMean[modValues.Insured.IssueAge + modValues.Policy.Duration()];
            }
            ReportGenerator(this, TerminalIndicator.Finished);
        }
        public void CalcNonforfeitureCV()
        {
            int endAge;
            bool expAllow;
            TerminalIndicator terminalType;
            BasicMinimumOptions terminalGroup;

            if ((int)modValues.CV.CVMethod >= 100 || modValues.CV.CVMethod == eCVMethod.CVMethodReserve)
            {
                if ((int)modValues.CV.CVMethod >= 100)
                {
                    modValues.Reserves.ModRes = (ModResType)((int)modValues.CV.CVMethod - 100);
                    modValues.CV.CVMethodFinal = eCVMethod.CVMethodReserve;
                }
                CalcFunctions();
            }
            else
            {
                modValues.CV.CVMethodFinal = modValues.CV.CVMethod;
                terminalType = TerminalIndicator.CashValue;
                terminalGroup = BasicMinimumOptions.BasicOnly;
                Premium.CalcFinalGrossPrems(Benefits.BenefitCeaseAge, Insured.IssueAge, terminalGroup);
                IntRates.CalcInterestRate(Insured.IssueAge, Benefits.BenefitCeaseAge, terminalType, "Nonforfeiture Calculation");
                Premium.PremiumCeaseAge = Math.Min(Premium.PremiumCeaseAge, Benefits.BenefitCeaseAge);
                Benefits.CalcDB(Insured.IssueAge);
                expAllow = true;
                Mortality.MortalityType = MortalityType.NonforfeitureMort;
                endAge = Mortality.CalcMortality(Insured.IssueAge, terminalType, Insured.IssueAge2, "Nonforfeiture Assumptions");
                CommFunctions.CalcCommFunctions(Insured.IssueAge, endAge);
                Benefits.CalcPVFB(Insured.IssueAge, Benefits.BenefitCeaseAge, Timing, terminalType);
                Premium.CalcPVFGrossPrems(Benefits.BenefitCeaseAge, Insured.IssueAge, Timing);
                Reserves.CalcPremiums(Insured.IssueAge, Benefits.BenefitCeaseAge, Insured.IssueAge, expAllow, true, Timing, terminalType);
                if (Insured.ETI)
                    CV.CalcETICashValue(Benefits.BenefitCeaseAge, Insured.IssueAge, expAllow, true, CVTiming, Policy.ProductType);
                else
                    CV.CalcCashValue(Benefits.BenefitCeaseAge, Insured.IssueAge, expAllow, true, CVTiming, Policy.ProductType, endAge);
                modValues.MidYear.CalcMY_MidYearReserve(Benefits.BenefitCeaseAge, Insured.IssueAge, terminalType);
                modValues.CV.FinalTerminalCV = modValues.MidYear.TerminalBasicTerminal[modValues.Insured.IssueAge + modValues.Policy.Duration()];
                modValues.MidYear.FinalMeanCV = modValues.MidYear.MYBasicMean[modValues.Insured.IssueAge + modValues.Policy.Duration()];
                ReportGenerator(this, terminalType);
                ReportGenerator(this, TerminalIndicator.Finished);
            }
        }
    }
}
