﻿using System;
namespace SLTValues
{
    public class SaltOutput
    {
        public int UnitDBRound { get; set; }
        public int UnitTerminalCVRound { get; set; }
        public int UnitAdjustedPremRound { get; set; }
        public int UnitTerminalReserveRound { get; set; }
        public int UnitMeanReserveRound { get; set; }
        public int UnitNetPremRound { get; set; }
        public int UnitMeanCVRound { get; set; }
        public int TotalDBRound { get; set; }
        public int TotalTerminalCVRound { get; set; }
        public int TotalMeanCVRound { get; set; }
        public int TotalAdjustedPremRound { get; set; }
        public int TotalTerminalReserveRound { get; set; }
        public int TotalMeanReserveRound { get; set; }
        public int TotalNetPremRound { get; set; }
        public eRound eUnitDBRound { get; set; }
        public eRound eUnitTerminalCVRound { get; set; }
        public eRound eUnitMeanCVRound { get; set; }
        public eRound eUnitAdjustedPremRound { get; set; }
        public eRound eUnitTerminalReserveRound { get; set; }
        public eRound eUnitMeanReserveRound { get; set; }
        public eRound eUnitNetPremRound { get; set; }
        public eRound eTotalDBRound { get; set; }
        public eRound eTotalTerminalCVRound { get; set; }
        public eRound eTotalMeanCVRound { get; set; }
        public eRound eTotalAdjustedPremRound { get; set; }
        public eRound eTotalTerminalReserveRound { get; set; }
        public eRound eTotalMeanReserveRound { get; set; }
        public eRound eTotalNetPremRound { get; set; }
    }
}
