﻿using System;
namespace SLTValues
{
    public class SaltBenefits
    {
        public enum DBTypeConstants { DBConstant, DBVary };
        public double[] DeathBenefit { get; set; }
        public double[] PVFB { get; set; }
        public double[] PVFB_NoEndow { get; set; }
        public double[] PVFB_PE { get; set; }
        public double DBConstant { get; set; }
        public double[] EndowAmount { get; set; }
        public int BenefitCeaseAge { get; set; }
        public double Units { get; set; }
        public DBTypeConstants DBType { get; set; }
        private double[] db;

        public SaltBenefits()
        {
            DeathBenefit = new double[131];
            PVFB = new double[131];
            PVFB_NoEndow = new double[131];
            PVFB_PE = new double[131];
            EndowAmount = new double[131];
            db = new double[131];
        }
        public void CalcDB(int issueAge)
        {
            for (int i = issueAge; i < BenefitCeaseAge; i++)
                if (DBType == DBTypeConstants.DBConstant)
                {
                    db[i] = DBConstant;
                    DeathBenefit[i] = DBConstant;
                }
                else
                {
                    db[i] = DBConstant;
                }
        }
        public void CalcPVFB(int beginAge, int endAge, TimingType timing, TerminalIndicator terminalType)
        {
            double myIntRate;
            bool endowCV = false, endowBasicUnitary = false, endowMinUnitary = false;
            if (modValues.Reserves.ModRes == ModResType.XXX1999 || modValues.Reserves.ModRes == ModResType.NY147 || modValues.Reserves.ModRes == ModResType.XXX1995 || modValues.Reserves.EndowOptionXXX != EndowOptionXXX.NotUsed)
            {
                if ((terminalType == TerminalIndicator.BasicSegmented && (modValues.Reserves.EndowChoiceXXX == BasicMinimumOptions.BasicMinimum || modValues.Reserves.EndowChoiceXXX == BasicMinimumOptions.BasicOnly)) ||
                    (terminalType == TerminalIndicator.MinimumSegmented && (modValues.Reserves.EndowChoiceXXX == BasicMinimumOptions.BasicMinimum || modValues.Reserves.EndowChoiceXXX == BasicMinimumOptions.BasicOnly)))
                {
                    if (modValues.Reserves.EndowOptionXXX == EndowOptionXXX.EndowCV)
                        endowCV = true;
                    else if (terminalType == TerminalIndicator.BasicSegmented)
                        endowBasicUnitary = true;
                    else if (terminalType == TerminalIndicator.MinimumSegmented)
                        endowMinUnitary = true;
                }
            }
            for (int i = endAge; i > beginAge; i--)
            {
                myIntRate = modValues.IntRate.FinalInterestRate[i - beginAge];
                if (i < endAge)
                {
                    if (i > beginAge)
                        PVFB_PE[i] = EndowAmount[i - 1] + (PVFB_PE[i + 1]) * (1 - modValues.Mortality.FinalMortality[i]) / (1 + myIntRate);
                    else
                        PVFB_PE[i] = (PVFB_PE[i + 1]) * (1 - modValues.Mortality.FinalMortality[i]) / (1 + myIntRate);
                    if (timing == TimingType.Curtate)
                        PVFB_NoEndow[i] = (db[i] * modValues.Mortality.FinalMortality[i] + ((1 - modValues.Mortality.FinalMortality[i]) * PVFB_NoEndow[i + 1])) / (1 + myIntRate);
                    else
                        PVFB_NoEndow[i] = (db[i] * modValues.Mortality.FinalMortality[i] * (myIntRate / modValues.Delta(myIntRate)) + ((1 - modValues.Mortality.FinalMortality[i]) * PVFB_NoEndow[i + 1])) / (1 + myIntRate);
                }
                else
                {
                    PVFB_NoEndow[i] = 0;
                    if (i == BenefitCeaseAge)
                        PVFB_PE[i] = EndowAmount[endAge - 1];
                    else if (endowCV)
                        PVFB_PE[i] = modValues.CV.CashValue[endAge];
                    else if (endowMinUnitary)
                        PVFB_PE[i] = modValues.Reserves.TerminalReserveMinimumUnitary[endAge];
                    else if (endowBasicUnitary)
                        PVFB_PE[i] = modValues.Reserves.BasicUnitary[endAge];
                    else
                        PVFB_PE[i] = EndowAmount[endAge - 1];
                }
                PVFB[i] = PVFB_NoEndow[i] + PVFB_PE[i];
            }
            if (endAge > 0)
            {
                PVFB_PE[endAge] = EndowAmount[endAge - 1];
                PVFB[endAge] = PVFB_PE[endAge];
            }

        }

    }
}
