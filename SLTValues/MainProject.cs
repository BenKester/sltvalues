﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SLTValues
{
    enum MessageBoxButton { OK}
    class MessageBoxCustom
    {
        public static void Show(string s, string t, MessageBoxButton mb) { }
    }
    class PMRLModel {
        public DateTime ExpiryDate { get; set; }
        public int ProductId { get; set; }
    }
    class Product {
        public int CVMethod { get; set; }
        public int CVInterest { get; set; }
        public int CVTiming { get; set; }
    }
    class MainProject
    {
        private DateTime dtpDate;
        private void GetETI(PMRLModel model, Product rsaProduct, int pintAge, int pintDuration, double pdblCV, double pdblFA,
                 int pintEndAge, DateTime pdatPolExpiry, ref DateTime pdatETIExpiry, ref double pdblPureEndow)
        {

            int intAge = 0;
            double dblCV = 0;
            int intAttAge = 0;
            double dblInterpolatedM = 0;
            double dblMore = 0;
            string strSQL = null;
            double dblLess = 0;
            DateTime? datMaturity = default(System.DateTime);
            bool blnPastMaturity = false;
            int intYears = 0;
            long lngDays = 0;
            System.DateTime datTemp = default(System.DateTime);
            //ADODB.Recordset rsaProduct = new ADODB.Recordset();
            float[] asngMortality = new float[121];
            int i = 0;
            double dblEndowPV = 0;
            eCVMethod enuCVMethod = default(eCVMethod);

            if (rsaProduct == null)
            {
                MessageBoxCustom.Show("Calculate CV", "Could not find product record when calculating the ETI(Product ID#" + model.ProductId + ")", MessageBoxButton.OK);
                return;
            }
            enuCVMethod = (eCVMethod)rsaProduct.CVMethod;
            datMaturity = model.ExpiryDate;
            intAttAge = pintAge + pintDuration;

            SLTValues.SaltTRCommon oSaltCommon = new SaltTRCommon();

            oSaltCommon.IntRates.InterestRateConstant = Convert.ToDouble(rsaProduct.CVInterest);
            oSaltCommon.CV.CVMethod = enuCVMethod;

            //Call ReadAgeDur(eTableType.TableAttainedAge, "tbl07Insurance", gcnnMain, _
            //       "tbl" & rsaProduct("CVMort"), "AA", 0, asngMortality(), 0, 115)

            for (i = 0; i <= asngMortality.GetUpperBound(0); i++)
            {
                oSaltCommon.Mortality.MortalityRate[0, i] = asngMortality[i] / 1000;
            }

            oSaltCommon.CommFunctions.CalcCommFunctions(0, 100);

            dblCV = pdblCV;

            if (dblCV > 0)
            {
                if (rsaProduct.CVTiming > 0)
                {
                    dblInterpolatedM = Math.Round(oSaltCommon.CommFunctions.Mx[intAttAge] - (dblCV * oSaltCommon.CommFunctions.Dx[intAttAge] / pdblFA));

                    if (dblInterpolatedM <= 0)
                    {
                        pdatETIExpiry = pdatPolExpiry;
                        intYears = pintEndAge - (intAttAge) + 1;

                        if (oSaltCommon.CommFunctions.Dx[Convert.ToInt32(MortalityType.MinimumMort + intAttAge + intYears)] > 0)
                        {
                            pdblPureEndow = (dblCV * oSaltCommon.CommFunctions.Dx[intAttAge] - pdblFA * (oSaltCommon.CommFunctions.Mx[intAttAge] - oSaltCommon.CommFunctions.Mx[intAttAge + intYears])) / oSaltCommon.CommFunctions.Dx[intAttAge + intYears];
                        }
                        else
                        {
                            pdblPureEndow = 0;
                        }
                        return;
                    }

                    for (intAge = intAttAge; intAge <= pintEndAge; intAge++)
                    {
                        if (oSaltCommon.CommFunctions.Mx[intAge] < dblInterpolatedM)
                        {
                            intYears = intAge - (intAttAge) - 1;
                            break; // TODO: might not be correct. Was : Exit For
                        }
                    }
                    dblLess = oSaltCommon.CommFunctions.Mx[intAttAge + intYears + 1];
                    dblMore = oSaltCommon.CommFunctions.Mx[intAttAge + intYears] - dblLess;
                    dblInterpolatedM = dblInterpolatedM - dblLess + 0 * oSaltCommon.CommFunctions.Mx[intAttAge + intYears - 1];
                }
                else
                {
                    dblInterpolatedM = oSaltCommon.CommFunctions.MBarx[intAttAge] - (dblCV * oSaltCommon.CommFunctions.Dx[intAttAge] / pdblFA);

                    if (dblInterpolatedM <= 0)
                    {
                        pdatETIExpiry = pdatPolExpiry;
                        //Me.dtpETIExpiryDate
                        intYears = pintEndAge - (intAttAge) + 1;
                        if (oSaltCommon.CommFunctions.Dx[intAttAge + intYears] > 0)
                        {
                            pdblPureEndow = (dblCV * oSaltCommon.CommFunctions.Dx[intAttAge] -
                                pdblFA * (oSaltCommon.CommFunctions.MBarx[intAttAge] -
                                oSaltCommon.CommFunctions.MBarx[intAttAge + intYears])) /
                                oSaltCommon.CommFunctions.Dx[intAttAge + intYears];
                        }
                        else
                        {
                            pdblPureEndow = 0;
                        }
                        return;
                    }


                    for (intAge = intAttAge; intAge <= pintEndAge; intAge++)
                    {
                        if (oSaltCommon.CommFunctions.MBarx[intAge] < dblInterpolatedM)
                        {
                            intYears = intAge - (intAttAge) - 1;
                            break; // TODO: might not be correct. Was : Exit For
                        }
                    }
                    dblLess = oSaltCommon.CommFunctions.MBarx[intAttAge + intYears + 1];
                    dblMore = oSaltCommon.CommFunctions.MBarx[intAttAge + intYears] - dblLess;
                    dblInterpolatedM = dblInterpolatedM - dblLess + 0 * oSaltCommon.CommFunctions.MBarx[intAttAge + intYears - 1];
                }

                lngDays = Convert.ToInt64(Math.Ceiling(365 - (dblInterpolatedM / dblMore) * 365));

                pdatETIExpiry = dtpDate.AddDays(lngDays).AddYears(intYears);

                if (pdatETIExpiry < datMaturity)
                    pdblPureEndow = 0;
                else
                {
                    if (oSaltCommon.CommFunctions.Dx[intAttAge + intYears] > 0)
                    {
                        pdblPureEndow = (dblCV * oSaltCommon.CommFunctions.Dx[intAttAge] - pdblFA * (oSaltCommon.CommFunctions.Mx[intAttAge] - oSaltCommon.CommFunctions.Mx[intAttAge + intYears])) / oSaltCommon.CommFunctions.Dx[intAttAge + intYears];
                    }
                    else
                    {
                        pdblPureEndow = 0;
                    }
                }
            }
            else
            {
                pdatETIExpiry = dtpDate;
            }

        }
    }
}
