﻿using System;
namespace SLTValues
{
    public class SaltRecursive
    {
        public double EOYValue { get; set; }
        public double Benefit { get; set; }
        public double MortalityRate { get; set; }
        public double MortalityRateX_1 { get; set; }
        public double NP { get; set; }
        public double Endow { get; set; }
        public bool ImmediateBenefit { get; set; }
        public bool ContinuousPayment { get; set; }
        public int PremMode { get; set; }
        public double IntRate { get; set; }
        public int TheType { get; set; }
        public double CalcReserve(double other, bool forward)
        {
            double Px, V;
            V = modValues.v(IntRate);
            if (forward)
            {
                Px = 1 - MortalityRateX_1;
                return Endow + (other - DBAdjust() * Benefit * MortalityRateX_1 * V + PremAdjust(true) * NP) / (Px * V);
            }
            else
            {
                Px = 1 - MortalityRate;
                return (other + Endow) * (Px) * V + DBAdjust() * Benefit * MortalityRate * V - PremAdjust(true) * NP;
            }
        }
        public double CalcLifeAnnuity(double other, bool forward)
        {
            double Px, V;
            V = modValues.v(IntRate);
            if (forward)
            {
                Px = 1 - MortalityRateX_1;
                return (other - PremAdjust(true) * NP) / (Px * V);
            }
            else
            {
                Px = 1 - MortalityRate;
                return (other * Px * V) + PremAdjust(true) * NP;
            }
        }
        public double CalcNSP(double other, bool forward)
        {
            double V = modValues.v(IntRate);
            if (forward)
            {
                double Px = 1 - MortalityRateX_1;
                return Endow + (other - DBAdjust() * Benefit * MortalityRateX_1 * V) / (Px * V);
            }
            else
            {
                double Px = 1 - MortalityRate;
                return (other + Endow) * (Px) * V + DBAdjust() * Benefit * MortalityRate * V;
            }
        }
        public double CalcIntAnnuity(double other, bool forward)
        {
            double V = modValues.v(IntRate);
            if (forward)
                return (other * V) + PremAdjust(false) * NP;
            else
                return (other - PremAdjust(false) * NP) / V;
        }
        public double Calc_lx(double other)
        {
            return other * (1 - MortalityRateX_1);
        }
        public double Calc_Dx(double otherCurtate)
        {
            double V = modValues.v(IntRate);
            double Px = 1 - MortalityRateX_1;
            return PremAdjust(true) * otherCurtate * Px * V;
        }
        public double Calc_Cx(double Dx_Curtate)
        {
            double V = modValues.v(IntRate);
            return DBAdjust() * Dx_Curtate * MortalityRate * V;
        }
        public double Calc_Nx(double other, double Dx)
        { return other + Dx; }
        public double Calc_Mx(double other, double Cx)
        { return other + Cx; }
        public double Calc_Rx(double other, double Mx)
        { return other + Mx; }
        public double Calc_Sx(double other, bool forward, double Nx)
        { return other + Nx; }
        public double Calc_Dx_Curtate(double other)
        {
            double Px = 1 - MortalityRateX_1;
            double V = modValues.v(IntRate);
            return other * Px * V;
        }
        public double DBAdjust()
        {
            if (ImmediateBenefit)
                return IntRate / Math.Log(1 + IntRate);
            else
                return 1;
        }
        public double PremAdjust(bool ILC)
        {
            double V = modValues.v(IntRate);
            double Px = 1 - MortalityRate;
            if (!ILC)   // Interest annuity
                return modValues.DUpperM(IntRate, 1) / modValues.DUpperM(IntRate, PremMode);
            else if (ContinuousPayment)  // continuous
                return modValues.Alpha(IntRate, 0) - modValues.Beta(IntRate, 0) * (1 - Px * V);
            else if (PremMode != 1) // not annual
                return modValues.Alpha(IntRate, PremMode) - modValues.Beta(IntRate, PremMode) * (1 - Px * V);
            else
                return 1;
        }
        public double CalcRecursive()
        {
            double V = modValues.v(IntRate);
            double Px = 1 - MortalityRate;
            return (EOYValue + Endow) * Px * V + DBAdjust() * Benefit * MortalityRate * V - PremAdjust(true) * NP;
        }
    }
}
